<?php
return [
    'welcome' => 'Hush kelibsiz',
    'menu'=>'Menyu',
    'admin_menu'=>'Admin menyusi',
    'site_menu'=> 'Sayt menyusi',
    'logout'=>'Chiqish',
    'admission'=>'Magistraturaga qabul',
    'academic_year'=>'Академик ўқув йили',
    'create'=>'Қўшиш',
    'admission_faculties'=>'Magistratura fakultetlari',
    'applicants'=>'Номзодлар аризалари',
    'applicants_grading'=>'Номзодлар баҳолари',
    'stat'=>'Statistika',
    'news_category'=>'Yangilik kategoriyalari',
    'Сайт'=>'Sayt',
    'Сообщения'=>'Хабарлар',
    'title'=>'Номи',
    'status'=>'Статус',
    'active'=>'Фаол',
    'inactive'=>'Нофаол',
    'all'=>'Ҳаммаси',
    'description'=>'Маълумот',
    'created'=>'Киритилган санаси',
    'updated'=>'Қайта таҳрирланган санаси',
    'start'=>'Бошланиш санаси',
    'finish'=>'Тугаш санаси',
    'update'=>'Ўзгартириш',
    'delete'=>'Ўчириш',
    'full_name'=>'Ф.И.Ш',
    'app_info'=>'Аризалар ҳақида маълумот',
    'accept_app'=>'Ҳужжатлар тўғрилигини тасдиқлаш',
    'accept_app_all'=>'Белгиланган ҳужжатлар тўғрилигини тасдиқлаш',
    'cancel_app'=>'Бекор қилиш',
    'full_cancel_app'=>'Аризани тўлиқ бекор қилиш',
    'send_message'=>'Белгиланганларга хабар юбориш',
    'download_archives'=>'Архив ҳолатида юклаб олиш',
    'essay_result'=>'Ёзма иш натижалари',
    'speaking_result'=>'Суҳбат натижалари',
    'language_result'=>'Чет тили натижалари',
    'overall_result'=>'Умумий тўпланган баллар',
    'message_templates'=>'Хабарлар шаблонлари',
    'unknown' => 'Номаълум',
    'accepted' => 'Қабул қилинган',
    'finished' => 'Форма тўлдирилган',
    'aborted' => 'Камчилик аниқланган',
    'cancelled' => 'Бекор қилинган',
    'updated_status' => 'Ҳужжатларни қайта юклаган',
    'show_doc'=>'Ҳужжатларни кўрсатиш'  

];