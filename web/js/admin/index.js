$(document).ready(function () {
    var post = $('#post-configtitle-ru')
    var menu = $('#menu-ltitle-ru')
    var news=$('#newscategory-jtitle-ru')
    var news_item=$('#news-jtitle-ru')
    var tags=$('#tags-jtitle-ru')
    var сat_news=$('#category-jtitle-ru')
    if (сat_news.length) {
        сat_news.syncTranslit({ destination: 'category-alias' });
    }
    if (post.length) {
        post.syncTranslit({ destination: 'post-alias' });
    }
    if (menu.length) {
        menu.syncTranslit({ destination: 'menu-alias' });

    }
    if (news.length) {
        news.syncTranslit({ destination: 'newscategory-alias' });

    }
    if (news_item.length) {
        news_item.syncTranslit({ destination: 'news-alias' });
    }
    if (tags.length) {
        tags.syncTranslit({ destination: 'tags-alias' });
    }
})

