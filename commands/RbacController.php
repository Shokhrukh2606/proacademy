<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        $admin = $auth->createRole('admin');
        $moderator=$auth->createRole('moderator');
        $mastermoderator = $auth->createRole('mastermoderator');
        $applicant = $auth->createRole('applicant');
        // Create Roles
        $auth->add($admin);
        $auth->add($moderator);
        $auth->add($mastermoderator);
        $auth->add($applicant);
        $changePost = $auth->createPermission('changePost');
        $changePost->description = 'Manage Posts';
        $auth->add($changePost);
        $auth->addChild($moderator, $changePost);
        $uploadInfo = $auth->createPermission('uploadInformation');
        $uploadInfo->description = 'Applicant can upload info about himself/herself';
        $auth->add($uploadInfo);
        $auth->addChild($applicant, $uploadInfo);
        $manageApplicants = $auth->createPermission('manageApplicants');
        $manageApplicants->description = 'Manage applicants docs';
        $auth->add($manageApplicants);
        $auth->addChild($mastermoderator, $manageApplicants);
        $auth->addChild($admin, $moderator);
        $auth->addChild($admin, $mastermoderator);

        // // Permission give to all roles inheriting admin roles
        // $auth->addChild($admin,$moderator);
        // $auth->addChild($admin, $mastermoderator);
        // $auth->addChild($admin, $applicant);
        // // Give specific id to each roles
        // $auth->assign($admin, 666);
        // $auth->assign($moderator, 667);
        // $auth->assign($mastermoderator, 668);
        // $auth->assign($applicant, 111);


    }
}