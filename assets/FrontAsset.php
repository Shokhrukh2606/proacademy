<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/gorom';
    public $css = [
        // 'css/animate.css',
        'css/flaticon.css',
        'fonts/flaticon.css',
        // 'css/magnific-popup.css',
        // 'css/menu.css',
        // 'css/nice-select.css',
        'css/owl.carousel.min.css',
        'css/slick.css',
        // 'css/spacing.min.css',
        'css/style.css',
        'css/responsive.css',
        'css/owl.theme.default.min.css',
        'css/myspec.css',
    ];
    public $js = [
        // 'js/jquery.min.js',
        'js/popper.min.js',
        'js/bootstrap.min.js',
        'js/jquery.nice-select.min.js',
        'js/owl.carousel.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/jquery.simpleLoadMore.min.js',
        'js/jquery.fancybox.pack.js',
        'js/jquery.fancybox-media.js',
        'js/slick.min.js',
        'js/mixitup.js',
        'js/popover.js',
        'js/wow.min.js',
        'js/appear.js',
        'js/script.js'
    ];
    // public $depends = [
        
    // ];
}
