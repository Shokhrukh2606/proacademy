<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PluginsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/plugins';
    public $css = [
        // Selectize assets
        'selectize/css/selectize.css',
        'bstreeview/dist/css/bstreeview.min.css',
        'quiz/jquery.quiz-min.css',
        'jqtree.css'
        // Selectize assets

    ];
    public $js = [
        'microplugin/microplugin.js',
        'sifter/sifter.js',
        'lodash.js',
        'selectize/js/selectize.js',
        'qrcode_generator/kjua.min.js',
        'bstreeview/dist/js/bstreeview.min.js',
        'quiz/jquery.quiz-min.js',
        'tree.jquery.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
