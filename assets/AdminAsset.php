<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'mycss/admin/index.css',
        'mycss/admin/jqtree.css'
    ];
    public $js = [
        'js/admin/jquery.synctranslit.min.js',
        'js/admin/index.js',
        'js/admin/jquery.inputmask.js',
        'js/admin/sortable.js',
        'js/admin/lodash.js',
        'js/admin/tree.jquery.js'

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
