<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SmartWizardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/smartwizard';
    public $css = [
        'dist/css/smart_wizard_all.min.css',
        'dist/css/custom.css'

    ];
    public $js = [
        'dist/js/jquery.smartWizard.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
    public $jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
}
