<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FullPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/fullPage/dist';
    public $css = [
        'jquery.fullpage.css',

    ];
    public $js = [
        'scrolloverflow.js',
        'jquery.fullpage.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
    public $jsOptions = [ 'position' => \yii\web\View::POS_END ];
}
