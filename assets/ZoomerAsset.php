<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ZoomerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/special_view';
    public $css = [

        'site.min.css',


    ];
    public $js = [
        'specialview.js'
    ];
    public $depends = [
        
    ];
}
