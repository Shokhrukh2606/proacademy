-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июн 14 2020 г., 02:14
-- Версия сервера: 10.4.11-MariaDB
-- Версия PHP: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `proacademy_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admission_faculties`
--

CREATE TABLE `admission_faculties` (
  `id` int(11) NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admission_faculties`
--

INSERT INTO `admission_faculties` (`id`, `title`, `status`, `created_at`, `updated_at`) VALUES
(1, '{\"ru\":\"Прокурорская деятельность\",\"uz-Lat\":\"Prokurorlik faoliyati\",\"uz-Cyr\":\"Prokurorlik faoliyati\",\"en\":\"Прокурорская деятельность\"}', 'active', 1590453009, 1591297358),
(2, '{\"ru\":\"Следственная деятельность\",\"uz-Lat\":\"Tergov faoliyati\",\"uz-Cyr\":\"Следственная деятельность\",\"en\":\"Следственная деятельность\"}', 'active', 1590453041, 1591297554),
(3, '{\"ru\":\"Противодействие коррупции\",\"uz-Lat\":\"Korrupsiyaga qarshi kurashish\",\"uz-Cyr\":\"Противодействие коррупции\",\"en\":\"Противодействие коррупции\"}', 'active', 1590453140, 1591877544);

-- --------------------------------------------------------

--
-- Структура таблицы `admission_year`
--

CREATE TABLE `admission_year` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `description` text NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admission_year`
--

INSERT INTO `admission_year` (`id`, `title`, `status`, `description`, `created_at`, `updated_at`) VALUES
(2, 'Qabul 2020-2021', '1', 'Qabul 2020-2021', 1590451406, 1591739704);

-- --------------------------------------------------------

--
-- Структура таблицы `applicants`
--

CREATE TABLE `applicants` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unique_code` varchar(8) NOT NULL,
  `academic_year_id` int(4) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `applicant_type` enum('inner','outer') DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `district_birth` int(3) DEFAULT NULL,
  `district_temporary` int(3) DEFAULT NULL,
  `address_temporary` varchar(255) DEFAULT NULL,
  `district_permanent` int(3) DEFAULT NULL,
  `address_permanent` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `telegram_phone` varchar(15) DEFAULT NULL,
  `nationality` varchar(30) DEFAULT NULL,
  `other_nationality` varchar(30) DEFAULT NULL,
  `diplom_number` varchar(255) DEFAULT NULL,
  `high_school_name` varchar(255) DEFAULT NULL,
  `high_school_specialty` varchar(255) DEFAULT NULL,
  `high_school_finished` date DEFAULT NULL,
  `inner_recomendation` varchar(255) DEFAULT NULL,
  `can_speak` int(1) DEFAULT NULL,
  `other_can_speak` varchar(255) DEFAULT NULL,
  `faculty` smallint(6) DEFAULT NULL,
  `passport_given_date` date DEFAULT NULL,
  `passport_given_from` varchar(255) DEFAULT NULL,
  `passport_serial_number` varchar(255) DEFAULT NULL,
  `diploma` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `certificate` varchar(255) DEFAULT NULL,
  `biography` varchar(255) DEFAULT NULL,
  `relatives` varchar(255) DEFAULT NULL,
  `career` varchar(255) DEFAULT NULL,
  `step` smallint(6) DEFAULT 1,
  `essay_result` float DEFAULT 0,
  `speaking_result` float DEFAULT 0,
  `language_result` float DEFAULT 0,
  `overall_result` float DEFAULT 0,
  `status` enum('unknown','accepted','aborted','cancelled','updated','finished') DEFAULT 'unknown',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `applicants`
--

INSERT INTO `applicants` (`id`, `user_id`, `unique_code`, `academic_year_id`, `avatar`, `applicant_type`, `birth_date`, `district_birth`, `district_temporary`, `address_temporary`, `district_permanent`, `address_permanent`, `phone`, `telegram_phone`, `nationality`, `other_nationality`, `diplom_number`, `high_school_name`, `high_school_specialty`, `high_school_finished`, `inner_recomendation`, `can_speak`, `other_can_speak`, `faculty`, `passport_given_date`, `passport_given_from`, `passport_serial_number`, `diploma`, `passport`, `certificate`, `biography`, `relatives`, `career`, `step`, `essay_result`, `speaking_result`, `language_result`, `overall_result`, `status`, `created_at`, `updated_at`) VALUES
(8, 83, 'P0832020', 2, 'http://localhost:8081/uploads/avatars/3f\\ek/3Fek9lnYOSFA6ZzZamBS4fD4H8pR4GXq.jpg', 'inner', '2020-06-14', 60, 76, 'Yunusobod', 60, 'Yunusobod', '99-888-46-84', '99-888-46-84', '1', '', 'AA1234578', 'Westminister', 'Yurist', '2020-06-12', 'http://localhost:8081/uploads/recomendations/af\\xc/afXcc8JRkOmNdPkYrrhkqchz--WfCuxP.pdf', 1, '', 1, '2020-06-12', 'Yunusobod tumani IIB', 'AA7899898', 'http://localhost:8081/uploads/diplomas/2k\\by/2kbyO2dg36eO3XpcThhty-u3f0IAdW2c.pdf', 'http://localhost:8081/uploads/passports/pb\\9n/pb9nY_aTlFqVmWWugE1hjpfDtt70oUzK.jpg', 'http://localhost:8081/uploads/certificates/lh\\ra/lHRAewPQxRgsab-zqZL_St-C59lnDX30.pdf', 'http://localhost:8081/uploads/biographies/kw\\sf/kwsf1GU7JFXdbm_W4Xd5rPagQq2pf8Ct.pdf', 'http://localhost:8081/uploads/relatives/cn\\re/CNrE37ZH2Zw_RBLaJDpTgeSqtu7Rn0Qf.pdf', 'http://localhost:8081/uploads/careers/dz\\5d/dz5DtM1P3MXp4QOIMRyRwKUUgkC_O4jq.pdf', 3, 0, 0, 0, 0, 'finished', 0, 1592049046),
(10, 85, 'P0852020', 2, 'http://localhost:8081/uploads/avatars/nl\\j5/nLJ5c6arLFZK7razWcKCrXtZydv36eXM.jpg', 'inner', '1998-06-26', 60, 60, 'Yunusobod', 60, 'Yunusobod', '99-888-46-84', '99-888-46-84', '1', NULL, 'AA1234578', 'Westminister', 'Yurist', '2017-06-10', 'http://localhost:8081/uploads/recomendations/ax\\ld/AxLd7GXjjMCL4-bY2shYRvnav6OLFxYy.pdf', 1, '', 1, NULL, NULL, '', 'http://localhost:8081/uploads/diplomas/46\\mv/46mVCartqn54WAhc3JcmZWn_qvbrRcX8.pdf', 'http://localhost:8081/uploads/passports/xg\\gj/XGgjuggLgNk0EXt6fIayjOGbVNWf7oJQ.jpg', 'http://localhost:8081/uploads/certificates/qd\\_7/QD_7MQIRkoyJjNtFBWmvVMJuut1q0Zab.pdf', 'http://localhost:8081/uploads/biographies/e4\\7l/E47LvCYoY13rFr_SKpnKmAJ_u8_2aM2H.pdf', 'http://localhost:8081/uploads/relatives/tn\\aj/tNajDEy48eAeGD2VzXUHSXYz8oQ8IXEa.pdf', 'http://localhost:8081/uploads/careers/m2\\qc/M2qcl30RQfFWUI1j5KRiwMXVxiVt1_px.pdf', 4, 0, 0, 0, 0, 'accepted', 0, 1591738262),
(11, 86, 'P0862020', 2, 'http://localhost:8081/uploads/avatars/rk\\qq/RKQq3OAlIGpssofCMVOldSnzMnqPr8cs.jpg', 'inner', '0000-00-00', 60, 60, 'Yunusobod', 60, 'Yunusobod', '99-888-46-84', '99-888-46-84', '1', '', 'AA1234578', 'Westminister', 'Yurist', '0000-00-00', '', 1, '', 1, NULL, NULL, '', 'http://localhost:8081/uploads/diplomas/ov\\t7/OvT7QN8BA0Davm42lFGZn4y6_6evRl70.pdf', 'http://localhost:8081/uploads/passports/y1\\fm/y1fM44B1qDWUYKKCDzw_mfpPFoc3IT9n.jpg', 'http://localhost:8081/uploads/certificates/ig\\np/ignpaKT1ilcaKk2npuIUv414hwKkCpCD.pdf', '', '', '', 4, 0, 0, 0, 0, 'aborted', 0, 1591750735),
(12, 90, 'P0902020', 2, 'http://localhost:8081/uploads/avatars/l6\\zn/l6znLLQthhvy3UuZxH_YRf9HEWCbsuZ8.jpg', 'inner', '1998-06-26', 60, 60, 'Yunusobod', 60, 'Yunusobod', '99-888-46-84', '99-888-46-84', '1', NULL, 'AA1234578', 'Westminister', 'Yurist', '2020-05-14', 'http://localhost:8081/uploads/recomendations/ah\\ye/ahYEXOf-ZIHRxs2w6oJLHZhugDNyHufP.pdf', 1, '', 1, NULL, NULL, '', 'http://localhost:8081/uploads/diplomas/wu\\s3/wUS3irCW5gqe-Wg-Spq0XLgrvBhTr6rj.pdf', 'http://localhost:8081/uploads/passports/bm\\uf/BmuFQ3FHlgJSxZu2f9nwUCAVO_d5CguR.jpg', 'http://localhost:8081/uploads/certificates/l3\\xc/L3xCqr-DYLu8Hi0B63wQXf99EOzsqpSO.pdf', 'http://localhost:8081/uploads/biographies/os\\3x/os3x4dGFL5YkLfedrCTo2DJf4ITWb5Sm.pdf', 'http://localhost:8081/uploads/relatives/0n\\x1/0Nx1DA_IkFC9EsqSqegY27Wi44X9_qH_.pdf', 'http://localhost:8081/uploads/careers/gw\\nk/gWnKQvvDOun1dXqxy7FDJVojk9wsvGMw.pdf', 4, 0, 0, 0, 0, 'cancelled', 0, 0),
(13, 97, 'P0972020', 2, 'http://localhost:8081/uploads/avatars/kb\\ko/kBKOYiV0-CqZn58vQqytN8k05v2fLGwM.jpg', 'inner', '0000-00-00', 60, 60, 'Yunusobod', 60, 'Yunusobod', '99-888-46-84', '99-888-46-84', '1', '', 'AA1234578', 'Westminister', 'Yurist', '0000-00-00', 'http://localhost:8081/uploads/recomendations/1-\\yv/1-YV_p2DmTExcHVJcMp9xBJaMiHnRr1g.pdf', 1, '', 1, '2020-06-11', 'Yunusobod IIB', 'AA7899898', 'http://localhost:8081/uploads/diplomas/l9\\lx/L9lx6UD6qx58OiFOQzokpfWcieC_PK60.pdf', 'http://localhost:8081/uploads/passports/0r\\cu/0RCUjiSt2k55--Rx0zECLEPklo87wWbV.jpg', 'http://localhost:8081/uploads/certificates/po\\h4/poH4EYGb9E_KyurWN3samRAC99l8qHDk.pdf', 'http://localhost:8081/uploads/biographies/ro\\zd/roZDFHFPixe_rX2PKns-2Z7nRwVgOLd1.pdf', 'http://localhost:8081/uploads/relatives/df\\lj/DFLj7d8K74iG8NcqqHTj_G1DNEpOdK4Q.pdf', 'http://localhost:8081/uploads/careers/pe\\41/pe41vqNmWHkCjQ_2xl95uDZ4kiIDbc9d.pdf', 4, 0, 0, 0, 0, 'updated', 1591876622, 1591880793);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '81', 1591562057),
('applicant', '83', 1591562685),
('applicant', '85', 1591564451),
('applicant', '86', 1591598652),
('applicant', '90', 1591610115),
('applicant', '97', 1591876562),
('applicant', '99', 1591963831);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, NULL, NULL, NULL, 1591561832, 1591561832),
('applicant', 1, NULL, NULL, NULL, 1591561832, 1591561832),
('changePost', 2, 'Applicant can upload info about himself/herself', NULL, NULL, 1591561832, 1591561832),
('mastermoderator', 1, NULL, NULL, NULL, 1591561832, 1591561832),
('moderator', 1, NULL, NULL, NULL, 1591561832, 1591561832),
('uploadInformation', 2, 'Applicant can upload info about himself/herself', NULL, NULL, 1591561832, 1591561832);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'changePost'),
('applicant', 'uploadInformation');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `title`) VALUES
(1, '{\"ru\":\"Республики Каракалпакстан\",\"uz-Lat\":\"Qoraqalpog\'iston Respublikasi\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(2, '{\"ru\":\"Андижанской области\",\"uz-Lat\":\"Andijon viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(3, '{\"ru\":\"Бухарской области\",\"uz-Lat\":\"Buxoro viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(4, '{\"ru\":\"Джизакской области\",\"uz-Lat\":\"Jizzax viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(5, '{\"ru\":\"Кашкадарьинской области\",\"uz-Lat\":\"Qashqadaryo viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(6, '{\"ru\":\"Навоийской области\",\"uz-Lat\":\"Navoiy viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(7, '{\"ru\":\"Наманганской области\",\"uz-Lat\":\"Namangan viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(8, '{\"ru\":\"Самаркандской области\",\"uz-Lat\":\"Samarqand viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(9, '{\"ru\":\"Сурхандарьинской области\",\"uz-Lat\":\"Surxondaryo viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(10, '{\"ru\":\"Сырдарьинской области\",\"uz-Lat\":\"Sirdaryo viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(11, '{\"ru\":\"Ташкентской области\",\"uz-Lat\":\"Toshkent viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(12, '{\"ru\":\"Ферганской области\",\"uz-Lat\":\"Farg\'ona viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(13, '{\"ru\":\"Хорезмской области\",\"uz-Lat\":\"Xorazm viloyati\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(14, '{\"ru\":\"г.Ташкент\",\"uz-Lat\":\"Toshkent shahar\",\"uz-Cyr\":\"\",\"en\":\"\"}');

-- --------------------------------------------------------

--
-- Структура таблицы `districts`
--

CREATE TABLE `districts` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `districts`
--

INSERT INTO `districts` (`id`, `city_id`, `title`) VALUES
(60, 1, '{\"ru\":\"г. Нукус\",\"uz-Lat\":\"Nukus shahri\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(61, 1, '{\"ru\":\"Элликкалинский\",\"uz-Lat\":\"Ellikqal\'a tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(62, 1, '{\"ru\":\"Шуманайский\",\"uz-Lat\":\"Shumanay tumani:\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(63, 1, '{\"ru\":\"Чимбайский\",\"uz-Lat\":\"Chimboy tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(64, 1, '{\"ru\":\"Ходжейлийский\",\"uz-Lat\":\"Xo\'jayli tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(65, 1, '{\"ru\":\"Турткульский\",\"uz-Lat\":\"To\'rtko\'l tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(66, 1, '{\"ru\":\"Тахтакупырский\",\"uz-Lat\":\"Taxtako\'pir tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(67, 1, '{\"ru\":\"Нукусский\",\"uz-Lat\":\"Nukus tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(68, 1, '{\"ru\":\"Муйнакский\",\"uz-Lat\":\"Mo\'ynoq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(69, 1, '{\"ru\":\"Канлыкульский\",\"uz-Lat\":\"Qanliko\'l tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(70, 1, '{\"ru\":\"Кунградский\",\"uz-Lat\":\"Qo\'ng\'irot tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(71, 1, '{\"ru\":\"Кегейлийский\",\"uz-Lat\":\"Kegeyli tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(72, 1, '{\"ru\":\"Караузякский\",\"uz-Lat\":\"Qorao\'zak tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(73, 1, '{\"ru\":\"Амударьинский\",\"uz-Lat\":\"Amudaryo tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(74, 1, '{\"ru\":\"Тахиаташский район\",\"uz-Lat\":\"Taxiatosh tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(75, 1, '{\"ru\":\"Берунийский\",\"uz-Lat\":\"Beruniy tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(76, 2, '{\"ru\":\"Шахриханский\",\"uz-Lat\":\"Shahrixon tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(77, 2, '{\"ru\":\"Ходжаабадский\",\"uz-Lat\":\"Xo\'jaobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(78, 2, '{\"ru\":\"Улугнорский\",\"uz-Lat\":\"Ulug\'nor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(79, 2, '{\"ru\":\"Пахтаабадский\",\"uz-Lat\":\"Paxtaobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(80, 2, '{\"ru\":\"Мархаматский\",\"uz-Lat\":\"Marhamat tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(81, 2, '{\"ru\":\"Кургантепинский\",\"uz-Lat\":\"Qo\'rg\'ontepa tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(82, 2, '{\"ru\":\"Избасканский\",\"uz-Lat\":\"Izboskan tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(83, 2, '{\"ru\":\"Джалакудукский\",\"uz-Lat\":\"Jalaquduq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(84, 2, '{\"ru\":\"Алтынкульский\",\"uz-Lat\":\"Oltinko\'l tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(85, 2, '{\"ru\":\"Булакбашинский\",\"uz-Lat\":\"Buloqboshi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(86, 2, '{\"ru\":\"Бозский\",\"uz-Lat\":\"Bo\'z tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(87, 2, '{\"ru\":\"Балыкчинский\",\"uz-Lat\":\"Baliqchi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(88, 2, '{\"ru\":\"Асакинский\",\"uz-Lat\":\"Asaka tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(89, 2, '{\"ru\":\"Андижанский\",\"uz-Lat\":\"Andijon tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(90, 3, '{\"ru\":\"Шафирканский\",\"uz-Lat\":\"Shofirkon tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(91, 3, '{\"ru\":\"Ромитанский\",\"uz-Lat\":\"Romitan tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(92, 3, '{\"ru\":\"Пешкунский\",\"uz-Lat\":\"Peshku tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(93, 3, '{\"ru\":\"Караулбазарский\",\"uz-Lat\":\"Qorovulbozor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(94, 3, '{\"ru\":\"Каракульский\",\"uz-Lat\":\"Qorako\'l tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(95, 3, '{\"ru\":\"Каганский\",\"uz-Lat\":\"Kogon tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(96, 3, '{\"ru\":\"Жондорский\",\"uz-Lat\":\"Jondor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(97, 3, '{\"ru\":\"Гиждуванский\",\"uz-Lat\":\"G\'ijduvon tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(98, 3, '{\"ru\":\"Вабкентский\",\"uz-Lat\":\"Vobkent tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(99, 3, '{\"ru\":\"Бухарский\",\"uz-Lat\":\"Buxoro tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(100, 3, '{\"ru\":\"Алатский\",\"uz-Lat\":\"Olot tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(101, 4, '{\"ru\":\"Янгиабадский\",\"uz-Lat\":\"Yangiobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(102, 4, '{\"ru\":\"Фаришский\",\"uz-Lat\":\"Forish tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(103, 4, '{\"ru\":\"Пахтакорский\",\"uz-Lat\":\"Paxtakor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(104, 4, '{\"ru\":\"Мирзачульский\",\"uz-Lat\":\"Mirzacho\'l tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(105, 4, '{\"ru\":\"Зафарабадский\",\"uz-Lat\":\"Zafarobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(106, 4, '{\"ru\":\"Зарбдарский\",\"uz-Lat\":\"Zarbdor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(107, 4, '{\"ru\":\"Зааминский\",\"uz-Lat\":\"Zomin tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(108, 4, '{\"ru\":\"Дустликский\",\"uz-Lat\":\"Do\'stlik tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(109, 4, '{\"ru\":\"Джизакский\",\"uz-Lat\":\"Jizzax tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(110, 4, '{\"ru\":\"Галляаральский\",\"uz-Lat\":\"G\'allaorol tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(111, 4, '{\"ru\":\"Бахмальский\",\"uz-Lat\":\"Baxmal tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(112, 4, '{\"ru\":\"Арнасайский\",\"uz-Lat\":\"Arnasoy tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(113, 4, '{\"ru\":\"Шароф Рашидов\",\"uz-Lat\":\"Sharof Rashidov tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(114, 5, '{\"ru\":\"Китабский район\",\"uz-Lat\":\"Kitob tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(115, 5, '{\"ru\":\"Касбийский район\",\"uz-Lat\":\"Kasbi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(116, 5, '{\"ru\":\"Касанский район\",\"uz-Lat\":\"Koson tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(117, 5, '{\"ru\":\"Каршинский район\",\"uz-Lat\":\"Qarshi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(118, 5, '{\"ru\":\"Камашинский район\",\"uz-Lat\":\"Qamashi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(119, 5, '{\"ru\":\"Дехканабадский район\",\"uz-Lat\":\"Dehqonobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(120, 5, '{\"ru\":\"Гузарский район\",\"uz-Lat\":\"G\'uzor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(121, 5, '{\"ru\":\"Яккабагский район\",\"uz-Lat\":\"Yakkabog\' tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(122, 5, '{\"ru\":\"Шахрисабзский район\",\"uz-Lat\":\"Shahrisabz tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(123, 5, '{\"ru\":\"Чиракчинский район\",\"uz-Lat\":\"Chiroqchi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(124, 5, '{\"ru\":\"Нишанский район\",\"uz-Lat\":\"Nishon tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(125, 5, '{\"ru\":\"Мубарекский район\",\"uz-Lat\":\"Muborak tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(126, 5, '{\"ru\":\"Миришкорский район\",\"uz-Lat\":\"Mirishkor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(127, 6, '{\"ru\":\"г.Зарафшон\",\"uz-Lat\":\"Zarafshon shahri\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(128, 6, '{\"ru\":\"Учкудукский\",\"uz-Lat\":\"Uchquduq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(129, 6, '{\"ru\":\"Хатырчинский\",\"uz-Lat\":\"Xatirchi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(130, 6, '{\"ru\":\"Тамдынский\",\"uz-Lat\":\"Tomdi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(131, 6, '{\"ru\":\"Нуратинский\",\"uz-Lat\":\"Nurota tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(132, 6, '{\"ru\":\"Карманинский\",\"uz-Lat\":\"Karmana tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(133, 6, '{\"ru\":\"Навбахорский\",\"uz-Lat\":\"Navbahor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(134, 6, '{\"ru\":\"Кызылтепинский\",\"uz-Lat\":\"Qiziltepa tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(135, 6, '{\"ru\":\"Канимехский\",\"uz-Lat\":\"Konimex tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(136, 6, '{\"ru\":\"г.Навои\",\"uz-Lat\":\"Navoiy shahri\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(137, 7, '{\"ru\":\"Янгикурганский\",\"uz-Lat\":\"Yangiqo\'rg\'on tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(138, 7, '{\"ru\":\"Чустский\",\"uz-Lat\":\"Chust tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(139, 7, '{\"ru\":\"Чартакский\",\"uz-Lat\":\"Chortoq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(140, 7, '{\"ru\":\"Учкурганский\",\"uz-Lat\":\"Uchqo\'rg\'on tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(141, 7, '{\"ru\":\"Уйчинский\",\"uz-Lat\":\"Uychi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(142, 7, '{\"ru\":\"Туракурганский\",\"uz-Lat\":\"To\'raqo\'rg\'on tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(143, 7, '{\"ru\":\"Папский\",\"uz-Lat\":\"Pop tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(144, 7, '{\"ru\":\"Нарынский\",\"uz-Lat\":\"Norin tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(145, 7, '{\"ru\":\"Наманганский\",\"uz-Lat\":\"Namangan tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(146, 7, '{\"ru\":\"Мингбулакский\",\"uz-Lat\":\"Mingbuloq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(147, 7, '{\"ru\":\"Касансайский\",\"uz-Lat\":\"Kosonsoy tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(148, 8, '{\"ru\":\"Ургутский\",\"uz-Lat\":\"Urgut tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(149, 8, '{\"ru\":\"Тайлакский\",\"uz-Lat\":\"Toyloq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(150, 8, '{\"ru\":\"Самаркандский\",\"uz-Lat\":\"Samarqand tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(151, 8, '{\"ru\":\"Пахтачийский\",\"uz-Lat\":\"Paxtachi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(152, 8, '{\"ru\":\"Пастдаргомский\",\"uz-Lat\":\"Pastdarg\'om tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(153, 8, '{\"ru\":\"Пайарыкский\",\"uz-Lat\":\"Payariq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(154, 8, '{\"ru\":\"Нурабадский\",\"uz-Lat\":\"Nurobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(155, 8, '{\"ru\":\"Нарпайский\",\"uz-Lat\":\"Narpay tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(156, 8, '{\"ru\":\"Кошрабадский\",\"uz-Lat\":\"Qo\'shrabod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(157, 8, '{\"ru\":\"Каттакурганский\",\"uz-Lat\":\"Kattaqo\'rg\'on tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(158, 8, '{\"ru\":\"Иштыханский\",\"uz-Lat\":\"Ishtixon tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(159, 8, '{\"ru\":\"Джамбайский\",\"uz-Lat\":\"Jomboy tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(160, 8, '{\"ru\":\"Булунгурский\",\"uz-Lat\":\"Bulung\'ur tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(161, 8, '{\"ru\":\"Акдарьинский\",\"uz-Lat\":\"Oqdaryo tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(162, 9, '{\"ru\":\"Кумкурганский\",\"uz-Lat\":\"Qumqo\'rg\'on tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(163, 9, '{\"ru\":\"Шурчинский\",\"uz-Lat\":\"Sho\'rchi tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(164, 9, '{\"ru\":\"Шерабадский\",\"uz-Lat\":\"Sherobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(165, 9, '{\"ru\":\"Узунский\",\"uz-Lat\":\"Uzun tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(166, 9, '{\"ru\":\"Термезский\",\"uz-Lat\":\"Termiz tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(167, 9, '{\"ru\":\"Сариасийский\",\"uz-Lat\":\"Sariosiyo tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(168, 9, '{\"ru\":\"Музрабадский\",\"uz-Lat\":\"Muzrabot tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(169, 9, '{\"ru\":\"Кизирикский\",\"uz-Lat\":\"Qiziriq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(170, 9, '{\"ru\":\"Джаркурганский\",\"uz-Lat\":\"Jarqo\'rg\'on tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(171, 9, '{\"ru\":\"Денауский\",\"uz-Lat\":\"Denov tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(172, 9, '{\"ru\":\"Байсунский\",\"uz-Lat\":\"Boysun tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(173, 9, '{\"ru\":\"Ангорский\",\"uz-Lat\":\"Angor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(174, 9, '{\"ru\":\"Алтынсайский\",\"uz-Lat\":\"Oltinsoy tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(175, 10, '{\"ru\":\"Хавастский\",\"uz-Lat\":\"Xovos tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(176, 10, '{\"ru\":\"Сырдарьинский\",\"uz-Lat\":\"Sirdaryo tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(177, 10, '{\"ru\":\"Сардобский\",\"uz-Lat\":\"Sardoba tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(178, 10, '{\"ru\":\"Сайхунабадский\",\"uz-Lat\":\"Sayxunobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(179, 10, '{\"ru\":\"Мирзаабадский\",\"uz-Lat\":\"Mirzaobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(180, 10, '{\"ru\":\"Гулистанский\",\"uz-Lat\":\"Guliston tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(181, 10, '{\"ru\":\"Баяутский\",\"uz-Lat\":\"Boyovut tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(182, 10, '{\"ru\":\"Акалтынский\",\"uz-Lat\":\"Oqoltin tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(183, 11, '{\"ru\":\"Янгиюльский\",\"uz-Lat\":\"Yangiyo\'l tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(184, 11, '{\"ru\":\"Юкоричирчикский\",\"uz-Lat\":\"Yuqori Chirchiq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(185, 11, '{\"ru\":\"Чиназский\",\"uz-Lat\":\"Chinoz tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(186, 11, '{\"ru\":\"Уртачирчикский\",\"uz-Lat\":\"O\'rta Chirchiq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(187, 11, '{\"ru\":\"Зангиатинский\",\"uz-Lat\":\"Zangiota tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(188, 11, '{\"ru\":\"Пскентский\",\"uz-Lat\":\"Piskent tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(189, 11, '{\"ru\":\"Паркентский\",\"uz-Lat\":\"Parkent tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(190, 11, '{\"ru\":\"Куйичирчикский\",\"uz-Lat\":\"Quyi Chirchiq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(191, 11, '{\"ru\":\"Кибрайский\",\"uz-Lat\":\"Qibray tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(192, 11, '{\"ru\":\"Букинский\",\"uz-Lat\":\"Bo\'ka tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(193, 11, '{\"ru\":\"Бостанлыкский\",\"uz-Lat\":\"Bo\'stonliq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(194, 11, '{\"ru\":\"Бекабадский\",\"uz-Lat\":\"Bekobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(195, 11, '{\"ru\":\"Ахангаранский\",\"uz-Lat\":\"Ohangaron tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(196, 11, '{\"ru\":\"Аккурганский\",\"uz-Lat\":\"Oqqo\'rg\'on tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(197, 11, '{\"ru\":\"Ташкент\",\"uz-Lat\":\"Toshkent tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(198, 11, '{\"ru\":\"Ангрен\",\"uz-Lat\":\"Angren tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(199, 12, '{\"ru\":\"Язъяванский\",\"uz-Lat\":\"Yozyovon tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(200, 12, '{\"ru\":\"Фуркатский\",\"uz-Lat\":\"Furqat tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(201, 12, '{\"ru\":\"Ферганский\",\"uz-Lat\":\"Farg\'ona tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(202, 12, '{\"ru\":\"Учкуприкский\",\"uz-Lat\":\"Uchko\'prik tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(203, 12, '{\"ru\":\"Узбекистанский\",\"uz-Lat\":\"O\'zbekiston tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(204, 12, '{\"ru\":\"Ташлакский\",\"uz-Lat\":\"Toshloq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(205, 12, '{\"ru\":\"Сохский\",\"uz-Lat\":\"So\'x tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(206, 12, '{\"ru\":\"Риштанский\",\"uz-Lat\":\"Rishton tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(207, 12, '{\"ru\":\"Куштепинский\",\"uz-Lat\":\"Qo\'shtepa tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(208, 12, '{\"ru\":\"Кувинский\",\"uz-Lat\":\"Quva tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(209, 12, '{\"ru\":\"Дангаринский\",\"uz-Lat\":\"Dang\'ara tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(210, 12, '{\"ru\":\"Бувайдинский\",\"uz-Lat\":\"Buvayda tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(211, 12, '{\"ru\":\"Бешарыкский\",\"uz-Lat\":\"Beshariq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(212, 12, '{\"ru\":\"Багдадский\",\"uz-Lat\":\"Bag\'dod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(213, 12, '{\"ru\":\"Алтыарыкский\",\"uz-Lat\":\"Oltiariq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(214, 13, '{\"ru\":\"Янгиарыкский\",\"uz-Lat\":\"Yangiariq tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(215, 13, '{\"ru\":\"Янгибазарский\",\"uz-Lat\":\"Yangibozor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(216, 13, '{\"ru\":\"Шаватский\",\"uz-Lat\":\"Shovot tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(217, 13, '{\"ru\":\"Хивинский\",\"uz-Lat\":\"Xiva tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(218, 13, '{\"ru\":\"Ханкинский\",\"uz-Lat\":\"Xonqa tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(219, 13, '{\"ru\":\"Хазараспский\",\"uz-Lat\":\"Hazorasp tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(220, 13, '{\"ru\":\"Ургенчский\",\"uz-Lat\":\"Urganch tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(221, 13, '{\"ru\":\"Кошкупырский\",\"uz-Lat\":\"Qo\'shko\'pir tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(222, 13, '{\"ru\":\"Гурленский\",\"uz-Lat\":\"Gurlan tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(223, 13, '{\"ru\":\"Багатский\",\"uz-Lat\":\"Bog\'ot tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(224, 13, '{\"ru\":\"г.Ургенч\",\"uz-Lat\":\"Urganch shahri\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(225, 14, '{\"ru\":\"Яшнабадский район\",\"uz-Lat\":\"Yashnobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(226, 14, '{\"ru\":\"Яккасарайский район\",\"uz-Lat\":\"Yakkasaroy tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(227, 14, '{\"ru\":\"Юнусабадский район\",\"uz-Lat\":\"Yunusobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(228, 14, '{\"ru\":\"Шайхантахурский район\",\"uz-Lat\":\"Shayxontohur tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(229, 14, '{\"ru\":\"Чиланзарский район\",\"uz-Lat\":\"Chilonzor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(230, 14, '{\"ru\":\"Учтепинский район\",\"uz-Lat\":\"Uchtepa tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(231, 14, '{\"ru\":\"Сергелийский район\",\"uz-Lat\":\"Sergeli tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(232, 14, '{\"ru\":\"Мирзо-Улугбекский район\",\"uz-Lat\":\"Mirzo Ulug\'bek tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(233, 14, '{\"ru\":\"Мирабадский район\",\"uz-Lat\":\"Mirobod tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(234, 14, '{\"ru\":\"Бектемирский район\",\"uz-Lat\":\"Bektemir tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}'),
(235, 14, '{\"ru\":\"Алмазарский район\",\"uz-Lat\":\"Olmazor tumani\",\"uz-Cyr\":\"\",\"en\":\"\"}');

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `scenario` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `mime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `thumbnails` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `scenario`, `hash`, `user_id`, `name`, `ext`, `size`, `mime`, `width`, `height`, `thumbnails`, `created_at`) VALUES
(17, 'document', '8ECIDKKk222EK72NbMcDbtkfMe_vvJCA', 79, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-05 01:36:23'),
(18, 'document', '241TQetps0yNbwE0G6tK2XXR0fkotszx', 79, '51oTVlqzzLL._SY450_.jpg', 'jpg', 24721, 'image/jpeg', 350, 450, NULL, '2020-06-05 01:36:35'),
(19, 'recomendations', '6juW2TIH5pViAL4q3nZs-P1btVHl1tpy', 78, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 20:03:10'),
(20, 'biographies', 'Yq7OFVDO-d1yNj9xzK0e8HOEE3w4ADr3', 78, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 20:03:29'),
(21, 'relatives', 'DBU08FTOIiqWCJHBnqJVN7_xyVbQeXjA', 78, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 20:03:29'),
(22, 'careers', 'IN393GhSdfqhFWiV4RDi77znwUiC-3AN', 78, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 20:03:29'),
(23, 'avatar', '2TZ5Qro_Y23u-tzznQ5NxwfSBGRuhrht', 83, '7fece3fdb4c0f8c0fe86647ac25ac90d7b4bbcdc-t710.jpg', 'jpg', 30770, 'image/jpeg', 413, 531, NULL, '2020-06-07 21:04:33'),
(24, 'diplomas', 'FUa26m8m9J1qGlILp1MfQbnhrHXL9-aY', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:04:33'),
(25, 'passports', 'hD2i1Vuni1pGluYNnHom166fSe_t5vTt', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-07 21:04:33'),
(26, 'certificates', 'QYJRu5Iufls9scSomLKTKoOMnzEX12Fo', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:04:33'),
(27, 'avatar', '5nxu6PcfcmlUIAnMJ6vmCfbgfyuwBPN2', 85, '7fece3fdb4c0f8c0fe86647ac25ac90d7b4bbcdc-t710.jpg', 'jpg', 30770, 'image/jpeg', 413, 531, NULL, '2020-06-07 21:16:34'),
(28, 'diplomas', '35_CoTDgfncQDzNaO7fiIGFW5pdc6li6', 85, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:16:35'),
(29, 'passports', '-KIFil1kuvXEw42Nw_TRcqm0UqckMXEi', 85, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-07 21:16:35'),
(30, 'certificates', 'tM5cMhIxiIfR4SRJ6GQon6woDMZjdWry', 85, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:16:35'),
(31, 'avatar', 'nLJ5c6arLFZK7razWcKCrXtZydv36eXM', 85, '7fece3fdb4c0f8c0fe86647ac25ac90d7b4bbcdc-t710.jpg', 'jpg', 30770, 'image/jpeg', 413, 531, NULL, '2020-06-07 21:27:18'),
(32, 'diplomas', '46mVCartqn54WAhc3JcmZWn_qvbrRcX8', 85, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:27:18'),
(33, 'passports', 'XGgjuggLgNk0EXt6fIayjOGbVNWf7oJQ', 85, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-07 21:27:18'),
(34, 'certificates', 'QD_7MQIRkoyJjNtFBWmvVMJuut1q0Zab', 85, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:27:18'),
(35, 'recomendations', 'AxLd7GXjjMCL4-bY2shYRvnav6OLFxYy', 85, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:27:31'),
(36, 'biographies', 'E47LvCYoY13rFr_SKpnKmAJ_u8_2aM2H', 85, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:27:54'),
(37, 'relatives', 'tNajDEy48eAeGD2VzXUHSXYz8oQ8IXEa', 85, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:27:54'),
(38, 'careers', 'M2qcl30RQfFWUI1j5KRiwMXVxiVt1_px', 85, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-07 21:27:54'),
(39, 'document', 'JlbJ0-_axSOFvzaHlxcddhpeAxkNSmBg', 81, 'image_2020-06-06_11-21-07.png', 'png', 28164, 'image/png', 1168, 106, NULL, '2020-06-08 03:41:33'),
(40, 'document', 'F6rsMp6D1sWBoQK-SLjVXF8qc6tTEWI0', 81, 'image_2020-06-06_11-21-07.png', 'png', 28164, 'image/png', 1168, 106, NULL, '2020-06-08 03:46:15'),
(41, 'document', 'vRSDwb_SM4xkuuYFWwz5Dl_iDh6jzTmA', 81, 'image_2020-06-07_21-10-11.png', 'png', 110710, 'image/png', 1342, 934, NULL, '2020-06-08 03:47:14'),
(45, 'avatar', 'RKQq3OAlIGpssofCMVOldSnzMnqPr8cs', 86, '7fece3fdb4c0f8c0fe86647ac25ac90d7b4bbcdc-t710.jpg', 'jpg', 30770, 'image/jpeg', 413, 531, NULL, '2020-06-08 06:48:38'),
(46, 'diplomas', 'KVEliCeQL7Fr-DwbOmLyo-I9_Qm9E8hy', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 06:48:38'),
(47, 'passports', 'x4Bryn5Rgs8MvqF4g-MmeMv-mChclFXL', 86, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-08 06:48:38'),
(48, 'certificates', 'wPMTly22c5xA27AORjA-oQUUChu2dgev', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 06:48:38'),
(49, 'recomendations', 'LPLywB_FVS1uMMqlG7_54rRHdymGsH7m', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 06:49:23'),
(50, 'biographies', 'nNZ1lz9l9aLd0XVF1VB7n9R8nq4UIzIH', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 06:49:47'),
(51, 'relatives', 'OlX-D62mkLjJk4urhTSINt7zLokS2JMn', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 06:49:47'),
(52, 'careers', '_N0IR4zQPsP4gjB8980gMFUe3dQiW2-1', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 06:49:47'),
(53, 'avatar', 'l6znLLQthhvy3UuZxH_YRf9HEWCbsuZ8', 90, '7fece3fdb4c0f8c0fe86647ac25ac90d7b4bbcdc-t710.jpg', 'jpg', 30770, 'image/jpeg', 413, 531, NULL, '2020-06-08 10:16:15'),
(54, 'diplomas', 'wUS3irCW5gqe-Wg-Spq0XLgrvBhTr6rj', 90, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 10:16:16'),
(55, 'passports', 'BmuFQ3FHlgJSxZu2f9nwUCAVO_d5CguR', 90, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-08 10:16:16'),
(56, 'certificates', 'L3xCqr-DYLu8Hi0B63wQXf99EOzsqpSO', 90, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 10:16:16'),
(57, 'recomendations', 'ahYEXOf-ZIHRxs2w6oJLHZhugDNyHufP', 90, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 10:17:44'),
(58, 'biographies', 'os3x4dGFL5YkLfedrCTo2DJf4ITWb5Sm', 90, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 10:23:30'),
(59, 'relatives', '0Nx1DA_IkFC9EsqSqegY27Wi44X9_qH_', 90, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 10:23:30'),
(60, 'careers', 'gWnKQvvDOun1dXqxy7FDJVojk9wsvGMw', 90, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-08 10:23:30'),
(61, 'certificates', 'lE5oOPhQeCVcNrR6FwymS1fo4PxYwzX-', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 00:29:57'),
(62, 'passports', 'i-wcdMy-h5tOqBHhaiAhy1bVwBD5ogLW', 86, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 00:37:04'),
(63, 'passports', 'k3_myYjKEuqDtJc570uWNnOlDQiFumHY', 86, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 00:48:00'),
(64, 'diplomas', 'Fsbby6tQlSoSVq5WrywG9wogc5HKjh92', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 00:51:39'),
(65, 'certificates', 'sTN_AsBRZP7CEcS1G3vKMMmUQqPppzWb', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 00:51:39'),
(66, 'certificates', 'EcDli0zRWe3mhlVbu_dNGgDcndJebuS9', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 00:55:14'),
(67, 'diplomas', 'd0i0eZufBvhkubIMT8Zl9t2QcYMFnRCs', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 00:58:03'),
(68, 'passports', 'zyv-QkmndlpsBpVnQpP0XX_5U5DDdWXm', 86, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 00:58:03'),
(69, 'certificates', '57ZWlEz3UGBqkec6Df9MdL9VpLCFFyqh', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 00:58:03'),
(70, 'diplomas', 'OvT7QN8BA0Davm42lFGZn4y6_6evRl70', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 00:58:55'),
(71, 'passports', 'y1fM44B1qDWUYKKCDzw_mfpPFoc3IT9n', 86, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 00:58:55'),
(72, 'certificates', 'ignpaKT1ilcaKk2npuIUv414hwKkCpCD', 86, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 00:58:55'),
(73, 'diplomas', 'PWrrDlYmCV1rWMRAF3CRsfgzzeK6nfSd', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:24:00'),
(74, 'passports', 'wv301NaCNj_PaNXkryDXT-d0SmFvE-Hr', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 01:24:00'),
(75, 'certificates', 'zzw5asnYTsq9wECjHZixnY829qvRQpe0', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:24:00'),
(76, 'recomendations', 'mdXba4CBRPgy1QTADhIKUxRSLDietYBw', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:30:47'),
(77, 'recomendations', '31HaO2uOUrsoxa_SxWkGX5L1Y0ui8FWv', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:32:08'),
(78, 'biographies', '-rVxOUsnUATS7KyZ5PzUX4QX8A6qFJVQ', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:32:08'),
(79, 'relatives', 'I2CsyBjPa6vQj-W9HiyJZ62dT3GSepkr', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:32:08'),
(80, 'careers', '3EIXs6AS6BNYNwvEtSIdUjdrZwSRd-Vs', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:32:08'),
(81, 'diplomas', 'Gw_U-PotGZINM9jsL7b73sCIEvlCuZFV', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:33:40'),
(82, 'passports', 'ZXSB7ya3D5ede5k2m2ujsV0wmEJhMsWx', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 01:33:40'),
(83, 'certificates', 'ivRA1c48LnlIdPSe1SMHS0Rdp3TbDW5t', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:33:41'),
(84, 'recomendations', 'EgCkwhtd2uXKpvsEvw8CRx-04Ii4eGgn', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:38:40'),
(85, 'biographies', 'QaQItsUobunYpHHkX_scwzhPUGVx3bEC', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:38:40'),
(86, 'relatives', 'UF4P_TavuCbuOMinNavT_oZiawORYJ8l', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:38:40'),
(87, 'careers', 'xAdNQF4ndd5aa1YXRoR2XF3zI-asEq_M', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 01:38:40'),
(89, 'document', 'ym45SmBahgKLV1H-JTCdU39eBkPfZRMF', 81, 'back5.jpg', 'jpg', 340395, 'image/jpeg', 1920, 969, NULL, '2020-06-10 20:03:09'),
(90, 'document', 'bU4WuWrqRBs4mwzMUDDnBEXkHTzsGqwI', 81, 'photo.jpg', 'jpg', 642663, 'image/jpeg', 1440, 1440, NULL, '2020-06-10 21:02:24'),
(91, 'diplomas', 'ZoxtFwqLb_Lw_Zf0bjLg-P0LfAamyQFH', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 21:16:15'),
(92, 'passports', 'hbYzT-Z1y2sBtuaJyCkuw_-3Rp0qErbd', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 21:16:15'),
(93, 'diplomas', 'xGhuE4yFsnmrhazpIlBR3XiRtUwhv3IT', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 21:18:25'),
(94, 'passports', '3WT2rvf8069fCoTqOfmABpnWZ2MQbmTZ', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 21:18:25'),
(95, 'passports', 'nmSCkfhS3M9PEKL4AqKC8RdFcTWaoyr2', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 21:25:23'),
(96, 'passports', 'rmfim_6WF5x1f6oCdsRw6H4dnD6bzpPR', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 21:31:47'),
(97, 'passports', 'cd9cuKYd85dg5UmiIFUdFNpw69_F1jFi', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 21:33:07'),
(98, 'passports', 'i4yuyJkxvcnH4jzacH_bMa4QNojM1kK0', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 21:38:04'),
(99, 'passports', 'kI9tH6xI4oeW_fAzyWO6L0Rbr1C4jN1O', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 21:39:19'),
(100, 'passports', '0XYZogWaAm8v3fFvpL96LvCQlnyuHNAz', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-10 21:44:55'),
(101, 'passports', '2EFgNzkXQO5w7NkoVgXqhw2bfOczGiDw', 83, '723cc841dbaeca7bd38eef243d565a57.jpg', 'jpg', 16992, 'image/jpeg', 200, 300, NULL, '2020-06-10 21:47:58'),
(102, 'diplomas', 'F6svRwAAF4ghfoAC72CwIc8Lnq0ylqpB', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 21:51:54'),
(103, 'certificates', 'Y8J8qrHAz7QUCUW8_LClffutang9ak4x', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 21:51:54'),
(104, 'recomendations', 'afXcc8JRkOmNdPkYrrhkqchz--WfCuxP', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 21:51:54'),
(105, 'biographies', 'kwsf1GU7JFXdbm_W4Xd5rPagQq2pf8Ct', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 21:51:54'),
(106, 'relatives', 'CNrE37ZH2Zw_RBLaJDpTgeSqtu7Rn0Qf', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 21:51:54'),
(107, 'careers', 'dz5DtM1P3MXp4QOIMRyRwKUUgkC_O4jq', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-10 21:51:54'),
(108, 'passports', 'NioAapMnPc6L3Kicm7ER79WeXDrSq3OQ', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-11 05:30:34'),
(109, 'avatar', 'kBKOYiV0-CqZn58vQqytN8k05v2fLGwM', 97, '7fece3fdb4c0f8c0fe86647ac25ac90d7b4bbcdc-t710.jpg', 'jpg', 30770, 'image/jpeg', 413, 531, NULL, '2020-06-11 12:50:49'),
(110, 'diplomas', 'L9lx6UD6qx58OiFOQzokpfWcieC_PK60', 97, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-11 12:50:50'),
(111, 'passports', '0RCUjiSt2k55--Rx0zECLEPklo87wWbV', 97, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-11 12:50:50'),
(112, 'certificates', 'poH4EYGb9E_KyurWN3samRAC99l8qHDk', 97, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-11 12:50:50'),
(113, 'recomendations', '1-YV_p2DmTExcHVJcMp9xBJaMiHnRr1g', 97, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-11 12:51:50'),
(114, 'biographies', 'roZDFHFPixe_rX2PKns-2Z7nRwVgOLd1', 97, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-11 12:58:21'),
(115, 'relatives', 'DFLj7d8K74iG8NcqqHTj_G1DNEpOdK4Q', 97, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-11 12:58:21'),
(116, 'careers', 'pe41vqNmWHkCjQ_2xl95uDZ4kiIDbc9d', 97, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-11 12:58:21'),
(117, 'avatar', '3Fek9lnYOSFA6ZzZamBS4fD4H8pR4GXq', 83, '7fece3fdb4c0f8c0fe86647ac25ac90d7b4bbcdc-t710.jpg', 'jpg', 30770, 'image/jpeg', 413, 531, NULL, '2020-06-12 13:06:40'),
(118, 'diplomas', '2kbyO2dg36eO3XpcThhty-u3f0IAdW2c', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-12 13:06:40'),
(119, 'passports', 'pb9nY_aTlFqVmWWugE1hjpfDtt70oUzK', 83, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-12 13:06:40'),
(120, 'certificates', 'lHRAewPQxRgsab-zqZL_St-C59lnDX30', 83, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-12 13:06:40'),
(121, 'document', 'si8F7_d8_36_vjUIO-cTSbHWPiCOhp0v', 81, 'swift.pdf', 'pdf', 831868, 'application/pdf', NULL, NULL, NULL, '2020-06-13 17:42:50'),
(122, 'document', 'R1EgQ023X3khCKI-T0HKjEODHkxeIDOX', 81, 'swift.pdf', 'pdf', 831868, 'application/pdf', NULL, NULL, NULL, '2020-06-13 17:46:49'),
(123, 'document', 'fhLxzNxZJRJkiNXpQtsLfELwwSQdrKMa', 81, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-13 17:46:50'),
(124, 'document', 'KWHBBmTdonWg7kKAD-7Lm6ySKGepW96K', 81, 'Предупреждение_об_уголовной_ответственности.docx', 'docx', 14086, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', NULL, NULL, NULL, '2020-06-13 18:14:47'),
(125, 'document', 'JrN5REc08_g-IKJ1IV7ctqpwpQ5pyEg5', 81, 'Предупреждение_об_уголовной_ответственности.docx', 'docx', 14086, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', NULL, NULL, NULL, '2020-06-13 18:15:10'),
(126, 'document', 'ZQzjKHl0cRJ6NGIVYQOkTfWNfd_-pHfm', 81, 'Предупреждение_об_уголовной_ответственности.docx', 'docx', 14086, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', NULL, NULL, NULL, '2020-06-13 18:16:00'),
(127, 'document', 'YYD00DV-PfOODc865fQsqD7q9PbcQaJU', 81, '4a4b963bb9b85c35b934e3d50c6ed7a0.jpg', 'jpg', 13134, 'image/jpeg', 200, 300, NULL, '2020-06-13 20:12:32'),
(128, 'document', 'LFzJQ6Rm1cs6ADYr8bCaWGjL6kVVRefo', 81, 'Shokhrukh_Toshniyozov.pdf', 'pdf', 1070559, 'application/pdf', NULL, NULL, NULL, '2020-06-13 20:17:58'),
(129, 'document', 'J7e4QbJEBWkbXlwA_-hysVIXhuXkdJ3D', 81, 'biography_example.docx', 'docx', 16268, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', NULL, NULL, NULL, '2020-06-13 20:28:41'),
(130, 'document', 'JJmfM8mhP3ExDZ1TMSr5-E8l95F78yEq', 81, 'relatives_example.docx', 'docx', 18236, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', NULL, NULL, NULL, '2020-06-13 20:29:44'),
(131, 'document', 'Dp_wB8oej-eXY1xNcmY9UKnnv3M8-jGR', 81, 'career_example.docx', 'docx', 15068, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', NULL, NULL, NULL, '2020-06-13 20:30:04'),
(132, 'document', 'yodP5RCJI3XdhqOP7ix9XBK_jmdstb-8', 81, 'biography_example.docx', 'docx', 16114, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', NULL, NULL, NULL, '2020-06-13 20:32:44');

-- --------------------------------------------------------

--
-- Структура таблицы `media_order`
--

CREATE TABLE `media_order` (
  `id` int(11) NOT NULL,
  `media_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `url` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `media_order`
--

INSERT INTO `media_order` (`id`, `media_id`, `model_id`, `category`, `url`) VALUES
(33, 130, 24, 'settings', 'http://localhost:8081/uploads/documents/jj\\mf/JJmfM8mhP3ExDZ1TMSr5-E8l95F78yEq.docx'),
(34, 131, 25, 'settings', 'http://localhost:8081/uploads/documents/dp\\_w/Dp_wB8oej-eXY1xNcmY9UKnnv3M8-jGR.docx'),
(35, 132, 20, 'settings', 'http://localhost:8081/uploads/documents/yo\\dp/yodP5RCJI3XdhqOP7ix9XBK_jmdstb-8.docx');

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `category_id2` int(11) DEFAULT NULL,
  `group` varchar(30) DEFAULT NULL,
  `alias` text NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `content_html` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `short_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT NULL,
  `option` enum('yes','no') DEFAULT NULL,
  `options` varchar(255) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `tags` text DEFAULT NULL,
  `meta_title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `meta_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `meta_keywords` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `category_id`, `category_id2`, `group`, `alias`, `title`, `content`, `content_html`, `short_content`, `status`, `option`, `options`, `views`, `tags`, `meta_title`, `meta_description`, `meta_keywords`, `sort_order`, `created_at`, `updated_at`) VALUES
(53, 0, 0, NULL, 'glavnaja', '{\"ru\":\"Главная\",\"uz-Cyr\":\"Бош саҳифа\", \"en\":\"Home\",\"uz-Lat\":\"Bosh sahifa\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, 'no', 'home', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:19:51', '2020-05-13 01:54:08'),
(54, 0, 0, NULL, 'ob-akademii', '{\"ru\":\"Об Академии\",\"uz-Cyr\":\"Академия ҳақида\",\"en\":\"About Academy\",\"uz-Lat\":\"Akademiya haqida\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, 'no', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:23:29', '2020-05-13 01:58:06'),
(55, 0, 0, NULL, 'press-tsentr', '{\"ru\":\"Пресс-центр\",\"uz-Cyr\":\"Матбуот маркази\",\"en\":\"Press center\",\"uz-Lat\":\"Matbuot markazi\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, 'no', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:24:24', '2020-05-13 01:58:06'),
(56, 0, 0, NULL, 'uchebnyj-protsess', '{\"ru\":\"Учебный процесс\",\"uz-Cyr\":\"Ўқув жараёни\",\"en\":\"Studying proccess\",\"uz-Lat\":\"O\'quv jarayoni\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, 'no', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:24:46', '2020-05-13 01:58:06'),
(58, 0, 0, NULL, 'nauchnaja-dejatelnost', '{\"ru\":\"Научная деятельность\",\"uz-Cyr\":\"Илмий фаолият\",\"en\":\"Scientific activity\",\"uz-Lat\":\"Ilmiy faoliyat\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, 'no', 'scientific-research', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:25:16', '2020-05-13 01:58:06'),
(59, 0, 0, NULL, 'sotrudnichestvo', '{\"ru\":\"Сотрудничество\",\"uz-Cyr\":\"Ҳамкорлик\",\"en\":\"Cooperation\",\"uz-Lat\":\"Hamkorlik\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, 'no', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:25:34', '2020-05-13 01:58:06'),
(60, 54, 0, NULL, 'obschie', '{\"ru\":\"Общие сведения\",\"uz-Cyr\":\"Умумий маълумот\",\"uz-Lat\":\"Umumiy ma\'lumot\",\"en\":\"General information\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">С первых дней приобретения независимости Республики Узбекистан, на ряду с усовершенствованием деятельности органов прокуратуры уделено особое внимание повышению квалификации кадров органов прокуратуры.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">С этой целью 24 января 1992 года было вынесено Постановление Кабинета Министров Республики Узбекистан &laquo;О вопросах организации деятельности органов прокуратуры Республики Узбекистан&raquo; №33, согласно которому при прокуратуре Республики Узбекистан создан &laquo;Центр проблем укрепления законности и повышения квалификации прокурорско-следственных кадров&raquo;. На Центр, кроме повышение квалификации кадров органов прокуратуры также было возложено и ведение научной деятельности, в связи с чем Приказом Генерального прокурора Республики Узбекистан №869 от 20 декабря 1994 года был создан научный совет центра проблем укрепления законности и повышения квалификации прокурорско-следственных кадров.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">В следствии проводимых за прошедшие годы реформ в нашей республике, возникла потребность в усовершенствовании деятельности центра и Постановлением Президента Республики Узбекистан от 7 ноября 2007 года на базе данного центра были созданы Высшие учебные курсы Генеральной прокуратуры Республики Узбекистан.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">Осуществляемые преобразования в стране, новые приоритеты социально-экономического развития и задачи по обеспечению верховенства закона и укреплению законности обусловили необходимость дальнейшего совершенствования системы организации подготовки, переподготовки и повышения квалификации кадров органов прокуратуры.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">За прошедший период сформирован определенный образовательный и научный потенциал по обучению работников органов прокуратуры знаниям и навыкам, необходимым для комплексного анализа состояния правоприменения, своевременного принятия мер по устранению причин и условий совершения нарушений закона, обеспечения защиты прав и свобод граждан, всемерной поддержки субъектов предпринимательства.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">Вместе с тем, определение принципиально новых направлений деятельности органов прокуратуры, повышение их роли в реализации осуществляемых реформ и преобразований потребовали формирования достойного кадрового состава, способного эффективно выполнять поставленные задачи.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">В соответствии с задачами, определенными Стратегией действий по пяти приоритетным направлениям развития Республики Узбекистан в 2017-2021 годах, а также в целях создания современной системы подготовки, переподготовки и повышения квалификации кадров органов прокуратуры 8 мая 2018 года Президентом Республики Узбекистан принят&nbsp;Указ &laquo;О мерах по коренному совершенствованию системы подготовки, переподготовки и повышения квалификации кадров органов прокуратуры&raquo;. Согласно Указу, Высшие учебные курсы Генеральной прокуратуры Республики Узбекистан преобразованы в Академию Генеральной прокуратуры Республики Узбекистан.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">Академия Генеральной прокуратуры Республики Узбекистан является образовательным и научно-исследовательским учреждением.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">Академия в пределах своей компетенции организует деятельность во взаимодействии со структурными подразделениями Генеральной прокуратуры, нижестоящими прокуратурами, Департаментом&nbsp; по борьбе с экономическими преступлениями, Бюро принудительного исполнения, правоохранительными и другими государственными органами, образовательными и научными учреждениями, институтами гражданского общества и общественностью.&nbsp;<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">Академия осуществляет:<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">- подготовку, переподготовку и повышение квалификации работников органов прокуратуры, в том числе без отрыва от производства;<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">- подготовку кадров на бюджетной и платно-контрактной основе,&nbsp; с присвоением степени магистра и выдачей диплома государственного образца;<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">- профессиональную переподготовку кандидатов на замещение должностей в органах прокуратуры в виде первичной специализации;<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">- профессиональную переподготовку кадров, состоящих в резерве&nbsp; на замещение руководящих должностей;<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">- повышение квалификации сотрудников государственных органов и иных организаций по вопросам взаимодействия в сфере профилактики правонарушений и борьбы с преступностью и коррупцией по краткосрочным программам;<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">- подготовку научных и научно-педагогических кадров высшей квалификации в соответствии с законодательством;<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">- все виды учебной, методической, научно-исследовательской, информационно-аналитической деятельности, в том числе с привлечением национальных, зарубежных и международных грантов;<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">- направление профессорско-преподавательского состава, работников&nbsp; и слушателей на стажировку в органы государственной власти, государственного и хозяйственного управления, общественные организации, а также в высшие образовательные и научно-исследовательские учреждения, в том числе зарубежных стран;<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">- издательскую деятельность.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">Программы по подготовке, переподготовке и повышению квалификации кадров в Академии осуществляются в установленном порядке&nbsp; на бюджетной и платно-контрактной основе.<\\/span><\\/p>\\r\\n<p style=\\\"text-align: justify;\\\"><span style=\\\"font-size: 14pt;\\\">Основной целью Академии является организация эффективной системы подготовки, переподготовки и повышения квалификации работников органов прокуратуры и других кадров, а также проведение фундаментальных&nbsp; и прикладных научных исследований.<\\/span><\\/p>\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', 'active', 'no', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:25:47', '2020-05-13 01:58:06'),
(61, 0, 0, NULL, 'esche', '{\"ru\":\"Еще\",\"uz-Cyr\":\"Бошқалар\",\"en\":\"Others\",\"uz-Lat\":\"Boshqalar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, 'no', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:26:18', '2020-05-13 01:58:06'),
(62, 54, 0, NULL, 'pravovye-akty', '{\"ru\":\"Правовые акты\",\"uz-Cyr\":\"Ҳуқуқий ҳужжатлар\",\"en\":\"Legal acts\",\"uz-Lat\":\"Huquqiy hujjatlar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, 'no', 'law-acts', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:27:45', '2020-05-13 01:58:06'),
(63, 54, 0, NULL, 'struktura-akademii-', '{\"ru\":\"Структура Академии \",\"uz-Cyr\":\"Ўзбекистон Республикаси Бош Прокуратураси Академиясининг тузилиши\",\"uz-Lat\":\"O\'zbekiston Respublikasi Bosh Prokuraturasi Akademiyasining tuzilishi\",\"en\":\"Structure\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', 'active', '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:27:56', '2020-05-13 01:58:06'),
(64, 54, 0, NULL, 'tseli-i-zadachi', '{\"ru\":\"Цели и задачи\",\"uz-Cyr\":\"Мақсад ва вазифалар\",\"en\":\"Targets and goals\",\"uz-Lat\":\"Maqsad va vazifalar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:29:54', '2020-05-13 01:58:06'),
(65, 54, 0, NULL, 'rukovodstvo', '{\"ru\":\"Руководство\",\"uz-Cyr\":\"Раҳбарият\",\"en\":\"Management\",\"uz-Lat\":\"Rahbariyat\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'heads', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:30:12', '2020-05-13 01:58:06'),
(66, 54, 0, NULL, 'podrazdelenija', '{\"ru\":\"Подразделения\",\"uz-Cyr\":\"Тармоқлар\",\"en\":\"Branches\",\"uz-Lat\":\"Tarmoqlar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:30:24', '2020-05-13 01:58:06'),
(67, 54, 0, NULL, 'nauchnyj-sovet', '{\"ru\":\"Научный совет\",\"uz-Cyr\":\"Илмий кенгаш\",\"en\":\"Science Council\",\"uz-Lat\":\"Ilmiy Kengash\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:30:40', '2020-05-13 01:58:06'),
(68, 2, 0, NULL, 'vakansii', '{\"ru\":\"Вакансии\",\"uz-Cyr\":\"Иш ўринлари\",\"en\":\"Jobs\",\"uz-Lat\":\"Ish o\'rinlari\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'vacancies', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:30:52', '2020-05-13 01:58:06'),
(69, 55, 0, NULL, 'novosti', '{\"ru\":\"Новости\",\"uz-Cyr\":\"Янгиликлар\",\"en\":\"News\",\"uz-Lat\":\"Yangiliklar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'news', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:31:29', '2020-05-13 01:58:06'),
(70, 55, 0, NULL, 'galereja', '{\"ru\":\"Галерея\",\"uz-Cyr\":\"Галерея\",\"en\":\"Gallery\",\"uz-Lat\":\"Galereya\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'gallery', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:31:44', '2020-05-13 01:58:06'),
(71, 3, 0, NULL, 'meroprijatija', '{\"ru\":\"Мероприятия\",\"uz-Cyr\":\"Ҳодисалар\",\"en\":\"Events\",\"uz-Lat\":\"Ҳодисалар\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'activities', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:32:00', '2020-05-13 01:58:06'),
(72, 55, 0, NULL, 'vestnik-akademii', '{\"ru\":\"Вестник Академии\",\"uz-Cyr\":\"Академия Ахборотномаси\",\"en\":\"Academy Herald\",\"uz-Lat\":\"Akademiya Axborotnomasi\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'tests-archive', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:32:10', '2020-05-13 01:58:06'),
(73, 56, 0, NULL, 'perepodgotovka', '{\"ru\":\"Переподготовка\",\"uz-Cyr\":\"Қайта тайёрлаш\",\"en\":\"Retraining\",\"uz-Lat\":\"Qayta tayyorlash\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:32:30', '2020-05-13 01:58:06'),
(74, 56, 0, NULL, 'povyshenie-kvalifikatsii', '{\"ru\":\"Повышение квалификации\",\"uz-Cyr\":\"Малака ошириш\",\"en\":\"Training\",\"uz-Lat\":\"Malaka oshirish\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:32:42', '2020-05-13 01:58:06'),
(75, 56, 0, NULL, 'magistratura', '{\"ru\":\"Магистратура\",\"uz-Cyr\":\"Магистратура\",\"en\":\"Master degree\",\"uz-Lat\":\"Magistratura\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'masters-degree', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:32:58', '2020-05-13 01:58:06'),
(76, 5, 0, NULL, 'raspisanie', '{\"ru\":\"Расписание\",\"uz-Cyr\":\"Жадвал\",\"en\":\"Timetable\",\"uz-Lat\":\"Jadval\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:33:14', '2020-05-13 01:58:06'),
(77, 5, 0, NULL, 'objavlenie', '{\"ru\":\"Объявление\",\"uz-Cyr\":\"Эълон\",\"en\":\"Advertisement\",\"uz-Lat\":\"Э’лон\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:33:37', '2020-05-13 01:58:06'),
(84, 4, 0, NULL, 'metodicheskie-materialy', '{\"ru\":\"Методические материалы\",\"uz-Cyr\":\"Ўқув материаллари\",\"en\":\"Teaching materials\",\"uz-Lat\":\"O\'quv materillari\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:33:53', '2020-05-13 02:25:08'),
(85, 59, 0, NULL, 'mezhdunarodnoe-sotrudnichestvo', '{\"ru\":\"Международное сотрудничество\",\"uz-Cyr\":\"Халқаро ҳамкорлик\",\"en\":\"The international cooperation\",\"uz-Lat\":\"Xalqaro hamkorlik\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:34:21', '2020-05-13 02:26:09'),
(86, 59, 0, NULL, 'mezhvedomstvennoe-sotrudnichestvo', '{\"ru\":\"Межведомственное сотрудничество\",\"uz-Cyr\":\"Идоралараро ҳамкорлик\",\"en\":\"Interagency cooperation\",\"uz-Lat\":\"Idoralararo hamkorlik\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:34:39', '2020-05-13 02:27:09'),
(87, 61, 0, NULL, 'interaktivnye-uslugi', '{\"ru\":\"Интерактивные услуги\",\"uz-Cyr\":\"Интерфаол хизматлар\",\"en\":\"Interactive Services\",\"uz-Lat\":\"Interfaol xizmatlar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'interactive', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:35:01', '2020-05-13 02:28:26'),
(88, 61, 0, NULL, 'povyshenie-pravovoj-gramotnosti', '{\"ru\":\"Повышение правовой грамотности\",\"uz-Cyr\":\"Ўқитиш ва тадқиқотлар\",\"uz-Lat\":\"Ўқитиш ва тадқиқотлар\",\"en\":\"Training and research\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', 'active', '', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-06 12:35:23', '2020-05-13 02:29:28'),
(89, 6, 0, NULL, 'o-tsentre', '{\"ru\":\"О центре\",\"uz-Cyr\":\"Марказ ҳақида\",\"en\":\"\",\"uz-Lat\":\"Markaz haqida\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-10 13:28:00', '2020-05-13 02:31:11'),
(90, 61, 0, NULL, '-elektronnaja-biblioteka', '{\"ru\":\" Электронная библиотека\",\"uz-Cyr\":\"Электрон кутубхона\",\"en\":\"E-library\",\"uz-Lat\":\"Elektron kutubxona\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'e-library', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-20 14:32:13', '2020-05-13 02:32:07'),
(91, 58, 0, NULL, 'prioritetnye-zadachi', '{\"ru\":\"Приоритетные задачи\",\"uz-Cyr\":\"Устувор вазифалар\",\"uz-Lat\":\"Ustuvor vazifalar\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', 'active', '', 'higher-tasks', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-23 09:50:00', '2020-05-13 02:33:10'),
(92, 55, 0, NULL, 'onlajn-priem-statej-v-vestnik-akademii', '{\"ru\":\"Онлайн приём статей в Вестник Академии\",\"uz-Cyr\":\"Ахборотномага мақолаларни онлайн қабул қилиш\",\"en\":\"\",\"uz-Lat\":\"Axborotnomaga maqolalarni onlayn qabul qilish\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'states-receiver', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-25 01:47:48', '2020-05-13 02:34:40'),
(93, 9, 0, NULL, 'povyshenie-pravovoj-gramotnosti', '{\"ru\":\"Повышение правовой грамотности\",\"uz-Cyr\":\"Ҳуқуқий саводхонликни ошириш\",\"en\":\"\",\"uz-Lat\":\"Huquqiy savodxonlikni oshirish\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', 'improve-legal-knowledge', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-09-26 10:58:18', '2020-05-13 02:35:41'),
(95, 58, 0, NULL, 'trebovanija-k-priemu', '{\"ru\":\"Требования к приёму\",\"uz-Cyr\":\"Қабул талаблари\",\"uz-Lat\":\"Qabul talablari\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', 'active', '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 18:20:00', '2020-05-13 02:37:52'),
(96, 58, 0, NULL, 'perspektivnye-temy', '{\"ru\":\"Перспективные темы\",\"uz-Cyr\":\"Истиқболли мавзулар\",\"en\":\"\",\"uz-Lat\":\"Istiqbolli mavzular\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 18:23:00', '2020-05-13 02:38:41'),
(97, 58, 0, NULL, 'nauchnyj-sovets', '{\"ru\":\"Научный совет\",\"uz-Cyr\":\" Илмий кенгаш\",\"en\":\"\",\"uz-Lat\":\"Ilmiy kengash\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 18:46:00', '2020-05-13 02:39:24'),
(98, 58, 0, NULL, 'nauchnyj-seminar', '{\"ru\":\"Научный семинар\",\"uz-Cyr\":\"Илмий семинар\",\"en\":\"\",\"uz-Lat\":\"Ilmiy seminar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 18:50:00', '2020-05-13 02:40:11'),
(99, 58, 0, NULL, 'soiskateli', '{\"ru\":\"Соискатели\",\"uz-Cyr\":\"Изланувчилар\",\"en\":\"\",\"uz-Lat\":\"Izlanuvchilar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 18:53:00', '2020-05-13 02:40:59'),
(100, 58, 0, NULL, 'poleznaja-informatsija', '{\"ru\":\"Полезная информация\",\"uz-Cyr\":\"Фойдали маълумотлар\",\"en\":\"\",\"uz-Lat\":\"Foydali ma`lumotlar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 18:56:00', '2020-05-13 02:41:50'),
(101, 58, 0, NULL, 'nauchnaja-tribuna', '{\"ru\":\"Научная трибуна\",\"uz-Cyr\":\"Илмий минбар\",\"uz-Lat\":\"Ilmiy minbar\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', 'active', '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 18:59:00', '2020-05-13 02:42:47'),
(102, 58, 0, NULL, 'faq-na-50-voprosov-50-otvetov', '{\"ru\":\"FAQ – На 50 Вопросов 50 ответов\",\"uz-Cyr\":\"FAQ – 50 саволга 50 жавоб\",\"en\":\"\",\"uz-Lat\":\"FAQ – 50 savolga 50 javob\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 18:59:00', '2020-05-13 02:44:00'),
(103, 58, 0, NULL, 'objavlenija', '{\"ru\":\"Объявления\",\"uz-Cyr\":\" Эълонлар\",\"en\":\"\",\"uz-Lat\":\"E\'lonlar\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 19:01:00', '2020-05-13 02:44:52'),
(104, 58, 0, NULL, 'priznanija', '{\"ru\":\"Признания\",\"uz-Cyr\":\"Эътироф\",\"en\":\"\",\"uz-Lat\":\"E\'tirof\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '', '0', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '2019-10-11 19:03:00', '2020-05-13 02:45:34');

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content_ru` text DEFAULT NULL,
  `content_uz` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `title`, `content_ru`, `content_uz`) VALUES
(1, 'Akademiya', '<p>Kelishingiz kerak</p>', '<p>Kelishingiz kerak</p>'),
(2, 'Hurmatli', '<p>Proacademy uz qabul</p>', '<p>Proacademy uz qabul</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `messages_order`
--

CREATE TABLE `messages_order` (
  `id` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `body` text DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages_order`
--

INSERT INTO `messages_order` (`id`, `from`, `to`, `message_id`, `body`, `status`) VALUES
(3, 81, 83, 1, NULL, 'active'),
(4, 81, 83, 1, NULL, 'inactive'),
(5, 81, 83, 1, NULL, 'inactive'),
(6, 81, 83, 1, NULL, 'inactive'),
(7, 81, 83, 1, NULL, 'inactive'),
(8, 81, 85, 1, NULL, 'active'),
(9, 81, 83, 1, NULL, 'inactive'),
(10, 81, 85, 1, NULL, 'active'),
(11, 81, 83, 1, NULL, 'inactive'),
(12, 81, 85, 1, NULL, 'active'),
(13, 81, 83, 1, NULL, 'inactive'),
(14, 81, 85, 1, NULL, 'active'),
(15, 81, 83, 1, NULL, 'inactive'),
(16, 81, 85, 1, NULL, 'inactive'),
(17, 81, 83, 1, NULL, 'inactive'),
(18, 81, 85, 1, NULL, 'inactive'),
(19, 81, 83, 1, NULL, 'inactive'),
(20, 81, 85, 1, NULL, 'inactive'),
(21, 81, 83, 1, NULL, 'inactive'),
(22, 81, 85, 1, NULL, 'inactive'),
(23, 81, 83, 2, NULL, 'inactive'),
(24, 81, 85, 2, NULL, 'inactive'),
(25, 81, 86, 2, NULL, 'active'),
(26, 81, 83, 1, NULL, 'inactive'),
(27, 81, 85, 1, NULL, 'inactive'),
(28, 81, 86, 1, NULL, 'inactive'),
(29, 81, 90, 1, NULL, 'inactive'),
(30, 81, 83, 0, '<p>Sizda kamda</p>', 'inactive'),
(31, 81, 83, 0, '<p>Siz yomonsiz</p>', 'inactive'),
(32, 81, 83, 0, '<p>Yomon</p>', 'inactive'),
(33, 81, 83, 0, '<p>Dabdala</p>', 'inactive'),
(34, 81, 85, 0, '<p>Yaxwimas</p>', 'inactive'),
(35, 81, 85, 0, '<p>Yaxwimas</p>', 'inactive'),
(36, 81, 85, 0, '<p>Yaxwimas</p>', 'inactive'),
(37, 81, 85, 0, '<p>Yaxwimas</p>', 'inactive'),
(38, 81, 85, 0, '<p>Yaxwimas</p>', 'inactive'),
(39, 81, 86, 0, '<p>Yomon</p>', 'inactive'),
(40, 81, 83, 0, '<p>dsdsadasdas</p>', 'inactive'),
(41, 81, 83, 0, '<p>Yomon</p>', 'inactive'),
(42, 81, 83, 0, '<p>Yomon</p>', 'inactive'),
(43, 81, 83, 0, '<p>Yomon</p>', 'inactive'),
(44, 81, 83, 0, '<p>Yomon</p>', 'inactive'),
(45, 81, 83, 0, '<p>Yomon</p>', ''),
(46, 81, 83, 0, '<p>Yomon&nbsp;</p>', 'active'),
(47, 81, 83, 0, '<p>Yomon&nbsp;</p>', ''),
(48, 81, 83, 0, '<p>Yomon&nbsp;</p>', 'inactive'),
(49, 81, 83, 0, '<p>Yomon&nbsp;</p>', ''),
(50, 81, 83, 0, '<p>Yomon&nbsp;</p>', ''),
(51, 81, 83, 0, '<p>Xato</p>', ''),
(52, 81, 83, 0, '<p>dasdasdas</p>', ''),
(53, 81, 83, 0, '<p>Yomon</p>', 'inactive'),
(54, 81, 85, 0, '<p>Nono</p>', 'inactive'),
(55, 81, 85, 0, '<p>adasdasdas</p>', 'inactive'),
(56, 81, 83, 0, '<p>Yomon</p>\n<p>&nbsp;</p>', 'inactive'),
(57, 81, 83, 0, '<p>Yomon</p>', 'inactive'),
(58, 81, 83, 0, '<p>dasdsdsd</p>', 'inactive'),
(59, 81, 83, 0, '<p>dasdsdsd</p>', 'inactive'),
(60, 81, 83, 0, '<p>sdcasdcsd</p>', 'inactive'),
(61, 81, 97, 0, '<p>Nimadir kam</p>', 'inactive');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1590008184),
('m200507_231625_init_post', 1591315692),
('m200512_163436_create_menu_table', 1591315692),
('m200513_010308_create_media_order_table', 1591315692),
('m200521_013952_create_users_table', 1591315692),
('m200525_232222_create_admission_year_table', 1591315692),
('m200526_001246_create_admission_faculties_table', 1591315692),
('m200526_004231_create_applicants_table', 1591315692),
('m200526_005731_create_districts_table', 1591315692),
('m200526_010357_create_cities_table', 1591315692),
('m200605_000703_create_files_table', 1591315754),
('m200607_221432_create_messages_table', 1591568533),
('m200607_224338_create_messages_order_table', 1591569928);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `category_id2` int(11) DEFAULT NULL,
  `group` varchar(30) DEFAULT NULL,
  `alias` text NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `content_html` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `short_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT NULL,
  `option` enum('yes','no') DEFAULT NULL,
  `options` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `tags` text DEFAULT NULL,
  `meta_title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `meta_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `meta_keywords` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `category_id2`, `group`, `alias`, `title`, `content`, `content_html`, `short_content`, `status`, `option`, `options`, `icon`, `link`, `views`, `tags`, `meta_title`, `meta_description`, `meta_keywords`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'interactives_category', 'akademijada-ish-uchun-uzhzhat-topshirish', '{\"ru\":\"Академияда ўқиш учун ҳужжат топшириш\",\"uz-Cyr\":\"Академияда ўқиш учун ҳужжат топшириш\",\"uz-Lat\":\"Akademiyada o\'qish uchun hujjat topshirish\",\"en\":\"Академияда ўқиш учун ҳужжат топшириш\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'active', NULL, NULL, 'flaticon-collaboration', 'applicant', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, NULL, NULL, 'interactives_category', 'attestatni-tekshirish', '{\"ru\":\"Аттестатни текшириш\",\"uz-Cyr\":\"Аттестатни текшириш\",\"uz-Lat\":\"Attestatni tekshirish\",\"en\":\"Аттестатни текшириш\"}', '{\"ru\":\"<p>Attestatni tekshirish uchun Akademiya telefon raqamiga telefon qiling<\\/p>\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'active', NULL, NULL, 'flaticon-loupe', 'site/development', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, NULL, NULL, 'interactives_category', 'masofavij-talim', '{\"ru\":\"Масофавий таълим\",\"uz-Cyr\":\"Масофавий таълим\",\"uz-Lat\":\"Masofaviy ta\'lim\",\"en\":\"Масофавий таълим\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'active', NULL, NULL, 'flaticon-seminar', 'site/development', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, NULL, NULL, 'interactives_category', 'ovoz-berish', '{\"ru\":\"Овоз бериш\",\"uz-Cyr\":\"Овоз бериш\",\"uz-Lat\":\"Ovoz berish\",\"en\":\"Овоз бериш\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'active', NULL, NULL, 'flaticon-team', 'site/development', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, NULL, NULL, 'interactives_category', 'statistika', '{\"ru\":\"Статистика\",\"uz-Cyr\":\"Статистика\",\"uz-Lat\":\"Статистика\",\"en\":\"Статистика\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'inactive', NULL, NULL, 'flaticon-diagram', '', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, NULL, NULL, '_category', '', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'inactive', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, NULL, NULL, 'news_category', 'uzbekistan', '{\"ru\":\"Узбекистан\",\"uz-Cyr\":\"Ўзбекистон\",\"uz-Lat\":\"O\'zbekiston\",\"en\":\"Uzbekistan\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, NULL, NULL, 'interactives_category', 'zingizni-sinab-kring', '{\"ru\":\"Ўзингизни синаб кўринг\",\"uz-Cyr\":\"Ўзингизни синаб кўринг\",\"uz-Lat\":\"O\'zingizni sinab ko\'ring\",\"en\":\"Ўзингизни синаб кўринг\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'active', NULL, NULL, 'flaticon-sports-and-competition', 'site/development', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, NULL, NULL, 'interactives_category', '', '{\"ru\":\"Электрон кутубхона\",\"uz-Cyr\":\"Электрон кутубхона\",\"uz-Lat\":\"Elektron kutubxona\",\"en\":\"Электрон кутубхона\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'active', NULL, NULL, 'flaticon-money-2', 'site/development', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, NULL, NULL, 'interactives_category', 'faq', '{\"ru\":\"FAQ\",\"uz-Cyr\":\"FAQ\",\"uz-Lat\":\"FAQ\",\"en\":\"FAQ\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'active', NULL, NULL, 'flaticon-next-1', 'site/development', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, NULL, NULL, 'interactives_category', '', '{\"ru\":\"Юридик клиника\",\"uz-Cyr\":\"Юридик клиника\",\"uz-Lat\":\"Yuridik klinika\",\"en\":\"Юридик клиника\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', 'active', NULL, NULL, 'flaticon-network', 'site/development', NULL, NULL, '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', '{\"ru\":\"\",\"en\":\"\",\"uz-Lat\":\"\",\"uz-Cyr\":\"\"}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, NULL, NULL, 'settings', 'example-biography', '{\"ru\":\"Example Biography\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 9, NULL, 'news', '', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'inactive', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, NULL, NULL, 'news_category', 'bad', '{\"ru\":\"Bad\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, NULL, NULL, 'news_category', 'good', '{\"ru\":\"Good\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, NULL, NULL, 'settings', 'example-relatives', '{\"ru\":\"Example Relatives\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, NULL, NULL, 'settings', 'example-career', '{\"ru\":\"Example Career\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, NULL, NULL, 'settings', 'telegram-kanal', '{\"ru\":\"Телеграм канал\",\"uz-Cyr\":\"\",\"uz-Lat\":\"Telegram kanal\",\"en\":\"\"}', '{\"ru\":\"https:\\/\\/t.me\\/ProAcademyUzb\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, NULL, NULL, 'settings', 'glavnyj-telegram-kanal', '{\"ru\":\"Главный телеграм канал\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"https:\\/\\/t.me\\/ProsecutorAcademy\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, NULL, NULL, 'settings', 'instagram', '{\"ru\":\"Instagram\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"https:\\/\\/www.instagram.com\\/ProsecutorAcademy\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, NULL, NULL, 'settings', 'twitter', '{\"ru\":\"Twitter\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"https:\\/\\/twitter.com\\/ProAcademyUz\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, NULL, NULL, 'settings', 'facebook', '{\"ru\":\"Facebook\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":\"https:\\/\\/www.facebook.com\\/ProsecutorAcademy\",\"uz-Cyr\":\"\",\"uz-Lat\":\"\",\"en\":\"\"}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', 'active', NULL, NULL, NULL, NULL, NULL, NULL, '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', '{\"ru\":null, \"en\":null, \"uz-Lat\":null, \"uz-Cyr\":null}', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('KEuOabvJAxP20uZYvb0mUxm0zpOzdzNYPm7yEw3w', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36', 'YTo3OntzOjY6Il90b2tlbiI7czo0MDoiNFdZSmVFS21hdklCRmRLSnBOVW5SMHQzcFBaRFpFZ3BPQUp3aWlVZiI7czo2OiJsb2NhbGUiO3M6MjoicnUiO3M6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI0OiJodHRwOi8vbG9jYWxob3N0OjgwODEvcnUiO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjM6InVybCI7YTowOnt9czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO2k6MTtzOjEyOiJib25lX2NhcHRjaGEiO3M6NToiNmUzODciO30=', 1590216743),
('V3UC551mK5POQkN2C5akMed6JVMVIa5JU0Eq8ivN', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoibXdOVFVpZ1RIeFJTOFhhOFRLSFU1ZmNuTE1IN0N4aVo1SFRhV0lFUCI7czozOiJ1cmwiO2E6MDp7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI3OiJodHRwOi8vbG9jYWxob3N0OjgwMDAvYWRtaW4iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO30=', 1590457062);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `activation_key` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `last_name`, `middle_name`, `auth_key`, `activation_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(81, 'Admin', 'Adminov', 'Adminovich', 'tqc4bmB35WeQKvOSsQtalXlP6MOZpkvP', '', '$2y$13$AxnjtH7Tipy4eKukx2j1Buu/OKdwVffnceLY9HUHumackWwKD8EMC', NULL, 'admin@webper.uz', 10, 1591562057, 1591562057),
(83, 'Jamshid', 'Husniddinov', 'Karimovich', 'rz6Hq47nbr3j5rgHuFGMw2hFie2g-x5y', 'a755ccd015ac2af6b5798eee257096ef56e08688', '$2y$13$WYVqiKU4plVqctDOabpf5ei2mHAWAnigNrDB35eZFmtGxYHqX0UHq', NULL, 'a@a.com', 10, 1591562685, 1591562713),
(85, 'Xurshid', 'Rasulov ', 'Daminovich', 'I4xwyH-fRBwozgpFc_r676czfJOVZZfJ', '48e1352d177e8bc353648598ca013068e00e31d6', '$2y$13$id1wOEGkVUWHd.6FjV/ATu7cFlMkvQ7eBLIvo8MzA0v5uGIXLFCr2', NULL, 'w@w.com', 10, 1591564451, 1591564507),
(86, 'Jamshid', 'Jumanov', 'Otajonovich', '2378wY2oheWB-LPICpClaRkF9qBNxE0i', 'c822b9f28590829ddc073848268784cb0f97ec76', '$2y$13$Wswb3VYB04UHzHYp0E2TOuQaTIVS7CtnOFkj3R0RKA64B6ULTTmWK', NULL, 'r@r.com', 10, 1591598652, 1591598684),
(90, 'Jamshid', 'O\'tanov', 'Kosimov', 'Hw4XEX2ZFiRAFc5npzgbla7dJvC076B5', '4d8508710df1dd211c1eefb72cb70803b678f22f', '$2y$13$0hkuQ4x89VnTXARRxd/Jze//q4zohyzegsJAZ17wFjInbxGkQgr7K', NULL, 'i@i.com', 10, 1591610115, 1591610218),
(99, 'Bahodir ', 'Karimov', 'Dilshodovich', 'JS2VFr3u5dHj-BAaosVMK8NW7smM3SX_', '2174be9ddfeb55958d41da1dba0bbcb3fff0f624', '$2y$13$6/BwN8w1WdWe76PlcrHvqO6cS6.0Cwvidh0vFMq1vOLNxMSNdu0ei', NULL, 't@t.com', 5, 1591963831, 1591963831);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admission_faculties`
--
ALTER TABLE `admission_faculties`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admission_year`
--
ALTER TABLE `admission_year`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-district-city_id` (`city_id`);

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `media_order`
--
ALTER TABLE `media_order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messages_order`
--
ALTER TABLE `messages_order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admission_faculties`
--
ALTER TABLE `admission_faculties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `admission_year`
--
ALTER TABLE `admission_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT для таблицы `media_order`
--
ALTER TABLE `media_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `messages_order`
--
ALTER TABLE `messages_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `fk-district-city_id` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
