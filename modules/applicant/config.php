<?php
return [
    'id' => 'applicant',
    'modules' => [

    ],
    'components' => [
        'user' => [
            'class' => 'app\models\User',
            'enableAutoLogin' => true
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
    ]
];
