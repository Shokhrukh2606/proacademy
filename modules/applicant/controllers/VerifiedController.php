<?php

namespace app\modules\applicant\controllers;

use app\models\Applicants;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\AdmissionYear;

class VerifiedController extends \yii\web\Controller
{  public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup', 'error'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'step' => ['POST']
                ],
            ],
        ];
    }
    public $layout = 'info/main';
    /**
     * Set Applicant status to finished
     * @return string
     */
    public function actionSendForm()
    {
        $user=Yii::$app->user->identity;
        $applicant=Applicants::findOne(['user_id'=>$user->id]);
        if($applicant->status=='unknown'){
            $applicant->status='finished';
            $this->sendEmail($user->email, $user, $applicant->unique_code);
        }
        if($applicant->status=='aborted'){
            $applicant->status='updated';
        }
        if($applicant->save()){
            return $this->redirect(['index']);
        }
    }
    public function sendEmail($email, $user, $app_code)
    {
        Yii::$app
            ->mailer
            ->compose(
                ['html' => 'pending-html'],
                ['user' => $user, 'app_code'=>$app_code]
            )
            ->setFrom([Yii::$app->params['supportEmail']])
            ->setTo($email)
            ->setSubject('Ma\'lumotlar yuborildi')
            ->send();
    }
    /**
     * Renders the info of applicant
     * @return string
     */
    public function actionIndex()
    {
        $user=Yii::$app->user->identity;
        $academic_year=AdmissionYear::findOne(['status'=>1]);
        $applicant=Applicants::findOne(['user_id'=>$user->id, 'academic_year_id'=>$academic_year->id]);
        return $this->render('index', ['user'=>$user, 'applicant'=>$applicant]);
    }
    public function actionEdit()
    {
        $user=Yii::$app->user->identity;
        $academic_year=AdmissionYear::findOne(['status'=>1]);
        $applicant=Applicants::findOne(['user_id'=>$user->id, 'academic_year_id'=>$academic_year->id]);
        if ($applicant->load(Yii::$app->request->post()) && $applicant->updateAborted()) {
            return $this->redirect(['verified/index']);
        }
        return $this->render('edit', ['user'=>$user, 'model'=>$applicant]);
    }
    public function actionDeleteFile($app_id, $type)
    {
        $user=Yii::$app->user->identity;
        $academic_year=AdmissionYear::findOne(['status'=>1]);
        $applicant=Applicants::findOne(['user_id'=>$user->id, 'academic_year_id'=>$academic_year->id]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($type=='certificate'&&$applicant){
            Yii::$app->db->createCommand()->update('applicants', ['certificate' => ''], 'id='.$applicant->id)->execute();
            return ['success'=>true];
        }else{
            return ['success'=>false];
        }
    }
}
