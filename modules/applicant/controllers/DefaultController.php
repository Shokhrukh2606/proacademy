<?php

namespace app\modules\applicant\controllers;

use app\models\Applicants;
use yii\web\Controller;
use Yii;
use app\models\StepOneForm;
use app\models\StepThreeForm;
use app\models\StepTwoForm;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Default controller for the `applicant` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup', 'error'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'step', 'step-one-validate', 'step-one-submission','step-two-validate', 'step-two-submission','step-three-validate', 'step-three-submission'],
                        'roles' => ['uploadInformation'],
                    ],
                ],
            ],
            // 'verbs' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //         // 'step' => ['POST']
            //     ],
            // ],
        ];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $current_step=Applicants::getApplicantStep();
        if($current_step==4){
            $this->redirect(['verified/index']);
        }
        return $this->render('index',['current_step'=>$current_step]);

    }
    public function actionStep($step_id)
    {
        if(Applicants::getApplicantStep()==$step_id){
            $getView=$step_id;
            switch($getView){
                case '1':
                    $user = Yii::$app->user->identity;
                    $model = new StepOneForm();
                    return $this->render('stepOne', ['model' => $model, 'user' => $user]);
                break;
                case '2':
                    $user = Yii::$app->user->identity;
                    $model = new StepTwoForm();
                    return $this->render('stepTwo',['model' => $model, 'user' => $user]);
                break;
                case '3':
                    $user = Yii::$app->user->identity;
                    $model = new StepThreeForm();
                    return $this->render('stepThree',['model' => $model, 'user' => $user]);
                break;
                default:
                    $this->redirect('applicant/verified/index');
            }                        
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    public function actionStepOneValidate()
    {
        $model = new StepOneForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
    public function actionStepOneSubmission()
    {
        $cameData = Yii::$app->request->post();
        $model = new StepOneForm();
        if ($model->load($cameData) && $model->saver()) {
            return $this->asJson(['success' => true]);
        } else {
            return $this->asJson(['success' => false, ['errors' => $model->getErrors()]]);
        }
    }
    public function actionStepTwoValidate()
    {
        $model = new StepTwoForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
    public function actionStepTwoSubmission()
    {
        $cameData = Yii::$app->request->post();
        $model = new StepTwoForm();
        if ($model->load($cameData) && $model->save()) {
            return $this->asJson(['success' => true]);
        } else {
            return $this->asJson(['success' => false, ['errors' => $model->getErrors()]]);
        }
    }
    public function actionStepThreeValidate()
    {
        $model = new StepThreeForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
    public function actionStepThreeSubmission()
    {
        $cameData = Yii::$app->request->post();
        $model = new StepThreeForm();
        if ($model->load($cameData) && $model->save()) {
            return $this->asJson(['success' => true]);
        } else {
            return $this->asJson(['success' => false, ['errors' => $model->getErrors()]]);
        }
    }
}
