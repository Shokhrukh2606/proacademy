<?php

namespace app\modules\applicant\controllers;

use app\models\Messages;
use app\models\MessagesOrder;
use Yii;
class MessagesController extends \yii\web\Controller
{
    public $layout = 'info/main';

    public function actionIndex()
    {   
        $messages=MessagesOrder::find()->where(['to'=>Yii::$app->user->identity->id])->orderBy('id DESC')->all();
        $newMessages=MessagesOrder::find()->where(['to'=> Yii::$app->user->identity->id, 'status'=>'inactive'])->all();
        return $this->render('index', ['newMessages'=>$newMessages, 'messages'=>$messages]);
    }
    public function actionView($id)
    {   
        $messageOrder=MessagesOrder::findOne(['id'=>$id]);
        if($messageOrder->body){
            $messageOrder->status='active';
            $messageOrder->save();
            return $this->render('view', ['messageOrder'=>$messageOrder->body]);            
        }
        $message=Messages::findOne(['id'=>$messageOrder->message_id]);
        $messageOrder->status='active';
        $messageOrder->save();
        return $this->render('view', ['messageOrder'=>$messageOrder, 'message'=>$message]);
    }

}
