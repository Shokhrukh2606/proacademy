<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;
?>

<div class="container bg-light">
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <?= Html::img($applicant->avatar, ['class' => 'avatar']) ?>
                    <?= Html::tag('h4', $user->last_name . ' ' . $user->username . ' ' . $user->middle_name, ['class'=>'text-center']) ?>
                </div>
                <div class="col-md-12">
                    <label>
                        Tug'ilgan sana:
                    </label>
                    <?= Html::tag('h4', $applicant->birth_date) ?>
                </div>
                <div class="col-md-12">
                    <label>
                        Tug'ilgan shahar:
                    </label>
                    <?= Html::tag('h4', MyHelpers::getCityDistrict($applicant->district_birth)->city) ?>
                </div>
                <div class="col-md-12">
                    <label>
                        Tug'ilgan tuman:
                    </label>
                    <?= Html::tag('h4', MyHelpers::getCityDistrict($applicant->district_birth)->district) ?>
                </div>
                <div class="col-md-12">
                    <label>Telefon raqam</label>
                    <?= Html::tag('h4',  $applicant->telegram_phone) ?>                
                </div>
                <div class="col-md-12">
                    <label>Telegram raqam</label>
                    <?= Html::tag('h4',  $applicant->telegram_phone) ?>                
                </div>
                <div class="col-md-12">
                    <label>Telegram raqam</label>
                    <?= Html::tag('h4',  MyHelpers::getNationality($applicant->nationality)) ?>                
                </div>
            </div>
        </div>
    </div>
</div>