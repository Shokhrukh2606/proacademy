<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
$lang=Yii::$app->language;
$this->registerJS(
    "
    $('#stepThree').on('beforeSubmit', 
        function() {
            var bioblock=$('.field-stepthreeform-biography')
            var bio=$($('input[name=\"StepThreeForm[biography]\"]')[1])
            var relblock=$('.field-stepthreeform-relatives')
            var rel=$($('input[name=\"StepThreeForm[relatives]\"]')[1])
            var carblock=$('.field-stepthreeform-career')
            var car=$($('input[name=\"StepThreeForm[career]\"]')[1])
            if(bio.val()==''){
                bioblock.find('.help-block').css({'color': 'red'}).text('Nomzod biografiyasi yuklanishi shart')
                bioblock[0].scrollIntoView()
            }else if(rel.val()==''){
                relblock.find('.help-block').css({'color': 'red'}).text('Nomzod yaqinlari haqida ma\'lumot yuklanishi shart')
                relblock[0].scrollIntoView()
            }else if(car.val()==''){
                carblock.find('.help-block').css({'color': 'red'}).text('Nomzod mehnat faoliyati haqida ma\'lumot yuklanishi shart')
                carblock[0].scrollIntoView()
            } else {
                var formData = new FormData(this);
                $.ajax({
                    url: $('#stepThree').attr('action'),
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('.preloader').show();
                    },
                    success: function (res) {
                        $('.preloader').hide();  
                        if(res.success==true){
                            window.location.replace('" . Url::to('applicant', true) . "');
                            return false
                        }else{
                            alert('Xato')
                            return false
                        }
                    },
                    error: function(jqXHR, errMsg) {
                        $('.preloader').hide();
                        alert(errMsg);
                    }
                });
            }
            // prevent default submit
            return false;
        }
    );
    ",
    View::POS_READY
)
?>
<div class="container">
    <div class="mainBody">
        <div class="row examples">
            <div class="jumbotron">
                <h3> <?=Yii::t('front', 'examples')?>:</h3>
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <?= Html::a(Html::img(Url::to('@web/images/icons/link.svg', true)) . Yii::t('front', 'biography_text'), MyHelpers::getFirstMedia(($lang=='ru')?32:20), ['class' => 'btn btn-info', 'target'=>'_blank']) ?>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <?= Html::a(Html::img(Url::to('@web/images/icons/link.svg', true)) . Yii::t('front', 'rel'), MyHelpers::getFirstMedia(($lang=='ru')?33:24), ['class' => 'btn btn-info', 'target'=>'_blank']) ?>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <?= Html::a(Html::img(Url::to('@web/images/icons/link.svg', true)) . Yii::t('front', 'career_text'), MyHelpers::getFirstMedia(($lang=='ru')?34:25), ['class' => 'btn btn-info', 'target'=>'_blank']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'stepThree',
                'class' => 'row',
                'enctype' => 'multipart/form-data'
            ],
            'action' => Url::to(['default/step-three-submission'], true),
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['default/step-three-validate'], true),
        ]) ?>
        <div class="col-xs-12 col-md-4 form-group">
            <?= $form->field($model, 'biography')->widget(\kartik\file\FileInput::className(), ['bsVersion' => '3.4.1',  'options' => ['required' => true], 'pluginOptions' => ['showUpload' => false]]); ?>
        </div>
        <div class="col-xs-12 col-md-4 form-group">
            <?= $form->field($model, 'relatives')->widget(\kartik\file\FileInput::className(), ['bsVersion' => '3.4.1',  'options' => ['required' => true], 'pluginOptions' => ['showUpload' => false]]); ?>
        </div>
        <div class="col-xs-12 col-md-4 form-group">
            <?= $form->field($model, 'career')->widget(\kartik\file\FileInput::className(), ['bsVersion' => '3.4.1',  'options' => ['required' => true], 'pluginOptions' => ['showUpload' => false]]); ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>