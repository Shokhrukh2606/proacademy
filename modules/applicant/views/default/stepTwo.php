<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

$lang=Yii::$app->language;
$this->registerJS(
    "
    $('#stepTwo').on('beforeSubmit', 
        function() {
            var recblock=$('.field-steptwoform-inner_recomendation')
            var decblock=$('.field-steptwoform-decision')
            var rec=$($('input[name=\"StepTwoForm[inner_recomendation]\"]')[1])
            var dec=$($('input[name=\"StepTwoForm[decision]\"]')[1])
            if(rec.val()==''){
                recblock.find('.help-block').css({'color': 'red'}).text('Nomzod tavsiyanomasi yuklanishi shart')
                recblock[0].scrollIntoView()
            }else if(dec.val()==''){
                decblock.find('.help-block').css({'color': 'red'}).text('Nomzod tavsiyanomasi yuklanishi shart')
                decblock[0].scrollIntoView()
            }else{
                var formData = new FormData(this);
                $.ajax({
                    url: $('#stepTwo').attr('action'),
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('.preloader').show();
                    },
                    success: function (res) {
                        $('.preloader').hide();
                        if(res.success==true){
                            window.location.replace('" . Url::to('/applicant', true) . "');
                            return false
                        }else{
                            alert('Xato')
                            return false
                        }
                    },
                    error: function(jqXHR, errMsg) {
                        $('.preloader').hide();    
                        alert(errMsg);
                    }
                });
            }
            // prevent default submit
            return false;
        }
    );
    ",
    View::POS_READY
)
?>
<div class="container">
    <div class="mainBody">
        <div class="jumbotron examples">
            <h3> <?= Yii::t('front', 'examples') ?>:</h3>
            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <?= Html::a(Html::img(Url::to('@web/images/icons/link.svg', true)) . Yii::t('front', 'recomendation_text'), MyHelpers::getFirstMedia(($lang=='ru')?35:31), ['class' => 'btn btn-info', 'target' => '_blank']) ?>
                </div>
            </div>
        </div>
        <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'stepTwo',
                'class' => 'row',
                'enctype' => 'multipart/form-data'
            ],
            'action' => Url::to(['default/step-two-submission'], true),
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['default/step-two-validate'], true),
        ]) ?>
        <div class="col-xs-12 col-md-12">
            <?= $form->field($model, 'position')->textInput(['value' => $model->position]); ?>
        </div>
        <div class="col-xs-12 col-md-6 form-group">
            <?= $form->field($model, 'inner_recomendation')->widget(\kartik\file\FileInput::className(), ['bsVersion' => '3.4.1',  'options' => ['required' => true], 'pluginOptions' => ['showUpload' => false]]); ?>
        </div>
        <div class="col-xs-12 col-md-6 form-group">
            <?= $form->field($model, 'decision')->widget(\kartik\file\FileInput::className(), ['bsVersion' => '3.4.1',  'options' => ['required' => true], 'pluginOptions' => ['showUpload' => false]]); ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>