<?php

use  yii\web\View;
use yii\helpers\Url;

$this->title = \app\models\AdmissionYear::findOne(['status' => '1'])->title;
$this->registerJS(
    "
    $(document).ready(function(){
        $('#smartwizard').on('stepContent', function(e, anchorObject, stepIndex, stepDirection) {
 
            // Read data value from the anchor element
            var repo    = anchorObject.data('repo');
            var ajaxURL = '" . URL::to(['default/step'], true) . "'+'?step_id='+repo;
            // Return a promise object
                return new Promise((resolve, reject) => {      
                    // Ajax call to fetch your content
                    $.ajax({
                        method  : 'GET',
                        url     : ajaxURL,
                        beforeSend: function( xhr ) {
                            // Show the loader
                            // $('#smartwizard').smartWizard('loader', 'show');
                            $('.preloader').css({'display':'block'})
                        }
                    }).done(function( res ) {
                        resolve(res);
                        $('.preloader').css({'display':'none'})
                        // $('#smartwizard').smartWizard('loader', 'hide');
                    }).fail(function(err) {
                        
                        // Reject the Promise with error message to show as content
                        reject( 'Hatolik yuzaga keldi saytga qaytadan kiring' );
        
                        // Hide the loader
                        $('.preloader').hide()
                    });
               
                });
        });

        $('#smartwizard').on('leaveStep', function(e, anchorObject, stepIndex, stepDirection) {
            if(stepIndex==0&&!anchorObject.data('sended')){
                $('#stepOne').trigger('submit')            
                return false;
            }else if(stepIndex==1&&!anchorObject.data('sended')){
                $('#stepTwo').trigger('submit')            
                return false;
            }else if(stepIndex==2&&!anchorObject.data('sended')){
                $('#stepThree').trigger('submit')
                console.log('dsas')            
                return false;
            }
            else{
                return true;
            }
        })
         
        // SmartWizard initialize
        $('#smartwizard').smartWizard({
            selected: '$current_step'-1,
            theme: 'arrows',
            lang: { // Language variables for button
                next: '" . Yii::t('front', 'next_step') . "'
            },  
            autoAdjustHeight: false,
            backButtonSupport: false,
            toolbarSettings: {
                toolbarPosition: 'bottom', // none, top, bottom, both
                toolbarButtonPosition: 'left', // left, right, center
                showNextButton: true, // show/hide a Next button
                showPreviousButton: false, // show/hide a Previous button
            },  
            anchorSettings: {
                anchorClickable: false, // Enable/Disable anchor navigation
                enableAllAnchors: false, // Activates all anchors clickable all times
                markDoneStep: true, // Add done css
                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                removeDoneStepOnNavigateBack: false, // While navigate back done step after active step will be cleared
                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
            },

        });
      });
    ",
    View::POS_HEAD
);
?>
<div class="preloader"></div>
<h1 class="text-center"><?=Yii::t('front', 'priem')?></h1>
<div id="smartwizard">
    <ul class="nav">
        <li>
            <a class="nav-link" href="#step-1" data-repo="1" data-sended="false">
                <span><?= Yii::t('front', 'step_one') ?></span><br>
                <?= Yii::t('front', 'private') ?>
            </a>
        </li>
        <li>
            <a class="nav-link" href="#step-2" data-repo="2" data-sended="false">
                <span><?= Yii::t('front', 'step_two') ?></span><br>
                <?= Yii::t('front', 'files') ?>
            </a>
        </li>
        <li>
            <a class="nav-link" href="#step-3" data-repo="3" data-sended="false">
                <span><?= Yii::t('front', 'step_three') ?></span><br>
                <?= Yii::t('front', 'files') ?>
            </a>
        </li>
        <li>
            <a class="nav-link" href="#step-4" data-repo="4" data-sended="false">
                <span><?= Yii::t('front', 'step_four') ?></span><br>
                <?= Yii::t('front', 'files') ?>
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="step-1" class="tab-pane" role="tabpanel">
            Step content
        </div>
        <div id="step-2" class="tab-pane" role="tabpanel">
            Step content
        </div>
        <div id="step-3" class="tab-pane" role="tabpanel">
            Step content
        </div>
        <div id="step-4" class="tab-pane" role="tabpanel">
            Step content
        </div>
    </div>
</div>