<?php

use app\models\Cities;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\components\helpers\MyHelpers as M;
use app\models\AdmissionFaculties;
use app\models\Districts;
use kartik\date\DatePicker;
use yii\helpers\Url;

$cities = [];
foreach (Cities::find()->all() as $item) {
    $cities[$item->id] = M::t($item->title);
}
$districts = [];
$districtsJS = [];
foreach (Districts::find()->all() as $item) {
    $districts[$item->id] = M::t($item->title);
}
foreach (Districts::find()->all() as $item) {
    array_push($districtsJS, [
        'c_id' => $item->city_id,
        'd_id' => $item->id
    ]);
}
$faculties = [];
foreach (AdmissionFaculties::find()->all() as $item) {
    $faculties[$item->id] = M::t($item->title);
}
$this->registerJS("
    var districts=" . json_encode($districtsJS) . "
    function changeDistricts(){
        $(\"#steponeform-district_birth option\").css({'display':'none'})
        var selectedCity = $(\"#steponeform-city_birth\").children(\"option:selected\").val();
        let listorder=[]
        for (const item of districts) {
            if(item.c_id==selectedCity){
                $('#steponeform-district_birth option[value=\"'+item.d_id+'\"]').css({'display':'block'})
            }
        }
    }
    function changeDistrictsT(){
        $(\"#steponeform-district_temporary option\").css({'display':'none'})
        var selectedCity = $(\"#steponeform-city_temporary\").children(\"option:selected\").val();
        let listorder=[]
        for (const item of districts) {
            if(item.c_id==selectedCity){
                $('#steponeform-district_temporary option[value=\"'+item.d_id+'\"]').css({'display':'block'})
            }
        }
    }
    function changeDistrictsP(){
        $(\"#steponeform-district_permanent option\").css({'display':'none'})
        var selectedCity = $(\"#steponeform-city_permanent\").children(\"option:selected\").val();
        let listorder=[]
        for (const item of districts) {
            if(item.c_id==selectedCity){
                $('#steponeform-district_permanent option[value=\"'+item.d_id+'\"]').css({'display':'block'})
            }
        }
    }
    function selectedOthers(elem){
        if($(elem).val()==5){
            $('.select-nationality-other').removeClass('hidden')
            $('.select-nationality-other input').prop('required',true);
        }
        else{
            $('.select-nationality-other').addClass('hidden')
        }
    }
    function selectedOthersLang(elem){
        if($(elem).val()==0){
            $('.selected-other-can-speak').removeClass('hidden')
        }
        else{
            $('.selected-other-can-speak').addClass('hidden')
        }
    }
", View::POS_END);
$this->registerJsFile('@web/js/admin/jquery.inputmask.js');
$this->registerJS(
    "   
    $('#steponeform-phone, #steponeform-telegram_phone').inputmask({'mask': '99-999-99-99'});
    $('#stepOne').on('beforeSubmit', 
        function() {
            var avatarblock=$('.field-steponeform-avatar')
            var avatar=$($('input[name=\"StepOneForm[avatar]\"]')[1])
            var passportblock=$('.field-steponeform-passport')
            var passport=$($('input[name=\"StepOneForm[passport]\"]')[1])
            var diplomablock=$('.field-steponeform-diploma')
            var diploma=$($('input[name=\"StepOneForm[diploma]\"]')[1])
            if(avatar.val()==''){
                avatarblock.find('.help-block').css({'color': 'red'}).text('Nomzod rasmi yuklanishi shart')
                avatarblock[0].scrollIntoView()
            }else if(passport.val()==''){
                passportblock.find('.help-block').css({'color': 'red'}).text('Nomzod passporti yuklanishi shart')
                passportblock[0].scrollIntoView()
            }else if(diploma.val()==''){
                diplomablock.find('.help-block').css({'color': 'red'}).text('Nomzod diplomi yuklanishi shart')
                diplomablock[0].scrollIntoView()
            } else{
                var formData = new FormData(this);
                $.ajax({
                    url: $('#stepOne').attr('action'),
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $('.preloader').show();
                    },
                    success: function (res) {
                        $('.preloader').hide();
                        if(res.success==true){
                            $('.nav-link.active').data('sended', true)
                            window.location.replace('" . Url::to('applicant', true) . "');
                            return false
                        }else{
                            alert('Xato')
                            return false
                        }
                    },
                    error: function(jqXHR, errMsg) {
                        $('.preloader').hide();
                        alert(errMsg);
                    }
                });
            }
            // prevent default submit
            return false;
        }
    );
    ",
    View::POS_READY
)
?>
<div class="container">
    <div class="mainBody">
        <?php $form = ActiveForm::begin([
            'options' => [
                'id' => 'stepOne',
                'class' => 'row',
                'enctype' => 'multipart/form-data'
            ],
            'action' => Url::to(['default/step-one-submission'], true),
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['default/step-one-validate'], true),
        ]) ?>
        <div class="col-xs-12 col-md-6">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <?= $form->field($model, 'avatar')->widget(\kartik\file\FileInput::className(), ['bsVersion' => '3.4.1',  'options' => ['accept' => 'image/*', 'required' => true], 'pluginOptions' => ['showUpload' => false]]); ?>
                </div>
                <div class="col-xs-12 form-group">
                    <?= $form->field($model, 'last_name')->textInput(['disabled' => true, 'value' => $user->last_name]); ?>
                </div>
                <div class="col-xs-12 form-group">
                    <?= $form->field($model, 'username')->textInput(['disabled' => true, 'value' => $user->username]); ?>
                </div>
                <div class="col-xs-12 form-group">
                    <?= $form->field($model, 'middle_name')->textInput(['disabled' => true, 'value' => $user->middle_name]); ?>
                </div>
                <div class="col-xs-12 form-group">

                    <?= $form->field($model, 'birth_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => $model->getAttributeLabel('birth_date')],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'bsVersion' => '3.4.1',
                    ]);
                    ?>
                </div>
                <div class="col-xs-12 col-md-6 form-group">
                    <?= $form->field($model, 'passport_serial_number')->textInput(['value' => $model->passport_serial_number]); ?>
                </div>
                <div class="col-xs-12 col-md-6 form-group">
                    <?= $form->field($model, 'passport_given_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => $model->getAttributeLabel('birth_date')],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ],
                        'bsVersion' => '3.4.1',
                    ]);
                    ?>
                </div>
                <div class="col-xs-12 form-group">
                    <?= $form->field($model, 'passport_given_from')->textInput(['value' => $model->passport_given_from]); ?>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'city_birth')->dropDownList(
                                $cities,
                                [
                                    'prompt' => Yii::t('front', 'select'),
                                    'onchange' => 'changeDistricts()',
                                ]
                            ) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'district_birth')->dropDownList(
                                $districts,
                                ['prompt' => Yii::t('front', 'select')]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 form-group">
                    <?= $form->field($model, 'nationality')->dropDownList(
                        [
                            '1' => Yii::t('front', 'oz'),
                            '2' => Yii::t('front', 'ru'),
                            '3' => Yii::t('front', 'tj'),
                            '4' => Yii::t('front', 'kz'),
                            '5' => Yii::t('front', 'other')
                        ],
                        [
                            'prompt' => Yii::t('front', 'select'),
                            'onchange' => 'selectedOthers(this)'
                        ]
                    ) ?>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <?= $form->field($model, 'faculty')->dropDownList(
                        $faculties,
                        ['prompt' => Yii::t('front', 'select')]
                    ) ?>
                </div>
                <div class="col-xs-12 form-group hidden select-nationality-other">
                    <?= $form->field($model, 'other_nationality')->textInput();
                    ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <?= $form->field($model, 'city_permanent')->dropDownList(
                                $cities,
                                [
                                    'prompt' => Yii::t('front', 'select'),
                                    'onchange' => 'changeDistrictsP()'
                                ]
                            ) ?>
                        </div>
                        <div class="col-xs-12">
                            <?= $form->field($model, 'district_permanent')->dropDownList(
                                $districts,
                                ['prompt' => Yii::t('front', 'select')]
                            ) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 form-group">
                    <?= $form->field($model, 'address_permanent')->textInput() ?>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'city_temporary')->dropDownList(
                                $cities,
                                [
                                    'prompt' => Yii::t('front', 'select'),
                                    'onchange' => 'changeDistrictsT()'
                                ]
                            ) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <?= $form->field($model, 'district_temporary')->dropDownList(
                                $districts,
                                ['prompt' => Yii::t('front', 'select')]
                            ) ?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 form-group">
                    <?= $form->field($model, 'address_temporary')->textInput() ?>
                </div>
                <div class="col-xs-12 form-group">
                    <?= $form->field($model, 'can_speak')->dropDownList(
                        ['1' => Yii::t('front', 'enlang'), '2' => Yii::t('front', 'gnlang'), '3' => Yii::t('front', 'frlang')],
                        [
                            'prompt' => Yii::t('front', 'select'),
                            'onchange' => 'selectedOthersLang(this)'
                        ]
                    ) ?>
                </div>
                <div class="col-xs-12 form-group">

                    <?= $form->field($model, 'high_school_name')->textInput(); ?>
                </div>

                <div class="col-xs-12 form-group">

                    <?= $form->field($model, 'high_school_specialty')->textInput(); ?>
                </div>
                <div class="col-xs-6 form-group">

                    <?= $form->field($model, 'diplom_number')->textInput(); ?>
                </div>
                <div class="col-xs-6 form-group">

                    <?= $form->field($model, 'high_school_finished')->textInput(['type' => 'number']); ?>
                </div>
                <div class="col-xs-6 form-group">
                    <?= $form->field($model, 'phone')->textInput(['placeholder' => '99-000-00-00']); ?>
                </div>
                <div class="col-xs-6 form-group">
                    <?= $form->field($model, 'telegram_phone')->textInput(['placeholder' => '99-000-00-00']); ?>
                </div>
                <div class="col-xs-12  form-group text-center">
                    <div class="jumbotron">
                        <?= $form->field($model, 'applicant_type')->radioList(['inner' => Yii::t('front', 'inner'), 'outer' => Yii::t('front', 'outer')]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12"></div>
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'passport')->widget(\kartik\file\FileInput::className(), ['bsVersion' => '3.4.1', 'options' => ['required' => true], 'pluginOptions' => ['showUpload' => false]]); ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'diploma')->widget(\kartik\file\FileInput::className(), ['bsVersion' => '3.4.1', 'pluginOptions' => ['showUpload' => false]]); ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'certificate')->widget(\kartik\file\FileInput::className(), ['bsVersion' => '3.4.1', 'options' => ['required' => true], 'pluginOptions' => ['showUpload' => false]]); ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>
</div>