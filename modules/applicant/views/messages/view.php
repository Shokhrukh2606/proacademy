<?php if (isset($message)) { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $message->title ?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-read-message">
                            <?= $message->content_ru ?><br>
                            <?= $message->content_uz ?>
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>

                </div>
                <!-- /. box -->
            </div>
        </div>
    </section>
<?php } else { ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Eslatma</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-read-message">
                            <?=$messageOrder?>
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>

                </div>
                <!-- /. box -->
            </div>
        </div>
    </section>
<?php } ?>