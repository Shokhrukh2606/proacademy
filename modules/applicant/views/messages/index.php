<?php

use app\models\Messages;
use app\models\User;
use yii\helpers\Url;

?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Mailbox
        <small>





            <?= count($newMessages) ?> новые сообщения</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Mailbox</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped">
                            <tbody>
                                <?php foreach ($messages as $item) { ?>
                                    <tr>
                                        <td class="mailbox-star"><a href="#"><i class="fa <?= ($item->status == 'inactive') ? 'fa-star' : 'fa-star-o' ?> text-yellow"></i></a></td>
                                        <td class="mailbox-name"><a href="<?= Url::to(['messages/view', 'id' => $item->id]) ?>"><?= User::findOne(['id' => $item->from])->username ?></a></td>
                                        <?php $one_message=Messages::findOne(['id' => $item->message_id]); 
                                        if ($one_message&&isset($one_message->title)) { ?>
                                            <td class="mailbox-subject"><?= $one_message->title ?>
                                            </td>
                                        <?php } else{?>
                                            <td class="mailbox-subject">Eslatma!
                                            </td>
                                        <?php } ?>
                                        <td class="mailbox-attachment"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- /.table -->
                    </div>
                    <!-- /.mail-box-messages -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->