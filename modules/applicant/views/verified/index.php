<?php

use app\components\helpers\MyHelpers;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;

$script = <<< JS
        var allUrl = window.location.href
        var url = window.location.pathname
        var curLang = url.substring(1, 3)
        var alertmessage={
            ru: 'Подтверждаете ли вы, что вся информация заполнена правильно?',
            uz:'Hamma kiritilgan ma\'lumotlar to\'g\'riligini tasdiqlaysizmi?'
        }
        var mes;
        switch(curLang){
            case 'ru':
            mes=alertmessage['ru']
            break;
            case 'uz':
            mes=alertmessage['uz']
            break;
        }
        $('#finishForm').click(e=>{
            e.preventDefault();
            var link=$('#finishForm').attr('href')
            console.log(link)
            x = confirm(mes);
            if (!x) return false;
            location.replace(link)
        })
JS;
$this->registerJS($script, View::POS_READY);
?>

<div class="row">
    <div class="col-md-6">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Url::to('@web/uploads' . $applicant->avatar) ?>" class="img-circle" alt="User Image" />
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('front', 'actions') ?></h3>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?= Html::tag('p', $user->last_name . ' ' . $user->username . ' ' . $user->middle_name, ['class' => 'text-center']) ?>
                </div>
                <div class="col-sm-12">
                    <a href="#" style="color: #000;">
                        <?php
                        $current = $applicant->status;
                        switch ($current) {
                            case 'unknown':
                                echo Html::tag('span', Yii::t('front', 'unknown'), ['class' => 'badge badge-secondary','style'=>'margin-left:15px']);
                                break;
                            case 'accepted':
                                echo Html::tag('span', Yii::t('front', 'accepted'), ['class' => 'badge badge-success','style'=>'margin-left:15px']);
                                break;
                            case 'aborted':
                                echo Html::tag('span', Yii::t('front', 'aborted'), ['class' => 'badge badge-warning','style'=>'margin-left:15px']);
                                break;
                            case 'cancelled':
                                return Html::tag('span', Yii::t('front', 'cancelled'), ['class' => 'badge badge-danger','style'=>'margin-left:15px']);
                                break;
                            case 'updated':
                                echo Html::tag('span', Yii::t('front', 'status_finished'), ['class' => 'badge badge-primary','style'=>'margin-left:15px']);
                                break;
                            case 'finished':
                                echo Html::tag('span', Yii::t('front', 'status_finished'), ['class' => 'badge badge-dark','style'=>'margin-left:15px']);
                                break;
                        }
                        ?></a>
                </div>
                <div class="col-md-12 p-3 actions-panel">
                    <a href="<?= Url::to(['verified/send-form'], true) ?>" id="finishForm" class="btn btn-success btn-sm ml-5 <?= ($applicant->status == 'finished' || $applicant->status == 'cancelled' || $applicant->status == 'accepted' || $applicant->status == 'updated') ? 'd-none' : '' ?>"><i class="fa fa-tick"></i> <?= Yii::t('front', 'send_form') ?></a>
                </div>
            </div>
        </div>
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('front', 'info') ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
                <div class="box-body">
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('unique_code')) ?>
                        <?= Html::input('text', '', $applicant->unique_code, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('birth_date')) ?>
                        <?= Html::input('text', '', $applicant->birth_date, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('city_birth')) ?>
                        <?= Html::input('text', '', MyHelpers::getCityDistrict($applicant->district_birth)->city, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('district_birth')) ?>
                        <?= Html::input('text', '', MyHelpers::getCityDistrict($applicant->district_birth)->district, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($user->getAttributeLabel('email')) ?>
                        <?= Html::input('text', '', $user->email, ['disabled' => true]) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('nationality')) ?>
                        <?= Html::input('text', '', MyHelpers::getNationality($applicant->nationality), ['disabled' => true, 'class' => 'form-control'])
                            . Html::input('text', '', $applicant->other_nationality, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('phone')) ?>
                        <?= Html::input('text', '', $applicant->phone, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('telegram_phone')) ?>
                        <?= Html::input('text', '', $applicant->telegram_phone, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('faculty')) ?>

                        <?= Html::input('text', '', MyHelpers::getFaculty($applicant->faculty), ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('applicant_type')) ?>
                        <?= Html::input('text', '', MyHelpers::getAppType($applicant->applicant_type), ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('position')) ?>
                        <?= Html::input('text', '', $applicant->position, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('city_permanent')) ?>
                        <?= Html::input('text', '', MyHelpers::getCityDistrict($applicant->district_permanent)->city, ['disabled' => true, 'class' => 'form-control']) ?>
                        <?= Html::input('text', '', MyHelpers::getCityDistrict($applicant->district_permanent)->district, ['disabled' => true, 'class' => 'form-control']) ?>
                        <?= Html::input('text', '', $applicant->address_permanent, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                    <?php if ($applicant->district_temporary) { ?>
                        <div class="form-group">
                            <?= Html::label($applicant->getAttributeLabel('city_temporary')) ?>
                            <?= Html::input('text', '', MyHelpers::getCityDistrict($applicant->district_temporary)->city, ['disabled' => true, 'class' => 'form-control']) ?>
                            <?= Html::input('text', '', MyHelpers::getCityDistrict($applicant->district_temporary)->district, ['disabled' => true, 'class' => 'form-control']) ?>
                            <?= Html::input('text', '', $applicant->address_permanent, ['disabled' => true, 'class' => 'form-control']) ?>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('can_speak')) ?>
                        <?php if ($applicant->can_speak) { ?>
                            <?= Html::input('text', '', MyHelpers::getCanSpeak($applicant->can_speak), ['disabled' => true, 'class' => 'form-control']) ?>
                        <?php } else {
                            echo Html::input('text', '', $applicant->other_can_speak, ['disabled' => true, 'class' => 'form-control']);
                        } ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('high_school_name')) ?>
                        <?= Html::input('text', '', $applicant->high_school_name, ['disabled' => true, 'class' => 'form-control']) ?>
                        <?= Html::label($applicant->getAttributeLabel('high_school_finished')) ?>
                        <?= Html::input('text', '', $applicant->high_school_finished, ['disabled' => true, 'class' => 'form-control']) ?>
                        <?= Html::label($applicant->getAttributeLabel('high_school_specialty')) ?>
                        <?= Html::input('text', '', $applicant->high_school_specialty, ['disabled' => true, 'class' => 'form-control']) ?>
                        <?= Html::label($applicant->getAttributeLabel('diplom_number')) ?>
                        <?= Html::input('text', '', $applicant->diplom_number, ['disabled' => true, 'class' => 'form-control']) ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('front', 'files') ?></h3>
            </div>
            <form role="form">
                <div class="box-body">
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('avatar')) ?>
                        <?= Html::a(Yii::t('front', 'view'), '@web/uploads' . $applicant->avatar, ['class' => 'btn btn-info col-md-12', 'target' => '_blank']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('passport')) ?>
                        <?= Html::a(Yii::t('front', 'view'), '@web/uploads' . $applicant->passport, ['class' => 'btn btn-info col-md-12', 'target' => '_blank']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('diploma')) ?>
                        <?= Html::a(Yii::t('front', 'view'), '@web/uploads' . $applicant->diploma, ['class' => 'btn btn-info col-md-12', 'target' => '_blank']) ?>
                    </div>

                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('certificate')) ?>
                        <?php if ($applicant->certificate) {
                            echo  Html::a(Yii::t('front', 'view'), '@web/uploads' . $applicant->certificate, ['class' => 'btn btn-info col-md-12', 'target' => '_blank']);
                        } ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('inner_recomendation')) ?>
                        <?= Html::a(Yii::t('front', 'view'), '@web/uploads' . $applicant->inner_recomendation, ['class' => 'btn btn-info col-md-12', 'target' => '_blank']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('decision')) ?>
                        <?= Html::a(Yii::t('front', 'view'), '@web/uploads' . $applicant->decision, ['class' => 'btn btn-info col-md-12', 'target' => '_blank']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('biography')) ?>

                        <?= Html::a(Yii::t('front', 'view'), '@web/uploads' . $applicant->biography, ['class' => 'btn btn-info col-md-12', 'target' => '_blank']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('relatives')) ?>

                        <?= Html::a(Yii::t('front', 'view'), '@web/uploads' . $applicant->relatives, ['class' => 'btn btn-info col-md-12', 'target' => '_blank']) ?>
                    </div>
                    <div class="form-group">
                        <?= Html::label($applicant->getAttributeLabel('career')) ?>
                        <?= Html::a(Yii::t('front', 'view'), '@web/uploads' . $applicant->career, ['class' => 'btn btn-info col-md-12', 'target' => '_blank']) ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>