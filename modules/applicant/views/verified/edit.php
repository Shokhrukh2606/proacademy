<?php

use app\models\Cities;
use yii\web\View;
use yii\widgets\ActiveForm;
use app\components\helpers\MyHelpers as M;
use app\components\helpers\MyHelpers;
use app\models\AdmissionFaculties;
use app\models\Districts;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;

$cities = [];
foreach (Cities::find()->all() as $item) {
    $cities[$item->id] = M::t($item->title);
}
$districts = [];
$districtsJS = [];

foreach (Districts::find()->all() as $item) {
    $districts[$item->id] = M::t($item->title);
}
foreach (Districts::find()->all() as $item) {
    array_push($districtsJS, [
        'c_id' => $item->city_id,
        'd_id' => $item->id
    ]);
}
$faculties = [];
foreach (AdmissionFaculties::find()->all() as $item) {
    $faculties[$item->id] = M::t($item->title);
}
$this->registerJS("
var districts=" . json_encode($districtsJS) . "
    function changeDistricts(){
        $(\"#steponeform-district_birth option\").css({'display':'none'})
        var selectedCity = $(\"#steponeform-city_birth\").children(\"option:selected\").val();
        let listorder=[]
        for (const item of districts) {
            if(item.c_id==selectedCity){
                $('#steponeform-district_birth option[value=\"'+item.d_id+'\"]').css({'display':'block'})
            }
        }
    }
    function changeDistrictsT(){
        $(\"#steponeform-district_temporary option\").css({'display':'none'})
        var selectedCity = $(\"#steponeform-city_temporary\").children(\"option:selected\").val();
        let listorder=[]
        for (const item of districts) {
            if(item.c_id==selectedCity){
                $('#steponeform-district_temporary option[value=\"'+item.d_id+'\"]').css({'display':'block'})
            }
        }
    }
    function changeDistrictsP(){
        $(\"#steponeform-district_permanent option\").css({'display':'none'})
        var selectedCity = $(\"#steponeform-city_permanent\").children(\"option:selected\").val();
        let listorder=[]
        for (const item of districts) {
            if(item.c_id==selectedCity){
                $('#steponeform-district_permanent option[value=\"'+item.d_id+'\"]').css({'display':'block'})
            }
        }
    }
", View::POS_END);
?>

<div class="mainBody">
    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'row',
            'enctype' => 'multipart/form-data'
        ],
    ]) ?>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="row">
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'last_name')->textInput(['disabled' => true, 'value' => $user->last_name]); ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'username')->textInput(['disabled' => true, 'value' => $user->username]); ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'middle_name')->textInput(['disabled' => true, 'value' => $user->middle_name]); ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'birth_date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => $model->getAttributeLabel('birth_date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'bsVersion' => '3.4.1',
                ]);
                ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'passport_serial_number')->textInput(['value' => $model->passport_serial_number]); ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'passport_given_from')->textInput(['value' => $model->passport_given_from]); ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'passport_given_date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => $model->getAttributeLabel('birth_date')],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'bsVersion' => '3.4.1',
                ]);
                ?>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                    <?= Html::label($model->getAttributeLabel('city_birth')) ?>
                        <?= Html::dropDownList(
                            'city',
                            MyHelpers::getCityDistrict($model->district_birth, null, true),
                            $cities,
                            [
                                'class' => 'form-control',
                                'onchange' => 'changeDistricts()',
                                'id' => 'steponeform-city_birth',
                            ]
                        ) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'district_birth')->dropDownList(
                            $districts,
                            [
                                'prompt' => Yii::t('front', 'select'),
                                'id' => 'steponeform-district_birth'
                            ]
                        ) ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'nationality')->dropDownList(
                    [
                        '1' => Yii::t('front', 'oz'),
                        '2' => Yii::t('front', 'ru'),
                        '3' => Yii::t('front', 'tj'),
                        '4' => Yii::t('front', 'kz'),
                        '5' => Yii::t('front', 'other')
                    ],
                    ['prompt' => Yii::t('front', 'select')]
                ) ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'other_nationality')->textInput(['class' => 'hidden']); ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'phone')->textInput(['placeholder' => '99-000-00-00']); ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'telegram_phone')->textInput(['placeholder' => '99-000-00-00']); ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'position')->textInput(); ?>
            </div>
            <div class="col-xs-12 form-group text-center">
                <?= $form->field($model, 'applicant_type')->radioList(['inner' => 'Ichki nomzod', 'outer' => 'Tashqi nomzod'], ['value'=>$model->applicant_type]); ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <?= $form->field($model, 'faculty')->dropDownList(
                            $faculties,
                            ['prompt' => Yii::t('front', 'select')]
                        ) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                    <?= Html::label($model->getAttributeLabel('city_permanent')) ?>
                        
                        <?= Html::dropDownList(
                            'city',
                            ($model->district_permanent) ? MyHelpers::getCityDistrict($model->district_permanent, null, true) : '',
                            $cities,
                            [
                                'class' => 'form-control',
                                'onchange' => 'changeDistrictsP()',
                                'id' => 'steponeform-city_permanent'
                            ]
                        ) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'district_permanent')->dropDownList(
                            $districts,
                            [
                                'prompt' => Yii::t('front', 'select'),
                                'id' => 'steponeform-district_permanent'
                            ]
                        ) ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'address_permanent')->textInput() ?>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                    <?= Html::label($model->getAttributeLabel('city_temporary')) ?>
                        
                        <?= Html::dropDownList(
                            'city',
                            ($model->district_temporary) ? MyHelpers::getCityDistrict($model->district_temporary, null, true) : '',
                            $cities,
                            [
                                'class' => 'form-control', 'onchange' => 'changeDistrictsT()',
                                'id' => 'steponeform-city_temporary'
                            ]
                        ) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= $form->field($model, 'district_temporary')->dropDownList(
                            $districts,
                            ['prompt' => Yii::t('front', 'select'), 'id' => 'steponeform-district_temporary']
                        ) ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'address_temporary')->textInput() ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'can_speak')->dropDownList(
                    ['1' => Yii::t('front', 'enlang'), '2' => Yii::t('front', 'gnlang'), '3' => Yii::t('front', 'frlang'), '0' => Yii::t('front', 'other')],
                    ['prompt' => Yii::t('front', 'select')]
                ) ?>
            </div>
            <div class="col-xs-12 form-group">

                <?= $form->field($model, 'other_can_speak')->textInput(['class' => 'hidden']); ?>
            </div>
            <div class="col-xs-12 form-group">

                <?= $form->field($model, 'high_school_name')->textInput(); ?>
            </div>
            <div class="col-xs-12 form-group">
                <?= $form->field($model, 'high_school_finished')->textInput(['type' => 'number']); ?>
            </div>
            <div class="col-xs-12 form-group">

                <?= $form->field($model, 'high_school_specialty')->textInput(); ?>
            </div>
            <div class="col-xs-12 form-group">

                <?= $form->field($model, 'diplom_number')->textInput(); ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12"></div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <?= Html::label($model->getAttributeLabel('avatar')) ?>
        <?php
        echo FileInput::widget([
            'name' => 'avatar',
            'bsVersion' => '3.4.1',
            'options' => [],
            'pluginOptions' => [
                'initialPreview' => $model->avatar?Url::to('@web/uploads'.$model->avatar):'',
                'initialPreviewAsData' => true,
                'overwriteInitial' => true,
                'showUpload' => false
            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <?= Html::label($model->getAttributeLabel('passport')) ?>
        <?php
        echo FileInput::widget([
            'name' => 'passport',
            'bsVersion' => '3.4.1',
            'options' => [],
            'pluginOptions' => [
                'initialPreview' => $model->passport?Url::to('@web/uploads'.$model->passport):'',
                'initialPreviewAsData' => true,
                'overwriteInitial' => true,
                'showUpload' => false
            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <?= Html::label($model->getAttributeLabel('diploma')) ?>
        <?php
        echo FileInput::widget([
            'name' => 'diploma',
            'bsVersion' => '3.4.1',
            'options' => [],
            'pluginOptions' => [
                'initialPreview' => $model->diploma?Url::to('@web/uploads'.$model->diploma):'',
                'initialPreviewAsData' => true,
                'overwriteInitial' => true,
                'showUpload' => false
            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <?= Html::label($model->getAttributeLabel('certificate')) ?>
        <?php
        echo FileInput::widget([
            'name' => 'certificate',
            'bsVersion' => '3.4.1',
            'options' => [],
            'pluginOptions' => [
                'initialPreview' => $model->certificate?Url::to('@web/uploads'.$model->certificate):'',
                'initialPreviewAsData' => true,
                'overwriteInitial' => true,
                'showUpload' => false,
                'deleteUrl' => Url::toRoute(['delete-file', 'app_id' => $model->id, 'type' => 'certificate'])
            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <?= Html::label($model->getAttributeLabel('inner_recomendation')) ?>
        <?php
        echo FileInput::widget([
            'name' => 'inner_recomendation',
            'bsVersion' => '3.4.1',
            'options' => [],
            'pluginOptions' => [
                'initialPreview' => $model->inner_recomendation?Url::to('@web/uploads'.$model->inner_recomendation):'',
                'initialPreviewAsData' => true,
                'initialPreviewFileType'=>'pdf',
                'overwriteInitial' => true,
                'showUpload' => false
            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 ">
        <?= Html::label($model->getAttributeLabel('biography')) ?>

        <?php
        echo FileInput::widget([
            'name' => 'biography',
            'bsVersion' => '3.4.1',
            'options' => [],
            'pluginOptions' => [
                'initialPreview' => $model->biography ?Url::to('@web/uploads'.$model->biography):'',
                'initialPreviewAsData' => true,
                'initialPreviewFileType'=>'pdf',
                'overwriteInitial' => true,
                'showUpload' => false
            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 ">
        <?= Html::label($model->getAttributeLabel('relatives')) ?>

        <?php
        echo FileInput::widget([
            'name' => 'relatives',
            'bsVersion' => '3.4.1',
            'options' => [],
            'pluginOptions' => [
                'initialPreview' => $model->relatives?Url::to('@web/uploads'.$model->relatives):'',
                'initialPreviewAsData' => true,
                'initialPreviewFileType'=>'pdf',
                'overwriteInitial' => true,
                'showUpload' => false
            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <?= Html::label($model->getAttributeLabel('career')) ?>

        <?php
        echo FileInput::widget([
            'name' => 'career',
            'bsVersion' => '3.4.1',
            'options' => [],
            'pluginOptions' => [
                'initialPreview' => $model->career?Url::to('@web/uploads'.$model->career):'',
                'initialPreviewAsData' => true,
                'initialPreviewFileType'=>'pdf',
                'overwriteInitial' => true,
                'showUpload' => false            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <?= Html::label($model->getAttributeLabel('decision')) ?>

        <?php
        echo FileInput::widget([
            'name' => 'decision',
            'bsVersion' => '3.4.1',
            'options' => [],
            'pluginOptions' => [
                'initialPreview' => $model->decision?Url::to('@web/uploads'.$model->decision):'',
                'initialPreviewAsData' => true,
                'initialPreviewFileType'=>'pdf',
                'overwriteInitial' => true,
                'showUpload' => false
            ]
        ]);
        ?>
    </div>
    <div class="col-xs-12"></div>
    <div class="col-12">
        <?= Html::submitButton(Yii::t('front', 'save'), ['class' => 'btn btn-success pull-right']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>