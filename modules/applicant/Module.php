<?php

namespace app\modules\applicant;
use Yii;
/**
 * applicant module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\applicant\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require __DIR__ . '/config.php');
        $this->layoutPath = \Yii::getAlias('@app/themes/applicant/layouts/');
        $this->layout = 'main';
        
    }
}
