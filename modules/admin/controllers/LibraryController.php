<?php

namespace app\modules\admin\controllers;

use ozerich\filestorage\models\File;

use app\models\OrderMedia;
use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\UploadedFile;


/**
 * PostController implements the CRUD actions for Post model.
 */
class LibraryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'signup', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['uploadInformation'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index'],
                        'roles' => ['changePost'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-media' => ['POST'],
                    'image' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionAll($group)
    {
        $searchModel = new  PostSearch(); //::find()->where(['group' => $group])->orderBy(['created_at' => SORT_DESC]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'books');
        return $this->render(
            // $group . '/all',
            '/books/all',
            [
                'searchModel' => $searchModel,
                'pagination' => ['pageSize' => 20],
                'dataProvider' => $dataProvider,

            ]
        );
    }
     public function actionIndex($sub = null)
    {
        $query = Post::find()->where(['group' => 'books', 'category_id' => $sub])->orderBy(['created_at' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 20],
        ]);
        return $this->render('/books/index', [
            'dataProvider' => $dataProvider,
        ]);
        // $query = Post::find()->where(['group' => 'books']);
        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        //     'pagination' => ['pageSize' => 20],
        // ]);
        // return $this->render('index', [
        //     'dataProvider' => $dataProvider,
        // ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->updateCounters(['views' => 1]);
        if ($model) {
            return $this->render($model->group . '/view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($sub = null)
    {
        $model = new Post();
        $model->group = 'books';
        $model->category_id = $sub;
        $model->title = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->content = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->short_content = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->content_html = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->meta_title = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->meta_description = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->meta_keywords = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->status = 'inactive';
        $newId = $model->save();
        return $this->redirect(['update', 'id' => $model->id]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /* app\controllers\UploadController.php */

    public function actionImage($id, $group)
    {
        $cameFile = UploadedFile::getInstanceByName('neededfiles');
        $model = Yii::$app->media->createFileFromUploadedFile($cameFile, 'books');
        $mediaOrder = new OrderMedia();
        $mediaOrder->media_id = (int) $model->id;
        $mediaOrder->model_id = (int) $id;
        $mediaOrder->category = 'books';
        $mediaOrder->url = $model->getPath();
        if ($mediaOrder->save()) {
            if ($model->ext == 'zip' || $model->ext == 'rar') {
                $zip = new \ZipArchive;
                $openedZip = $zip->open($model->getAbsolutePath());
                if ($openedZip === TRUE) {
                    $zip->extractTo('postfiles/books/' . $this->getInnerDirectory($model->hash));
                    $zip->close();
                }
            }else  if ($model->ext == 'pdf'){

            }    
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'success'=>true
        ];
    }
    public function actionDeleteMedia()
    {
        $request = Yii::$app->request;
        $mediaOrder = OrderMedia::findOne(['media_id' => $request->post('key')]);
        $media = File::findOne(['id' => $request->post('key')]);
        Yii::$app->media->deleteFile($media);
        if($media->ext=='zip'){
            $this->deleteDir(Yii::$app->basePath . '/web/postfiles/books/' . $this->getParentDirectory($media->hash));
        }
        $media->delete();
        $mediaOrder->delete();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => 'true'];
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $mediaOrder = OrderMedia::find()->where(['model_id' => $id, 'category'=>'books'])->all();
        $files = [];
        foreach ($mediaOrder as $item) {
            $media = File::findOne(['id' => $item->media_id]);
            array_push($files, $media);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'index',
                'sub' => $model->category_id
            ]);
        }
        return $this->render('/books/update', [
            'model' => $model,
            'files' => $files
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $post = $this->findModel($id);
        $group = $post['group'];
        $post->delete();
        return $this->redirect(['index', 'group' => $group]);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function getInnerDirectory($file_hash)
    {
        return implode(DIRECTORY_SEPARATOR, [
            mb_strtolower(mb_substr($file_hash, 0, 2)),
            mb_strtolower(mb_substr($file_hash, 2, 2))
        ]);
    }
    protected function getParentDirectory($file_hash)
    {
        return implode(DIRECTORY_SEPARATOR, [
            mb_strtolower(mb_substr($file_hash, 0, 2))
        ]);
    }
    public static function deleteDir($dirPath)
    {
        if (!is_dir($dirPath)) {
            throw new \InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
}
