<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\News;
use app\models\NewsCategory;
use app\models\NewsSearch;
use app\models\NewsTags;
use app\models\OrderMedia;
use ozerich\filestorage\models\File;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'signup', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['uploadInformation'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index'],
                        'roles' => ['changePost'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex($group = null)
    {
        if ($group) {
            $searchModel = NewsCategory::findAll(['category_id' => $group]);
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->saveNew()) {
            return $this->redirect(['index']);
        }

        $model->published_date = date('Y-m-d H:m');
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelcats = (new \yii\db\Query())
            ->select(['category_id'])
            ->from('news_category')
            ->where(['news_id' => $id])
            ->all();
        $mediaOrder = OrderMedia::find()->where(['model_id' => $id, 'category' => 'news'])->all();
        $files = [];
        foreach ($mediaOrder as $item) {
            $media = File::findOne(['id' => $item->media_id]);
            array_push($files, $media);
        }
        if ($model->load(Yii::$app->request->post()) && $model->updateNews()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', compact('model', 'modelcats', 'files'));
    }
    public function actionDeleteMedia()
    {
        $request = Yii::$app->request;
        $mediaOrder = OrderMedia::findOne(['media_id' => $request->post('key'), 'category' => 'news']);
        $media = File::findOne(['id' => $request->post('key')]);
        Yii::$app->media->deleteFile($media);
        $media->delete();
        $mediaOrder->delete();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => 'true'];
    }
    public function actionStatusChange($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $mediaOrder = OrderMedia::updateAll(['status' => 'inactive'], ['status' => 'active', 'media_id' => $id]);
        $oneMedia = OrderMedia::findOne(['media_id' => $id]);
        $oneMedia->status = 'active';
        if ($oneMedia->update()) {
            return ['success' => 'true'];
        } else {
            return ['success' => 'false'];
        }
    }
    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $orderMedias = OrderMedia::findAll(['media_id' => $id, 'category' => 'news']);
        if (count($orderMedias) > 0) {
            foreach ($orderMedias as $item) {
                $media = File::findOne(['id' => $item->media_id]);
                Yii::$app->media->deleteFile($media);
                $media->delete();
            }
        }
        NewsCategory::deleteAll(['news_id' => $id]);
        NewsTags::deleteAll(['news_id' => $id]);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
