<?php

namespace app\modules\admin\controllers;

use ozerich\filestorage\models\File;

use app\models\OrderMedia;
use Yii;
use app\models\PostSearch;
use app\models\Post;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use ozerich\filestorage\storage\FileStorage;


/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'signup', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['uploadInformation'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index'],
                        'roles' => ['changePost'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-media' => ['POST']
                ],
            ],
        ];
    }
    public function actionAll($group)
    {
        $searchModel = new  PostSearch(); //::find()->where(['group' => $group])->orderBy(['created_at' => SORT_DESC]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $group);
        return $this->render(
            // $group . '/all',
            $group.'/all',
            [
                'searchModel' => $searchModel,
                'pagination' => ['pageSize' => 20],
                'dataProvider' => $dataProvider,

            ]
        );
    }
    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex($group = null, $category = null, $sub = null)
    {
        // Kategoriyasiz postlarni guruh bo'ylab sarlaydi
        if ($group && !$sub) {
            $query = Post::find()->where(['group' => $group])->orderBy(['created_at' => SORT_DESC]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => ['pageSize' => 20],
            ]);
            return $this->render($group . '/index', [
                'dataProvider' => $dataProvider,
            ]);
        } else 
        if ($category) {
            // Agar kop kategoriyali bolsa shu kategoriyalar listini chiqaradi
            $query = Post::find()->where(['group' => $category . '_category'])->orderBy(['created_at' => SORT_DESC]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => ['pageSize' => 20],
            ]);
            return $this->render($category . '_category/index', [
                'dataProvider' => $dataProvider,
            ]);
        } else
        if ($sub && $group) {
            // Agar kop kategoriyali bolsa bitta kategoriyaga tegishli postlarni oladi
            $query = Post::find()->where(['group' => strstr($group, '_', true), 'category_id' => $sub])->orderBy(['created_at' => SORT_DESC]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => ['pageSize' => 20],
            ]);
            return $this->render(strstr($group, '_', true) . '/index', [
                'dataProvider' => $dataProvider,
            ]);
        }
        // $searchModel = new PostSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->updateCounters(['views' => 1]);
        if ($model) {
            return $this->render($model->group . '/view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($group = null, $sub = null)
    {
        if ($group) {
            $model = new Post();
            $model->group = $group;
            if ($sub) {
                $model->category_id = $sub;
                $model->group = strstr($group, '_', true);
            }
            $model->title = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
            $model->content = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
            $model->short_content = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
            $model->content_html = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
            $model->meta_title = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
            $model->meta_description = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
            $model->meta_keywords = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
            $model->status = 'inactive';
            $newId = $model->save();
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return 'Error';
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /* app\controllers\UploadController.php */

    public function actionImage($group, $id)
    {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = 'json';
            $files = UploadedFile::getInstancesByName('file');
            if (count($files) > 0) {
                foreach ($files as $item) {
                    $media = Yii::$app->media->createFileFromUploadedFile($item, 'document');
                    $mediaOrder = new OrderMedia();
                    $mediaOrder->media_id = (int) $media->id;
                    $mediaOrder->model_id = (int) $id;
                    $mediaOrder->category = $group;
                    $mediaOrder->url = $media->getPath();
                    $mediaOrder->save();
                }
                return [
                    'success' => true
                ];
            } else {
                return [
                    'success' => false
                ];
            }
        }
    }
    public function actionDeleteMedia()
    {
        $request = Yii::$app->request;
        $mediaOrder = OrderMedia::findOne(['media_id' => $request->post('key')]);
        $media = File::findOne(['id' => $request->post('key')]);
        Yii::$app->media->deleteFile($media);
        $media->delete();
        $mediaOrder->delete();
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => 'true'];
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $mediaOrder = OrderMedia::find()->where(['model_id' => $id, 'category'=>$model->group])->all();
        $files = [];
        foreach ($mediaOrder as $item) {
            $media = File::findOne(['id' => $item->media_id]);
            array_push($files, $media);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->category_id > 0) {
                return $this->redirect(['index', 'group' => $model->group . '_category', 'sub' => $model->category_id]);
            }
            return $this->redirect([
                'index', 'group' => $model->group,

                'model' => $model,
                'files' => $files
            ]);
        }
        return $this->render($model->group . '/update', [
            'model' => $model,
            'files' => $files
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $post = $this->findModel($id);
        $group = $post['group'];
        $mediaOrder = OrderMedia::find()->where(['model_id' => $id])->all();
        foreach ($mediaOrder as $item) {
            $media = File::findOne(['id' => $item->media_id]);
            Yii::$app->media->deleteFile($media);
            $media->delete();
        }
        OrderMedia::deleteAll(['model_id' => $id]);
        $post->delete();
        return $this->redirect(['index', 'group' => $group]);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
