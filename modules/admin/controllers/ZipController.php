<?php

namespace app\modules\admin\controllers;

use app\components\helpers\MyHelpers;
use app\models\AdmissionYear;
use app\models\Applicants;
use app\models\User;
use ozerich\filestorage\models\File;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * PostController implements the CRUD actions for Post model.
 */
class ZipController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'signup', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['uploadInformation'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index'],
                        'roles' => ['manageApplicants'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-media' => ['POST']
                ],
            ],
        ];
    }
    public static function zipper($source, $destination)
    {
        $zip = new \ZipArchive();
        $zip->open($destination, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($source),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($destination) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();
    }
    /**
     * Lists all Post models.
     * @return mixed
     */
    public static function actionIndex()
    {
        $academic_year = AdmissionYear::findOne(['status' => 1]);
        $academic_year_title = $academic_year['title'];
        $applicants = Applicants::find()->where(['academic_year_id' => $academic_year->id, 'status' => 'accepted'])->all();
        $archive_name = Yii::$app->basePath . "/web/uploads/applicants/$academic_year_title.zip";
        if (count($applicants) > 0) {
            foreach ($applicants as $applicant) {
                $archived_name = Yii::$app->basePath . "/web/uploads/applicants/$academic_year_title/{$applicant->unique_code}";
                self::zipper($archived_name, $archive_name);
            }
            if (file_exists($archive_name)) {
                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename=' . $archive_name);
                header('Content-Length: ' . filesize($archive_name));
                readfile($archive_name);
            } else {
                return 'Не обнаружена';
            }
        }else{
            return 'Нет принятых заявок';
        }
    }
}
