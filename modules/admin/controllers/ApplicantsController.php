<?php

namespace app\modules\admin\controllers;

use app\models\AdmissionYear;
use Yii;
use app\models\Applicants;
use app\models\ApplicantsSearch;
use app\models\Messages;
use app\models\MessagesOrder;
use app\models\User;
use yii2mod\editable\EditableAction;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ApplicantsController implements the CRUD actions for Applicants model.
 */
class ApplicantsController extends Controller
{
    public function actions()
    {
        return [
            'change-username' => [
                'class' => EditableAction::class,
                'modelClass' => Applicants::class,
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'signup', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['uploadInformation'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index'],
                        'roles' => ['manageApplicants'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'send-message' => ['POST'],
                    'make-active' => ['POST'],
                    'make-active-one' => ['POST'],
                    'make-aborted-one' => ['POST'],
                    'make-fully-aborted-one' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Applicants grading.
     * @return mixed
     */
    public function actionGrading()
    {
        $searchModel = new ApplicantsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('grading', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all Applicants models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApplicantsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Applicants model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            $applicant = $this->findModel($id);
            return $this->renderPartial('view', [
                'model' => $applicant,
                'user' => User::findOne(['id' => $applicant->user_id])
            ]);
        }
    }


    /**
     * Creates a new Applicants model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Applicants();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }

    //     return $this->render('create', [
    //         'model' => $model,
    //     ]);
    // }
    public function sendEmail($email, $message)
    {
        Yii::$app
            ->mailer
            ->compose(
                ['html' => 'applicant-message-html'],
                ['content' => $message]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => ' Robot'])
            ->setTo($email)
            ->setSubject($message->title)
            ->send();
    }
    public function sendPlainEmail($email, $message)
    {
        Yii::$app
            ->mailer
            ->compose(
                ['html' => 'plain-message-html'],
                ['content' => $message]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => ' Robot'])
            ->setTo($email)
            ->setSubject('Eslatma')
            ->send();
    }
    public function actionMakeActive()
    {
        $request = Yii::$app->request;
        $applicants = $request->post('users');
        foreach ($applicants as $item) {
            Yii::$app->db->createCommand("UPDATE applicants SET status='accepted' WHERE id=:id")
                ->bindValue(':id', $item)
                ->execute();
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => 'true'];
    }
    public function actionMakeActiveOne()
    {
        $request = Yii::$app->request;
        $applicant_id = $request->post('id');
        $applicant = Applicants::findOne(['id' => $applicant_id]);
        Yii::$app->db->createCommand("UPDATE applicants SET status='accepted' WHERE id=:id")
            ->bindValue(':id', $applicant_id)
            ->execute();
        $user = User::findOne(['id' => $applicant->user_id]);
        $message = new MessagesOrder();
        $message->from = (int) Yii::$app->user->identity->id;
        $message->to = (int) $applicant->user_id;
        $message->status = 'inactive';
        $message->message_id = 0;
        $message->body = '<p>Ҳурматли номзод! Сизнинг ҳужжатларингиз муваффақиятли қабул қилинди. Бундан кейинги босқичлар ҳақида маълумот Академиянинг расмий веб-сайти ва ижтимоий тармоқларида жойлаштириб борилади, шунингдек шахсий кабинетингиз ва электрон почта манзилингизга хабар юбориб турилади. Ҳурмат билан, Бош прокуратура Академияси Қабул комиссияси</p><p>Уважаемый кандидат! Ваши документы были успешно приняты. Информация о следующих этапах будет размещаться на официальном веб-сайте и в социальных сетях Академии, а также сообщение будет отправлено на Ваш личный кабинет и адрес электронной почты. С уважением, Приемная комиссия Академии Генеральной прокуратуры</p>';
        if ($message->save()) {
            $this->sendPlainEmail($user->email, $message->body);
        };
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => 'true'];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => 'true'];
    }
    public function actionMakeAbortedOne()
    {
        $request = Yii::$app->request;
        $applicant_id = $request->post('id');
        $applicant = Applicants::findOne(['id' => $applicant_id]);
        // $applicant->updateAttributes(['status' => 'aborted']);
        Yii::$app->db->createCommand("UPDATE applicants SET status='aborted' WHERE id=:id")
            ->bindValue(':id', $applicant_id)
            ->execute();
        $user = User::findOne(['id' => $applicant->user_id]);
        $message = new MessagesOrder();
        $message->from = (int) Yii::$app->user->identity->id;
        $message->to = (int) $applicant->user_id;
        $message->status = 'inactive';
        $message->message_id = 0;
        $message->body = $request->post('message');
        if ($message->save()) {
            $this->sendPlainEmail($user->email, $request->post('message'));
        };
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => 'true'];
    }
    public function actionMakeFullyAbortedOne()
    {
         $request = Yii::$app->request;
        $applicant_id = $request->post('id');
        $applicant = Applicants::findOne(['id' => $applicant_id]);
        // $applicant->updateAttributes(['status' => 'aborted']);
        Yii::$app->db->createCommand("UPDATE applicants SET status='cancelled' WHERE id=:id")
            ->bindValue(':id', $applicant_id)
            ->execute();
        $user = User::findOne(['id' => $applicant->user_id]);
        $message = new MessagesOrder();
        $message->from = (int) Yii::$app->user->identity->id;
        $message->to = (int) $applicant->user_id;
        $message->status = 'inactive';
        $message->message_id = 0;
        $message->body = $request->post('message');
        if ($message->save()) {
            $this->sendPlainEmail($user->email, $request->post('message'));
        };
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => 'true'];
    }
    public function actionSendMessage()
    {
        $request = Yii::$app->request;
        $applicants = $request->post('users');
        $template_message = Messages::findOne(['id' => $request->post('message_id')]);
        foreach ($applicants as $item) {
            $appli = Applicants::findOne(['id' => $item]);
            $user = User::findOne(['id' => $appli->user_id]);
            $message = new MessagesOrder();
            $message->from = (int) Yii::$app->user->identity->id;
            $message->to = (int) $appli->user_id;
            $message->status = 'inactive';
            $message->message_id = $request->post('message_id');
            if ($message->save()) {
                $this->sendEmail($user->email, $template_message);
            };
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => 'true'];
    }
    /**
     * Updates an existing Applicants model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionZip(){
        return ZipController::actionIndex();
    }
    /**
     * Deletes an existing Applicants model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Applicants model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Applicants the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Applicants::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
