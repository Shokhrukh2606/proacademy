<?php

namespace app\modules\admin\controllers;

use app\components\helpers\MyHelpers;
use app\models\AdmissionFaculties;
use app\models\AdmissionYear;
use app\models\Applicants;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\models\LoginForm;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    public $active_year;
    public function init()
    {
        parent::init();
        if (Yii::$app->user->can('manageApplicants')) {
            $this->active_year = AdmissionYear::findOne(['status' => 1]);
        }
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'signup', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['uploadInformation'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index'],
                        'roles' => ['changePost', 'manageApplicants'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $applicants = [
            'unknown' => Applicants::find()->where(['status' => 'unknown', 'academic_year_id' => $this->active_year])->count(),
            'accepted' => Applicants::find()->where(['status' => 'accepted', 'academic_year_id' => $this->active_year])->count(),
            'updated' => Applicants::find()->where(['status' => 'updated', 'academic_year_id' => $this->active_year])->count(),
            'aborted' => Applicants::find()->where(['status' => 'aborted', 'academic_year_id' => $this->active_year])->count(),
            'cancelled' => Applicants::find()->where(['status' => 'cancelled', 'academic_year_id' => $this->active_year])->count()
        ];

        $faculties = ArrayHelper::map(AdmissionFaculties::findAll(['status' => 'active']), function ($item) {
            return MyHelpers::t($item->title);
        }, function ($item) {
            return Applicants::find()->where(['faculty' => $item->id, 'academic_year_id' => $this->active_year])->count();
        });
        $facultiesAndType = ArrayHelper::map(AdmissionFaculties::findAll(['status' => 'active']), function ($item) {
            return MyHelpers::t($item->title);
        }, function ($item) {
            return [
                'inner' => Applicants::find()->where(['faculty' => $item->id, 'academic_year_id' => $this->active_year, 'applicant_type' => 'inner'])->count(),
                'outer' => Applicants::find()->where(['faculty' => $item->id, 'academic_year_id' => $this->active_year, 'applicant_type' => 'outer'])->count()
            ];
        });
        $app_type = [
            'inner' => Applicants::find()->where(['applicant_type' => 'inner', 'academic_year_id' => $this->active_year])->count(),
            'outer' => Applicants::find()->where(['applicant_type' => 'outer', 'academic_year_id' => $this->active_year])->count()
        ];
        return $this->render('index', compact('applicants', 'faculties', 'app_type', 'facultiesAndType'));
    }
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
}
