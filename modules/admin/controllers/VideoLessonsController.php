<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\VideoLessons;
use app\models\VideoLessonsSearch;
use app\models\OrderMedia;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use ozerich\filestorage\models\File;
use yii\filters\AccessControl;

/**
 * VideoLessonsController implements the CRUD actions for VideoLessons model.
 */
class VideoLessonsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'signup', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['uploadInformation'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index'],
                        'roles' => ['changePost'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'cat-delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionCats()
    {
        $cats = VideoLessons::find()->where(['lesson_group' => 'cats', 'category' => 0])->all();
        foreach ($cats as $item) {
            $childs = VideoLessons::find()->where(['category' => $item->id])->all();
            if (count($childs) > 0) {
                $item->childs = $childs;
            }
        }
        return $this->render('cats', compact('cats'));
    }
    /**
     * Lists all VideoLessons models.
     * @return mixed
     */
    public function actionIndex()
    {
        // echo sys_get_temp_dir(); 
        $searchModel = new VideoLessonsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VideoLessons model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VideoLessons model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VideoLessons();

        if ($model->load(Yii::$app->request->post())) {
            $model->lesson_group = 'video';
            $came = UploadedFile::getInstance($model, 'cover');
            if($came){
                $f = Yii::$app->media->createFileFromUploadedFile($came, 'lessons');
                $model->cover = $f->getPath();
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionCatsCreate()
    {
        $model = new VideoLessons();

        if ($model->load(Yii::$app->request->post())) {
            $model->lesson_group = 'cats';
            if(!$model->category){
                $model->category=0;
            }
            if ($model->save()) {
                return $this->redirect(['cats']);
            }
        }

        return $this->render('cats-create', [
            'model' => $model,
        ]);
    }
    public function actionCatUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('cats-update', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing VideoLessons model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $came = UploadedFile::getInstance($model, 'cover');

            if ($came) {
                $oldMedia = OrderMedia::findOne(['model_id' => $model->id]);
                if ($oldMedia) {
                    $oldFile = File::findOne(['id' => $oldMedia->media_id]);
                    Yii::$app->media->deleteFile($oldFile);
                    $oldFile->delete();
                    $oldMedia->delete();
                }
                $f = Yii::$app->media->createFileFromUploadedFile($came, 'lessons');
                if($f){
                    $model->cover = $f->getPath();
                    $mediaOrder = new OrderMedia();
                    $mediaOrder->media_id = (int) $f->id;
                    $mediaOrder->model_id = (int) $model->id;
                    $mediaOrder->category = 'lessons';
                    $mediaOrder->url = $f->getPath();
                    $mediaOrder->save();
                }
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                print_r($model->getErrors());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing VideoLessons model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    public function actionCatDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['cats']);
    }
    /**
     * Finds the VideoLessons model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VideoLessons the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VideoLessons::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
