<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Menu;
use app\models\OrderMedia;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;
/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'signup', 'logout', 'index'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles' => ['uploadInformation'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'index'],
                        'roles' => ['changePost'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ]
        ];
    }


    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = Json::encode(Menu::find()->select('id, category_id, title, status')->all());
        // $nestedMenu = array_map(function ($item) {
        //     $childs = Menu::find()->where(['category_id' => $item->id])->all();
        //     if(count($childs)>0){
        //       return $item->childs = $childs;
        //     }
        // }, $dataProvider);
        // foreach ($dataProvider as $item) {
        //     $childs = Menu::find()->where(['category_id' => $item->id])->all();
        //     if (count($childs) > 0) {
        //         $item->childs = $childs;
        //     }
        // }
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Menu();
        $model->group = 'menu';
        $model->status = 'inactive';
        $model->title = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->content = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->short_content = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->content_html = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->meta_title = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->meta_description = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->meta_keywords = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $model->alias='pro';
        $model->category_id=0;
        $newId = $model->save();
        return $this->redirect(['update', 'id' => $model->id]);
    }
    public function actionChangeStatus($m_id, $stat){
        $menu=Menu::findOne(['id'=>$m_id]);
        if($menu){
            $menu->status=$stat;
            $menu->save();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success'=>true];
        }else{
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success'=>false];
        }
    }
    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = null;
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionImage($postid)
    {
        if (Yii::$app->request->isPost) {
            Yii::$app->response->format = 'json';
            $file = UploadedFile::getInstanceByName('file');
            $newMedia = Yii::$app->media->createFileFromUploadedFile($file, 'document');
            $post = $this->findModel($postid);
            $orderMedia = new OrderMedia;
            $orderMedia->media_id = $newMedia->id;
            $orderMedia->model_id = $post->id;
            $orderMedia->category = $post->group;
            $orderMedia->save();
            return [
                'image' => $newMedia->toJSON()
            ];
        }
    }
}
