<?php

$this->title = 'Metrica';
$this->params['breadcrumbs'][] = $this->title;
// ********************************************Site statstics

?>
<?php if (!Yii::$app->user->can('manageApplicants')) { ?>
	<style>
		.chart {
			width: 100%;
			height: 300px;
		}

		#visits {
			height: 500px;
		}
	</style>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<h4>Источники трафика</h4>
					<div id="traffic" class="chart"></div>

				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<h4>Браузеры</h4>
					<div id="browsers" class="chart"></div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<h4>Посещения за неделю</h4>
					<div class="chart" id="visits"></div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
	<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
	<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

	<script>
		//id  66576448
		// token AgAAAAA20_QdAAaPGfFKKkTS90jmmf07RgpituQ
		source("https://api-metrika.yandex.net/stat/v1/data?preset=sources_summary&lang=ru&id=66841585&dimensions=ym:s:lastSignTrafficSource&metrics=ym:s:visits", "traffic");
		source("https://api-metrika.yandex.net/stat/v1/data?&id=66841585&lang=ru&metrics=ym:s:visits&dimensions=ym:s:browser", "browsers");
		source("https://api-metrika.yandex.net/stat/v1/data?&id=66841585&lang=ru&metrics=ym:s:visits&dimensions=ym:s:date", "visits")
		var stat = {
			"browsers": [],
			"traffic": []
		}

		function csvDownload(array, name) {

			let csvContent = "data:text/csv;charset=utf-8,";
			csvContent += "Name,Value(%)\r\n";
			array.forEach(function(rowArray) {
				let row = rowArray.name + "," + rowArray.val;
				csvContent += row + "\r\n";
			});
			var encodedUri = encodeURI(csvContent);
			var link = document.createElement("a");
			link.setAttribute("href", encodedUri);
			link.setAttribute("download", name + "_perepost_stats.csv");
			document.body.appendChild(link); // Required for FF

			link.click();
		}

		function source(url, elem) {
			fetch(url, {
					"method": "GET",
					"headers": {
						"Authorization": "OAuth AgAAAAA20_QdAAaS3NBROsYHrk9NiBswjJMNwRs"
					}
				})
				.then(res => {

					return res.json()
				})
				.then(json => {
					console.log(json);
					var cumulative = [];
					var data = json.data;
					if (elem != "visits") {
						for (var i = 0; i < data.length; i++) {
							console.log("1");
							cumulative.push({
								name: data[i].dimensions[0].name,
								val: data[i].metrics[0]
							});
						}
						stat[elem] = cumulative;

						buildchart(cumulative, elem);
					} else {
						for (var i = 0; i < data.length; i++) {
							cumulative.push({
								name: data[i].dimensions[0].name,
								val: data[i].metrics[0]
							});
						}

						function compare(a, b) {
							if (a.name < b.name) {
								return -1;
							}
							if (a.name > b.name) {
								return 1;
							}
							return 0;
						}

						cumulative.sort(compare);
						buildLineChart(cumulative, elem);
					}

				})
				.catch(err => console.log(err));
		}

		function buildLineChart(cumulative, elem) {
			am4core.ready(function() {
				console.log(cumulative);
				// Themes begin
				am4core.useTheme(am4themes_animated);
				// Themes end

				// Create chart instance
				var chart = am4core.create(elem, am4charts.XYChart);

				// Add data
				chart.data = cumulative
				// Create axes
				var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
				dateAxis.renderer.grid.template.location = 0;
				dateAxis.renderer.minGridDistance = 50;

				var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

				// Create series
				var series = chart.series.push(new am4charts.LineSeries());
				series.dataFields.valueY = "val";
				series.dataFields.dateX = "name";
				series.strokeWidth = 3;
				series.fillOpacity = 0.5;

				// Add vertical scrollbar
				chart.scrollbarY = new am4core.Scrollbar();
				chart.scrollbarY.marginLeft = 0;

				// Add cursor
				chart.cursor = new am4charts.XYCursor();
				chart.cursor.behavior = "zoomY";
				chart.cursor.lineX.disabled = true;

			}); // end am4core.ready()
		}

		function buildchart(cumulative, elem) {
			am4core.ready(function() {


				// Themes begin
				am4core.useTheme(am4themes_animated);
				// Themes end

				// Create chart instance
				var chart = am4core.create(elem, am4charts.PieChart);

				// Add data
				chart.data = cumulative;
				// Add and configure Series
				var pieSeries = chart.series.push(new am4charts.PieSeries());
				pieSeries.dataFields.value = "val";
				pieSeries.dataFields.category = "name";
				pieSeries.slices.template.stroke = am4core.color("#fff");
				pieSeries.slices.template.strokeOpacity = 1;

				// This creates initial animation
				pieSeries.hiddenState.properties.opacity = 1;
				pieSeries.hiddenState.properties.endAngle = -90;
				pieSeries.hiddenState.properties.startAngle = -90;

				chart.hiddenState.properties.radius = am4core.percent(0);


			}); // end am4core.ready()
		};
	</script>
<?php } else {
// ********************************************Applicants statstics
?>
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<style>
#chartdiv {
  width: 100%;
  height: 500px;
}
</style>

<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end



var chart = am4core.create('chartdiv', am4charts.XYChart)
chart.colors.step = 2;

chart.legend = new am4charts.Legend()
chart.legend.position = 'top'
chart.legend.paddingBottom = 20
chart.legend.labels.template.maxWidth = 95

var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
xAxis.dataFields.category = 'category'
xAxis.renderer.cellStartLocation = 0.1
xAxis.renderer.cellEndLocation = 0.9
xAxis.renderer.grid.template.location = 0;

var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
yAxis.min = 0;

function createSeries(value, name) {
    var series = chart.series.push(new am4charts.ColumnSeries())
    series.dataFields.valueY = value
    series.dataFields.categoryX = 'category'
    series.name = name

    series.events.on("hidden", arrangeColumns);
    series.events.on("shown", arrangeColumns);

    var bullet = series.bullets.push(new am4charts.LabelBullet())
    bullet.interactionsEnabled = false
    bullet.dy = 30;
    bullet.label.text = '{valueY}'
    bullet.label.fill = am4core.color('#ffffff')

    return series;
}

chart.data = [
	<?php foreach($facultiesAndType as $key=>$item): ?>
	{
        category: '<?=$key?>',
        inner: '<?=$item['inner']?>',
        outer: '<?=$item['outer']?>',
	},
	<?php endforeach;?>
]


createSeries('inner', '<?=Yii::t('front', 'inner')?>');
createSeries('outer', '<?=Yii::t('front', 'outer')?>');

function arrangeColumns() {

    var series = chart.series.getIndex(0);

    var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
    if (series.dataItems.length > 1) {
        var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
        var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
        var delta = ((x1 - x0) / chart.series.length) * w;
        if (am4core.isNumber(delta)) {
            var middle = chart.series.length / 2;

            var newIndex = 0;
            chart.series.each(function(series) {
                if (!series.isHidden && !series.isHiding) {
                    series.dummyData = newIndex;
                    newIndex++;
                }
                else {
                    series.dummyData = chart.series.indexOf(series);
                }
            })
            var visibleCount = newIndex;
            var newMiddle = visibleCount / 2;

            chart.series.each(function(series) {
                var trueIndex = chart.series.indexOf(series);
                var newIndex = series.dummyData;

                var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
            })
        }
    }
}

}); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv"></div>

	
	<style>
		.chart {
			width: 100%;
			height: 500px;
			padding: 40px;
		}
	</style>



	<div id='q1' class="chart"></div>
	<div id='q2' class="chart"></div>
	<div id='q3' class="chart"></div>

	<script>
		am4core.ready(function() {

			// Themes begin
			am4core.useTheme(am4themes_animated);
			// Themes end
			// ***********************************************Statistics by app status
			var chart = am4core.create('q1', am4charts.PieChart3D);
			chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

			chart.data =
				<?= json_encode(
					[
						[
							'country' => Yii::t('admin', 'accepted'),
							'litres' => $applicants['accepted']
						],
						[
							'country' => Yii::t('admin', 'aborted'),
							'litres' => $applicants['aborted']
						],
						[
							'country' => Yii::t('admin', 'cancelled'),
							'litres' => $applicants['cancelled']
						],
						[
							'country' => Yii::t('admin', 'updated_status'),
							'litres' => $applicants['updated']
						],
						[
							'country' => Yii::t('admin', 'unknown'),
							'litres' => $applicants['unknown']
						]
					]
				) ?>

			chart.innerRadius = am4core.percent(40);
			chart.depth = 50;

			chart.legend = new am4charts.Legend();

			var series = chart.series.push(new am4charts.PieSeries3D());
			series.dataFields.value = "litres";
			series.dataFields.depthValue = "litres";
			series.dataFields.category = "country";
			series.slices.template.cornerRadius = 5;
			series.colors.step = 3;


			// ***********************************************Statistics by app status
			// ***********************************************Statistics by app type

			var chart1 = am4core.create('q2', am4charts.PieChart3D);
			chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

			chart1.data =
				<?= json_encode(
					[
						[
							'country' =>Yii::t('front', 'inner'),
							'litres' => $app_type['inner']
						],
						[
							'country' => Yii::t('front', 'outer'),
							'litres' => $app_type['outer']
						]
					]
				) ?>

			chart1.innerRadius = am4core.percent(40);
			chart1.depth = 50;

			chart1.legend = new am4charts.Legend();

			var series1 = chart1.series.push(new am4charts.PieSeries3D());
			series1.dataFields.value = "litres";
			series1.dataFields.depthValue = "litres";
			series1.dataFields.category = "country";
			series1.slices.template.cornerRadius = 5;
			series1.colors.step = 3;
			// ***********************************************Statistics by app type


			// ***********************************************Statistics by app Faculty
			var chart2 = am4core.create('q3', am4charts.PieChart3D);
			chart2.hiddenState.properties.opacity = 0; // this creates initial fade-in

			chart2.data = [
				<?php foreach ($faculties as $key=>$value) : ?> {
            		country: "<?= $key ?>",
					litres: <?= $value ?>
          		},
        		<?php endforeach; ?>
			]
			chart2.innerRadius = am4core.percent(40);
			chart2.depth = 50;

			chart2.legend = new am4charts.Legend();

			var series2 = chart2.series.push(new am4charts.PieSeries3D());
			series2.dataFields.value = "litres";
			series2.dataFields.depthValue = "litres";
			series2.dataFields.category = "country";
			series2.slices.template.cornerRadius = 5;
			series2.colors.step = 3;
			// ***********************************************Statistics by app Faculty

		}); // end am4core.ready()
	</script>

<?php } ?>