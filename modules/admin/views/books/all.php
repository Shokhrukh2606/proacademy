<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Post;
use app\components\helpers\MyHelpers;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = $this->title;
$request = Yii::$app->request;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1> 
    <p>
        <?= Html::a('Добавить', ['create', 'sub'=>$request->get('sub')], ['class' => 'btn btn-success']) ?>
    </p>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'label' => 'Заголовок',
                'value' => function ($item) {
                    return (json_decode($item->title))->ru;
                }
            ],
            [
                'attribute' => 'category_id',
                'label' => 'Категория',
                'value' => function ($item) {
                    $p=Post::findOne(['id'=>$item->category_id]);
                    return $p?MyHelpers::t($p->title):$item->group;
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'category_id',
                    \yii\helpers\ArrayHelper::map(Post::find()->where(['group'=>'library_category'])->asArray()->all(), 'id', function ($item) {
                        return MyHelpers::t($item['title']);
                    }),
                    [
                        'class' => 'form-control', 'multiple' => false,
                        'prompt' => 'Все',
                    ]
                )
            ],
            [
                'attribute' => 'short_content',
                'label' => 'Короткое содержание',
                'value' => function ($model) {
                    return (json_decode($model->short_content)->ru);
                }
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'contentOptions' => function ($model) {
                    return  ['class' => ($model->status == 'active') ? 'bg-green' : 'bg-yellow'];
                },
                'value'=>function($model){
                    return ($model->status == 'active') ? 'Активный' : 'Неактивный';
                }
            ],
            // 'category_id',
            // 'category_id2',
            // 'group',
            // 'alias:ntext',
            //'title:ntext',
            //'content:ntext',
            //'content_html:ntext',
            //'short_content:ntext',
            //'status',
            //'option',
            //'options',
            //'views',
            //'tags:ntext',
            //'meta_title:ntext',
            //'meta_description:ntext',
            //'meta_keywords:ntext',
            //'sort_order',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>