<?php

use app\components\helpers\MyHelpers;
use app\models\Category;
use app\models\Tags;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mludvik\tagsinput\TagsInputWidget;
use yii\web\View;
use app\assets\PluginsAsset;
use app\models\NewsTags;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\Json;
use yii\helpers\Url;
use kartik\datetime\DateTimePicker;

PluginsAsset::register($this);
$tags = Json::encode(Tags::find()->all());


$this->registerJS("
    var tags =$tags.map(item=>({
        id: item.id,
        title: jQuery.parseJSON(item.title).ru
    }))
    $('#news-tags').selectize({
        valueField: 'id',
        labelField: 'title',
        placeholder: 'Выберите',
        options: tags,
        create: false,
        preload: true,
        render: {
            option: function (item, escape) {
                return '<div>' +
                    '<span class=\"title\">' +
                    '<span class=\"name\">' + escape(item.title) + '</span>' +
                    '</span>' +
                    '</div>';
            }
        },

    });
", View::POS_END);
?>

<div class="news-form">
    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Media</button>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Media</h4>
                </div>
                <div class="modal-body">
                    <?php echo FileInput::widget([
                        'name' => 'file',
                        'options' => [
                            'multiple' => true,
                            'id' => 'hyperFiles'
                        ],
                        'pluginOptions' => [
                            'uploadUrl' => Url::to(['post/image', 'id' => $model->id, 'group' => 'news']),
                            'initialPreview' => isset($files) ? MyHelpers::getMediaUrls($files) : '',
                            'initialPreviewConfig' =>
                            isset($files) ? MyHelpers::getEachMediaInfo($files) : '',
                            'maxFileCount' => 10,
                            'otherActionButtons' => '<button class="set-main" type="button" {dataKey} ><i class="glyphicon glyphicon-star"></i></button>',
                            'uploadAsync' => true,
                            'showUpload' => false,
                            'overwriteInitial' => false,
                            'minFileCount' => 1,
                            'maxFileCount' => 10,
                            'initialPreviewAsData' => true,
                            'browseOnZoneClick' => true,
                        ],
                        'pluginEvents' => [
                            'filebatchselected' => '
                            function(event, files) {
                                $("#hyperFiles").fileinput("upload");
                            }
                            '
                        ]
                    ]);
                    $this->registerJs("
    
            $('.set-main').on('click',function(){
                $.get('" . \yii\helpers\Url::to(['/admin/news/status-change']) . "', {id:$(this).attr('data-key')});
                
                });
    ");
        ?>
                    ?>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <?php $form = ActiveForm::begin();
    $hasLanguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            'content' => "<div>" .
                $form->field($model, "jtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->title)[$item]]) .
                $form->field($model, "jshcontent[{$item}]", ['labelOptions' => ['label' => "Анонс"]])->textarea(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->short_content)[$item]]) .
                $form->field($model, "jcontent[{$item}]", ['labelOptions' => ['label' => "Контент"]])->widget(TinyMce::className(), [
                    'options' => [
                        'value' => yii\helpers\Json::decode($model->content)[$item],
                    ],
                    'clientOptions' => [
                        // 'file_picker_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                        //     window.open('" . yii\helpers\Url::to(['/imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'tinymce']) . "&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                        // }"),
                        'height'=>'700',
                        'block_formats' => 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
                        'fontsize_formats' => '8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt',
                        'disk_cache' => true,
                        'plugins' => [
                            "emoticons",
                            "code",
                            "lists",
                            "anchor",
                            "fullpage",
                            "fullscreen",
                            "autosave",
                            "wordcount",
                            "textcolor colorpicker",
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                            "table contextmenu directionality emoticons paste textcolor code",
                            "save autosave template",
                        ],
                        // 'file_browser_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                        //     window.open('" . yii\helpers\Url::to(['imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'tinymce']) . "&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                        // }"),
                        'external_filemanager_path' => '/plugins/responsivefilemanager/filemanager/',
                        'filemanager_title' => 'Responsive Filemanager',
                        'external_plugins' => [
                            //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                            'filemanager' => '/plugins/responsivefilemanager/filemanager/plugin.min.js',
                            //Иконка/кнопка загрузки файла в панеле иснструментов.
                            'responsivefilemanager' => '/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                        ],
                        'theme_advanced_buttons1' => "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",

                        'toolbar' => "undo redo | fullpage | fullscreen | fontselect | fontsizeselect | emoticons | code | anchor | undo redo | forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media",
                    ]
                ])
                . "</div>"
        ];
    }, ['ru', "uz-Cyr", 'uz-Lat', 'en']);
    ?>
    <?= isset($modelcats) ? $form->field($model, 'news_category', ['labelOptions' => ['label' => "Категория"]])->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', function ($item) {
        return MyHelpers::t($item->title);
    }), ['multiple' => 'multiple', 'prompt' => 'Выбрать', 'options' => ArrayHelper::map($modelcats, 'category_id', function ($item) {
        return ['selected' => true];
    })])
        : $form->field($model, 'news_category', ['labelOptions' => ['label' => "Категория"]])->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', function ($item) {
            return MyHelpers::t($item->title);
        }), ['multiple' => 'multiple', 'prompt' => 'Выбрать']) ?>
    <?php
    echo yii\bootstrap\Tabs::widget([
        'items' => $hasLanguage
    ]);
    ?>
    <?= $form->field($model, 'tags')->textInput(['class' => '', 'value' => implode(',', ArrayHelper::getColumn(NewsTags::findAll(['news_id' => $model->id]), 'tag_id'))]) ?>
    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'published_date')->widget(DateTimePicker::classname(), [
            'options' => ['placeholder' => $model->getAttributeLabel('published_date')],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd HH:mm'
            ],
            'bsVersion' => '3.4.1',
        ]);
        ?>


    <div class="form-group mt-5">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>