<?php

use app\components\helpers\MyHelpers;
use app\models\Category;
use app\models\Tags;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mludvik\tagsinput\TagsInputWidget;
use yii\web\View;
use app\assets\PluginsAsset;
use app\models\NewsTags;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\Json;
use yii\helpers\Url;
use kartik\datetime\DateTimePicker;

PluginsAsset::register($this);
$tags = Json::encode(Tags::find()->all());
/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = 'Создать новости';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">
    <h3>Размер изображения должен быть 1024x521 пикселей!</h3>
    <h1><?= Html::encode($this->title) ?></h1>

    <?php




    $this->registerJS("
    var tags =$tags.map(item=>({
        id: item.id,
        title: jQuery.parseJSON(item.title).ru
    }))
    $('#news-tags').selectize({
        valueField: 'id',
        labelField: 'title',
        placeholder: 'Выберите',
        options: tags,
        create: false,
        preload: true,
        render: {
            option: function (item, escape) {
                return '<div>' +
                    '<span class=\"title\">' +
                    '<span class=\"name\">' + escape(item.title) + '</span>' +
                    '</span>' +
                    '</div>';
            }
        },

    });
", View::POS_END);
    ?>

    <div class="news-form">

        <?php $form = ActiveForm::begin();
        $hasLagnguage = array_map(function ($item) use ($form, $model) {
            return [
                'label' => strtoupper($item),
                'content' => "<div>" .
                    $form->field($model, "jtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true]) .
                    $form->field($model, "jshcontent[{$item}]", ['labelOptions' => ['label' => "Анонс"]])->textarea(['maxlength' => true]) .
                    $form->field($model, "jcontent[{$item}]", ['labelOptions' => ['label' => "Контент"]])->widget(TinyMce::className(), [
                        'clientOptions' => [
                            'file_picker_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                            window.open('" . yii\helpers\Url::to(['/imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'tinymce']) . "&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                        }"),
                            'block_formats' => 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
                            'fontsize_formats' => '8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt',
                            'disk_cache' => true,
                            'height'=>'700',
                            'plugins' => [
                                "emoticons",
                                "code",
                                "lists",
                                "anchor",
                                "fullpage",
                                "fullscreen",
                                "autosave",
                                "wordcount",
                                "textcolor colorpicker",
                                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                                "table contextmenu directionality emoticons paste textcolor code",
                                "save autosave template",
                            ],
                            'file_browser_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                            window.open('" . yii\helpers\Url::to(['imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'tinymce']) . "&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                        }"),
                            'theme_advanced_buttons1' => "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",

                            'toolbar' => "undo redo | fullpage | fullscreen | fontselect | fontsizeselect | emoticons | code | anchor | undo redo | forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media",
                        ]
                    ])
                    . "</div>"
            ];
        }, ['ru', "uz-Cyr", 'uz-Lat', 'en']);
        $hasMetaLagnguage = array_map(function ($item) use ($form, $model) {
            return [
                'label' => strtoupper($item),
                'content' => "<div>" .
                    $form->field($model, "mtitle[{$item}]", ['labelOptions' => ['label' => "Meta Заголовок"]])->textInput(['maxlength' => true]) .
                    $form->field($model, "mdescription[{$item}]", ['labelOptions' => ['label' => "Meta описание"]])->textarea(['maxlength' => true]) .
                    $form->field($model, "mkeywords[{$item}]", ['labelOptions' => ['label' => "Meta ключевые слова"]])->textarea(['maxlength' => true])
                    . "</div>"
            ];
        }, ['ru', "uz-Cyr", 'uz-Lat', 'en']);
        ?>
        <?= isset($modelcats) ? $form->field($model, 'news_category', ['labelOptions' => ['label' => "Категория"]])->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', function ($item) {
            return MyHelpers::t($item->title);
        }), ['multiple' => 'multiple', 'prompt' => 'Выбрать', 'options' => ArrayHelper::map($modelcats, 'category_id', function ($item) {
            return ['selected' => true];
        })])
            : $form->field($model, 'news_category', ['labelOptions' => ['label' => "Категория"]])->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', function ($item) {
                return MyHelpers::t($item->title);
            }), ['multiple' => 'multiple', 'prompt' => 'Выбрать']) ?>
        <?php
        echo yii\bootstrap\Tabs::widget([
            'items' => $hasLagnguage
        ]);
        ?>
        <?= $form->field($model, 'tags')->textInput(['class' => '', 'value' => implode(',', ArrayHelper::getColumn(NewsTags::findAll(['news_id' => $model->id]), 'tag_id'))]) ?>
        <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'published_date')->widget(DateTimePicker::classname(), [
            'options' => ['placeholder' => $model->getAttributeLabel('published_date')],
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd HH:ii'
            ],
            'bsVersion' => '3.4.1',
        ]);
        ?>
        <?= $form->field($model, 'files', ['labelOptions' => ['label' => "Медиа"]])->widget(FileInput::className(), [
            'name' => 'files',
            'options' => [
                'multiple' => true
            ],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['post/image', 'id' => $model->id, 'group' => 'news']),
                'maxFileCount' => 10,
                'initialPreview' => isset($files) ? MyHelpers::getMediaUrls($files) : '',
                'initialPreviewConfig' =>
                isset($files) ? MyHelpers::getEachMediaInfo($files) : '',
                'initialPreviewAsData' => true,
                'overwriteInitial' => false,
                'maxFileSize' => 2800
            ]
        ]); ?>
        <?php /*
        echo yii\bootstrap\Tabs::widget([
            'items' => $hasMetaLagnguage
        ]);*/
        ?>

        <div class="form-group mt-5">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>