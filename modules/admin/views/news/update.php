<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = 'Редактировать новости: ' . MyHelpers::t($model->title);

$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="news-update">
    <h3>Размер изображения должен быть 1024x521 пикселей!</h3>
  
    <?= $this->render('_form', [
        'model' => $model,
        'modelcats'=>$modelcats,
        'files'=>$files
    ]) ?>
    <script>
        const elem=document.getElementById("news-published_date");
        var oldVal=elem.value;
        var newVal=oldVal.split(":").slice(0,-1);
        elem.value=newVal.join(":");
    </script>
</div>
