<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = MyHelpers::t($model->title);
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'title',
                'label' => 'Краткое содержание',
                'value' => function ($item) {
                    return MyHelpers::t($item->title);
                }
            ],
            [
                'format'=>'raw',
                'attribute' => 'short_content',
                'label' => 'Краткое содержание',
                'value' => function ($item) {
                    return MyHelpers::t($item->short_content);
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'content',
                'label' => 'Краткое содержание',
                'value' => function ($item) {
                    return MyHelpers::t($item->content);
                }
            ],
            'views',
            'published_date'
        ],
    ]) ?>

</div>