<?php

use app\components\helpers\MyHelpers;
use app\models\Category;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать новости', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width:5%'],
            ],
            [
                'attribute' => 'published_date',
                'label' => 'Дата публикации',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'published_date',
                    [
                        '2018'=>'2018',
                        '2019'=>'2019',
                        '2020'=>'2020',
                        '2021'=>'2021',
                        '2022'=>'2022',
                        '2023'=>'2023',
                        '2024'=>'2024',
                        '2025'=>'2025',
                        '2026'=>'2026',
                        '2027'=>'2027',
                        '2027'=>'2028',
                        '2029'=>'2029',
                        '2030'=>'2030'
                    ],
                    [
                        'class' => 'form-control', 'multiple' => false,
                        'prompt' => 'Все',
                    ]
                )
            ],
            [
                'format' => 'raw',
                'attribute' => 'title',
                'label' => 'Заголовок',
                'value' => function ($item) {
                    return MyHelpers::limiter(MyHelpers::t($item->title), 70);
                },
                'headerOptions' => ['style' => 'width:45%'],
            ],
            
            [
                'format' => 'raw',
                'attribute' => 'short_content',
                'label' => 'Краткое содержание',
                'value' => function ($item) {
                    return MyHelpers::limiter(MyHelpers::t($item->short_content), 90);
                },
                'headerOptions' => ['style' => 'width:45%'],

            ],
            
            [
                'attribute' => 'category',
                'label' => 'Категория',
                'value' => function ($model) {
                        return MyHelpers::t($model->title);
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'category',
                    \yii\helpers\ArrayHelper::map(Category::find()->asArray()->all(), 'id', function ($item) {
                        return MyHelpers::t($item['title']);
                    }),
                    [
                        'class' => 'form-control', 'multiple' => false,
                        'prompt' => 'Все',
                    ]
                )
            ],
            //'meta_title:ntext',
            //'meta_description:ntext',
            //'meta_keywords:ntext',
            //'created_by',
            //'views',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'width:5%'],

            ],
        ],
    ]); ?>


</div>