<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionFacultiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('front', 'faculty');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table thead th{
        text-align: center;
    }
</style>
<div class="admission-faculties-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('admin', 'create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'label' => Yii::t('admin', 'title'),
                'value' => function ($item) {
                    return (json_decode($item->title, true))[Yii::$app->language];
                }
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('admin', 'status'),
                'contentOptions' => function ($model) {
                    return  ['class' => ($model->status == 'active') ? 'bg-green' : 'bg-yellow'];
                },
                'value'=>function($model){
                    return ($model->status == 'active') ? 'Активный' : 'Неактивный';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
