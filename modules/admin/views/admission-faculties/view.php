<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AdmissionFaculties */

$this->title = MyHelpers::t($model->title);
$this->params['breadcrumbs'][] = ['label' => 'Admission Faculties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="admission-faculties-view">

    <h1><?= Html::encode( MyHelpers::t($model->title)) ?></h1>

    <p>
        <?= Html::a(Yii::t('admin', 'update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('admin', 'delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'title',
                'value'=>function($model){
                    return  MyHelpers::t($model->title);
                }
            ],
            'status'
        ],
    ]) ?>

</div>
