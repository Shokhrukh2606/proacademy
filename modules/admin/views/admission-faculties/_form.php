<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdmissionFaculties */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="admission-faculties-form">

    <?php $form = ActiveForm::begin();
    $hasLagnguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            // 'content' => "<div>". $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->title)[$item]]).$form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->content)[$item]])."</div>"
            'content' => "<div>" .
                $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => Yii::t('admin', 'title')]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->title)[$item]])
                . "</div>"
        ];
    }, ['ru', 'uz-Lat', 'uz-Cyr', 'en']);
    ?>

    <?php echo yii\bootstrap\Tabs::widget([
        'items' => $hasLagnguage
    ]); ?>

    <?= $form->field($model, 'status')->dropDownList(['active' => Yii::t('admin', 'active'), 'inactive' => Yii::t('admin', 'inactive'),], ['prompt' => Yii::t('front', 'select')]) ?>

    <?= $form->field($model, 'sort_order')->textInput() ?>

    <!-- <?= $form->field($model, 'updated_at')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>