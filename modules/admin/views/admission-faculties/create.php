<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdmissionFaculties */

$this->title = Yii::t('admin', 'create');
$this->params['breadcrumbs'][] = ['label' => 'Admission Faculties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-faculties-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
