<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdmissionFaculties */

$this->title = 'Update Admission Faculties: ' . MyHelpers::t($model->title);
$this->params['breadcrumbs'][] = ['label' => 'Admission Faculties', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => MyHelpers::t($model->title), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="admission-faculties-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
