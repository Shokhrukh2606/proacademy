<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
    <?=$model->avatar?Html::img('@web/postfiles'.$model->avatar, ['style'=>'width:100px; height: 100px; object-fit: cover; ']):''?>

    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'last_name')->textInput() ?>
    <?= $form->field($model, 'middle_name')->textInput() ?>
    <?= $form->field($model, 'password')->passwordInput()  ?>
    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'status')->textInput(['value'=>10]) ?>
    <?= $form->field($model, 'image')->fileInput() ?>
    <?= Html::dropDownList('role','', ['admin' => 'Администратор', 'mastermoderator' => 'Научный руководитель магистратуры','moderator'=>'Модератор новостей'], ['prompt' => 'Выберите роль', 'class'=>'form-group', 'required'=>true]) ?>
    <?= Html::dropDownList('type','', ['admin' => 'Админ', 'moderator' => 'Модератор', 'master'=>'Степень магистра'], ['prompt' => 'Выберите тип', 'class'=>'form-group', 'required'=>true]) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
