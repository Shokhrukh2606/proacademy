<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

@app\assets\AdminAsset::register($this);

?>

<div class="post-form">
    <?php $form = ActiveForm::begin();
    $hasLagnguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            // 'content' => "<div>". $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->title)[$item]]).$form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->content)[$item]])."</div>"
            'content' => "<div>" .
                $form->field($model, "jtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->title)[$item]])
                . "</div>"
        ];
    }, ['ru', 'uz-Cyr', "uz-Lat", 'en']);

    ?>

    <?php
    echo yii\bootstrap\Tabs::widget([
        'items' => $hasLagnguage
    ]);
    ?>
    <?= $form->field($model, 'alias')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList(['active' => 'Aктивный', 'inactive' => 'Неактивный']) ?>
    <?= $form->field($model, 'prop', ['labelOptions'=>['label'=>'На главную страницу']])->dropDownList(['active' => 'Aктивный', 'inactive' => 'Неактивный']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>