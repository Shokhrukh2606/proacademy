<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicantsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="applicants-search mt-5">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php //echo  $form->field($model, 'id'); ?>

    <?php //echo $form->field($model, 'user_id'); ?>

    <?=$form->field($model, 'unique_code')->label('Фильтр по единственный код участника') ?>


    <?php // echo $form->field($model, 'avatar'); ?>

    <?php // echo $form->field($model, 'applicant_type') ?>

    <?php // echo $form->field($model, 'birth_date') ?>

    <?php // echo $form->field($model, 'district_birth') ?>

    <?php // echo $form->field($model, 'district_temporary') ?>

    <?php // echo $form->field($model, 'address_temporary') ?>

    <?php // echo $form->field($model, 'district_permanent') ?>

    <?php // echo $form->field($model, 'address_permanent') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'telegram_phone') ?>

    <?php // echo $form->field($model, 'nationality') ?>

    <?php // echo $form->field($model, 'other_nationality') ?>

    <?php // echo $form->field($model, 'diplom_number') ?>

    <?php // echo $form->field($model, 'high_school_name') ?>

    <?php // echo $form->field($model, 'high_school_specialty') ?>

    <?php // echo $form->field($model, 'high_school_finished') ?>

    <?php // echo $form->field($model, 'inner_recomendation') ?>

    <?php // echo $form->field($model, 'can_speak') ?>

    <?php // echo $form->field($model, 'other_can_speak') ?>

    <?php // echo $form->field($model, 'faculty') ?>

    <?php // echo $form->field($model, 'view_status') ?>

    <?php // echo $form->field($model, 'diploma') ?>

    <?php // echo $form->field($model, 'passport') ?>

    <?php // echo $form->field($model, 'certificate') ?>

    <?php // echo $form->field($model, 'biography') ?>

    <?php // echo $form->field($model, 'relatives') ?>

    <?php // echo $form->field($model, 'career') ?>

    <?php // echo $form->field($model, 'step') ?>

    <?php // echo $form->field($model, 'essay_result') ?>

    <?php // echo $form->field($model, 'speaking_result') ?>

    <?php // echo $form->field($model, 'language_result') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Сброс', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
