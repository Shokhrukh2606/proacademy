<?php

use app\components\helpers\MyHelpers;
use yii2mod\editable\EditableColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApplicantsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'applicants_grading');
$this->params['breadcrumbs'][] = $this->title;

$expcolumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'unique_code'
    ],
    [
        'attribute' => 'faculty',
        'value' => function ($model) {
            if ($model->faculty) {
                return MyHelpers::getFaculty($model->faculty);
            }
        }
    ],
    [
        'attribute' => 'essay_result'
    ],
    [
        'attribute' => 'speaking_result'
    ],
    [
        'attribute' => 'language_result'
    ],
    [
        'attribute' => 'overall_result'
    ]

];
?>
<style>
    table thead th{
        text-align: center;
    }
</style>
<div class="applicants-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
        echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $expcolumns,
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_CSV => false
        ]
    ]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'user_id',
            [
                'attribute' => 'unique_code'
            ],
            // 'academic_year_id',
            // 'avatar',
            //'applicant_type',
            //'birth_date',
            //'district_birth',
            //'district_temporary',
            //'address_temporary',
            //'district_permanent',
            //'address_permanent',
            //'phone',
            //'telegram_phone',
            //'nationality',
            //'other_nationality',
            //'diplom_number',
            //'high_school_name',
            //'high_school_specialty',
            //'high_school_finished',
            //'inner_recomendation',
            //'can_speak',
            //'other_can_speak',
            [
                'attribute' => 'faculty',
                'value' => function ($model) {
                    if ($model->faculty) {
                        return MyHelpers::getFaculty($model->faculty);
                    }
                }
            ],
            //'view_status',
            //'diploma',
            //'passport',
            //'certificate',
            //'biography',
            //'relatives',
            //'career',
            //'step',
            [
                'class' => EditableColumn::class,
                'attribute' => 'essay_result',
                'url' => ['change-username'],
                'editableOptions' => function ($model) {
                    return [
                        'source' => 0,
                        'value' => $model->essay_result,
                    ];
                },
            ],
            [
                'class' => EditableColumn::class,
                'attribute' => 'speaking_result',
                'url' => ['change-username'],
                'editableOptions' => function ($model) {
                    return [
                        'source' => 0,
                        'value' => $model->speaking_result,
                    ];
                },
            ],
            [
                'class' => EditableColumn::class,
                'attribute' => 'language_result',
                'url' => ['change-username'],
                'editableOptions' => function ($model) {
                    return [
                        'value' => $model->language_result,
                    ];
                },
            ],
            [
                'attribute' => 'overall_result',
            ]
            //'speaking_result',
            //'language_result',
        ],
    ]); ?>


</div>