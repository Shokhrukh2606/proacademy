<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\helpers\MyHelpers;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Applicants */

$this->title = $user->last_name . ' ' . $user->username . ' ' . $user->middle_name;
$this->params['breadcrumbs'][] = ['label' => 'Applicants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="applicants-view">
    <div class="my-row">
        <div class="my-col-8">
            <h1 id="hero-applicant"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="my-col-4">
            <?= Html::img('@web/uploads'.$model->avatar) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'unique_code',
                    [
                        'attribute' => 'applicant_type',
                        'value' => function ($model) {
                            return  MyHelpers::getAppType($model->applicant_type);
                        }
                    ],
                    'passport_serial_number',
                    'passport_given_date',
                    'passport_given_from',
                    [
                        'attribute' => 'faculty',
                        'value' => function ($model) {
                            return  MyHelpers::getFaculty($model->faculty);
                        }
                    ],
                    'birth_date',
                    [
                        'attribute' => 'city_birth',
                        'value' => function ($model) {
                            return MyHelpers::getCityDistrict($model->district_birth)->city;
                        }
                    ],
                    [
                        'attribute' => 'district_birth',
                        'value' => function ($model) {
                            return MyHelpers::getCityDistrict($model->district_birth)->district;
                        }
                    ],
                    'phone',
                    'telegram_phone',
                    [
                        'attribute' => 'city_permanent',
                        'value' => function ($model) {
                            return MyHelpers::getCityDistrict($model->district_permanent)->city;
                        }
                    ],
                    [
                        'attribute' => 'district_permanent',
                        'value' => function ($model) {
                            return MyHelpers::getCityDistrict($model->district_permanent)->district;
                        }
                    ],
                    'address_permanent',
                    [
                        'attribute' => 'city_temporary',
                        'value' => function ($model) {
                            return MyHelpers::getCityDistrict($model->district_temporary)->city;
                        }
                    ],
                    [
                        'attribute' => 'district_temporary',
                        'value' => function ($model) {
                            return MyHelpers::getCityDistrict($model->district_temporary)->district;
                        }
                    ],
                    'address_temporary',

                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'nationality',
                        'label'=>Yii::t('front', 'nationality'),
                        'value' => function ($model) {
                            return  $model->nationality!=5?MyHelpers::getNationality($model->nationality):$model->nationality;
                        }
                    ],
                    // 'other_nationality',
                    // [
                    //     'attribute' => 'can_speak',
                    //     'value' => function ($model) {
                    //         return MyHelpers::getCanSpeak($model->can_speak);
                    //     }
                    // ],
                    'diplom_number',
                    'high_school_name',
                    'high_school_specialty',
                    'high_school_finished',
                    [
                        'attribute' => 'passport',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(Yii::t('front', 'view'), Url::to('@web/uploads' .$model->passport), ['class' => $model->passport==null?'btn btn-danger showFileAjax':'btn btn-info showFileAjax']);
                        }
                    ],
                    [
                        'attribute' => 'diploma',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(Yii::t('front', 'view'), Url::to('@web/uploads' .$model->diploma), ['class' =>$model->diploma==null?'btn btn-danger showFileAjax': 'btn btn-info showFileAjax']);
                        }
                    ],
                    [
                        'attribute' => 'biography',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(Yii::t('front', 'view'), Url::to('@web/uploads' .$model->biography), ['class' => $model->biography==null?'btn btn-danger showFileAjax':'btn btn-info showFileAjax']);
                        }
                    ],
                    [
                        'visible'=>$model->applicant_type=='inner'?true:false,
                        'attribute' => 'inner_recomendation',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(Yii::t('front', 'view'), Url::to('@web/uploads' .$model->inner_recomendation), ['class' => $model->inner_recomendation==null?'btn btn-danger showFileAjax':'btn btn-info showFileAjax']);
                        }
                    ],
                    [
                        'visible'=>$model->applicant_type=='inner'?true:false,
                        'attribute' => 'decision',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(Yii::t('front', 'view'), Url::to('@web/uploads' .$model->decision), ['class' => $model->decision==null?'btn btn-danger showFileAjax':'btn btn-info showFileAjax']);
                        }
                    ],
                    [
                        'attribute' => 'certificate',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(Yii::t('front', 'view'), Url::to('@web/uploads' .$model->certificate), ['class' => $model->certificate==null?'btn btn-danger showFileAjax':'btn btn-info showFileAjax']);
                        }
                    ],
                    [
                        'attribute' => 'relatives',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(Yii::t('front', 'view'), Url::to('@web/uploads' .$model->relatives), ['class' =>$model->relatives==null?'btn btn-danger showFileAjax': 'btn btn-info showFileAjax']);
                        }
                    ],
                    [
                        'attribute' => 'career',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a(Yii::t('front', 'view'), Url::to('@web/uploads' .$model->career), ['class' => $model->career==null?'btn btn-danger showFileAjax':'btn btn-info showFileAjax']);
                        }
                    ]
                ],
            ]) ?>
        </div>
    </div>

    <div class="ajaxContent" id="ajaxContent">

    </div>
</div>