<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Applicants */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="applicants-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'unique_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'academic_year_id')->textInput() ?>

    <?= $form->field($model, 'avatar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'applicant_type')->dropDownList([ 'inner' => 'Inner', 'outer' => 'Outer', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'birth_date')->textInput() ?>

    <?= $form->field($model, 'district_birth')->textInput() ?>

    <?= $form->field($model, 'district_temporary')->textInput() ?>

    <?= $form->field($model, 'address_temporary')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'district_permanent')->textInput() ?>

    <?= $form->field($model, 'address_permanent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telegram_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nationality')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'other_nationality')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'diplom_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'high_school_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'high_school_specialty')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'high_school_finished')->textInput() ?>

    <?= $form->field($model, 'inner_recomendation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'can_speak')->textInput() ?>

    <?= $form->field($model, 'other_can_speak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'faculty')->textInput() ?>

    <?= $form->field($model, 'view_status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'diploma')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passport')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'certificate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biography')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'relatives')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'career')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'step')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
