<?php

use app\components\helpers\MyHelpers;
use app\models\AdmissionFaculties;
use app\models\Cities;
use app\models\Messages;
use app\models\User;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use kartik\export\ExportMenu;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'avatar',
        'format' => 'html',
        'value' => function ($model) {
            return  Html::img('@web/uploads' . $model->avatar, ['style' => 'width:80px; height: 80px; object-fit: contain']);
        }
    ],
    [
        'attribute' => 'unique_code',
    ],
    [
        'attribute' => 'can_speak',
        'label' => Yii::t('admin', 'full_name'),
        'value' => function ($model) {
            return         User::findOne(['id' => $model->user_id])->getFullName();
        }
    ],
    [
        'attribute' => 'phone',
    ],
    [
        'attribute' => 'faculty',
        'value' => function ($model) {
            if ($model->faculty) {
                return MyHelpers::getFaculty($model->faculty);
            }
        },
        'filter' => Html::activeDropDownList(
            $searchModel,
            'faculty',
            \yii\helpers\ArrayHelper::map(AdmissionFaculties::find()->asArray()->all(), 'id', function ($item) {
                return MyHelpers::t($item['title']);
            }),
            [
                'class' => 'form-control', 'multiple' => false,
                'prompt' => Yii::t('admin', 'all'),
            ]
        )
    ],
    [
        'attribute' => 'can_speak',
        'value' => function ($model) {
            if ($model->faculty) {
                return MyHelpers::getCanSpeak($model->can_speak);
            }
        },
        'filter' => Html::activeDropDownList(
            $searchModel,
            'can_speak',
            [
                '1' => Yii::t('front', 'enlang'),
                '2' => Yii::t('front', 'gnlang'),
                '3' => Yii::t('front', 'frlang')
            ],
            [
                'class' => 'form-control', 'multiple' => false,
                'prompt' => Yii::t('admin', 'all'),
            ]
        )
    ],
    [
        'label' => Yii::t('admin', 'status'),
        'format' => 'raw',
        'value' => function ($model) {
            return Html::button(MyHelpers::getStatus($model->status), ['class' => MyHelpers::getStatus($model->status, true)]);
        },
        'filter' => Html::activeDropDownList(
            $searchModel,
            'status',
            [
                'unknown' => Yii::t('admin', 'unknown'),
                'accepted' => Yii::t('admin', 'accepted'),
                'finished' => Yii::t('admin', 'finished'),
                'aborted' => Yii::t('admin', 'aborted'),
                'cancelled' => Yii::t('admin', 'cancelled'),
                'updated' => Yii::t('admin', 'updated_status'),
            ],
            [
                'class' => 'form-control', 'multiple' => false,
                'prompt' => Yii::t('admin', 'all'),
            ]
        )
    ],
    [
        'attribute' => 'applicant_type',
        'value' => function ($item) {
            return MyHelpers::getAppType($item->applicant_type);
        },
        'filter' => Html::activeDropDownList(
            $searchModel,
            'applicant_type',
            [
                'inner' => Yii::t('front', 'inner'),
                'outer' => Yii::t('front', 'outer'),
            ],
            [
                'class' => 'form-control', 'multiple' => false,
                'prompt' => Yii::t('admin', 'all'),
            ]
        )
    ],
    [
        'attribute' => 'city_birth',
        'value' => function ($item) {
            return MyHelpers::getCityDistrict($item->district_birth, Yii::$app->language)->city;
        },
        'filter' => Html::activeDropDownList(
            $searchModel,
            'city_birth',
            ArrayHelper::map(Cities::find()->all(), 'id', function ($item) {
                return MyHelpers::t($item->title);
            }),
            [
                'class' => 'form-control', 'multiple' => false,
                'prompt' => Yii::t('admin', 'all'),
            ]
        )
    ],
    [
        'label' => Yii::t('front', 'actions'),
        'format' => 'raw',
        'value' => function ($model) {
            return Html::a(Yii::t('admin', 'show_doc'), Url::to(['applicants/view', 'id' => $model->id], true), ['class' => 'btn btn-success btn-xs showAjax', 'data-applicant' => $model->id]);
        }
    ],
    ['class' => 'yii\grid\CheckboxColumn'],

];

$script = <<< JS
    var url_win=window.location.origin
    var cur_url=url_win+'/ru/admin/applicants'
    var current_app=0;
    var message_box=$(".message-box")
    var message_area=$('textarea[name="message"]')
    var param = $('meta[name=csrf-param]').attr("content");
    var token = $('meta[name=csrf-token]').attr("content");
    var loading=$("#preloader")
    $(document)
    .ajaxStart(function () {
        loading.css({
            "display": "flex"
        });
    })
    .ajaxStop(function () {
        loading.css({
            "display": "none"
        });
    });
    $("#activator").click((e)=>{
        e.preventDefault();
        var data = {};
            data[param] = token;
            data['id']=current_app
            $.ajax({
                url :  cur_url+'/make-active-one',
                type: 'POST',
                data: data,
                success: function(response, textStatus, jqXHR) {
                    if(response.success){
                        location.reload();
                    }else{
                        alert("Xatolik yuz berdi")
                    }
                }
            })
    })
    $("#cancel").click((e)=>{
        e.preventDefault();
        $("#send-one-message").attr("data-cause", "fully-abort")
        if(message_area.val()!=''){
            var data = {};
            data[param] = token;
            data['id']=current_app
            data['message']=message_area.val()
            $.ajax({
                url :  cur_url+'/make-fully-aborted-one',
                type: 'POST',
                data: data,
                success: function(response, textStatus, jqXHR) {
                    if(response.success){
                        location.reload();
                    }else{
                        alert("Xatolik yuz berdi")
                    }
                }
            })
        }else{
            message_box.fadeIn()
        }
    })
    $("#retake").click((e)=>{
        e.preventDefault();
        $("#send-one-message").attr("data-cause", "abort")
            if(message_area.val()!=''){
                var data = {};
                data[param] = token;
                data['id']=current_app
                data['message']=message_area.val()
                $.ajax({
                    url :  cur_url+'/make-aborted-one',
                    type: 'POST',
                    data: data,
                    success: function(response, textStatus, jqXHR) {
                        if(response.success){
                            location.reload();
                        }else{
                            alert("Xatolik yuz berdi")
                        }
                    }
                })
            }else{
                message_box.fadeIn()
            }
    })
    $("#send-one-message").click((e)=>{
        tinyMCE.triggerSave();
        e.preventDefault();
        var cause=$("#send-one-message").data("cause")
        switch(cause){
            case 'abort':
                $("#retake").trigger("click")
                break;
            case 'fully-abort':
                $("#cancel").trigger("click")
                break;
            default: 
                alert("Xatolik mavjud!")
        }
    })
   
    $("#acceptDocs").click(function(e){
        e.preventDefault();
        var keys = $('#applicants_table').yiiGridView('getSelectedRows');       
        if(keys.length){
            var data = {};
            data[param] = token;
            data['users']=keys
            $.ajax({
                url :  cur_url+'/make-active',
                type: 'POST',
                data: data,
                success: function(response, textStatus, jqXHR) {
                    if(response.success){
                        $(".select-on-check-all").prop('checked',false)
                        $("#applicants_table").find('input[name="selection[]"]').prop('checked', false);
                        location.reload();
                    }
                }
            })
        }else{
            alert('Кандидаты не были отобраны')
        }
    })
    $("#sendMessage").click(function(e){
        e.preventDefault();
        var keys = $('#applicants_table').yiiGridView('getSelectedRows');       
        var message_id=$('select[name="message_id"]').children("option:selected").val();
        if(message_id>0){
            var data = {};
            data[param] = token;
            data['users']=keys
            data['message_id']=message_id
            $.ajax({
                url :  cur_url+'/send-message',
                type: 'POST',
                data: data,
                success: function(response, textStatus, jqXHR) {
                    if(response.success){
                        console.log(response.success)
                        $(".select-on-check-all").prop('checked',false)
                        $("#applicants_table").find('input[name="selection[]"]').prop('checked', false);
                        $("#secondModal").modal('toggle');
                        alert('Сообщения отправлены')
                    }
                }
            })
        }else{
            alert('Название сообщения не выбрано')
        }
    })
    $("#openSend").click(function(e){
        e.preventDefault();
        var keys = $('#applicants_table').yiiGridView('getSelectedRows');       
        if(keys.length){
            var secondModal=$("#secondModal")
           secondModal.modal('show');
        }else{
            alert('Кандидаты не были отобраны')
        }
    })
    $(".showAjax").click(function(e){
        e.preventDefault();
        self=$(this)
        $.ajax({
            url :  self.attr("href"),
        }).done(function (data) {
            current_app=self.data("applicant")
            var my_modal=$('#myModal');
            $("#myModal .modal-body").html(data)
            $(".showFileAjax").click(function(e){
                e.preventDefault();
                var self=$(this)
                var contentBlock=$(".ajaxContent")
                if(contentBlock.hasClass("used")){
                    contentBlock.empty()
                    $('<iframe>', {
                        src: self.attr("href"),
                        id:  'docsFrame',
                        frameborder: 0,
                        scrolling: 'yes'
                    }).appendTo(contentBlock).ready(function(){
                        document.getElementById('ajaxContent').scrollIntoView();
                    });
                }else{
                    $('<iframe>', {
                        src: self.attr("href"),
                        id:  'docsFrame',
                        frameborder: 0,
                        scrolling: 'yes'
                    }).appendTo(contentBlock).ready(function(){
                        document.getElementById('ajaxContent').scrollIntoView();
                        contentBlock.addClass("used")
                    });
                } 
            })
            my_modal.modal('show');
        }).fail(function () {
            alert('Error in Loading Posts.');
        });
    })
    $(document).keyup(function(e) {
        e.preventDefault()
        var charCode = e.charCode || e.keyCode || e.which
        if (charCode === 27) {
            document.getElementById('hero-applicant').scrollIntoView();
        }
    });
JS;
$this->registerJs($script, View::POS_READY);

$this->title = Yii::t('admin', 'app_info');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table thead th{
        text-align: center;
    }
</style>
<div class="modal fade" data-keyboard="false" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= Yii::t('admin', 'app_info') ?></h4>
                <button class="btn btn-info" id="activator"><?= Yii::t('admin', 'accept_app') ?></button>
                <button class="btn btn-warning" id="retake"><?= Yii::t('admin', 'cancel_app') ?></button>
                <button class="btn btn-danger" id="cancel"><?= Yii::t('admin', 'full_cancel_app') ?></button>
                <div class="message-box d-none">
                    <?= TinyMce::widget([
                        // 'options' => [
                        //     'rows' => 18,
                        //     'value' => yii\helpers\Json::decode($model->content)[$item],
                        // ],
                        'name' => 'message',
                        'attribute' => 'input',
                        'language' => 'ru',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table paste image"
                            ],
                            // 'theme'=>"advanced",
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ]) ?>
                    <button id="send-one-message" data-cause="abort">Yuborish</button>
                </div>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" data-keyboard="false" id="secondModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= Yii::t('admin', 'message_templates') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 text-center">
                        <?= Html::dropDownList(
                            'message_id',
                            null,
                            ArrayHelper::map(Messages::find()->all(), 'id', 'title'),
                            ['prompt' => 'Выберите название сообщения']
                        ) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-12 mt-5 text-center">
                        <button class="btn btn-info" id="sendMessage"><?= Yii::t('admin', 'send_message') ?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="applicants-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <button class="btn btn-info btn-sm" id="openSend"><?= Yii::t('admin', 'send_message') ?></button>
    <button class="btn btn-info btn-sm" id="acceptDocs"><?= Yii::t('admin', 'accept_app_all') ?></button>
    <a class="btn btn-info btn-sm" href="<?= Url::to('zip') ?>"><?= Yii::t('admin', 'download_archives') ?></a>
    <?php
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_CSV => false
        ]
    ]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'options' => ['id' => 'applicants_table', 'style'=>'overflow-y:auto;']
    ]); ?>


</div>