<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title ='Межведомственное сотрудничество';
$this->params['breadcrumbs'][] = $this->title;
$request = Yii::$app->request;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1> 
    <p>
        <?= Html::a('Добавить', ['create', 'group'=>$_GET['group']], ['class' => 'btn btn-success']) ?>
    </p>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'label' => 'Заголовок',
                'value' => function ($item) {
                    return (json_decode($item->title))->ru;
                }
            ],
            [
                'attribute' => 'short_content',
                'label' => 'Короткое содержание',
                'value' => function ($model) {
                    return (json_decode($model->short_content)->ru);
                }
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'contentOptions' => function ($model) {
                    return  ['class' => ($model->status == 'active') ? 'bg-green' : 'bg-yellow'];
                },
                'value'=>function($model){
                    return ($model->status == 'active') ? 'Активный' : 'Неактивный';
                }
            ],
            // 'category_id',
            // 'category_id2',
            // 'group',
            // 'alias:ntext',
            //'title:ntext',
            //'content:ntext',
            //'content_html:ntext',
            //'short_content:ntext',
            //'status',
            //'option',
            //'options',
            //'views',
            //'tags:ntext',
            //'meta_title:ntext',
            //'meta_description:ntext',
            //'meta_keywords:ntext',
            //'sort_order',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>