<?php

use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */

@app\assets\AdminAsset::register($this);


?>
<div class="post-form">
    <?php $form = ActiveForm::begin();
    $hasLagnguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            // 'content' => "<div>". $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->title)[$item]]).$form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->content)[$item]])."</div>"
            'content' => "<div>" .
                $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->title)[$item]]) .
                $form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Настройка значения"]])->textarea(['style'=>'height:200px;','maxlength' => true, 'value' => yii\helpers\Json::decode($model->content)[$item]]) .
                "</div>"
        ];
    }, ['ru', 'uz-Cyr', "uz-Lat", 'en']);

    ?>

    <?php
    $this->registerJs(
        "$('#showLinkInput').on('change', function(e) {
            var vals=e.target.selectedOptions[0].value
            if (vals==1||vals==2){
                $('#link_type').css({display: 'block'})
            }else{
                $('#link_type').css({display: 'none'})
            }
         });",
        View::POS_READY
    );
    echo yii\bootstrap\Tabs::widget([
        'items' => $hasLagnguage
    ]);
    ?>
    <?= $form->field($model, 'alias')->textInput() ?>
    <?= $form->field($model, 'group')->textInput(['maxlength' => true, 'readonly' => true, 'value' => $model->group]) ?>
    <!-- <?= $form->field($model, 'content_html')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'short_content')->textarea(['rows' => 6]) ?> -->

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Активный', 'inactive' => 'Неактивный'], ['class'=>'col-sm-6 form-control','value' => ($model->status == 'inactive') ? 'active' : ""]) ?>
    <?=Html::label('Тип загрузок')?>
    <?=Html:: dropDownList( 'type', '',['1' => 'Youtube', '2' => 'Mover'], ['prompt'=>'Загрузит из другого сайта', 'class'=>'form-control col-sm-6','id'=>'showLinkInput']) ?>
    <?= $form->field($model, 'link', ['labelOptions'=>['label'=>'Ссылка на сайт']])->textInput(['class'=>'form-control d-none ', 'id'=>'link_type']) ?>     
    <!-- <?= $form->field($model, 'option')->dropDownList(['yes' => 'Yes', 'no' => 'No',], ['prompt' => '']) ?> -->
    <!-- <?= $form->field($model, 'options')->textInput() ?> -->


    <!-- <?= $form->field($model, 'tags')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?> -->
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>