<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
@app\assets\AdminAsset::register($this);
@app\assets\DropzoneAsset::register($this);

?>

<div class="post-form">
    <?php $form = ActiveForm::begin();
    $hasLagnguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            // 'content' => "<div>". $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->title)[$item]]).$form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->content)[$item]])."</div>"
            'content' => "<div>" .
                $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->title)[$item]]) .
                $form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент"]])->widget(TinyMce::className(), [
                    'options' => [
                        'rows' => 18,
                        'value' => yii\helpers\Json::decode($model->content)[$item],
                    ],
                    'language' => 'ru',
                    'clientOptions' => [
                        'file_browser_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                            window.open('".yii\helpers\Url::toRoute([Yii::getAlias('@web').'imagemanager/manager', 'view-mode'=>'iframe', 'select-type'=>'tinymce'])."&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                        }"),
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table paste image"
                        ],
                        // 'theme'=>"advanced",
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ]
                ])
                . "</div>"
        ];
    }, ['ru', 'uz-Cyr', "uz-Lat", 'en']);
    ?>

    <?php
    $this->registerJs(
        "$('#myButton').on('click', function() { alert('Button clicked!'); });",
        'my-button-handler'
    );
    echo yii\bootstrap\Tabs::widget([
        'items' => $hasLagnguage
    ]);
    ?>
    <?= $form->field($model, 'icon')->textInput()->label('Иконка') ?> 
    <?= $form->field($model, 'link')->textInput()->label('Ссылка') ?> 

    <?= $form->field($model, 'alias')->textInput() ?>

    <!-- <?= $form->field($model, 'content_html')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'short_content')->textarea(['rows' => 6]) ?> -->

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Активный', 'inactive' => 'Неактивный'], ['value' => ($model->status == 'inactive') ? 'active' : ""]) ?>

    <!-- <?= $form->field($model, 'option')->dropDownList(['yes' => 'Yes', 'no' => 'No',], ['prompt' => '']) ?> -->


    <!-- <?= $form->field($model, 'views')->textInput() ?> -->

    <!-- <?= $form->field($model, 'tags')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?> -->
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php ActiveForm::begin(['action' => ['post/image'], 'options' => ['id'=>'id_dropzone', 'class'=>'dropzone', 'enctype' => 'multipart/form-data']]); ?>
    <?php ActiveForm::end(); ?> 
</div>