<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

// $this->title = 'Update Post: ' . $model->title;
// $this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="post-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <?php echo FileInput::widget([
        'name' => 'file',
        'options' => [
            'multiple' => true,
            'id' => 'hyperFiles'
        ],
        'pluginOptions' => [
            'uploadUrl' => Url::to(['post/image', 'id' => $model->id, 'group' => $model->group]),
            'maxFileCount' => 10,
            'uploadAsync' => true,
            'showUpload' => false,
            'overwriteInitial' => false,
            'minFileCount' => 1,
            'maxFileCount' => 10,
            'initialPreviewAsData' => true,
            'browseOnZoneClick' => true,
            'initialPreview' => MyHelpers::getMediaUrls($files),
            'initialPreviewConfig' =>
            MyHelpers::getEachMediaInfo($files),
            'maxFileSize' => 10000,
        ],
        'pluginEvents' => [
            'filebatchselected' => '
            function(event, files) {
                $("#hyperFiles").fileinput("upload");
            }
            '
        ]
    ]); ?>
</div>