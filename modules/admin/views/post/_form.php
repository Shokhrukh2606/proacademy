<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">
    <?php $form = ActiveForm::begin();
    $hasLagnguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            // 'content' => "<div>". $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->title)[$item]]).$form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->content)[$item]])."</div>"
            'content' => "<div>" .
                $form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент( {$item} )"]])->widget(TinyMce::className(), [
                    'options' => [
                        'rows' => 18,
                        'value' => yii\helpers\Json::decode($model->content)[$item],
                    ],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table paste image imagetools"
                        ],
                        'force_br_newlines' => true,
                        'force_p_newlines' => false,
                        'forced_root_block' => '',
                        'initialValue' =>   yii\helpers\Json::decode($model->title)[$item],
                        'file_picker_callback' => new JsExpression("function(cb, value, meta) {
                            var input = document.createElement('input');
                            input.setAttribute('type', 'file');
                            input.setAttribute('accept', 'image/*');
                            
                            // Note: In modern browsers input[type=\"file\"] is functional without 
                            // even adding it to the DOM, but that might not be the case in some older
                            // or quirky browsers like IE, so you might want to add it to the DOM
                            // just in case, and visually hide it. And do not forget do remove it
                            // once you do not need it anymore.

                            input.onchange = function() {
                            var file = this.files[0];
                            
                            var reader = new FileReader();
                            reader.onload = function () {
                                // Note: Now we need to register the blob in TinyMCEs image blob
                                // registry. In the next release this part hopefully won't be
                                // necessary, as we are looking to handle it internally.
                                var id = 'blobid' + (new Date()).getTime();
                                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                                var base64 = reader.result.split(',')[1];
                                var blobInfo = blobCache.create(id, file, base64);
                                blobCache.add(blobInfo);

                                // call the callback and populate the Title field with the file name
                                cb(blobInfo.blobUri(), { title: file.name });
                            };
                            reader.readAsDataURL(file);
                            };
                            
                            input.click();
                        }"),
                        // 'theme'=>"advanced",
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ]
                ])
                . "</div>"
        ];
    }, ['ru', 'uz', 'oz', 'en']);
    ?>

    <?php
    echo yii\bootstrap\Tabs::widget([
        'items' => $hasLagnguage
    ]);
    ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'category_id2')->textInput() ?>

    <?= $form->field($model, 'group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'content_html')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'short_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Active', 'inactive' => 'Inactive',], ['prompt' => '']) ?>

    <?= $form->field($model, 'option')->dropDownList(['yes' => 'Yes', 'no' => 'No',], ['prompt' => '']) ?>

    <?= $form->field($model, 'options')->textInput() ?>

    <?= $form->field($model, 'views')->textInput() ?>

    <?= $form->field($model, 'tags')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sort_order')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>