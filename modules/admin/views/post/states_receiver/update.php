<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

// $this->title = 'Update Post: ' . $model->title;
// $this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="post-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <?php echo FileInput::widget([
        'name' => 'file',
        'options' => [
            'multiple' => true
        ],
        'pluginOptions' => [
            'uploadUrl' => Url::to(['post/image', 'id' => $model->id, 'group' => $model->group]),
            'maxFileCount' => 10,
            'initialPreview' => MyHelpers::getMediaUrls($files),
            'initialPreviewConfig' =>
            MyHelpers::getEachMediaInfo($files),
            'initialPreviewAsData' => true,
            'overwriteInitial' => false,
            'maxFileSize' => 2800
        ]
    ]); ?>
</div>