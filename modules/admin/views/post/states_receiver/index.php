<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
$request = Yii::$app->request;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавить', ['create', 'group' => $_GET['group']], ['class' => 'btn btn-success']) ?>
    </p>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'f_name',
                'label' => 'Ф.И.О',
            ],
            [
                'attribute' => 'f_phone',
                'label' => 'Номер телефона'
            ],
            [
                'attribute' => 'f_email',
                'label' => 'Эл. адрес'
            ],
            [
                'attribute' => 'f_position',
                'label' => 'Место работы и должность'
            ],
            [
                'format' => 'raw',
                'attribute' => 'views',
                'label' => 'Статус',
                'value' => function ($item) {
                    return Html::tag('span', $item->views == 0 ? 'Новый' : 'Старый', ['class' => $item->views == 0  ?     'label label-primary':'label label-default']);
                }
            ],
            // 'category_id',
            // 'category_id2',
            // 'group',
            // 'alias:ntext',
            //'title:ntext',
            //'content:ntext',
            //'content_html:ntext',
            //'short_content:ntext',
            //'status',
            //'option',
            //'options',
            //'views',
            //'tags:ntext',
            //'meta_title:ntext',
            //'meta_description:ntext',
            //'meta_keywords:ntext',
            //'sort_order',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>