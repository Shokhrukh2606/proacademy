<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

// $this->title = $model->title;
// $this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'f_name',
                'label' => 'Ф.И.О',
            ],
            [
                'attribute' => 'f_phone',
                'label' => 'Номер телефона'
            ],
            [
                'attribute' => 'f_email',
                'label' => 'Эл. адрес'
            ],
            [
                'format' => 'raw',
                'attribute' =>  'link',
                'value' => function ($item) {
                    return Html::a('Скачать', Yii::getAlias('@web').'/postfiles/documents/' . $item->link);
                }
            ],
            [
                'attribute' => 'alias',
                'label' => 'Учёная степень и учёное звание'
            ],
            [
                'attribute' =>  'created_at',
                'label' => 'Дата создания'
            ],
        ],
    ]) ?>

</div>