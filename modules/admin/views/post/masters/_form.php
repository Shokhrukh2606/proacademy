<?php

use app\components\helpers\MyHelpers;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */

@app\assets\AdminAsset::register($this);

?>
<div class="post-form">
    <?php $form = ActiveForm::begin();
     $hasLagnguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            // 'content' => "<div>". $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->title)[$item]]).$form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->content)[$item]])."</div>"
            'content' => "<div>" .
                $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->title)[$item]]) .
                $form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент"]])->widget(TinyMce::className(), [
                    'options' => [
                        'rows' => 30,
                        'value' => yii\helpers\Json::decode($model->content)[$item],
                    ],
                    'clientOptions' => [
                        //                        'plugins' => [
                        //                            "advlist autolink lists link charmap print preview anchor",
                        //                            "searchreplace visualblocks code fullscreen",
                        //                            "insertdatetime media table paste image"
                        //                        ],
                        'block_formats' => 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
                        'fontsize_formats' => '8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt',
                        'disk_cache' => true,
                        'plugins' => [
                            // "glvrd",
                            "emoticons",
                            "code",
                            "lists",
                            "anchor",
                            "fullpage",
                            "fullscreen",
                            "autosave",
                            "wordcount",
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                            "table directionality emoticons paste code",
                            "save autosave template",
                        ],
                        'file_browser_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                        window.open('" . yii\helpers\Url::to(['imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'tinymce']) . "&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                    }"),
                        'theme_advanced_buttons1' => "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
                        //                         'theme'=>"advanced",
                        'toolbar' => "undo redo | glvrd | fullpage | fullscreen | fontselect | fontsizeselect | emoticons | code | anchor | undo redo | forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media",
                    ]
                ]).
                $form->field($model, "lshcontent[{$item}]", ['labelOptions'=>['label'=>"Ссылка ({$item})"]])->textInput(['maxlength' => true, 'value' =>MyHelpers::t($model->short_content, $item)]).
                "</div>"
        ];
    }, ['ru', 'uz-Cyr', "uz-Lat", 'en']);

    ?>

    <?php
    $this->registerJs(
        "$('#myButton').on('click', function() { alert('Button clicked!'); });",
        'my-button-handler'
    );
    echo yii\bootstrap\Tabs::widget([
        'items' => $hasLagnguage
    ]);
    ?>
    <?= $form->field($model, 'alias')->textInput() ?>
    <?= $form->field($model, 'group')->textInput(['maxlength' => true, 'readonly' => true, 'value' => $model->group]) ?>
    <!-- <?= $form->field($model, 'content_html')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'short_content')->textarea(['rows' => 6]) ?> -->

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Активный', 'inactive' => 'Неактивный'], ['value' => ($model->status == 'inactive') ? 'active' : ""]) ?>

    <!-- <?= $form->field($model, 'option')->dropDownList(['yes' => 'Yes', 'no' => 'No',], ['prompt' => '']) ?> -->
    <!-- <?= $form->field($model, 'options')->textInput() ?> -->

    <!-- <?= $form->field($model, 'views')->textInput() ?> -->

    <!-- <?= $form->field($model, 'tags')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?> -->
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>