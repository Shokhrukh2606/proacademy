<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use kartik\file\FileInput;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */

@app\assets\AdminAsset::register($this);
@app\assets\DropzoneAsset::register($this);

?>

<div class="post-form">
    <?php $form = ActiveForm::begin();
    $hasLagnguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            // 'content' => "<div>". $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->title)[$item]]).$form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->content)[$item]])."</div>"
            'content' => "<div>" .
                $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->title)[$item]]) .
                $form->field($model, "lshcontent[{$item}]", ['labelOptions' => ['label' => "Правильный ответ(Ответ А)"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->short_content)[$item]]) .
                $form->field($model, "mtitle[{$item}]", ['labelOptions' => ['label' => "Ответ B"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->meta_title)[$item]]) .
                $form->field($model, "mdescription[{$item}]", ['labelOptions' => ['label' => "Ответ C"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->meta_description)[$item]]) .
                $form->field($model, "mkeywords[{$item}]", ['labelOptions' => ['label' => "Ответ D"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->meta_keywords)[$item]]) 
                
                . "</div>"
        ];
    }, ['ru', 'uz-Cyr', "uz-Lat", 'en']);

    ?>

    <?php
    $this->registerJs(
        "$('#myButton').on('click', function() { alert('Button clicked!'); });",
        'my-button-handler'
    );
    echo yii\bootstrap\Tabs::widget([
        'items' => $hasLagnguage
    ]);
    ?>

    <?= $form->field($model, 'alias')->textInput() ?>
    <?= $form->field($model, 'group')->textInput(['maxlength' => true, 'readonly' => true, 'value' => $model->group]) ?>
    <!-- <?= $form->field($model, 'content_html')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'short_content')->textarea(['rows' => 6]) ?> -->

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Активный', 'inactive' => 'Неактивный'], ['value' => ($model->status == 'inactive') ? 'active' : ""]) ?>

    <!-- <?= $form->field($model, 'option')->dropDownList(['yes' => 'Yes', 'no' => 'No',], ['prompt' => '']) ?> -->

    <!-- <?= $form->field($model, 'views')->textInput() ?> -->

    <!-- <?= $form->field($model, 'tags')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?> -->
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>