<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Последние добавленные вещи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <!-- <h1><?= Html::encode('Последние добавленные вещи') ?></h1> -->

    <p>
        <?= Html::a('Добавить пость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category_id',
            'category_id2',
            'group',
            'alias:ntext',
            //'title:ntext',
            //'content:ntext',
            //'content_html:ntext',
            //'short_content:ntext',
            //'status',
            //'option',
            //'options',
            //'views',
            //'tags:ntext',
            //'meta_title:ntext',
            //'meta_description:ntext',
            //'meta_keywords:ntext',
            //'sort_order',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
