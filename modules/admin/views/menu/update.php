<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = \Yii::t('admin', 'menu');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('admin', 'menu'), 'url' => ['index']];
?>
<div class="menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
