<?php

use yii\web\View;
use yii\helpers\Html;

$this->registerJs("
    const current_origin=window.location.origin
    unflatten = function( array, parent, tree ){
        tree = typeof tree !== 'undefined' ? tree : [];
        parent = typeof parent !== 'undefined' ? parent : { id: 0 };
            
        var children = _.filter( array, function(child){ return child.category_id == parent.id; });
        
        if( !_.isEmpty( children )  ){
            if( parent.id == 0 ){
               tree = children;   
            }else{
               parent['children'] = children
            }
            _.each( children, function( child ){ unflatten( array, child ) } );                    
        }
        
        return tree;
    }
    const  menus=${dataProvider}.map(item=>({
        \"id\": item.id,
        \"category_id\": item.category_id,
        \"children\": null,
        \"name\": JSON.parse(item.title).ru,
        \"status\": item.status
    }));
    const structured=unflatten(menus)
    // console.log(structured)
    $('#listWithHandle').tree({
        data: structured,
        autoOpen: true,
        dragAndDrop: true,
        onCreateLi: function(node, lit) {
            // Append a link to the jqtree-element div.
            // The link has an url '#node-[id]' and a data property 'node-id'.
            lit.find('.jqtree-element').append(
                '<div class=\"action-navs\">'+
                '<div class=\"material-switch pull-right\" style=\"margin-right: 15px;\"><input id=\"someSwitchOptionSuccess'+node.id +'\"' + (node.status== \"active\" ? ` checked data-id=\"\${node.id}\"` : ` data-id=\"\${node.id}\"`) + ' name=\"someSwitchOption\" type=\"checkbox\" />'+'<label for=\"someSwitchOptionSuccess'+node.id+'\" class=\"label-success\"></label></div>'+
                '<a href=\"'+current_origin+'/ru/admin/menu/update?id='+ node.id+ '\" class=\"edit circle\" data-node-id=\"'+ node.id +'\"><span class=\"glyphicon glyphicon-pencil\"></span></a>'+
                //+ '<a href=\"'+current_origin+'/ru/admin/menu/delete?id='+ node.id+ '\" class=\"edit circle red\" data-pjax=\"0\" data-confirm=\"Вы уверены, что хотите удалить этот элемент?\" data-method=\"post\" data-node-id=\"'+ node.id +'\"><span class=\"glyphicon glyphicon-trash\"></span></a>'
                '</div>'
            );
        }
    });
    $('input[type=\"checkbox\"]').on('change',function(e){
        var self=$(this)
        var stat=self.is(':checked')
        var m_id=self.data('id')
        $.ajax({
            url: current_origin+'/ru/admin/menu/change-status?m_id='+m_id+'&stat='+(stat?'active':'inactive'),
            success: function(res){
                if(!res.success){
                    alert('Произошла ошибка! Проверь свой интернет!')
                    return false
                }else{
                    return false
                }
            }
        })
        return false
    })
", View::POS_END);

$script = <<< JS

JS;
$this->registerJs($script, View::POS_END);
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавить', [
            'create'
        ], ['class' => 'btn btn-success']) ?>
    </p>
    <div id="listWithHandle">

    </div>
</div>