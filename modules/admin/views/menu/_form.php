<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use app\models\Menu;
use app\components\helpers\MyHelpers;
/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */

@app\assets\AdminAsset::register($this);
@app\assets\DropzoneAsset::register($this);

?>

<div class="post-form">
    <?php $form = ActiveForm::begin();
    $hasLagnguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            // 'content' => "<div>". $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->title)[$item]]).$form->field($model, "configcontent[{$item}]", ['labelOptions' => ['label' => "Контент( {$item} )"]])->textInput(['maxlength' => true,'value'=>yii\helpers\Json::decode($model->content)[$item]])."</div>"
            'content' => "<div>" .
                $form->field($model, "ltitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->title)[$item]]) .
                $form->field($model, "lshcontent[{$item}]", ['labelOptions' => ['label' => "Контент"]])->widget(TinyMce::className(), [
                    'options' => [
                        'value' => yii\helpers\Json::decode($model->short_content)[$item],
                    ],
                    'clientOptions' => [
                        // 'file_picker_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                        //     window.open('" . yii\helpers\Url::to(['/imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'tinymce']) . "&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                        // }"),
                        'height'=>'700',
                        'block_formats' => 'Paragraph=p;Header 1=h1;Header 2=h2;Header 3=h3',
                        'fontsize_formats' => '8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt',
                        'disk_cache' => true,
                        'plugins' => [
                            "emoticons",
                            "code",
                            "lists",
                            "anchor",
                            "fullpage",
                            "fullscreen",
                            "autosave",
                            "wordcount",
                            "textcolor colorpicker",
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                            "table contextmenu directionality emoticons paste textcolor code",
                            "save autosave template",
                        ],
                        // 'file_browser_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                        //     window.open('" . yii\helpers\Url::to(['imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'tinymce']) . "&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                        // }"),
                        'external_filemanager_path' => '/plugins/responsivefilemanager/filemanager/',
                        'filemanager_title' => 'Responsive Filemanager',
                        'external_plugins' => [
                            //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                            'filemanager' => '/plugins/responsivefilemanager/filemanager/plugin.min.js',
                            //Иконка/кнопка загрузки файла в панеле иснструментов.
                            'responsivefilemanager' => '/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                        ],
                        'theme_advanced_buttons1' => "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",

                        'toolbar' => "undo redo | fullpage | fullscreen | fontselect | fontsizeselect | emoticons | code | anchor | undo redo | forecolor backcolor | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media",
                    ]
                ])
                . "</div>"
        ];
    }, ['ru', "uz-Cyr", 'uz-Lat', 'en']);
    ?>

    <?php
    echo yii\bootstrap\Tabs::widget([
        'items' => $hasLagnguage
    ]);
    ?>
    <?php 
    $cats=ArrayHelper::map(Menu::find()->where(['status'=>'active'])->all(), 'id', function($mod){
        return MyHelpers::t($mod->title);
    });
    $cats['0']='Нет';
    echo $form->field($model, 'category_id')->dropDownList($cats, ['prompt' => 'Выбрать', 'selected'=>$model->id])?>
    <?= $form->field($model, 'options', ['labelOptions' => ['label' => 'Ссылка']])->textInput() ?>
    <?= $form->field($model, 'alias')->textInput(); ?>

    <!-- <?= $form->field($model, 'content_html')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'short_content')->textarea(['rows' => 6]) ?> -->

    <?= $form->field($model, 'status')->dropDownList(['active' => 'Активный', 'inactive' => 'Неактивный'], ['value' => ($model->status == 'inactive') ? 'active' : ""]) ?>

    <!-- <?= $form->field($model, 'option')->dropDownList(['yes' => 'Yes', 'no' => 'No',], ['prompt' => '']) ?> -->

    <!-- <?= $form->field($model, 'options')->textInput() ?> -->

    <!-- <?= $form->field($model, 'views')->textInput() ?> -->

    <!-- <?= $form->field($model, 'tags')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?> -->

    <!-- <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?> -->
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php ActiveForm::begin(['action' => ['image', 'postid' => $model->id], 'options' => ['id' => 'id_dropzone', 'class' => 'dropzone', 'enctype' => 'multipart/form-data']]); ?>
    <?php ActiveForm::end(); ?>
</div>