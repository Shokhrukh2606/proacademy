<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\BaseJson;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('admin', 'menu');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
    "
    var data = [
        {
            name: 'node1', id: 1,
            children: [
                { name: 'child1', id: 2 },
                { name: 'child2', id: 3 }
            ]
        },
        {
            name: 'node2', id: 4,
            children: [
                { name: 'child3', id: 5 }
            ]
        }
    ];
    $('#listWithHandle').tree({
        data: data,
        autoOpen: true,
        dragAndDrop: true
    });
    ",
    View::POS_END
);
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавить', [
            'create'
        ], ['class' => 'btn btn-success']) ?>
    </p>
    <div id="listWithHandle">
        
    </div>
</div>








<!-- //////////////////////// -->
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\BaseJson;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('admin', 'menu');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
    "
        var nestedSortables = [].slice.call(document.querySelectorAll('.nested-sortable'));

        // Loop through each nested sortable element
        for (var i = 0; i < nestedSortables.length; i++) {
            new Sortable(nestedSortables[i], {
                group: 'nested',
                animation: 150,
                fallbackOnBody: true,
                swapThreshold: 0.65,
                dataIdAttr: \"data-id\",
                onUpdate: function (/**Event*/ evt) {
                    console.log(
                        evt.to,
                        evt.from,
                        evt.oldDraggableIndex, // element's old index within old parent, only counting draggable elements
                        evt.newDraggableIndex, // element's new index within new parent, only counting draggable elements
                    )
                    // same properties as onEnd
                }
            });
        }
    ",
    View::POS_END
);
$this->registerJs(
    "
        // $('.list-group.nested-sortable.inner-childs').fadeOut()
        $('.plus').click(function(){
            var self=$(this)
            self.parent().parent().find('.list-group.nested-sortable.inner-childs').fadeToggle('fast')
            self.find('.glyphicon').toggleClass('glyphicon-minus')
        })
        // $('input[type=\"checkbox\"]').on('click',function(e){e.preventDefault()})
        $('input[type=\"checkbox\"]').on('change',function(e){
            var self=$(this)
            var stat=self.is(':checked')
            var m_id=self.data('id')
            $.ajax({
                url: 'http://qr.proacademy.uz/ru/admin/menu/change-status?m_id='+m_id+'&stat='+(stat?'active':'inactive'),
                success: function(res){
                    if(!res.success){
                        alert('Произошла ошибка! Проверь свой интернет!')
                        return false
                    }else{
                        return false
                    }
                }
            })
            return false
        })
    ",
    View::POS_READY
);
?>


<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавить', [
            'create'
        ], ['class' => 'btn btn-success']) ?>
    </p>
    <div id="listWithHandle" class="list-group nested-sortable">
        <?php foreach ($dataProvider as $key => $item) { ?>
            <div class="list-group-item nested-<?=$item->id?>" data-id="<?=$item->id?>">
                <?= Html::a('Удалить', ['menu/delete', 'id' => $item->id], [
                    'class' => 'btn btn-danger pull-right', 'onclick' => 'initDataMethods()',
                    'data-pjax' => '0', 'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?', 'data-method' => 'post'
                ]) ?>
                <a href=" <?= URL::to(['menu/update', 'id' => $item->id]) ?>" class="btn btn-info pull-right">Редактировать</a>
                <div class="drag-circle">
                    <div class="circle">
                        <span class="glyphicon glyphicon-move" aria-hidden="true"></span>
                    </div>
                    <div class="material-switch pull-right" style="margin-right: 15px;">
                        <input id="someSwitchOptionSuccess<?= $item->id ?>" data-id="<?= $item->id ?>" <?= $item->status == 'active' ? 'checked' : '0' ?> name="someSwitchOption" type="checkbox" />
                        <label for="someSwitchOptionSuccess<?= $item->id ?>" class="label-success"></label>
                    </div>
                    <div class="plus">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </div>
                    <?php
                    echo (json_decode($item->title))->ru;
                    // $t=json_decode($item->title);
                    // if(!isset($t->uz)){
                    //     echo $item->id;
                    // }
                    // print_r($t);
                    // (property_exists($t, 'ru'))?json_decode($item->title)->ru : ""
                    ?>
                </div>
                <?php if ($item->childs) { ?>
                    <div class="list-group nested-sortable inner-childs" style="display:none;">
                        <?php foreach ($item->childs as $subchild) { ?>
                            <div class="list-group-item nested-2" data-id="<?=$item->id?>">
                                <?= Html::a('Удалить', ['menu/delete', 'id' => $subchild->id], [
                                    'class' => 'btn btn-danger pull-right', 'onclick' => 'initDataMethods()',
                                    'data-pjax' => '0', 'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?', 'data-method' => 'post'
                                ]) ?>
                                <a href=" <?= URL::to(['menu/update', 'id' => $subchild->id]) ?>" class="btn btn-info pull-right">Редактировать</a>
                                <div class="material-switch pull-right" style="transform: translateY(8px);margin-right: 15px;">
                                    <input id="someSwitchOptionSuccess<?= $subchild->id ?>" data-id="<?= $subchild->id ?>" <?= $subchild->status == 'active' ? 'checked' : '0' ?> name="someSwitchOption" type="checkbox" />
                                    <label for="someSwitchOptionSuccess<?= $subchild->id ?>" class="label-success"></label>
                                </div>
                                <div class="drag-circle">
                                    <div class="circle">
                                        <span class="glyphicon glyphicon-move" aria-hidden="true"></span>
                                    </div>
                                    <?php
                                    echo (json_decode($subchild->title))->ru;
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>