<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\Messages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="messages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, "content_uz", ['labelOptions' => ['label' => "Контент(uz)"]])->widget(TinyMce::className(), [
        'options' => [
            'rows' => 18,
            'value' => $model->content_uz,
        ],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table paste image"
            ],
            'file_browser_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                            window.open('" . yii\helpers\Url::to(['imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'tinymce']) . "&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                        }"),
            // 'theme'=>"advanced",
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]) ?>
    <?= $form->field($model, "content_ru", ['labelOptions' => ['label' => "Контент(uz)"]])->widget(TinyMce::className(), [
        'options' => [
            'rows' => 18,
            'value' => $model->content_ru,
        ],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table paste image"
            ],
            'file_browser_callback' => new yii\web\JsExpression("function(field_name, url, type, win) {
                            window.open('" . yii\helpers\Url::to(['imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'tinymce']) . "&tag_name='+field_name,'','width=800,height=540 ,toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no');
                        }"),
            // 'theme'=>"advanced",
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('front', 'save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>