<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\BaseJson;
use yii\web\View;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категория видео';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(
    "
        var nestedSortables = [].slice.call(document.querySelectorAll('.nested-sortable'));

        // Loop through each nested sortable element
        for (var i = 0; i < nestedSortables.length; i++) {
            new Sortable(nestedSortables[i], {
                group: 'nested',
                animation: 150,
                fallbackOnBody: true,
                swapThreshold: 0.65
            });
        }
    ",
    View::POS_END
);
$this->registerJs(
    "
        // $('.list-group.nested-sortable.inner-childs').fadeOut()
        $('.plus').click(function(){
            var self=$(this)
            self.parent().parent().find('.list-group.nested-sortable.inner-childs').fadeToggle('fast')
            self.find('.glyphicon').toggleClass('glyphicon-minus')
        })
    ",
    View::POS_READY
);
?>


<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Добавить', [
            'cats-create'
        ], ['class' => 'btn btn-success']) ?>
    </p>
    <div id="listWithHandle" class="list-group nested-sortable">
        <?php foreach ($cats as $key => $item) { ?>
            <div class="list-group-item nested-1">
                <?= Html::a('Удалить', ['video-lessons/cat-delete', 'id' => $item->id], [
                    'class' => 'btn btn-danger pull-right', 'onclick' => 'initDataMethods()',
                    'data-pjax' => '0', 'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?', 'data-method' => 'post'
                ]) ?>
                <a href=" <?= URL::to(['video-lessons/cat-update', 'id' => $item->id]) ?>" class="btn btn-info pull-right">Редактировать</a>
                <div class="drag-circle">
                    <div class="circle">
                        <span class="glyphicon glyphicon-move" aria-hidden="true"></span>
                    </div>
                    <div class="plus">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </div>
                    <?php
                    echo (json_decode($item->title))->ru;
                    // $t=json_decode($item->title);
                    // if(!isset($t->uz)){
                    //     echo $item->id;
                    // }
                    // print_r($t);
                    // (property_exists($t, 'ru'))?json_decode($item->title)->ru : ""
                    ?>
                </div>
                <?php if ($item->childs) { ?>
                    <div class="list-group nested-sortable inner-childs" style="display:none;">
                        <?php foreach ($item->childs as $subchild) { ?>
                            <div class="list-group-item nested-2">
                                <?= Html::a('Удалить', ['video-lessons/cat-delete', 'id' => $subchild->id], [
                                    'class' => 'btn btn-danger pull-right', 'onclick' => 'initDataMethods()',
                                    'data-pjax' => '0', 'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?', 'data-method' => 'post'
                                ]) ?>
                                <a href=" <?= URL::to(['video-lessons/cat-update', 'id' => $subchild->id]) ?>" class="btn btn-info pull-right">Редактировать</a>
                                <div class="drag-circle">
                                    <div class="circle">
                                        <span class="glyphicon glyphicon-move" aria-hidden="true"></span>
                                    </div>
                                    <?php
                                    echo (json_decode($subchild->title))->ru;
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>