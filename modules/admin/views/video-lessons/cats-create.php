<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use app\models\VideoLessons;

@app\assets\AdminAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\VideoLessons */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-lessons-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $hasLagnguage = array_map(function ($item) use ($form, $model) {
        return [
            'label' => strtoupper($item),
            'content' => "<div>" .
                $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true])
                . "</div>"
        ];
    }, ['ru', "uz-Cyr", 'uz-Lat', 'en']);
    ?>

    <?php
    echo yii\bootstrap\Tabs::widget([
        'items' => $hasLagnguage
    ]);
    ?>
<?php
 $authors = VideoLessons::find()->where(['lesson_group'=>'cats'])->all();
 $items = ArrayHelper::map($authors,'id',function($item){return json_decode($item->title, true)['ru'];});
$params = [
    'prompt' => 'Укажите категория'
];
echo $form->field($model, 'category')->dropDownList($items,$params);
?>
    <!-- <?= $form->field($model, 'lesson_group')->textInput(['maxlength' => true]) ?> -->

    <!-- <?= $form->field($model, 'category')->textInput() ?> -->

    <!-- <?= $form->field($model, 'sub_category')->textInput() ?> -->

    <!-- <?= $form->field($model, 'youtube_link')->textInput(['maxlength' => true]) ?> -->

    <!-- <?= $form->field($model, 'mover_link')->textInput(['maxlength' => true]) ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
