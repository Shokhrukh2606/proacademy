<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VideoLessons */

$this->title = 'Создание видеоуроков';
$this->params['breadcrumbs'][] = ['label' => 'Video Lessons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-lessons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
