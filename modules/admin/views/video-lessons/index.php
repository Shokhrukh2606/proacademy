<?php

use app\models\VideoLessons;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VideoLessonsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Видео уроки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-lessons-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создание видеоуроков', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Категория видеоуроков', Url::to(['video-lessons/cats']), ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'title',
                'value' => function ($mod) {
                    return json_decode($mod->title, true)['ru'];
                }
            ],
            [
                'attribute' => 'category',
                'value' => function ($mod) {
                    $parent=VideoLessons::findOne(['id'=>$mod->category]);
                    return $parent?json_decode($parent->title, true)['ru']:'';
                }
            ],
            'category',
            'sub_category',
            //'youtube_link',
            //'mover_link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
