<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\DetailView;
use app\models\VideoLessons;

/* @var $this yii\web\View */
/* @var $model app\models\VideoLessons */

$this->title = Json::decode($model->title, true)['ru'];
$this->params['breadcrumbs'][] = ['label' => 'Video Lessons', 'url' => ['index']];
\yii\web\YiiAsset::register($this);
?>
<div class="video-lessons-view">



    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'title',
                'value' => function ($mod) {
                    return json_decode($mod->title, true)['ru'];
                }
            ],
            // 'lesson_group',
            [
                'attribute' => 'category',
                'value' => function ($mod) {
                    $parent=VideoLessons::findOne(['id'=>$mod->category]);
                    return json_decode($parent->title, true)['ru'];
                }
            ],
            'sub_category',
            'youtube_link',
            'mover_link',
        ],
    ]) ?>

</div>