<?php

use app\models\VideoLessons;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;

@app\assets\AdminAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\VideoLessons */

$this->title = 'Редактировать видео уроки: ';
$this->params['breadcrumbs'][] = ['label' => 'Video Lessons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="video-lessons-update">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="video-lessons-form">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?php $hasLagnguage = array_map(function ($item) use ($form, $model) {
            return [
                'label' => strtoupper($item),
                'content' => "<div>" .
                    $form->field($model, "configtitle[{$item}]", ['labelOptions' => ['label' => "Заголовок"]])->textInput(['maxlength' => true, 'value' => yii\helpers\Json::decode($model->title)[$item]])
                    . "</div>"
            ];
        }, ['ru', "uz-Cyr", 'uz-Lat', 'en']);
        ?>

        <?php
        echo yii\bootstrap\Tabs::widget([
            'items' => $hasLagnguage
        ]);
        ?>

        <!-- <?= $form->field($model, 'lesson_group')->textInput(['maxlength' => true]) ?> -->

        <?php
    $authors = VideoLessons::find()->where(['lesson_group' => 'cats'])->all();
    $items = ArrayHelper::map($authors, 'id', function ($item) {
        return json_decode($item->title, true)['ru'];
    });
    $params = [
        'prompt' => 'Укажите категория'
    ];
    echo $form->field($model, 'category')->dropDownList($items, $params);
    ?>

        <!-- <?= $form->field($model, 'sub_category')->textInput() ?> -->

        <?= $form->field($model, 'youtube_link')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'mover_link')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'cover')->fileInput() ?>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>