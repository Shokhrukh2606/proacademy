<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use kartik\datetime\DateTimePicker;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\AdmissionYear */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admission-year-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 1 => 'Активный', 0 => 'Неактивный', ], ['prompt' => '']) ?>
    <?= $form->field($model, 'start_from')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => $model->getAttributeLabel('start_from')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd HH:ii'
        ],
        'bsVersion' => '3.4.1',
    ]);
    ?>
    <?= $form->field($model, 'finishes_on')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => $model->getAttributeLabel('finishes_on')],
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd HH:ii'
        ],
        'bsVersion' => '3.4.1',
    ]);
    ?>
    <?= $form->field($model, "description", ['labelOptions' => ['label' => Yii::t('admin', 'description')]])->widget(TinyMce::className(), [
                    'options' => [
                        'rows' => 18
                    ],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            "advlist autolink lists link charmap print preview anchor",
                            "searchreplace visualblocks code fullscreen",
                            "insertdatetime media table paste image imagetools"
                        ],
                        'force_br_newlines' => true,
                        'force_p_newlines' => false,
                        'forced_root_block' => '',
                        'initialValue' => $model->description,
                        'file_picker_callback' => new JsExpression("function(cb, value, meta) {
                            var input = document.createElement('input');
                            input.setAttribute('type', 'file');
                            input.setAttribute('accept', 'image/*');
                            
                            // Note: In modern browsers input[type=\"file\"] is functional without 
                            // even adding it to the DOM, but that might not be the case in some older
                            // or quirky browsers like IE, so you might want to add it to the DOM
                            // just in case, and visually hide it. And do not forget do remove it
                            // once you do not need it anymore.

                            input.onchange = function() {
                            var file = this.files[0];
                            
                            var reader = new FileReader();
                            reader.onload = function () {
                                // Note: Now we need to register the blob in TinyMCEs image blob
                                // registry. In the next release this part hopefully won't be
                                // necessary, as we are looking to handle it internally.
                                var id = 'blobid' + (new Date()).getTime();
                                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                                var base64 = reader.result.split(',')[1];
                                var blobInfo = blobCache.create(id, file, base64);
                                blobCache.add(blobInfo);

                                // call the callback and populate the Title field with the file name
                                cb(blobInfo.blobUri(), { title: file.name });
                            };
                            reader.readAsDataURL(file);
                            };
                            
                            input.click();
                        }"),
                        // 'theme'=>"advanced",
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ]
                ]) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('front', 'save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
