<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdmissionYearSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'academic_year');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table thead th{
        text-align: center;
    }
</style>
<div class="admission-year-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('admin', 'create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout'=>"{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'title',
                'label'=>Yii::t('admin', 'title')
            ],
            [
                'attribute'=>'status',
                'value'=>function($model){return $model->status==1?Yii::t('admin', 'active'):Yii::t('admin', 'inactive');},
                'label'=>Yii::t('admin', 'status'),
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    [
                        '1'=>Yii::t('admin', 'active'),
                        '0'=>Yii::t('admin', 'inactive')
                    ],
                    [
                        'class' => 'form-control', 'multiple' => false,
                        'prompt' => Yii::t('admin', 'all')
                    ]
                )
            ],
            // 'description:ntext',
            // 'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
