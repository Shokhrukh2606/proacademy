<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdmissionYear */

$this->title = Yii::t('admin', 'create');
$this->params['breadcrumbs'][] = ['label' => 'Admission Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-year-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
