<?php

use app\assets\PluginsAsset;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Listener */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Listeners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
PluginsAsset::register($this);

?>
<div class="listener-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div id="qr-container">

    </div>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'full_name',
            'course_name',
            'begin_date',
            'end_date',
            'score',
            'content',
            'img',
            'img_link:ntext',
            'certificate_number',
        ],
    ]) ?>

</div>
<?php
$q = $model->certificate_number;
$this->registerJS("
     let _url = window.location.protocol+'\//'+window.location.hostname+'/check-attestation?certificate='+`${q}`
     console.log(_url)
      $('#qr-container').kjua({text: _url});
 ");
?>