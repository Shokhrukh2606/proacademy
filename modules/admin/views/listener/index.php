<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ListenerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listeners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listener-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Listener', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'first_name',
            'last_name',
            'middle_name',
            'full_name',
            //'course_name',
            //'begin_date',
            //'end_date',
            //'score',
            //'content',
            //'img',
            //'img_link:ntext',
            //'certificate_number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
