<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Listener */

$this->title = 'Create Listener';
$this->params['breadcrumbs'][] = ['label' => 'Listeners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="listener-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
