<?php

namespace app\modules\admin;

use yii\console\Application as ConsoleApplication;
use Yii;
use yii\web\GroupUrlRule;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    public $urlPrefix = 'admin';

    public function init()
    {
        parent::init();
        // initialize the module with the configuration loaded from config.php
        \Yii::configure($this, require __DIR__ . '/config.php');
        // \Yii::$app->language = 'ru';
        $this->layoutPath = \Yii::getAlias('@app/themes/admin/layouts/');
        $this->layout = 'main';
    }
 
    /** @inheritdoc */

    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            [
                'pattern' => 'posts',
                'route' => 'default/index'
            ]
        ], false);
    }
}
