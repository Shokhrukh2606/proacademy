<?php
return [
    'id' => 'admin',
    'modules' => [

    ],
    'components' => [
        'user' => [
            'class' => 'app\models\User',
            'enableAutoLogin' => true
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
    ]
];
