<?php

namespace app\models;

use app\models\MyMedia;
use Yii;
use  yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int|null $category_id
 * @property int|null $category_id2
 * @property string|null $group
 * @property string $alias
 * @property string|null $title
 * @property string|null $content
 * @property string|null $content_html
 * @property string|null $short_content
 * @property string|null $status
 * @property string|null $option
 * @property int|null $options
 * @property int|null $views
 * @property string|null $tags
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property int|null $sort_order
 * @property string $created_at
 * @property string|null $updated_at
 */
class Menu extends \yii\db\ActiveRecord
{
    public $childs = null;
    // public function getMedia()
    // {
    //     return $this->hasMany(MyMedia::className(), ['id' => 'media_id'])
    //     ->via('order_media', ['model_id' => 'media_id']);
    // }
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    public static function menuWithSameCategory($id)
    {
        $menus = Menu::find()->where(['category_id' => $id, 'status'=>'active'])->all();
        return $menus;
    }

    public static function menuTree()
    {
        $dataProvider = Menu::find()->where(['category_id' => 0, 'status'=>'active'])->all();
        foreach ($dataProvider as $item) {
            $childs = Menu::find()->where(['category_id' => $item->id, 'status'=>'active'])->all();
            if (count($childs) > 0) {
                $item->childs = $childs;
            }
        }
        return $dataProvider;
    }
    public function getHyperMenu(){
        
    }
    public function getLtitle()
    {
        return json_decode($this->title);
    }

    public function setLtitle($value)
    {
        $this->title = json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    public function getLshcontent()
    {
        return json_decode($this->short_content);
    }

    public function setLshcontent($value)
    {
        $this->short_content = json_encode($value, JSON_UNESCAPED_UNICODE);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'category_id2', 'views', 'sort_order'], 'integer'],
            // [['created_at'], 'required'],
            [['title'], 'string'],
            [['alias'], 'string'],
            [['alias', 'content', 'content_html', 'options','short_content', 'status', 'option', 'tags', 'meta_title', 'meta_description', 'meta_keywords'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['group'], 'string', 'max' => 30],
            [['ltitle', 'lshcontent'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'category_id2' => 'Category Id2',
            'group' => 'Group',
            'alias' => 'Alias',
            'title' => 'Title',
            'content' => 'Content',
            'content_html' => 'Content Html',
            'short_content' => 'Short Content',
            'status' => 'Status',
            'option' => 'Option',
            'options' => 'Options',
            'views' => 'Views',
            'tags' => 'Tags',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'sort_order' => 'Sort Order',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
