<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "video_lessons".
 *
 * @property int $id
 * @property string $title
 * @property string $lesson_group
 * @property int $category
 * @property int $sub_category
 * @property string|null $youtube_link
 * @property string|null $mover_link
 */
class VideoLessons extends \yii\db\ActiveRecord
{
    public $childs;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'video_lessons';
    }
    public function getConfigtitle()
    {
        return json_decode($this->title);
    }

    public function setConfigtitle($value)
    {
        $this->title = json_encode($value, JSON_UNESCAPED_UNICODE);
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'lesson_group'], 'required', 'message'=>Yii::t('front', 'val_req')],
            [['category', 'sub_category'], 'integer'],
            [['title', 'lesson_group', 'cover', 'youtube_link', 'mover_link'], 'string', 'max' => 255],
            [['configtitle'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'lesson_group' => 'Lesson Group',
            'category' => 'Category',
            'sub_category' => 'Sub Category',
            'youtube_link' => 'Youtube Ссылка',
            'mover_link' => 'Mover Ссылка',
            'cover'=>'Обложка'
        ];
    }
}
