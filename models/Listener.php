<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "listener".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string|null $full_name
 * @property string $course_name
 * @property string|null $begin_date
 * @property string|null $end_date
 * @property int|null $score
 * @property string|null $content
 * @property string|null $img
 * @property string|null $img_link
 * @property string|null $certificate_number
 */
class Listener extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'listener';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'middle_name', 'course_name'], 'required', 'message'=>Yii::t('front', 'val_req')],
            [['begin_date', 'end_date'], 'safe'],
            [['score'], 'integer'],
            [['img_link'], 'string'],
            [['first_name', 'last_name', 'middle_name', 'full_name', 'course_name', 'content', 'img', 'certificate_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'middle_name' => 'Middle Name',
            'full_name' => Yii::t('front', 'fio'),
            'course_name' => Yii::t('front', 'course_name'),
            'begin_date' => Yii::t('front', 'started'),
            'end_date' => Yii::t('front', 'finished'),
            'score' => Yii::t('front', 'score'),
            'content' => 'Content',
            'img' => 'Img',
            'img_link' => 'Img Link',
            'certificate_number' =>Yii::t('front', 'cer_number'),
        ];
    }
}
