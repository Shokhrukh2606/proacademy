<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 *
 * @property NewsTags[] $newsTags
 */
class Poll extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'poll';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['poll_id', 'question', 'options','total_votes'], 'required'],
            [['options', 'question'], 'string'],
            [['id', 'poll_id', 'total_votes'], 'integer'],
        ];
    }
}
