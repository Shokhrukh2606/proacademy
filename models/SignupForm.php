<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $username;
    public $last_name;
    public $middle_name;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required', 'message'=>Yii::t('front', 'val_req')],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['last_name', 'trim'],
            ['last_name', 'required', 'message'=>Yii::t('front', 'val_req')],
            ['last_name', 'string', 'min' => 2, 'max' => 255],
            ['middle_name', 'trim'],
            ['middle_name', 'required', 'message'=>Yii::t('front', 'val_req')],
            ['middle_name', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required', 'message'=>Yii::t('front', 'val_req')],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'Bu email ro\'yhatdan o\'tkazilgan'],
            ['password', 'required', 'message'=>Yii::t('front', 'val_req')],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('front', 'username'),
            'last_name' => Yii::t('front', 'last_name'),
            'middle_name' => Yii::t('front', 'middle_name'),
            'email' => Yii::t('front', 'email'),
            'password' => Yii::t('front', 'password')
        ];
    }
    public function sendEmail($email, $acnKey)
    {
        $activation_url = Url::to(['site/validate-key', 'key' => $acnKey], true);
        Yii::$app
            ->mailer
            ->compose(
                ['html' => 'registration-html'],
                ['activation_key' => $activation_url]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => ' Robot'])
            ->setTo($email)
            ->setSubject('Qabulga ro\'yhatdan o\'tish')
            ->send();
        return true;
    }
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if(!$this->validate()){
            return null;
        }
        $user = new User();
        $applicant = new Applicants();
        $user->username = $this->username;
        $user->last_name = $this->last_name;
        $user->middle_name = $this->middle_name;
        $user->email = $this->email;
        $user->created_at = date('Y-m-d H:i:s');
        $user->type='applicant';
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateActivationKey();
        if ($user->save()) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole('applicant');
            $auth->assign($role, $user->id);
            $this->sendEmail($user->email, $user->activation_key);
            return true;
        } else {
            return false;
        }
    }
}
