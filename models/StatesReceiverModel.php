<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class StatesReceiverModel extends Model
{
    public $last_name, $first_name, $middle_name, $priority;
    public $email, $phone, $position_place, $filer, $captcha;
    // array('name', 'filter', 'filter'=>'trim'),
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                //                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'middle_name', 'phone', 'email', 'position_place', 'priority'], 'required', 'message'=>Yii::t('front', 'val_req')],
            [['first_name', 'last_name', 'middle_name', 'phone', 'email', 'position_place', 'priority'], 'string'],
            ['email', 'email'],
            [['filer'], 'file', 'skipOnEmpty' => false, 'extensions' => 'doc, docx'],
            ['captcha', 'captcha']

        ];
    }
    public function attributeLabels()
    {
        return [
            'first_name' => Yii::t('front', 'username'),
            'last_name' => Yii::t('front', 'last_name'),
            'middle_name' => Yii::t('front', 'middle_name'),
            'phone' => Yii::t('front', 'phone'),
            'email' => Yii::t('front', 'email'),
            'position_place' => Yii::t('front', 'position_place'),
            'filer' => Yii::t('front', 'upload_state'),
            'priority' => Yii::t('front', 'priority')
        ];
    }

    public function saver()
    {
        $post = new Post();
        $post->title = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $post->content = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $post->short_content = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $post->content_html = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $post->meta_title = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $post->meta_description = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $post->meta_keywords = '{"ru":null, "en":null, "uz-Lat":null, "uz-Cyr":null}';
        $post->f_name = $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;
        $post->f_email = $this->email;
        $post->f_phone = $this->phone;
        $post->f_position = $this->position_place;
        $post->alias = $this->priority;
        $post->group = 'states_receiver';
        $save = $post->save();
        if(!$save){
            return false;
        }
        if ($d = UploadedFile::getInstance($this, 'filer')) {
            $state = Yii::$app->media->createFileFromUploadedFile($d, 'document');
            // print_r($state);
            $post->link = $state->getPath();
            $mediaOrder = new OrderMedia();
            $mediaOrder->media_id = (int) $state->id;
            $mediaOrder->model_id = (int) $post->id;
            $mediaOrder->category = 'states_receiver';
            $mediaOrder->url = $state->getPath();
            $mediaOrder->save();
        }
        return $post->save() ? true : false;
    }
}
