<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "admission_year".
 *
 * @property int $id
 * @property string $title
 * @property string $status
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 */
class AdmissionYear extends \yii\db\ActiveRecord
{
    const CURRENT_ACTIVE=1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admission_year';
    }
     /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required', 'message'=>Yii::t('front', 'val_req')],
            [['title', 'status', 'description'], 'string'],
            [['start_from', 'finishes_on'], 'safe'],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('admin', 'title'),
            'status' => Yii::t('admin', 'status'),
            'description' => Yii::t('admin', 'description'),
            'created_at' => Yii::t('admin', 'сreated'),
            'updated_at' => Yii::t('admin', 'updated'),
            'start_from'=>Yii::t('admin', 'start'),
            'finishes_on'=>Yii::t('admin', 'finish')

        ];
    }
}
