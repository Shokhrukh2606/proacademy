<?php

namespace app\models;

use Yii;
use  yii\db\ActiveRecord;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property int|null $category_id
 * @property int|null $category_id2
 * @property string|null $group
 * @property string $alias
 * @property string|null $title
 * @property string|null $content
 * @property string|null $content_html
 * @property string|null $short_content
 * @property string|null $status
 * @property string|null $option
 * @property int|null $options
 * @property int|null $views
 * @property string|null $tags
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property int|null $sort_order
 * @property string $created_at
 * @property string|null $updated_at
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }
    public function getConfigtitle()
    {
        return json_decode($this->title);
    }

    public function setConfigtitle($value)
    {
        $this->title = json_encode($value, JSON_UNESCAPED_UNICODE);
    }
    public function getConfigcontent()
    {
        return json_decode($this->content);
    }
    public function getFirstMedia(){
        $media=OrderMedia::findOne(['media_id'=>$this->id]);
        return $media->url;
    }
    public function setConfigcontent($value)
    {
        $this->content = json_encode($value, JSON_UNESCAPED_UNICODE);
    }
    public function getLshcontent()
    {
        return json_decode($this->short_content);
    }

    public function setLshcontent($value)
    {
        $this->short_content = json_encode($value, JSON_UNESCAPED_UNICODE);
    }
    public function getMtitle()
    {
        return json_decode($this->meta_title);
    }
    public function setMtitle($var)
    {
        $this->meta_title = json_encode($var, JSON_UNESCAPED_UNICODE);
    }
    public function getMdescription()
    {
        return json_decode($this->meta_description);
    }
    public function setMdescription($var)
    {
        $this->meta_description = json_encode($var, JSON_UNESCAPED_UNICODE);
    }
    public function getMkeywords()
    {
        return json_decode($this->meta_keywords);
    }
    public function setMkeywords($var)
    {
        $this->meta_keywords = json_encode($var, JSON_UNESCAPED_UNICODE);
    }
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => function () {
                    return date('Y-m-d H:i:s');
                }
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'category_id2', 'options', 'views', 'sort_order'], 'integer'],
            // [['created_at'], 'required'],
            [['title', 'f_position', 'f_name','f_phone'], 'string'],
            [['alias'], 'unique'],
            [['alias', 'content', 'content_html', 'short_content', 'status', 'option', 'tags', 'meta_title', 'meta_description', 'meta_keywords'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['icon', 'link'], 'string', 'max' => 255],
            [['group'], 'string', 'max' => 30],
            [['configtitle', 'configcontent', 'lshcontent', 'mtitle', 'mdescription', 'mkeywords'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'category_id2' => 'Category Id2',
            'group' => 'Group',
            'alias' => 'Alias',
            'title' => 'Title',
            'content' => 'Content',
            'content_html' => 'Content Html',
            'short_content' => 'Short Content',
            'status' => 'Status',
            'option' => 'Option',
            'options' => 'Options',
            'views' => 'Views',
            'tags' => 'Tags',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'sort_order' => 'Sort Order',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
