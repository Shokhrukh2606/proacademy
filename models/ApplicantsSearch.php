<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Applicants;
use yii\helpers\ArrayHelper;

/**
 * ApplicantsSearch represents the model behind the search form of `app\models\Applicants`.
 */
class ApplicantsSearch extends Applicants
{
    public $city_birth;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'academic_year_id', 'district_birth', 'district_temporary', 'district_permanent', 'can_speak', 'faculty', 'step', 'created_at', 'updated_at', 'city_birth'], 'integer'],
            [['unique_code', 'avatar', 'applicant_type', 'birth_date', 'address_temporary', 'address_permanent', 'phone', 'telegram_phone', 'nationality', 'other_nationality', 'diplom_number', 'high_school_name', 'high_school_specialty', 'high_school_finished', 'inner_recomendation', 'other_can_speak', 'passport_given_date', 'passport_given_from', 'passport_serial_number', 'diploma', 'passport', 'certificate', 'biography', 'relatives', 'career', 'status'], 'safe'],
            [['essay_result', 'speaking_result', 'language_result', 'overall_result'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $currentAdmission = AdmissionYear::findOne(['status' => 1]);
        $query = Applicants::find()->where(['academic_year_id'=>$currentAdmission->id]);
        if ( !($this->load($params) && $this->validate()) or (empty($this->status)) ) {
          $query = Applicants::find()->andWhere(['!=', 'status', 'unknown'])->andWhere(['!=', 'status', 'cancelled'])->andWhere(['academic_year_id'=>$currentAdmission->id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'academic_year_id' => $this->academic_year_id,
            'birth_date' => $this->birth_date,
            'city_birth' => $this->city_birth,
            'district_birth' => $this->district_birth,
            'district_birth' => $this->district_birth,
            'district_temporary' => $this->district_temporary,
            'district_permanent' => $this->district_permanent,
            'high_school_finished' => $this->high_school_finished,
            'can_speak' => $this->can_speak,
            'faculty' => $this->faculty,
            'passport_given_date' => $this->passport_given_date,
            'step' => $this->step,
            'essay_result' => $this->essay_result,
            'speaking_result' => $this->speaking_result,
            'language_result' => $this->language_result,
            'overall_result' => $this->overall_result,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'unique_code', $this->unique_code])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'applicant_type', $this->applicant_type])
            ->andFilterWhere(['like', 'address_temporary', $this->address_temporary])
            ->andFilterWhere(['like', 'address_permanent', $this->address_permanent])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'telegram_phone', $this->telegram_phone])
            ->andFilterWhere(['like', 'nationality', $this->nationality])
            ->andFilterWhere(['like', 'other_nationality', $this->other_nationality])
            ->andFilterWhere(['like', 'diplom_number', $this->diplom_number])
            ->andFilterWhere(['like', 'high_school_name', $this->high_school_name])
            ->andFilterWhere(['like', 'high_school_specialty', $this->high_school_specialty])
            ->andFilterWhere(['like', 'inner_recomendation', $this->inner_recomendation])
            ->andFilterWhere(['like', 'other_can_speak', $this->other_can_speak])
            ->andFilterWhere(['like', 'passport_given_from', $this->passport_given_from])
            ->andFilterWhere(['like', 'passport_serial_number', $this->passport_serial_number])
            ->andFilterWhere(['like', 'diploma', $this->diploma])
            ->andFilterWhere(['like', 'passport', $this->passport])
            ->andFilterWhere(['like', 'certificate', $this->certificate])
            ->andFilterWhere(['like', 'biography', $this->biography])
            ->andFilterWhere(['like', 'relatives', $this->relatives])
            ->andFilterWhere(['like', 'career', $this->career])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
