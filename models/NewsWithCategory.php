<?php

namespace app\models;
 
use yii\helpers\ArrayHelper;

class NewsWithCategory extends News
{
    /**
     * @var array IDs of the categories
     */
    public $category_ids = [];
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            // each category_id must exist in category table (*1)
            ['category_ids', 'each', 'rule' => [
                    'exist', 'targetClass' => NewsCategory::className(), 'targetAttribute' => 'id'
                ]
            ],
        ]);
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'category_ids' => 'Категории',
        ]);
    }

    /**
     * load the post's categories (*2)
     */
    public function loadCategories()
    {
        $this->category_ids = [];
        if (!empty($this->id)) {
            $rows = NewsCategory::find()
                ->select(['category_id'])
                ->where(['news_id' => $this->id])
                ->asArray()
                ->all();
            foreach($rows as $row) {
               $this->category_ids[] = $row['news_id'];
            }
        }
    }

    /**
     * save the post's categories (*3)
     */
    public function saveCategories()
    {
        /* clear the categories of the post before saving */
        NewsCategory::deleteAll(['news_id' => $this->id]);
        if (is_array($this->category_ids)) {
            foreach($this->category_ids as $category_id) {
                $pc = new NewsCategory();
                $pc->news_id = $this->id;
                $pc->category_id = $category_id;
                $pc->save();
            }
        }else{
            print_r($this->getErrors());
        }
        /* Be careful, $this->category_ids can be empty */
    }
}