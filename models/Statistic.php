<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statistic".
 *
 * @property int $id
 * @property int $parent_id
 * @property string|null $alias
 * @property string|null $title
 * @property string|null $content
 * @property string|null $content_html
 * @property string|null $short_content
 * @property string|null $status
 * @property string|null $option
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property int|null $sort_order
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Statistic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'statistic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id'], 'required'],
            [['parent_id', 'sort_order'], 'integer'],
            [['alias', 'title', 'content', 'content_html', 'short_content', 'status', 'option', 'meta_title', 'meta_description', 'meta_keywords'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['parent_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('admin', 'ID'),
            'parent_id' => Yii::t('admin', 'Parent ID'),
            'alias' => Yii::t('admin', 'Alias'),
            'title' => Yii::t('admin', 'Title'),
            'content' => Yii::t('admin', 'Content'),
            'content_html' => Yii::t('admin', 'Content Html'),
            'short_content' => Yii::t('admin', 'Short Content'),
            'status' => Yii::t('admin', 'Status'),
            'option' => Yii::t('admin', 'Option'),
            'meta_title' => Yii::t('admin', 'Meta Title'),
            'meta_description' => Yii::t('admin', 'Meta Description'),
            'meta_keywords' => Yii::t('admin', 'Meta Keywords'),
            'sort_order' => Yii::t('admin', 'Sort Order'),
            'created_at' => Yii::t('admin', 'Created At'),
            'updated_at' => Yii::t('admin', 'Updated At'),
        ];
    }
}
