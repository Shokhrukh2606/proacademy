<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use Yii\helpers\Html;
use app\models\Applicants;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 5;
    const STATUS_ACTIVE = 10;
    public $password, $image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    /**
     * @inheritdoc
     */
    // public function getApplicant()
    // {
    //     return $this->hasOne(Applicants::className(), ['id' => 'user_id']);
    // }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'last_name', 'middle_name', 'password', 'type', 'image', 'avatar'], 'string'],
            [['email'], 'required', 'message'=>Yii::t('front', 'val_req')],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            [
                ['image'],
                'image',
                'skipOnEmpty' => true,
                'extensions' => 'png, jpeg, jpg'
            ],
            // ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }
    public function getFullName()
    {
        return $this->last_name.' '.$this->username.' '.$this->middle_name;
    }
    public function saveHard($role)
    {
        $this->setPassword($this->password);
        $this->generateAuthKey();
        $this->generateActivationKey();
        $this->created_at = date('Y-m-d H:i:s');
        $auth = Yii::$app->authManager;
        if ($this->save()) {
            if ($role == 'admin') {
                $role = $auth->getRole('admin');
                $auth->assign($role, $this->id);
                return true;
            }
            if ($role == 'mastermoderator') {
                $role = $auth->getRole('mastermoderator');
                $auth->assign($role, $this->id);
                return true;
                
            }
            if ($role == 'moderator') {
                $role = $auth->getRole('moderator');
                $auth->assign($role, $this->id);
                return true;
            }
            return false;
        }
    }
    public function setPassword($pass)
    {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($pass);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {

        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {

        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    public function generateActivationKey()
    {
        $this->activation_key = sha1(mt_rand(10000, 99999) . time() . $this->email);
    }
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    public function activateUser()
    {
        $this->status = self::STATUS_ACTIVE;
        return ($this->save()) ? true : false;
    }
}
