<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\AdmissionYear;
/**
 * This is the model class for filling second step data to "applicants".
 *
 * @property string|null $inner_recomendation
 */
class StepTwoForm extends Model
{
    public $inner_recomendation, $decision;
    public $position;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inner_recomendation', 'decision'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
            ['position', 'string'],
            ['position', 'required', 'message'=>Yii::t('front', 'val_req')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'inner_recomendation' => Yii::t('front', 'recomendation'),
            'decision' => Yii::t('front', 'decision'),
            'position'=>Yii::t('front', 'position')
        ];
    }
    public function save(){
        $academic_year=AdmissionYear::findOne(['status'=>1]);
        $applicant = Applicants::findOne(['user_id' => Yii::$app->user->identity->id, 'academic_year_id'=>$academic_year->id]);
        if($i=UploadedFile::getInstance($this, 'inner_recomendation')){
            $applicant->inner_recomendation=$this->hyperSaver($applicant, 'inner_recomendation', $i);
        }
        if($d=UploadedFile::getInstance($this, 'decision')){
            $applicant->decision=$this->hyperSaver($applicant, 'decision', $d);
        }
        $applicant->position=$this->position;
        $applicant->step=Applicants::STEP_THREE;
        return ($applicant->save())?true:false;
    }
    public function hyperSaver($applicant, $name, $saved)
    {
        $mainFolder = AdmissionYear::findOne(['status' => 1]);
        $structure = Yii::$app->basePath . '/web/uploads/applicants/' . $mainFolder->title  . '/'.$applicant->unique_code;
        if(!is_dir($structure)){
            if (!mkdir($structure, 0777, true)) {
                die('Не удалось создать директории...');
            }
        }
        $saved->saveAs($structure . '/' . $name . '.' . $saved->extension);
        return '/applicants/' . $mainFolder->title . $applicant->unique_code . '/' . $name . '.' . $saved->extension;
        # code...
    }
}
