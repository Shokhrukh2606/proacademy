<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $alias
 * @property string|null $title
 * @property string|null $short_content
 * @property string|null $content
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property int $created_by
 * @property int|null $views
 *
 * @property NewsCategory[] $newsCategories
 * @property NewsTags[] $newsTags
 */
class News extends \yii\db\ActiveRecord
{
    public $tags;
    public $news_category;
    public $files;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'created_by','news_category', 'title'], 'required', 'message'=>Yii::t('front', 'val_req')],
            [['title', 'tags', 'short_content', 'content', 'meta_title', 'meta_description', 'meta_keywords'], 'string'],
            [['created_by', 'views'], 'integer'],
            ['alias', 'unique'],
            [['alias'], 'string', 'max' => 255],
            ['published_date', 'datetime'],
            [['news_category', 'jtitle', 'jcontent', 'jshcontent', 'mtitle', 'mdescription', 'mkeywords'], 'safe'],
            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg, pdf, jpg, mp4, webm'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Title',
            'short_content' => 'Short Content',
            'content' => 'Content',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'created_by' => 'Created By',
            'views' => 'Views',
            'published_date'=>'Дата публикации',
            'views'=>'Количество людей, прочитавших пост'
        ];
    }

    /**
     * Gets query for [[NewsCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNewsCategories()
    {
        return $this->hasMany(Category::className(), ['category_id' => 'id'])->viaTable(
            'news_category',
            ['category_id' => 'news_id']
        );
    }

    /**
     * Gets query for [[NewsTags]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNewsTags()
    {
        return $this->hasMany(NewsTags::className(), ['news_id' => 'id']);
    }
    public function getJtitle()
    {
        return json_decode($this->title);
    }
    public function setJtitle($var)
    {
        $this->title = json_encode($var, JSON_UNESCAPED_UNICODE);
    }
    public function getJcontent()
    {
        return json_decode($this->content);
    }
    public function setJcontent($var)
    {
        $this->content = json_encode($var, JSON_UNESCAPED_UNICODE);
    }
    public function getJshcontent()
    {
        return json_decode($this->short_content);
    }
    public function setJshcontent($var)
    {
        $this->short_content = json_encode($var, JSON_UNESCAPED_UNICODE);
    }
    public function getMtitle()
    {
        return json_decode($this->meta_title);
    }
    public function setMtitle($var)
    {
        $this->meta_title = json_encode($var, JSON_UNESCAPED_UNICODE);
    }
    public function getMdescription()
    {
        return json_decode($this->meta_description);
    }
    public function setMdescription($var)
    {
        $this->meta_description = json_encode($var, JSON_UNESCAPED_UNICODE);
    }
    public function getMkeywords()
    {
        return json_decode($this->meta_keywords);
    }
    public function setMkeywords($var)
    {
        $this->meta_keywords = json_encode($var, JSON_UNESCAPED_UNICODE);
    }
    public function saveNew()
    {
        $this->created_by = Yii::$app->user->identity->id;
        $savedNewPost = $this->save();
        if (count($came = UploadedFile::getInstances($this, 'files'))>0) {
            foreach ($came as $item) {
                $media = Yii::$app->media->createFileFromUploadedFile($item, 'document');
                $mediaOrder = new OrderMedia();
                $mediaOrder->media_id = (int) $media->id;
                $mediaOrder->model_id = (int) $this->id;
                $mediaOrder->category = 'news';
                $mediaOrder->url = $media->getPath();
                $mediaOrder->save();
            }
        }
        if (count($this->news_category) > 0) {
            foreach ($this->news_category as $item) {
                $newCat = new NewsCategory();
                $newCat->news_id = $this->id;
                $newCat->category_id = $item;
                $newCat->save();
            }
        }
        if ($this->tags) {
            $newTags = explode(',', $this->tags);
            foreach ($newTags as $item) {
                $tagSaved = new NewsTags();
                $tagSaved->tag_id = $item;
                $tagSaved->news_id = $this->id;
                $tagSaved->save();
            }
        }
        return $savedNewPost ? true : false;
    }
    public function updateNews()
    {
        $this->created_by = Yii::$app->user->identity->id;
        $savedNewPost = $this->save();
        if (count($this->news_category) > 0) {
            NewsCategory::deleteAll(['news_id' => $this->id]);
            foreach ($this->news_category as $item) {
                $newCat = new NewsCategory();
                $newCat->news_id = $this->id;
                $newCat->category_id = $item;
                $newCat->save();
            }
        }
        if ($this->tags) {
            NewsTags::deleteAll(['news_id' => $this->id]);
            $newTags = explode(',', $this->tags);
            foreach ($newTags as $item) {
                $tagSaved = new NewsTags();
                $tagSaved->tag_id = $item;
                $tagSaved->news_id = $this->id;
                $tagSaved->save();
            }
        }
        return $savedNewPost ? true : false;
    }
}
