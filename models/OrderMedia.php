<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_media".
 *
 * @property int $id
 * @property int $media_id
 * @property int $model_id
 * @property string $category
 */
class OrderMedia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['media_id', 'model_id', 'category'], 'required', 'message'=>Yii::t('front', 'val_req')],
            [['media_id', 'model_id'], 'integer'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'media_id' => 'Media ID',
            'model_id' => 'Model ID',
            'category' => 'Category',
        ];
    }
}
