<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "admission_faculties".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $status
 * @property int $created_at
 * @property int $updated_at
 */
class AdmissionFaculties extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admission_faculties';
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'string'],
            [['created_at', 'updated_at', 'sort_order'], 'integer'],
            [['configtitle'], 'safe']
        ];
    }
    public function getConfigtitle()
    {
        return json_decode($this->title);
    }

    public function setConfigtitle($value)
    {
        $this->title = json_encode($value, JSON_UNESCAPED_UNICODE);
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('admin', 'title'),
            'status' => Yii::t('admin', 'status'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
