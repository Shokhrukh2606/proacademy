<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_category".
 *
 * @property int $news_id
 * @property int $category_id
 *
 * @property Category $category
 * @property News $news
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'category_id'], 'required'],
            [['news_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News ID',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getCategory()
    // {
    //     return $this->hasMany(News::className(), ['id' => 'news_id'])->viaTable('news_category', ['category_id' => 'id']);
    // }

    /**
     * Gets query for [[News]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getNews()
    // {
    //     return $this->hasOne(News::className(), ['id' => 'news_id']);
    // }
}
