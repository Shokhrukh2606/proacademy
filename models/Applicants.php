<?php

namespace app\models;

use app\components\helpers\MyHelpers;
use app\components\helpers\Setup;
use ozerich\filestorage\models\File;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "applicants".
 *
 * @property int $id
 * @property int $user_id
 * @property int $unique_code
 * @property int $academic_year_id
 * @property string|null $applicant_type
 * @property string|null $birth_date
 * @property int|null $district_birth
 * @property int|null $district_temporary
 * @property string|null $address_temporary
 * @property int|null $district_permanent
 * @property string|null $address_permanent
 * @property string|null $phone
 * @property string|null $telegram_phone
 * @property string|null $nationality
 * @property string|null $other_nationality
 * @property string|null $diplom_number
 * @property string|null $high_school_name
 * @property string|null $high_school_specialty
 * @property string|null $inner_recomendation
 * @property int|null $can_speak
 * @property string|null $other_can_speak
 * @property int|null $faculty
 * @property string|null $view_status
 * @property string|null $diploma
 * @property string|null $passport
 * @property string|null $certificate
 * @property string|null $biography
 * @property string|null $relatives
 * @property string|null $career
 * @property int|null $step
 */
class Applicants extends \yii\db\ActiveRecord
{
    const STEP_ONE = 1;
    const STEP_TWO = 2;
    const STEP_THREE = 3;
    const STEP_FINISHED = 4;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applicants';
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * One this is one 
     */
    public function getApplicant()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unique_code', 'academic_year_id'], 'required', 'message'=>Yii::t('front', 'val_req')],
            [['user_id', 'academic_year_id', 'district_birth', 'district_temporary', 'district_permanent', 'can_speak', 'faculty', 'step', 'high_school_finished'], 'integer'],
            [['applicant_type', 'unique_code'], 'string'],
            [['unique_code', 'diplom_number'], 'unique'],
            [['birth_date', 'passport_given_date'], 'date'],
            [['address_temporary', 'address_permanent', 'diplom_number', 'high_school_name', 'high_school_specialty', 'inner_recomendation', 'other_can_speak', 'diploma', 'passport', 'certificate', 'biography', 'relatives', 'career', 'avatar', 'position'], 'string', 'max' => 255],
            [['phone', 'telegram_phone'], 'string', 'max' => 15],
            [['nationality', 'other_nationality'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'avatar' => Yii::t('front', 'avatar'),
            'username' => Yii::t('front', 'username'),
            'middle_name' => Yii::t('front', 'middle_name'),
            'last_name' => Yii::t('front', 'last_name'),
            'applicant_type' => Yii::t('front', 'applicant_type'),
            'birth_date' => Yii::t('front', 'birth_date'),
            'city_birth' => Yii::t('front', 'city_birth'),
            'district_birth' => Yii::t('front', 'district_birth'),
            'city_temporary' => Yii::t('front', 'city_temporary'),
            'district_temporary' => Yii::t('front', 'district_temporary'),
            'address_temporary' => Yii::t('front', 'address_temporary'),
            'district_permanent' => Yii::t('front', 'district_permanent'),
            'city_permanent' => Yii::t('front', 'city_permanent'),
            'address_permanent' =>  Yii::t('front', 'address_permanent'),
            'phone' =>  Yii::t('front', 'phone'),
            'telegram_phone' =>  Yii::t('front', 'telegram_phone'),
            'nationality' =>  Yii::t('front', 'nationality'),
            'other_nationality' =>  Yii::t('front', 'other_nationality'),
            'diplom_number' =>  Yii::t('front', 'diplom_number'),
            'high_school_name' =>  Yii::t('front', 'high_school_name'),
            'high_school_finished' => Yii::t('front', 'high_school_finished'),
            'high_school_specialty' => Yii::t('front', 'high_school_specialty'),
            'can_speak' =>  Yii::t('front', 'can_speak'),
            'other_can_speak' =>  Yii::t('front', 'other_can_speak'),
            'faculty' =>  Yii::t('front', 'faculty'),
            'diploma' =>  Yii::t('front', 'diploma'),
            'passport' =>  Yii::t('front', 'passport'),
            'certificate' =>  Yii::t('front', 'certificate'),
            'biography' =>  Yii::t('front', 'biography'),
            'relatives' =>  Yii::t('front', 'relatives'),
            'career' =>  Yii::t('front', 'career'),
            'inner_recomendation' => Yii::t('front', 'recomendation'),
            'biography' => Yii::t('front', 'biography'),
            'relatives' => Yii::t('front', 'relatives'),
            'career' => Yii::t('front', 'career'),
            'passport_serial_number' => Yii::t('front', 'p_ser'),
            'passport_given_from' => Yii::t('front', 'p_given'),
            'passport_given_date' => Yii::t('front', 'p_given_date'),
            'decision' => Yii::t('front', 'decision'),
            'position' => Yii::t('front', 'position'),
            'unique_code' => Yii::t('front', 'unique_code'),
            'essay_result'=>Yii::t('admin', 'essay_result'),
            'speaking_result'=>Yii::t('admin', 'speaking_result'),
            'language_result'=>Yii::t('admin', 'language_result'),
            'overall_result'=>Yii::t('admin', 'overall_result')

        ];
    }
    public function setBirthDate($date)
    {
        $this->birth_date = Yii::$app->formatter->asDate($date);
    }
    public static function getApplicantStep()
    {
        $academic_year=AdmissionYear::findOne(['status'=>1]);
        $applicant = self::findOne(['user_id' => Yii::$app->user->identity->id,  'academic_year_id'=>$academic_year->id]);
        return $applicant->step;
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->overall_result = $this->essay_result + $this->speaking_result + $this->language_result;
            return true;
        }
        return false;
    }
    /**
     * Gets query for [[thiss]].
     *
     * @return \yii\db\ActiveQuery
     */
    public static function getUser()
    {
        return Yii::$app->user->identity;
    }
    public function generateCheckCode()
    {
        // $current_year = date("Y");
        // if ($this->user_id < 10) {
        //     $this->unique_code = 'P00' . $this->user_id . $current_year;
        // }else if (10 <= $this->user_id && $this->user_id < 100) {
        //     $this->unique_code = 'P0' . $this->user_id . $current_year;
        // }else if (100 <= $this->user_id && $this->user_id < 1000) {
        //     $this->unique_code = 'P' . $this->user_id . $current_year;
        // } else{
        //     $this->unique_code = 'P' . $this->user_id . $current_year;
        // }
        $this->unique_code = substr(md5(time()), 0, 7);
    }
    public function updateAborted()
    {
        $user = Yii::$app->user->identity;
        if ($a = UploadedFile::getInstanceByName('avatar')) {
            $this->avatar = $this->hyperSaver($this, 'avatar', $a);
        }
        if ($d = UploadedFile::getInstanceByName('diploma')) {
            $this->diploma = $this->hyperSaver($this, 'diploma', $d);
        }
        if ($p = UploadedFile::getInstanceByName('passport')) {
            $this->passport = $this->hyperSaver($this, 'passport', $p);
        }
        if ($c = UploadedFile::getInstanceByName('certificate')) {
            $this->certificate = $this->hyperSaver($this, 'certificate', $c);
        }
        if ($i = UploadedFile::getInstanceByName('inner_recomendation')) {
            $this->inner_recomendation = $this->hyperSaver($this, 'inner_recomendation', $i);
        }
        if ($b = UploadedFile::getInstanceByName('biography')) {
            $this->biography = $this->hyperSaver($this, 'biography', $b);
        }
        if ($r = UploadedFile::getInstanceByName('relatives')) {
            $this->relatives = $this->hyperSaver($this, 'relatives', $r);
        }
        if ($ca = UploadedFile::getInstanceByName('career')) {
            $this->career = $this->hyperSaver($this, 'career', $ca);
        }
        if ($de = UploadedFile::getInstanceByName('decision')) {
            $this->decision = $this->hyperSaver($this, 'decision', $de);
        }
        
        if ($this->status != 'unknown') {
            $this->status = 'updated';
        }
        return ($this->save()) ? true : false;
    }
    public function hyperSaver($applicant, $name, $saved)
    {
        $mainFolder = AdmissionYear::findOne(['status' => 1]);
        $structure = Yii::$app->basePath . '/web/uploads/applicants/' . $mainFolder->title.'/'.$applicant->unique_code;
        if(!is_dir($structure)){
            if (!mkdir($structure, 0777, true)) {
                die('Не удалось создать директории...');
            }
        }
        $saved->saveAs($structure . '/' . $name . '.' . $saved->extension);
        return '/applicants/' . $mainFolder->title. $applicant->unique_code . '/' . $name . '.' . $saved->extension;
        # code...
    }
}
