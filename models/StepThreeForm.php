<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\AdmissionYear;

/**
 * This is the model class for filling second step data to "applicants".
 *
 * @property string|null $inner_recomendation
 */
class StepThreeForm extends Model
{
    public $biography;
    public $relatives;
    public $career;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['biography', 'relatives','career'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'biography' => Yii::t('front', 'biography'),
            'relatives'=>Yii::t('front', 'relatives'),
            'career'=>Yii::t('front', 'career')
        ];
    }
    public function save(){
        $academic_year=AdmissionYear::findOne(['status'=>1]);
        $applicant = Applicants::findOne(['user_id' => Yii::$app->user->identity->id, 'academic_year_id'=>$academic_year->id]);
        if($b=UploadedFile::getInstance($this, 'biography')){
            $applicant->biography=$this->hyperSaver($applicant, 'biography', $b);
        }
        if($r=UploadedFile::getInstance($this, 'relatives')){
            $applicant->relatives=$this->hyperSaver($applicant, 'relatives', $r);
        }
        if($c=UploadedFile::getInstance($this, 'career')){
            $applicant->career=$this->hyperSaver($applicant, 'career', $c);
        }
        $applicant->step=Applicants::STEP_FINISHED;
        return ($applicant->save())?true:false;
    }
    public function hyperSaver($applicant, $name, $saved)
    {
        $mainFolder = AdmissionYear::findOne(['status' => 1]);
        $structure = Yii::$app->basePath . '/web/uploads/applicants/' . $mainFolder->title  .'/'. $applicant->unique_code;
        if(!is_dir($structure)){
            if (!mkdir($structure, 0777, true)) {
                die('Не удалось создать директории...');
            }
        }
        $saved->saveAs($structure . '/' . $name . '.' . $saved->extension);
        return '/applicants/' . $mainFolder->title .'/'. $applicant->unique_code . '/' . $name . '.' . $saved->extension;
        # code...
    }
}
