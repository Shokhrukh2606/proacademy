<?php

namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 *
 * @property NewsCategory[] $newsCategories
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias'], 'required', 'message'=>Yii::t('front', 'val_req')],
            [['title', 'status', 'prop'], 'string'],
            ['alias','unique'],
            [['alias'], 'string', 'max' => 255],
            [['jtitle'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'alias' => 'Alias',
        ];
    }
    public function getJtitle()
    {
        return json_decode($this->title);
    }
    public function setJtitle($var)
    {
        $this->title=json_encode($var, JSON_UNESCAPED_UNICODE);        
    }
    /**
     * Gets query for [[NewsCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    // public function getNewsCategories()
    // {
    //     return $this->hasMany(NewsCategory::className(), ['category_id' => 'id']);
    // }
    public static function getAvailableCategories()
    {
        $categories = self::find()->orderBy('title')->asArray()->all();
        $items = ArrayHelper::map($categories, 'id', function($item){return json_decode($item['title'], true)['ru'];});
        return $items;
    }
}
