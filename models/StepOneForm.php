<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\components\helpers\Setup;
use app\models\AdmissionYear;

/**
 * This is the model class for filling first step data to "applicants".
 *
 * @property int $id
 * @property int $user_id
 * @property int $unique_code
 * @property int $academic_year_id
 * @property string|null $applicant_type
 * @property string|null $birth_date
 * @property int|null $district_birth
 * @property int|null $district_temporary
 * @property string|null $address_temporary
 * @property int|null $district_permanent
 * @property string|null $address_permanent
 * @property string|null $phone
 * @property string|null $telegram_phone
 * @property string|null $nationality
 * @property string|null $other_nationality
 * @property string|null $diplom_number
 * @property string|null $high_school_name
 * @property string|null $high_school_specialty
 * @property string|null $inner_recomendation
 * @property int|null $can_speak
 * @property string|null $other_can_speak
 * @property int|null $faculty
 * @property string|null $view_status
 * @property string|null $diploma
 * @property string|null $passport
 * @property string|null $certificate
 * @property string|null $biography
 * @property string|null $relatives
 * @property string|null $career
 * @property int|null $step
 */
class StepOneForm extends Model
{
    public $first_name;
    public $last_name;
    public $birth_date;
    public $city_birth;
    public $district_birth;
    public $nationality;
    public $other_nationality;
    public $phone;
    public $telegram_phone;
    public $faculty;
    public $city_permanent;
    public $district_permanent;
    public $address_permanent;
    public $city_temporary;
    public $district_temporary;
    public $address_temporary;
    public $high_school_name;
    public $high_school_specialty;
    public $high_school_finished;
    public $applicant_type;
    public $can_speak;
    public $other_can_speak;
    public $passport_serial_number;
    public $passport_given_from;
    public $passport_given_date;
    // Files
    public $passport;
    public $diploma;
    public $diplom_number;
    public $certificate;
    public $avatar;



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['passport_given_date', 'passport_given_from', 'passport_serial_number', 'birth_date', 'city_birth', 'district_birth', 'nationality', 'phone', 'telegram_phone', 'faculty', 'city_permanent', 'district_permanent', 'address_permanent', 'high_school_name', 'high_school_specialty', 'high_school_finished', 'applicant_type', 'can_speak'], 'required', 'message'=>Yii::t('front', 'val_req')],
            [['district_birth', 'district_temporary', 'district_permanent', 'city_birth','city_temporary', 'city_permanent', 'can_speak', 'faculty'], 'integer'],
            [['applicant_type', 'birth_date', 'passport_given_date'], 'string'],
            [['high_school_finished'], 'integer'],
            [['passport_given_from', 'passport_serial_number', 'address_temporary', 'address_permanent', 'diplom_number', 'high_school_name', 'high_school_specialty', 'other_can_speak', 'diploma', 'passport', 'certificate'], 'string', 'max' => 255],
            [['phone', 'telegram_phone'], 'string', 'max' => 12, 'min' => 12],
            [['phone'], 'match', 'pattern' => '/\^?([0-9]{2})\^?([-]?)([0-9]{3})\^?([-]?)\2([0-9]{2})\^?([-]?)\2([0-9]{2})/', 'message' => Yii::t('front', 'Формат номера телефона должен быть XX-XXX-XX-XX')],
            ['passport_serial_number', 'match', 'pattern' => '/[A-Z]{2}\d{7}/', 'message' => 'Passport seriya raqami AA0000000 formatda bo\'lishi kerak'],
            ['passport_serial_number', 'unique', 'targetClass' => '\app\models\Applicants','on'=>'insert', 'message' => Yii::t('front', 'passport_exist')],
            ['diplom_number', 'unique', 'targetClass' => '\app\models\Applicants','on'=>'insert', 'message' => Yii::t('front', 'diploma_exist')],
            [['telegram_phone'], 'match', 'pattern' => '/\^?([0-9]{2})\^?([-]?)([0-9]{3})\^?([-]?)\2([0-9]{2})\^?([-]?)\2([0-9]{2})/', 'message' => Yii::t('front', 'Формат номера телефона должен быть XX-XXX-XX-XX')],
            [['nationality', 'other_nationality'], 'string', 'max' => 30],
            [['diploma'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, png, jpeg, jpg'],
            [['passport'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg, pdf, jpg'],
            [
                ['avatar'],
                'image',
                'skipOnEmpty' => true,
                'extensions' => 'png, jpeg, jpg',
                // 'minWidth' => 400,
                // 'maxWidth' => 420,
                // 'minHeight' => 530,
                // 'maxHeight' => 550,
            ],
            [['certificate'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg, pdf, jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'avatar' => Yii::t('front', 'avatar'),
            'username' => Yii::t('front', 'username'),
            'middle_name' => Yii::t('front', 'middle_name'),
            'last_name' => Yii::t('front', 'last_name'),
            'applicant_type' => Yii::t('front', 'applicant_type'),
            'birth_date' => Yii::t('front', 'birth_date'),
            'city_birth' => Yii::t('front', 'city_birth'),
            'district_birth' => Yii::t('front', 'district_birth'),
            'city_temporary' => Yii::t('front', 'city_temporary'),
            'district_temporary' => Yii::t('front', 'district_temporary'),
            'address_temporary' => Yii::t('front', 'address_temporary'),
            'district_permanent' => Yii::t('front', 'district_permanent'),
            'city_permanent' => Yii::t('front', 'city_permanent'),
            'address_permanent' =>  Yii::t('front', 'address_permanent'),
            'phone' =>  Yii::t('front', 'phone'),
            'telegram_phone' =>  Yii::t('front', 'telegram_phone'),
            'nationality' =>  Yii::t('front', 'nationality'),
            'other_nationality' =>  Yii::t('front', 'other_nationality'),
            'diplom_number' =>  Yii::t('front', 'diplom_number'),
            'high_school_name' =>  Yii::t('front', 'high_school_name'),
            'high_school_finished' => Yii::t('front', 'high_school_finished'),
            'high_school_specialty' => Yii::t('front', 'high_school_specialty'),
            'can_speak' =>  Yii::t('front', 'can_speak'),
            'other_can_speak' =>  Yii::t('front', 'other_can_speak'),
            'faculty' =>  Yii::t('front', 'faculty'),
            'diploma' =>  Yii::t('front', 'diploma'),
            'passport' =>  Yii::t('front', 'passport'),
            'certificate' =>  Yii::t('front', 'certificate'),
            'biography' =>  Yii::t('front', 'biography'),
            'relatives' =>  Yii::t('front', 'relatives'),
            'career' =>  Yii::t('front', 'career'),
            'passport_serial_number' => Yii::t('front', 'p_ser'),
            'passport_given_from' => Yii::t('front', 'p_given'),
            'passport_given_date' => Yii::t('front', 'p_given_date')
        ];
    }
    public function saver()
    {
        $academic_year=AdmissionYear::findOne(['status'=>1]);
        $applicant = Applicants::findOne(['user_id' => Yii::$app->user->identity->id, 'academic_year_id'=>$academic_year->id]);
        $app_id = $applicant->id;
        $applicant->setBirthDate($this->birth_date);
        $applicant->city_birth = $this->city_birth;
        $applicant->district_birth = $this->district_birth;
        $applicant->nationality = $this->nationality;
        $applicant->phone = $this->phone;
        $applicant->telegram_phone = $this->telegram_phone;
        $applicant->applicant_type = $this->applicant_type;
        $applicant->faculty = $this->faculty;
        $applicant->city_permanent = $this->city_permanent;
        $applicant->district_permanent = $this->district_permanent;
        $applicant->address_permanent = $this->address_permanent;
        $applicant->city_temporary = $this->city_temporary;
        $applicant->district_temporary = $this->district_temporary;
        $applicant->address_temporary = $this->address_temporary;
        $applicant->can_speak = $this->can_speak;
        $applicant->other_can_speak = $this->other_can_speak;
        $applicant->high_school_name = $this->high_school_name;
        $applicant->high_school_finished = $this->high_school_finished;
        $applicant->high_school_specialty = $this->high_school_specialty;
        $applicant->diplom_number = $this->diplom_number;
        $applicant->passport_serial_number = $this->passport_serial_number;
        $applicant->passport_given_from = $this->passport_given_from;
        $applicant->passport_given_date = Yii::$app->formatter->asDate($this->passport_given_date);

        if ($a = UploadedFile::getInstance($this, 'avatar')) {
            $applicant->avatar = $this->hyperSaver($applicant, 'avatar', $a);
        }
        if ($d = UploadedFile::getInstance($this, 'diploma')) {
            $applicant->diploma =  $this->hyperSaver($applicant, 'diploma', $d);
        }
        if ($p = UploadedFile::getInstance($this, 'passport')) {
            $applicant->passport =  $this->hyperSaver($applicant, 'passport', $p);
        }
        if ($c = UploadedFile::getInstance($this, 'certificate')) {
            $applicant->certificate = $this->hyperSaver($applicant, 'certificate', $c);
        }
        if ($applicant->applicant_type == 'inner') {
            $applicant->step = Applicants::STEP_TWO;
        } else {
            $applicant->step = Applicants::STEP_THREE;
        }
        return ($applicant->save()) ? true : false;
    }
    public function hyperSaver($applicant, $name, $saved)
    {
        $mainFolder = AdmissionYear::findOne(['status' => 1]);
        $structure = Yii::$app->basePath . '/web/uploads/applicants/' . $mainFolder->title  .'/'. $applicant->unique_code;
        if(!is_dir($structure)){
            if (!mkdir($structure, 0777, true)) {
                echo('Не удалось создать директории...');
            }
        }
        $saved->saveAs($structure . '/' . $name . '.' . $saved->extension);
        return '/applicants/' . $mainFolder->title .'/'. $applicant->unique_code . '/' . $name . '.' . $saved->extension;
        # code...
    }
}
