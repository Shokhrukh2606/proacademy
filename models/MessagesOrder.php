<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages_order".
 *
 * @property int $id
 * @property int $from
 * @property int $to
 * @property int $message_id
 * @property string|null $status
 */
class MessagesOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'messages_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from', 'to', 'message_id'], 'required'],
            [['from', 'to', 'message_id'], 'integer'],
            [['status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'From',
            'to' => 'To',
            'message_id' => 'Message ID',
            'status' => 'Status',
        ];
    }
}
