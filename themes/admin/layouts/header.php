<?php

use yii\helpers\Html;
use yii\helpers\Url;

$user = Yii::$app->user->identity;
/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">Pro</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?=$user->avatar?Url::to('@web/postfiles'.$user->avatar, true): Url::to('/images/avatar.png') ?>" class="user-image" alt="User Image" />
                        <span class="hidden-xs"><?= $user->last_name . ' ' . $user->username . ' ' . $user->middle_name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?=$user->avatar?Url::to('@web/postfiles'.$user->avatar, true): Url::to('/images/avatar.png') ?>" class="img-circle" alt="User Image" />
                        </li>
                        <!-- Menu Body -->
                        <!-- <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </li> -->
                        <!-- Menu Footer-->

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    Yii::t('admin', 'logout'),
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li class="dropdown user user-menu">
                <li class="dropdown user user-menu">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" style="line-height: 2.728571;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Язык интерфейса
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?= Url::to('ru/admin', true) ?>"><img style="width:30px;height:30px;" src="<?= Yii::$app->request->baseUrl ?>/images/ru.svg" alt=""></a>
                            <a class="dropdown-item" href="<?= Url::to('uz-lat/admin', true) ?>"><img style="width:30px;height:30px;" src="<?= Yii::$app->request->baseUrl ?>/images/uz.svg" alt=""></a>
                            <a class="dropdown-item" href="<?= Url::to('uz-cyr/admin', true) ?>"><img style="width:30px;height:30px;" src="<?= Yii::$app->request->baseUrl ?>/images/uz.svg" alt=""></a>

                        </div>
                    </div>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>