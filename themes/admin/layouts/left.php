<?php

use app\models\AdmissionYear;
use app\models\Applicants;
use app\models\Post;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\ArrayHelper;

$user=Yii::$app->user->identity;
$script = <<< JS
    $(".parent-childed>a").click(function(e){
        e.preventDefault();
        var self=$(this)
        var childs=self.parent().find(".treeview-menu")
        if(childs.hasClass("open")){
            childs.css({display: 'none'})
            self.find("span.pull-right-container i").removeClass("fa-angle-down").addClass("fa-angle-left")
            childs.removeClass("open")
        }else{
            childs.addClass("open")
            self.find("span.pull-right-container i").removeClass("fa-angle-left").addClass("fa-angle-down")
            childs.css({display: 'block'})
        }
    })
JS;

$this->registerJS($script, View::POS_READY);
$user = Yii::$app->user->identity;
// $faq_category = array_map(function ($item) {
//     return [
//         'label' => json_decode($item['title'], true)[Yii::$app->language],
//         'url' => ['/admin/post', 'group' => $item['group'], 'sub' => $item['id']],
//         'icon' => 'fa fa-file-code-o'
//     ];
// }, Post::find()->where(['group' => 'faq_category'])->asArray()->all());
// array_unshift($faq_category, [
//     'label' => 'FAQ категория',
//     'url' => ['/admin/post', 'category' => 'faq'],
//     'icon' => 'fa fa-list'
// ]);
// $checks_category = array_map(function ($item) {
//     return [
//         'label' => json_decode($item['title'], true)[Yii::$app->language],
//         'url' => ['/admin/post', 'group' => $item['group'], 'sub' => $item['id']],
//         'icon' => 'fa fa-file-code-o'
//     ];
// }, Post::find()->where(['group' => 'checks_category'])->asArray()->all());
// array_unshift($checks_category, [
//     'label' => 'Проверь себя категория',
//     'url' => ['/admin/post', 'category' => 'checks'],
//     'icon' => 'fa fa-list'
// ]);
// $library_category = array_map(function ($item) {
//     return [
//         'label' => json_decode($item['title'], true)[Yii::$app->language],
//         'url' => ['/admin/library', 'group' => $item['group'], 'sub' => $item['id']],
//         'icon' => 'fa fa-file-code-o'
//     ];
// }, Post::find()->where(['group' => 'library_category'])->asArray()->all());
// array_unshift($library_category, [
//     'label' => 'Библиотека категория',
//     'url' => ['/admin/post', 'category' => 'library'],
//     'icon' => 'fa fa-book'
// ]);
// $branches_category = array_map(function ($item) {
//     return [
//         'label' => json_decode($item['title'], true)[Yii::$app->language],
//         'url' => ['/admin/post', 'group' => $item['group'], 'sub' => $item['id']],
//         'icon' => 'fa fa-file-code-o'
//     ];
// }, Post::find()->where(['group' => 'branches_category'])->asArray()->all());
// array_unshift($branches_category, [
//     'label' => 'Подразделения категория',
//     'url' => ['/admin/post', 'category' => 'branches'],
//     'icon' => 'fa fa-list'
// ]);
$news_category = Post::find()->where(['group' => 'news_category'])->asArray()->all();
$count_new_states = Post::find()->where(['group' => 'states_receiver', 'views' => '0'])->count();
$active_year=AdmissionYear::findOne(['status'=>1]);
if($active_year){
    $count_new_applicants=Applicants::find()->where(['academic_year_id'=>$active_year->id, 'status'=>['unknown', 'updated']])->count();
}else{
    $count_new_applicants=0;
}
$menu = [
    //    ['label' => \Yii::t('admin', 'admin_menu'), 'options' => ['class' => 'header']],
    ['label' => \Yii::t('front', 'main'), 'icon' => 'fa fa-archive', 'url' => ['/admin']],
    ['label' => \Yii::t('admin', 'admission'), 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->can('manageApplicants')],
    ['label' => \Yii::t('admin', 'academic_year'), 'icon' => 'fa fa-archive', 'url' => ['/admin/admission-year'],  'visible' => Yii::$app->user->can('manageApplicants')],
    ['label' => \Yii::t('front', 'faculty'), 'icon' => 'fa fa-certificate', 'url' => ['/admin/admission-faculties'],  'visible' => Yii::$app->user->can('manageApplicants')],
    ['label' => \Yii::t('admin', 'applicants'), 'icon' => 'fa fa-bookmark', 'url' => ['/admin/applicants/index'], 'visible' => Yii::$app->user->can('manageApplicants'), 'template' => $count_new_applicants > 0 ? '<a href="{url}">{icon} {label} <span class="pull-right-container"><span class="label label-danger pull-right">' . $count_new_applicants . '</span></span></a>' : '<a href="{url}">{icon} {label}</a>'],
    ['label' => \Yii::t('admin', 'Сообщения'), 'icon' => 'fa fa-envelope', 'url' => ['/admin/messages'],  'visible' => Yii::$app->user->can('manageApplicants')],
    ['label' => \Yii::t('admin', 'applicants_grading'), 'icon' => 'fa fa-asterisk', 'url' => ['/admin/applicants/grading'],  'visible' => Yii::$app->user->can('manageApplicants')],
    ['label' => \Yii::t('admin', 'Сайт'), 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Меню сайта'), 'icon' => 'fa fa-list', 'url' => ['/admin/menu'],'visible' => Yii::$app->user->can('changePost')],
    ['label' => 'Интерактивные услуги', 'icon' => 'fa fa-gg', 'url' => ['/admin/post', 'category' => 'interactives'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Дистанционное обучение'), 'icon' => 'fa fa-user', 'url' => Url::to(['/admin/video-lessons']), 'visible' => Yii::$app->user->can('changePost')],
    [
        'label' => 'FAQ',
        'icon' => 'fa fa-list',
        'url' => '#',
        'items' => [
            [
                'label' => 'FAQ категория',
                'url' => ['/admin/post', 'category' => 'faq'],
                'icon' => 'fa fa-list'
            ],
            [
                'label' => 'Все FAQs',
                'url' => ['/admin/post/all', 'group' => 'faq'],
                'icon' => 'fa fa-list'
            ]
        ],
        'options' => ['class' => 'parent-childed'],
        'visible' => Yii::$app->user->can('changePost')
    ],
    [
        'label' => 'Проверь себя вопросы',
        'icon' => 'fa fa-list',
        'url' => '#',
        'items' => [
            [
                'label' => 'Проверь себя категория',
                'url' => ['/admin/post', 'category' => 'checks'],
                'icon' => 'fa fa-list'
            ],
            [
                'label' => 'Все Проверь себя',
                'url' => ['/admin/post/all', 'group' => 'checks'],
                'icon' => 'fa fa-list'
            ]
        ],
        'options' => ['class' => 'parent-childed'],
        'visible' => Yii::$app->user->can('changePost')
    ],
    [
        'label' => 'Библиотека',
        'icon' => 'fa fa-list',
        'url' => '#',
        'items' => [
            [
                'label' => 'Библиотека категория',
                'url' => ['/admin/post', 'category' => 'library'],
                'icon' => 'fa fa-book'
            ],
            [
                'label' => 'Все книги',
                'url' => ['/admin/library/all', 'group' => 'library'],
                'icon' => 'fa fa-book'
            ]
        ],
        'options' => ['class' => 'parent-childed'],
        'visible' => Yii::$app->user->can('changePost')
    ],
    ['label' => 'Об академии', 'icon' => 'fa fa-gg', 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => 'Правовые акты', 'icon' => 'fa fa-gg', 'url' => ['/admin/post', 'group' => 'law-acts'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Руководство'), 'icon' => 'fa fa-user', 'url' => ['/admin/post', 'group' => 'heads'], 'visible' => Yii::$app->user->can('changePost')],
    [
        'label' => 'Подразделения',
        'icon' => 'fa fa-list',
        'url' => '#',
        'items' => [
            [
                'label' => 'Подразделения категория',
                'url' => ['/admin/post', 'category' => 'branches'],
                'icon' => 'fa fa-list'
            ],
            [
                'label' => 'Все Подразделения',
                'url' => ['/admin/post/all', 'group' => 'branches'],
                'icon' => 'fa fa-list'
            ]
        ],
        'options' => ['class' => 'parent-childed'],
        'visible' => Yii::$app->user->can('changePost')
    ],
    ['label' => \Yii::t('admin', 'Пресс-центр'), 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->can('changePost')],
    [
        'label' => 'Новости',
        'icon' => 'fa fa-list',
        'url' => '#',
        'items' => [
            ['label' => \Yii::t('admin', 'Новости категория'), 'icon' => 'fa fa-archive', 'url' => ['/admin/category']],
            ['label' => \Yii::t('admin', 'Новости лист'), 'icon' => 'fa fa-archive', 'url' => ['/admin/news']],
            ['label' => \Yii::t('admin', 'Теги'), 'icon' => 'fa fa-archive', 'url' => ['/admin/tags']]
        ],
        'options' => ['class' => 'parent-childed'],
        'visible' => Yii::$app->user->can('changePost')
    ],
    ['label' => \Yii::t('admin', 'Онлайн приём статей'), 'icon' => 'fa fa-archive', 'url' => ['/admin/post', 'group' => 'states_receiver'], 'template' => $count_new_states > 0 ? '<a href="{url}">{icon} {label} <span class="pull-right-container"><span class="label label-primary pull-right">' . $count_new_states . '</span></span></a>' : '<a href="{url}">{icon} {label}</a>', 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Вестник Академии'), 'icon' => 'fa fa-archive', 'url' => ['/admin/sets'],'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Учебный процесс'), 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Магистратура'), 'icon' => 'fa fa-gear', 'url' => ['/admin/post', 'group' => 'masters'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Еще'), 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Повышение правовой грам...'), 'icon' => 'fa fa-gear', 'url' => ['/admin/post', 'group' => 'literacy'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Doodly'), 'icon' => 'fa fa-play', 'url' => ['/admin/post', 'group' => 'doodly'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Вопрос и ответы'), 'icon' => 'fa fa-play', 'url' => ['/admin/post', 'group' => 'questionnaire'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Международное...'), 'icon' => 'fa fa-gear', 'url' => ['/admin/post', 'group' => 'cooperation_int'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Межведомственное...'), 'icon' => 'fa fa-gear', 'url' => ['/admin/post', 'group' => 'cooperation_inter'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Настройки'), 'options' => ['class' => 'header'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Пользователей'), 'icon' => 'fa fa-user', 'url' => ['/admin/user'], 'visible' => Yii::$app->user->can('manageApplicants')&&Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Полезные ссылки'), 'icon' => 'fa fa-gear', 'url' => ['/admin/post', 'group' => 'recommended_links'], 'visible' => Yii::$app->user->can('changePost')],
    
    [
        'label' => \Yii::t('admin', 'Media'),
        'icon' => 'fa fa-play',
        'url' => '#',
        'items' => [
            [
                'label' => \Yii::t('admin', 'Видео лента'),
                'url' => ['/admin/post', 'group' => 'videos'],
                'icon' => 'fa fa-video'
            ],
            [
                'label' => \Yii::t('admin', 'Фото лента'),
                'url' => ['/admin/post', 'group' => 'photos'],
                'icon' => 'fa fa-video'
            ]
        ],
        'options' => ['class' => 'parent-childed'],
        'visible' => Yii::$app->user->can('changePost')
    ],
    ['label' => \Yii::t('admin', 'Настройки'), 'icon' => 'fa fa-gear', 'url' => ['/admin/post', 'group' => 'settings'], 'visible' => Yii::$app->user->can('changePost')],
    ['label' => \Yii::t('admin', 'Статистика'), 'icon' => 'fa fa-gear', 'url' => ['/admin/post', 'group' => 'statistics'], 'visible' => Yii::$app->user->can('changePost')],
];
// foreach ($news_category as $item) {
//     array_push(
//         $menu,
//         [
//             'label' => json_decode($item->title, true)['ru'],
//             'url' => ['/admin/post', 'group' => $item->group, 'sub' => $item->id],
//             'icon' => 'fa fa-file-code-o'
//         ]
//     );
// }

?>
<aside class="main-sidebar">

    <section class="sidebar">


        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=$user->avatar?Url::to('@web/postfiles'.$user->avatar, true): Url::to('/images/avatar.png') ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?= $user->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>


        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form> -->
        <?= dmstr\widgets\Menu::widget(
            [
                'items' => $menu,
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree']
            ]
        ) ?>
    </section>

</aside>