<?php

use app\models\Applicants;
use app\models\MessagesOrder;
use app\models\Post;
use yii\helpers\Url;
use yii\web\View;
$count_new_messages=MessagesOrder::find()->where(['status'=>'inactive', 'to'=>Yii::$app->user->identity->id])->count();
$status=Applicants::findOne(['user_id'=>Yii::$app->user->identity->id])->status;
$menu = [
    ['label' => Yii::t('front', 'main'),'icon'=>'fa fa-user', 'url' => ['verified/index'], 'visible' => !Yii::$app->user->isGuest],
    ['label' => Yii::t('front', 'edit'),'icon'=>'fa fa-edit', 'url' => Url::to(['verified/edit']), 'visible' => $status!='finished'&&$status!='accepted'&&$status!='cancelled'&&$status!='updated'],
    ['label' => Yii::t('front', 'messages'),'icon'=>'fa fa-envelope', 'url' => ['/applicant/messages'], 'template'=>$count_new_messages>0?'<a href="{url}">{icon} {label} <span class="pull-right-container"><span class="label label-primary pull-right">'.$count_new_messages.'</span></span></a>':'<a href="{url}">{icon} {label}</a>'],
    ['label' => Yii::t('front', 'exit'),'icon'=>'fa fa-sign-out', 'url' => Url::to(['/site/logout']), 'visible' => !Yii::$app->user->isGuest],
];

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => $menu,
            ]
        ) ?>

    </section>

</aside>