<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\SmartWizardAsset;

SmartWizardAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <link rel="shortcut icon" href="<?= Yii::$app->request->baseUrl ?>/images/logo.png" type="image/png">    
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <?php if (!Yii::$app->user->getIsGuest()) { ?>
        <div class="user-signed">
            <i class="flaticon-user"></i></br>
            <?= Html::a(Html::tag('i', '', ['class' => 'flaticon-next-1']), ['/site/logout']) ?>
        </div>
    <?php } ?>
    <!--==================================================================== 
                                End Header area
        =====================================================================-->
    <?php
    echo Alert::widget([
        'options' => ['class' => 'alert-info']
    ]);
    ?>
    <?= $content ?>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>