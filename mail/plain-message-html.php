<?php

use yii\helpers\Html;

?>
<div class="password-reset">
    <p>
        <?= $content ?>
    </p>
</div>