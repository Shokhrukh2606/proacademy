<?php

use yii\helpers\Html;

?>
<div class="password-reset">
    <h3> Assalomu alaykum!</h3>
    <p> Arizangiz ko’rib chiqish uchun qabul qilindi. Sizning noyob raqam <?= $app_code ?>. Iltimos noyob raqamingizni saqlab qo’ying. Siz aynan shu noyob raqam bilangina imtihon topshirishingiz mumkin. Agar ma’lumotlarda xatoliklar aniqlansa bu haqda electron pochta manzilingizga yoki shaxsiy kabinetingizga xabar yuboriladi.</p>
    <p> Hurmat bilan, Qabul komissiyasi!<br>
        Ma’lumot uchun telefon raqami: (71) 202-04-96 (1795)
    </p>

    <h3> Здравствуйте!</h3>

    <p> Ваше заявление было принято для рассмотрения.

        Ваш уникальный номер <?= $app_code ?>.

        Пожалуйста, сохраните свой уникальный номер.
        Вы можете сдать экзамен только по этому уникальному номеру.

        При обнаружении ошибок в данных об этом будет отправлено сообщение на ваш адрес электронной почты или в личный кабинет.
    </p>
    <p>
        С уважением, Приемная комиссия! <br>
        Номер телефона для справок: &nbsp; (71) 202-04-96 (1795)
    </p>
</div>