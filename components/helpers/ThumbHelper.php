<?php
namespace app\components\helpers;

class ThumbHelper extends \himiklab\thumbnail\EasyThumbnailImage
{

    protected static function errorHandler($error, $filename)
    {
        if ($error instanceof \himiklab\thumbnail\FileNotFoundException) {
            return \yii\helpers\Html::img('@web/images/building.jpg');
        }
    }
} 