<?php

namespace app\components\helpers;

use app\models\AdmissionFaculties;
use Yii;
use app\models\Cities;
use app\models\Districts;
use app\models\OrderMedia;
use app\models\Post;
use InvalidArgumentException;
use ozerich\filestorage\models\File;
use yii\helpers\FileHelper;

class MyHelpers
{
    public static function matchUrl($cont, $act)
    {
        return ((Yii::$app->controller->id == $cont) && (Yii::$app->controller->action->id == $act)) ? true : false;
    }
    public static function limiter($x, $limit)
    {
        if (mb_strlen($x) > $limit)
            $x = mb_substr($x, 0, $limit) . ' &hellip;';
        else
            $x = $x;
        return $x;
    }
    public static function parse_date($given_date)
    {
        return (Yii::$app->language == 'ru') ? self::mysql_date($given_date) : ((Yii::$app->language == 'uz-Lat') ? self::mysql_oz_date($given_date) : ((Yii::$app->language == 'uz-Cyr') ? self::mysql_uz_date($given_date) : self::mysql_english_date($given_date)));
    }
    public static function mysql_oz_date($datestr = '')
    {
        if ($datestr == '')
            return '';

        // получаем значение даты и времени
        list($day) = explode(' ', $datestr);

        switch ($day) {
                // Если дата совпадает с сегодняшней
            case date('Y-m-d'):
                $result = 'Bugun';
                break;

                //Если дата совпадает со вчерашней
            case date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))):
                $result = 'Kecha';
                break;

            default: {
                    // Разделяем отображение даты на составляющие
                    list($y, $m, $d)  = explode('-', $day);

                    $month_str = array(
                        'Yanvar', 'Fevral', 'Mart',
                        'Aprel', 'May', 'Iyun',
                        'Iyul', 'Avgust', 'Sentabr',
                        'Oktabr', 'Noyabr', 'Dekabr'
                    );
                    $month_int = array(
                        '01', '02', '03',
                        '04', '05', '06',
                        '07', '08', '09',
                        '10', '11', '12'
                    );

                    // Замена числового обозначения месяца на словесное (склоненное в падеже)
                    $m = str_replace($month_int, $month_str, $m);
                    // Формирование окончательного результата
                    $result = $d . ' ' . $m . ' ' . $y;
                }
        }
        return $result;
    }
    public static function mysql_english_date($datestr = '')
    {
        if ($datestr == '')
            return '';

        // получаем значение даты и времени
        list($day) = explode(' ', $datestr);

        switch ($day) {
                // Если дата совпадает с сегодняшней
            case date('Y-m-d'):
                $result = 'Today';
                break;

                //Если дата совпадает со вчерашней
            case date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))):
                $result = 'Yesterday';
                break;

            default: {
                    // Разделяем отображение даты на составляющие
                    list($y, $m, $d)  = explode('-', $day);

                    $month_str = array(
                        'January', 'February', 'March',
                        'April', 'May', 'June',
                        'July', 'August', 'September',
                        'October', 'November', 'December'
                    );
                    $month_int = array(
                        '01', '02', '03',
                        '04', '05', '06',
                        '07', '08', '09',
                        '10', '11', '12'
                    );

                    // Замена числового обозначения месяца на словесное (склоненное в падеже)
                    $m = str_replace($month_int, $month_str, $m);
                    // Формирование окончательного результата
                    $result = $d . ' ' . $m . ' ' . $y;
                }
        }
        return $result;
    }
    public static function mysql_uz_date($datestr = '')
    {
        if ($datestr == '')
            return '';

        // получаем значение даты и времени
        list($day) = explode(' ', $datestr);

        switch ($day) {
                // Если дата совпадает с сегодняшней
                /* case date('Y-m-d'):
                       $result = 'Bugun';
                        break;

         //Если дата совпадает со вчерашней
         case date( 'Y-m-d', mktime(0, 0, 0, date("m")  , date("d")-1, date("Y")) ):
                      $result = 'Kecha';
                        break;*/

                // Если дата совпадает с сегодняшней
            case date('Y-m-d'):
                $result = 'Бугун';
                break;

                //Если дата совпадает со вчерашней
            case date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))):
                $result = 'Кеча';
                break;


            default: {
                    // Разделяем отображение даты на составляющие
                    list($y, $m, $d)  = explode('-', $day);

                    $month_str = array(
                        /*'Yanvar', 'Fevral', 'Mart',
                   'Aprel', 'May', 'Iyun',
                   'Iyul', 'Avgust', 'Sentyabr',
                   'Oktyabr', 'Noyabr', 'Dekabr'*/
                        'Январ', 'Феврал', 'Март',
                        'Апрел', 'Май', 'Июн',
                        'Июл', 'Август', 'Сентабр',
                        'Октабр', 'Ноябр', 'Декабр'
                    );
                    $month_int = array(
                        '01', '02', '03',
                        '04', '05', '06',
                        '07', '08', '09',
                        '10', '11', '12'
                    );

                    // Замена числового обозначения месяца на словесное (склоненное в падеже)
                    $m = str_replace($month_int, $month_str, $m);
                    // Формирование окончательного результата
                    $result = $d . ' ' . $m . ' ' . $y;
                }
        }
        return $result;
    }
    public static function mysql_date($datestr = '')
    {
        if ($datestr == '')
            return '';

        // получаем значение даты и времени
        list($day) = explode(' ', $datestr);

        switch ($day) {
                // Если дата совпадает с сегодняшней
            case date('Y-m-d'):
                $result = 'Сегодня';
                break;

                //Если дата совпадает со вчерашней
            case date('Y-m-d', mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"))):
                $result = 'Вчера';
                break;

            default: {
                    // Разделяем отображение даты на составляющие
                    list($y, $m, $d)  = explode('-', $day);


                    $month_str = array(
                        'Января', 'Февраля', 'Марта',
                        'Апреля', 'Мая', 'Июня',
                        'Июля', 'Августа', 'Сентября',
                        'Октября', 'Ноября', 'Декабря'
                    );
                    $month_int = array(
                        '01', '02', '03',
                        '04', '05', '06',
                        '07', '08', '09',
                        '10', '11', '12'
                    );

                    // Замена числового обозначения месяца на словесное (склоненное в падеже)
                    $m = str_replace($month_int, $month_str, $m);
                    // Формирование окончательного результата
                    $result = $d . ' ' . $m . ' ' . $y;
                }
        }
        return $result;
    }
    public static function getMonthName($date, $lang)
    {
        $monthAr = array(
            "uz-Cyr" => array(
                ('Январ'),
                ('Феврал'),
                ('Март'),
                ('Апрел'),
                ('Май'),
                ('Июн'),
                ('Июл'),
                ('Август'),
                ('Сентябр'),
                ('Октябр'),
                ('Ноябр'),
                ('Декабр')
            ),
            "uz-Lat" => array(
                ('Yanvar'),
                ('Fevral'),
                ('Mart'),
                ('Aprel'),
                ('May'),
                ('Iyun'),
                ('Iyul'),
                ('Avgust'),
                ('Sentyabr'),
                ('Oktyabr'),
                ('Noyabr'),
                ('Dekabr')
            ),
            "en" => array(
                ('January'),
                ('February'),
                ('March'),
                ('April'),
                ('May'),
                ('June'),
                ('July'),
                ('August'),
                ('September'),
                ('October'),
                ('November'),
                ('December')
            ),
            'ru' => array(
                ('Января'),
                ('Февраля'),
                ('Марта'),
                ('Апреля'),
                ('Мая'),
                ('Июня'),
                ('Июля'),
                ('Августа'),
                ('Сентября'),
                ('Октября'),
                ('Ноября'),
                ('Декабря')
            ),
        );
        $date_arr = date_parse($date);
        $month_number = $date_arr['month'];
        if ($month_number >= 1 && $month_number <= 12)
            return $monthAr[$lang][$month_number - 1];
        else
            return -1;
    }
    public static function getCategoryNews($id)
    {
        $query = new \yii\db\Query();
        $rows = $query->select(['news.*'])
            ->from('news_category')->where(['category_id' => $id])
            ->join('INNER JOIN', 'news', 'news_category.news_id = news.id')->limit(2)->orderBy(['published_date' => SORT_DESC])->all();
        return $rows;
    }
    public static function getCategoryPosts($c_id, $group)
    {
        $posts = Post::find()->where(['category_id' => $c_id, 'group' => $group, 'status' => 'active'])->all();
        return $posts;
    }
    public static function getEachMediaInfo($medias = [])
    {
        return array_map(function ($item) {
            return  ['caption' => $item->name, 'size' => $item->size, 'width' => $item->width, 'url' => "delete-media", 'key' => $item->id];
        }, $medias);
    }
    public static function getSetting($id)
    {
        $post = Post::findOne(['id' => $id]);
        if ($post) {
            return $post->content;
        } else {
            return '';
        }
    }
    public static function deleteDir($dirPath)
    {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
    public static function getMediaInnerDirctory($file_hash, $group = null)
    {
        if ($group) {
            return Yii::getAlias('@web') . "/postfiles/$group/" . implode(DIRECTORY_SEPARATOR, [
                mb_strtolower(mb_substr($file_hash, 0, 2)),
                mb_strtolower(mb_substr($file_hash, 2, 2))
            ]);
        } else {
            return implode(DIRECTORY_SEPARATOR, [
                mb_strtolower(mb_substr($file_hash, 0, 2)),
                mb_strtolower(mb_substr($file_hash, 2, 2))
            ]);
        }
    }
    public static function getMediaUrls($medias = [])
    {
        return array_map(function ($item) {
            return $item->getUrl();
        }, $medias);
    }
    public static function t($given, $lang = null)
    {
        $modfd = json_decode($given, true);
        if ($lang) {
            return isset($modfd) ? $modfd[$lang] : '';
        }
        return isset($modfd) ? $modfd[Yii::$app->language] : '';
    }
    public static function getNationality($n)
    {
        if (!$n) {
            return 'Неизвестный';
        }
        $nations = [
            'uz-Lat' => [
                '1' => 'O\'zbek',
                '2' => 'Rus',
                '3' => 'Tojik',
                '4' => 'Qozoq',
                '5' => 'Boshqa'
            ],
            'uz-Cyr' => [
                '1' => 'Ўзбек',
                '2' => 'Рус',
                '3' => 'Тожик',
                '4' => 'Қозоқ',
                '5' => 'Boshqa'
            ],
            'ru' => [
                '1' => 'Узбек',
                '2' => 'Русский',
                '3' => 'Таджик',
                '4' => 'Казак',
                '5' => 'Другое'
            ],
        ];
        return $nations[Yii::$app->language][$n];
    }
    public static function getStatus($n, $class = false)
    {
        $words = [
            'unknown' =>Yii::t('admin', 'unknown'),
            'accepted' => Yii::t('admin', 'accepted'),
            'finished' => Yii::t('admin', 'finished'),
            'aborted' => Yii::t('admin', 'aborted'),
            'cancelled' => Yii::t('admin', 'cancelled'),
            'updated' => Yii::t('admin', 'updated_status'),
        ];
        $classes = [
            'unknown' => 'btn btn-warning btn-xs',
            'accepted' => 'btn btn-success btn-xs',
            'finished' => 'btn btn-primary btn-xs',
            'aborted' => 'btn btn-info btn-xs',
            'cancelled' => 'btn btn-danger btn-xs',
            'updated' => 'btn btn-warning btn-xs',


        ];
        if ($class) {
            return $classes[$n];
        } else {
            return $words[$n];
        }
    }
    public static function getAppType($n)
    {
        if (!$n) {
            return 'Неизвестный';
        }
        $types = [
            'uz-Lat' => [
                'inner' => 'Ichki nomzod',
                'outer' => 'Tashqi nomzod',
            ],
            'uz-Cyr' => [
                'inner' => 'Ички номзод',
                'outer' => 'Ташқи номзод',
            ],
            'ru' => [
                'inner' => 'Внутренний кандидат',
                'outer' => 'Внешний кандидат',
            ],
        ];
        return $types[Yii::$app->language][$n];
    }
    public static function getCanSpeak($n)
    {
        if (!$n) {
            return 'Неизвестный';
        }
        $types = [
            'uz-Lat' => [
                '1' => 'Ingliz',
                '2' => 'Nemis',
                '3' => 'Fransuz'
            ],
            'uz-Cyr' => [
                '1' => 'Инглиз',
                '2' => 'Немис',
                '3' => 'Франсуз'
            ],
            'ru' => [
                '1' => 'Английский',
                '2' => 'Немецкий',
                '3' => 'Французский'
            ],
        ];
        return $types[Yii::$app->language][$n];
    }
    public static function getFaculty($n, $lang = null)
    {
        if (!$n) {
            return 'Неизвестный';
        }
        $district = AdmissionFaculties::findOne(['id' => $n]);
        if ($lang) {
            return json_decode($district->title, true)[$lang];
        }
        return json_decode($district->title, true)[Yii::$app->language];
    }
    public static function getCityDistrict($c_id, $lang = null, $c_id_needed = false, $d_id_needed = false)
    {
        $district = Districts::findOne(['id' => $c_id]);
        if (!$district) {
            return (object) [
                'city' => 'Не указано',
                'district' => 'Не указано'
            ];
        }
        $city = Cities::findOne(['id' => $district->city_id]);
        if ($lang) {
            return (object) [
                'city' => json_decode($city->title, true)[$lang],
                'district' => json_decode($district->title, true)[$lang]
            ];
        }
        if ($c_id_needed) {
            return $city->id;
        }
        if ($d_id_needed) {
            return $district->id;
        }
        return (object) [
            'city' => json_decode($city->title, true)[Yii::$app->language],
            'district' => json_decode($district->title, true)[Yii::$app->language]
        ];
    }
    public static function getFirstMedia($id)
    {
        $mediaOrder = OrderMedia::findOne(['model_id' => $id]);
        if ($mediaOrder) {
            return  Yii::getAlias('@web') . '/postfiles/documents/' . $mediaOrder->url;
        } else {
            return '';
        }
    }
    public static function getPoster($id, $group)
    {
        $mediaOrder = OrderMedia::find()->where(['model_id' => $id])->andWhere(['like', 'url', '%jpg', false])->one();
        if ($mediaOrder) {
            return  Yii::getAlias('@web') . "/postfiles/${group}/" . $mediaOrder->url;
        } else {
            return '';
        }
    }
    public static function hasMedia($id, $group = null)
    {
        if ($group) {
            $mediaOrder = OrderMedia::findOne(['model_id' => $id, 'category' => $group]);
            if ($mediaOrder) {
                return true;
            } else {
                return false;
            }
        }
        $mediaOrder = OrderMedia::findOne(['model_id' => $id]);
        if ($mediaOrder) {
            return true;
        } else {
            return false;
        }
    }
    public static function getFirstMediaFileFull($id, $group, $ext)
    {
        $mediaOrder = OrderMedia::find()->where(['model_id' => $id])->andWhere(['like', 'url', "%${ext}", false])->one();
        if ($mediaOrder) {
            $found = File::findOne(['id' => $mediaOrder->media_id]);
            return [
                'id' => $found['id'],
                'parentDir' => self::getMediaInnerDirctory($found['hash'], $group)
            ];
        } else {
            return [
                'id' => '',
                'parentDir' => ''
            ];
        }
    }

    public static function getFirstMediaFile($id)
    {
        $mediaOrder = OrderMedia::findOne(['model_id' => $id]);
        if ($mediaOrder) {
            return File::findOne(['id' => $mediaOrder->media_id]);
        } else {
            return '';
        }
    }
    public static function getFirstImagePath($id)
    {
        $media = OrderMedia::findOne(['category' => 'news', 'model_id' => $id]);
        if ($media) {
            return (new File)->findOne(['id' => $media->media_id])->getAbsolutePath();
        } else {
            return Yii::$app->request->baseUrl . 'images/logo.png';
        }
    }

    public static function getFirstNewsMedia($id)
    {
        $mediaOrder = OrderMedia::findOne(['model_id' => $id, 'category' => 'news']);
        if ($mediaOrder) {
            return  Yii::getAlias('@web') . '/postfiles/documents/' . $mediaOrder->url;
        } else {
            return '';
        }
    }
    public static function getFirstNewsMediaFile($id)
    {
        $mediaOrder = OrderMedia::findOne(['model_id' => $id, ['category' => 'news']]);
        if ($mediaOrder) {
            return File::findOne(['id' => $mediaOrder->media_id]);
        } else {
            return '';
        }
    }
    public static function deleteFolder($given_path)
    {
        $url = parse_url($given_path);
        $params = explode('/', $url['path']);
        if (strpos($params[3], '\\') !== false) {
            $fcd_path = explode('\\', $params[3])[0];
            $path = Yii::$app->basePath . '/web/uploads/' . $params[2] . '/' . $fcd_path;
        } else {
            $path = Yii::$app->basePath . '/web/uploads/' . $params[2] . '/' . $params[3];
        }
        FileHelper::removeDirectory($path);
    }
    public static function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") self::rrmdir($dir . "/" . $object);
                    else unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
    
    // public static function getDistrict($d_id, $lang=null){
    //     $district=Districts::findOne(['id'=>$d_id])->city();
    //     if($lang){
    //         return json_decode($district->title, true)[$lang];            
    //     }
    //     return json_decode($district->title, true)[Yii::$app->language];
    // }
}
