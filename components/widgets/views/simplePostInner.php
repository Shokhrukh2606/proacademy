<?php

use yii\helpers\Html as H;
?>
<div class="blog-details mb-50">
    <!-- <div class="blog-details-img">
                        <img src="assets/images/news/blog-details.jpg" alt="Blog Details">
                    </div> -->
    <div class="blog-details-author">
        <?=$post?>
        <span class="news-date"><?= (isset($post->created_at)) ? H::encode($post->created_at) : '' ?></span>
    </div>
    <div class="blog-details-content page">
        <?= (isset($post->short_content)) ? $post->short_content : '' ?>
        <?= (isset($post->content)) ? $post->content : '' ?>
    </div>
</div>