<?php

use yii\helpers\Html as H;
use yii\helpers\Json as J;
use  yii\helpers\Url;
?>

<?php
echo H::ul($menu, [
    'class' => 'navigation clearfix',
    'item' => function ($item, $index) {
        if ($item->options === 'home') {
            return H::tag('li', H::a(J::decode($item->title)[Yii::$app->language], '/'));
        } else
        if ($item->childs != null) {
            $parentNavigator = H::a(J::decode($item->title)[Yii::$app->language], '#');
            $subChilds = H::ul($item->childs, [
                'item' => function ($subItem, $subIndex) {
                    return H::tag('li', H::a(J::decode($subItem->title)[Yii::$app->language], ($subItem->options) ? Url::to([$subItem->options . '/index']) : Url::toRoute(['menu/' . $subItem->alias])));
                }
            ]);
            return H::tag('li', $parentNavigator . $subChilds, ['class' => 'dropdown']);
        } else {
            return H::tag('li', H::a(J::decode($item->title)[Yii::$app->language], ($item->options) ? Url::to([$item->options . '/index']) : Url::toRoute(['menu/' . $item->alias])));
        }
    }
]);
// Yii::app()->controller->getAction()->getId()
?>