<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\widgets;

use app\components\helpers\MyHelpers as M;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html as H;
use  yii\helpers\Url;

class CardWidget extends Widget
{
    public $template = '<div class="col-md-6 col-lg-4 mb-4">
    <div class="big-news">
        <div class="img">
            <a href="{link}"><img src="{imglink}" alt="news"></a>
            <div class="category">{category}</div>
        </div>
        <div class="content">
            <a href="{link}">
                {title}
            </a>
            <div>
                <p>{short_content}</p>
            </div>

            <div class="infos">
                <div class="date">
                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 18 20">
                        <path id="ic_date_range_24px" d="M9,11H7v2H9Zm4,0H11v2h2Zm4,0H15v2h2Zm2-7H18V2H16V4H8V2H6V4H5A1.991,1.991,0,0,0,3.01,6L3,20a2,2,0,0,0,2,2H19a2.006,2.006,0,0,0,2-2V6A2.006,2.006,0,0,0,19,4Zm0,16H5V9H19Z" transform="translate(-3 -2)" fill="#9e9da6" />
                    </svg>
                    <span>{date}</span>
                </div>
                <div class="views">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="13" viewBox="0 0 22 15">
                        <path id="ic_remove_red_eye_24px" d="M12,4.5A11.827,11.827,0,0,0,1,12a11.817,11.817,0,0,0,22,0A11.827,11.827,0,0,0,12,4.5ZM12,17a5,5,0,1,1,5-5A5,5,0,0,1,12,17Zm0-8a3,3,0,1,0,3,3A3,3,0,0,0,12,9Z" transform="translate(-1 -4.5)" fill="#9e9da6" />
                    </svg>
                    <span>{view_count}</span>
                </div>
            </div>
        </div>
    </div>
</div>';
    public $title, $short_content, $content, $link, $imglink, $comment_count, $category,$date;
    public function run()
    {
        if (empty($this->title)) {
            return;
        }
        echo strtr($this->template, [
            '{title}' => $this->limiter(M::t($this->title), 40),
            '{content}' => $this->limiter(M::t($this->content), 25),
            '{category}'=>M::t($this->category),
            '{short_content}' => $this->limiter(M::t($this->short_content), 45),
            '{view_count}'=>$this->comment_count,
            '{link}' => Url::to(['news/view', 'alias' => $this->link]),
            '{date}'=>(Yii::$app->language=='ru')?M::mysql_date($this->date):((Yii::$app->language=='uz-Lat')?M::mysql_oz_date($this->date):((Yii::$app->language=='uz-Cyr')?M::mysql_uz_date($this->date):M::mysql_english_date($this->date))),
            '{imglink}' => $this->imglink
        ]);
    }
    public function limiter($x, $limit)
    {
        if (mb_strlen($x) > $limit)
            $x = mb_substr($x, 0, $limit) . '&hellip;';
        else
            $x = $x;
        return $x;
    }
}
