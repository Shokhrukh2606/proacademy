<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html as H;
use  yii\helpers\Url;

class CategoryWidget extends Widget
{
//     <div class="widget">
//     <h3 class="title">Categories</h3>
//     <div class="category-widget">
//         <ul>
//             <li><a href="#">Market Growthing</a></li>
//             <li><a href="#">Creative Invention</a></li>
//             <li><a href="#">Startup Investment</a></li>
//             <li><a href="#">Professional Approach</a></li>
//             <li><a href="#">Invesment Planning</a></li>
//         </ul>
//     </div>
// </div>
    public $template='<div class="widget">{content}</div>';
    public $links = [];
    public $parentTitle;
    public $tag='div';
    public $activeId;
    public function run()
    {
        if (empty($this->links)) {
            return;
        }
        $neededLinks = [];
        foreach ($this->links as $link) {
            $linker=H::tag('a',json_decode($link->title, true)[Yii::$app->language], ['href'=>($link->options) ? Url::to([$link->options . '/index']) : Url::toRoute(['menu/' . $link->alias])]);
            $neededLinks[] = H::tag('li', $linker,['class'=>($this->activeId==$link->id)?'active':'inactive']);
        }
        $linksList=H::tag('ul', implode('', $neededLinks));
        $area=H::tag('div', $linksList, ['class'=>'category-widget']);
        $parentTitle=H::tag('h2', $this->parentTitle);
        echo strtr($this->template, ['{content}' => $parentTitle.$area]);
    }
}
