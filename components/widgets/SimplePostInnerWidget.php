<?php
namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
class SimplePostInnerWidget extends Widget
{
    public $created_at;
    public $title;
    public $content;
    public $short_content;
    public $sended;
    public function init()
    {
        parent::init();
        if ($this->created_at) {
            $this->sended['created_at']=$this->created_at;
        }
        if ($this->title) {
            $this->sended['title']=json_decode($this->title)['ru'];
        }
        if ($this->title) {
            $this->sended['short_content']=json_decode($this->short_content)['ru'];
        }
        if ($this->title) {
            $this->sended['content']=json_decode($this->content)['ru'];
        }
    }

    public function run()
    {
        return $this->render('simplePostInner', ['post'=>$this->title]);
    }
}