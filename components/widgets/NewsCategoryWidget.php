<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html as H;
use  yii\helpers\Url;

class NewsCategoryWidget extends Widget
{

    public $template='<div class="widget">{content}</div>';
    public $links = [];
    public $parentTitle;
    public $tag='div';
    public $activeId;
    public function run()
    {
        if (empty($this->links)) {
            return;
        }
        $neededLinks = [];
        foreach ($this->links as $link) {
            if($link->id!=2){
            $linker=H::tag('a',json_decode($link->title, true)[Yii::$app->language], ['href'=> Url::toRoute(['news/category/', 'id'=>$link->id])]);
            $neededLinks[] = H::tag('li', $linker,['class'=>($this->activeId==$link->id)?'active':'inactive']);
            }
        }
        $linksList=H::tag('ul', implode('', $neededLinks));
        $area=H::tag('div', $linksList, ['class'=>'category-widget']);
        $parentTitle=H::tag('h3', Yii::t('front', 'news_cat'));
        echo strtr($this->template, ['{content}' => $parentTitle.$area]);
    }
}
