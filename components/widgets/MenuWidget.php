<?php
namespace app\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use app\models\Menu;
class MenuWidget extends Widget
{
    public $menu;

    public function init()
    {
        parent::init();
        if ($this->menu === null) {
            $this->menu=Menu::menuTree();
        }
    }

    public function run()
    {
        return $this->render('menu', ['menu'=>$this->menu]);
    }
}