<?php

namespace app\controllers;

use app\models\Category;
use app\models\News;
use app\models\NewsCategory;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use app\models\OrderMedia;
use app\components\helpers\MyHelpers;
use Yii;
use yii\helpers\Url;


class NewsController extends \yii\web\Controller
{
    public $news_category;
    public function init()
    {
        parent::init();
        $this->news_category = ArrayHelper::index(Category::findBySql('SELECT id, title, alias FROM category WHERE `status`=\'active\'')->all(), 'id');
    }
    public function actionIndex()
    {
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => Yii::t('front', 'site') ,
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:type',
            'content' => 'website',
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => 'Et docere et discere servitute legis',
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => Url::to('/images/logo.png', true),
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image:type',
            'content' => 'image/png',
        ]);
        $query = News::find()->where(['status' => 'active'])->orderBy(['published_date' => SORT_DESC]);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count,
        'defaultPageSize' => 10]);
        $news = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        return $this->render('index', compact('news', 'pagination'));
    }
    public function actionView($alias)
    {
        $post = News::findOne(['id' => $alias]);
        $post->updateCounters(['views' => 1]);
        //Далее регистрируем теги для соцсетей
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => MyHelpers::t($post->title),
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:type',
            'content' => 'website',
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => MyHelpers::t($post->short_content),
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => Url::to('/images/logo.png', true),
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image:type',
            'content' => 'image/png',
        ]);
        $query = new \yii\db\Query();
        $tags = $query->select(['tags.*'])
            ->from('news_tags')->where(['news_id' => $post->id])
            ->join('INNER JOIN', 'tags', 'news_tags.tag_id = tags.id')->all();
        $medias = OrderMedia::find()->where(['model_id' => $post->id, 'category' => 'news'])->all();
        return $this->render('view', compact('post', 'tags', 'medias'));
    }
    public function actionCategory($id)
    {
        $cat = Category::findOne(['id' => $id]);
        $query = new \yii\db\Query();
        $rows = $query->select(['news.*'])
            ->from('news_category')->where(['category_id' => $id])
            ->join('INNER JOIN', 'news', 'news_category.news_id = news.id')->orderBy(['published_date' => SORT_DESC]);
        $count = $rows->count();
        $pagination = new Pagination(['totalCount' => $count,
        'defaultPageSize' => 10]);
        $news = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        return $this->render('category', compact('news', 'cat', 'pagination'));
    }
}
