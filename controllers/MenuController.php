<?php

namespace app\controllers;

use app\models\Menu;
use app\models\Post;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\components\helpers\MyHelpers;
use yii\helpers\Url;

class MenuController extends Controller
{
    public $menu;
    public function init()
    {
        parent::init();
        $this->menu = ArrayHelper::index(Menu::findBySql('SELECT id, title, category_id FROM menu')->where(['status' => 'active'])->all(), 'id');
    }
    
    public function actionIndex($alias)
    {
        $menu = Menu::find()->where(['alias' => $alias])->one();
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => MyHelpers::t($menu->title) ,
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:type',
            'content' => 'website',
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' =>  MyHelpers::t($menu->short_content)?MyHelpers::limiter(strip_tags(MyHelpers::t($menu->short_content)), 200) :MyHelpers::limiter(strip_tags(MyHelpers::t($menu->content)),200) ,
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => Url::to('/images/logo.png', true),
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image:type',
            'content' => 'image/png',
        ]);
        if(isset($menu->category_id)&&$menu){
            $categories = Menu::menuWithSameCategory($menu->category_id);
            return $this->render('index', ['menu'=>$menu,'categories' => $categories]);
        }else{
            $post=Post::find()->where(['alias'=>$alias])->one();            
            return $this->render('index', ['menu'=>$post]);
        }
        
    }
}
