<?php

namespace app\controllers;

use app\components\helpers\MyHelpers;
use app\models\AdmissionFaculties;
use app\models\AdmissionYear;
use app\models\Applicants;
use app\models\Menu;
use app\models\News;
use app\models\Tags;
use app\models\NewsTags;
use app\models\Post;
use app\models\Listener;
use app\models\StatesReceiverModel;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use Yii;
use yii\db\Expression;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\VideoLessons;
use app\models\Poll;
use yii\helpers\Json;
use app\models\Subscribers;
use app\models\Polls;
use Codeception\Util\HttpCode;

class ProacademyController extends \yii\web\Controller
{
    public $menu, $interactives;
    public $active_year;
    public function init()
    {
        parent::init();
        $this->active_year = AdmissionYear::findOne(['status' => 1]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => Yii::t('front', 'site'),
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:type',
            'content' => 'website',
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => 'Et docere et discere servitute legis',
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => Url::to('/images/logo.png', true),
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image:type',
            'content' => 'image/png',
        ]);
        $this->menu = ArrayHelper::index(Menu::findBySql('SELECT id, title, category_id FROM menu')->where(['status' => 'active'])->all(), 'id');
        $this->interactives = ArrayHelper::index(Post::findBySql('SELECT id, title, category_id FROM posts')->where(['status' => 'active', 'interactives_category'])->all(), 'id');
    }
    public function actionLessons($category = null)
    {
        $categories = VideoLessons::find()->where(['lesson_group' => 'cats'])->all();
        if ($category) {
            $query = VideoLessons::find()->where(['category' => $category, 'lesson_group' => 'video']);
            $count = $query->count();
            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => 20
            ]);
            $videos = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
        } else {
            $query = VideoLessons::find()->where(['lesson_group' => 'video']);
            $count = $query->count();
            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => 20
            ]);
            $videos = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
        }
        return $this->render('/proacademy/lessons', compact('videos', 'pagination', 'categories'));
    }
    public function actionSubscribe($email)
    {
        $subscriber = new Subscribers();
        $subscriber->email = $email;
        $subscriber->save();
        return $this->redirect(Yii::$app->request->referrer);
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'global-search' => ['GET']
                ],
            ],
        ];
    }
    public function actionHeads()
    {
        $heads = Post::find()->where(['status' => 'inactive', 'group' => 'heads'])->orderBy(['id' => SORT_DESC])->all();
        return $this->render('heads', compact('heads'));
    }
    public function actionDoodly()
    {
        $doodly = Post::find()->where(['status' => 'active', 'group' => 'doodly'])->orderBy(['id' => SORT_DESC])->all();
        return $this->render('doodly', compact('doodly'));
    }
    public function actionQuestionnaire()
    {
        $questionnaire = Post::find()->where(['status' => 'active', 'group' => 'questionnaire'])->orderBy(['id' => SORT_DESC])->all();
        return $this->render('questionnaire', compact('questionnaire'));
    }
    public function actionQuestionnaireView($alias)
    {
        if($alias){
            $question = Post::findOne(['alias' => $alias, 'group' => 'questionnaire']);
            return $this->render('questionnaire', compact('question'));
        }else{
            return HttpCode::NOT_FOUND;
        }
    }
    public function bot($method, $datas = [])
    {
        $url = "https://api.telegram.org/bot" . Yii::$app->params['TELEGRAM_API_KEY'] . "/" . $method;
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, $datas);

        $r = curl_exec($c);
        if (curl_error($c)) {
            var_dump(curl_error($c));
        } else {
            return json_decode($r);
        }
    }
    public function filter_dateset($update)
    {
        $polls = array();
        foreach ($update as $item) {
            if (isset($item->poll))
                array_push($polls, $item->poll);
        }
        return $polls;
    }
    public function actionPollsStatistics()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);


        // basic function to make request to telegram bot api endpoints

        $updates = $this->bot('getUpdates', ['allowed_updates' => json_encode(['poll'])]);


        $polls = $this->filter_dateset($updates->result);
        // print_r($polls);
        for ($i = 0; $i < count($polls); $i++) {
            $oldPoll = Poll::findOne(['poll_id' => $polls[$i]->id]);
            if ($oldPoll) {
                continue;
            } else {
                $tPoll = $polls[$i];
                $newPoll = new Poll();
                $newPoll->poll_id = $tPoll->id;
                $newPoll->question = $tPoll->question;
                $newPoll->options = json_encode($tPoll->options, JSON_UNESCAPED_UNICODE);
                $newPoll->total_votes = $tPoll->total_voter_count;
                $newPoll->save();
            }
        }
    }
    public function actionPolls()
    {
        $this->actionPollsStatistics();
        $query = Poll::find()->orderBy(['id' => SORT_DESC]);
        $count = $query->count();
        $pagination = new Pagination([
            'totalCount' => $count,
            'defaultPageSize' => 12
        ]);
        $polls = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        return $this->render('polls', compact('polls', 'pagination'));
    }
    public function actionPollsView($id)
    {
        $lastPoll = Poll::findOne(['id' => $id]);
        return $this->render('polls_view', compact('lastPoll'));
    }
    public function actionELibrary($alias = null, $category = null)
    {
        $categories = Post::find()->where(['group' => 'library_category', 'status' => 'active'])->orderBy(['id' => SORT_ASC])->all();
        if ($alias) {
            $book = Post::findOne(['group' => 'books', 'status' => 'active', 'id' => $alias]);
            return $this->render('book_info', compact('categories', 'book'));
        }
        if ($category) {
            $query = Post::find()->where(['group' => 'books', 'status' => 'active', 'category_id' => $category])->orderBy(['id' => SORT_DESC]);
            $count = $query->count();
            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => 20
            ]);
            $books = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            return $this->render('e-library', compact('books', 'categories', 'pagination'));
        } else {
            $query = Post::find()->where(['group' => 'books', 'status' => 'active'])->orderBy(['id' => SORT_DESC]);
            $count = $query->count();
            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => 20
            ]);
            $books = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            return $this->render('e-library', compact('books', 'categories', 'pagination'));
        }
    }
    public function actionCheckYourself($category = null)
    {
        $categories = Post::find()->where(['group' => 'checks_category', 'status' => 'active'])->orderBy(['id' => SORT_DESC])->all();
        if ($category) {
            $questions = Post::find()->where(['group' => 'checks', 'status' => 'active', 'category_id' => $category])->limit(20)->orderBy(['id' => 'rand()'])->all();
        } else {
            $questions = Post::find()->where(['group' => 'checks', 'status' => 'active', 'category_id' => $categories[0]->id])->limit(20)->orderBy(['id' => 'rand()'])->all();
        }
        return $this->render('check_yourself', compact('questions', 'categories'));
    }
    public function actionGallery($category = null)
    {
        if ($category == 'videos') {
            try {
            $query = Post::find()->where(['status' => 'active', 'group' => 'videos'])->andWhere(['!=', new Expression("JSON_EXTRACT(title, '$.\"" . Yii::$app->language . "\"')"), ''])->orderBy(['created_at' => SORT_DESC]);
            $count = $query->count();
            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => 20
            ]);
            $images = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            return $this->render('gallery', compact('images', 'pagination'));
            }
            catch (\Exception $ex) {
                //Выводим сообщение об исключении.
                $images=[];
                $pagination=new Pagination([
                    'totalCount' => 20,
                    'defaultPageSize' => 20
                ]);;
                return $this->render('gallery', compact('images', 'pagination'));
            }
        }
        if ($category == 'photos') {
            $query = new \yii\db\Query();
            $count = $query->select(['files.*'])
                ->from('media_order')->where(['category' => ['photos', 'news']])
                ->join('INNER JOIN', 'files', 'media_order.media_id = files.id');
            $count = $query->count();
            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => 20
            ]);
            $images = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            return $this->render('photos', compact('images', 'pagination'));
        } else {
            try {
                $query = Post::find()->where(['status' => 'active', 'group' => 'videos'])->andWhere(['!=', new Expression("JSON_EXTRACT(title, '$.\"" . Yii::$app->language . "\"')"), ''])->orderBy(['created_at' => SORT_DESC]);
                $count = $query->count();
                $pagination = new Pagination([
                    'totalCount' => $count,
                    'defaultPageSize' => 20
                ]);
                $images = $query->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();
                return $this->render('gallery', compact('images', 'pagination'));
            }
            //Перехватываем (catch) исключение, если что-то идет не так.
            catch (\Exception $ex) {
                //Выводим сообщение об исключении.
                $images=[];
                $pagination=new Pagination([
                    'totalCount' => 20,
                    'defaultPageSize' => 20
                ]);;
                return $this->render('gallery', compact('images', 'pagination'));
            }
        }
    }
    public function actionStatesReceiver()
    {
        $model = new StatesReceiverModel();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->saver()) {
                Yii::$app->session->setFlash('success', 'Siz kiritgan ma\'lumotlar yuborildi');
                return $this->goHome();
            } else {
                $model->setAttributes([
                    'last_name' => null, 'first_name' => null, 'middle_name' => null,
                    'email' => null, 'phone' => null, 'position_place' => null, 'file' => null
                ]);
                Yii::$app->session->setFlash('error', print_r($model->getErrors()));
            }
        }
        return $this->render('states_receiver', compact('model'));
    }
    public function actionTests($alias = null)
    {
        if ($alias) {
            $set = Post::findOne(['alias' => $alias, 'group' => 'sets']);
            $query = new \yii\db\Query();
            $files = $query->select(['files.*'])
                ->from('media_order')->where(['model_id' => $set->id, 'category' => 'sets'])
                ->join('INNER JOIN', 'files', 'media_order.media_id = files.id')->all();
            return $this->render('one_test_archive', compact('set', 'files'));
        } else {
            $sets = Post::find()->where(['group' => 'sets'])->orderBy('sort_order', SORT_ASC)->all();
            return $this->render('tests_archive', compact('sets'));
        }
    }
    public function actionMasters($alias = null)
    {
        $menuCat = 56;
        $menuActive = 75;
        if ($alias) {
            $post = News::findOne(['id' => $alias]);
            return $this->render('static', compact('post', 'menuCat', 'menuActive'));
        }
        $query = new \yii\db\Query();
        $rows = $query->select(['news.*'])
            ->from('news_category')->where(['category_id' => 12])
            ->join('INNER JOIN', 'news', 'news_category.news_id = news.id')->orderBy(['published_date'=>SORT_DESC]);
        $count = $rows->count();
        $pagination = new Pagination(['totalCount' => $count]);
        $masters = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        return $this->render('masters', compact('masters', 'pagination'));
    }
    public function actionCooperationInt($alias = null)
    {
        $menuCat = 61;
        $menuActive = 88;
        if ($alias) {
            $post = Post::findOne(['alias' => $alias, 'group' => 'cooperation_int']);
            return $this->render('static', compact('post', 'menuCat', 'menuActive'));
        }
        $literacy = Post::find()->where(['status' => 'active', 'group' => 'cooperation_int'])->all();
        return $this->render('cooperation_int', compact('literacy'));
    }
    public function actionCooperationInter($alias = null)
    {
        $menuCat = 61;
        $menuActive = 88;
        if ($alias) {
            $post = Post::findOne(['alias' => $alias, 'group' => 'cooperation_inter']);
            return $this->render('static', compact('post', 'menuCat', 'menuActive'));
        }
        $literacy = Post::find()->where(['status' => 'active', 'group' => 'cooperation_inter'])->all();
        return $this->render('cooperation_inter', compact('literacy'));
    }
    public function actionLiteracy($alias = null)
    {
        $menuCat = 61;
        $menuActive = 88;
        if ($alias) {
            $post = Post::findOne(['alias' => $alias, 'group' => 'literacy']);
            return $this->render('static', compact('post', 'menuCat', 'menuActive'));
        }
        $literacy = Post::find()->where(['status' => 'active', 'group' => 'literacy'])->all();
        return $this->render('literacy', compact('literacy'));
    }
    public function actionAntiCorruption()
    {
        $dataProvider = Json::encode(Menu::findBySql('SELECT id, title, category_id, alias FROM menu WHERE id IN (110,113,114,115,116,117,118,119)')->all());
        // $dataProvider = Json::encode(Menu::find()->select('id, category_id, title, alias')->where(['=','id',[110,113,114,115,116,117,118,119]])->all());
        return $this->render('anti_corruption', compact('dataProvider'));
    }
    public function actionInteractives()
    {
        $interactives = Post::find()->where(['group' => 'interactives_category', 'status' => 'active'])->all();
        return $this->render('interactives', compact('interactives'));
    }
    // Interactives/FAQ 
    public function actionFaq($category = null)
    {

        $categories = Post::find()->where(['group' => 'faq_category', 'status' => 'active'])->orderBy(['id' => SORT_DESC])->all();
        if ($category) {
            $cat = Post::find()->where(['id' => $category])->one();
            $query = Post::find()->where(['group' => 'faq', 'category_id' => $category, 'status' => 'active'])->orderBy(['id' => SORT_DESC]);
            $count = $query->count();
            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => 20
            ]);
            $faqs = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            return $this->render('faqs', compact('faqs', 'pagination', 'categories', 'cat'));
        } else {
            $query = Post::find()->where(['group' => 'faq', 'status' => 'active'])->orderBy(['id' => SORT_DESC]);
            $count = $query->count();
            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => 20
            ]);
            $faqs = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            return $this->render('faqs', compact('faqs', 'pagination', 'categories'));
        }
    }
    // Interactives/FAQ 
    // Interactives/Check Attestation 

    public function actionCheckAttestation($certificate = null)
    {
        if ($certificate) {
            $found = Listener::findOne(['certificate_number' => $certificate]);
            return $this->render('check_attestation', compact('found'));
        }
        $model = new Listener();
        if ($model->load(Yii::$app->request->post())) {
            $found = Listener::findOne(['certificate_number' => $model->certificate_number]);
            return $this->render('check_attestation', compact('found'));
        }
        return $this->render('check_attestation', compact('model'));
    }

    // Interactives/Check Attestation 
    // Search Page
    public function actionGlobalSearch($param, $hashed = null)
    {
        $came = $param;
        if ($hashed) {
            $needed = substr($came, strpos($came, "#") + 1);
            $tag = Tags::find()->where(['like', 'title',  '%' . $needed . '%', false])->one();
            if ($tag) {
                $query = new \yii\db\Query();
                $count = $query->select(['news.*'])
                    ->from('news_tags')->where(['tag_id' => $tag->id])
                    ->join('INNER JOIN', 'news', 'news_tags.news_id = news.id');
                $count = $query->count();
                $pagination = new Pagination([
                    'totalCount' => $count,
                    'defaultPageSize' => 20
                ]);
                $news = $query->offset($pagination->offset)
                    ->limit($pagination->limit)
                    ->all();
                // echo '<pre>' . $tag->title . '</pre>';
                // echo '<pre>' . print_r($results) . '</pre>';
                return $this->render('/proacademy/search', compact('news', 'pagination'));
            }
        } else {
            $query = new \yii\db\Query();
            $count = $query->select('*')->from('news')->orFilterWhere(['like', 'title', $param])->orFilterWhere(['like', 'content', $param])->orFilterWhere(['like', 'short_content', $param]);
            $count = $query->count();
            $pagination = new Pagination([
                'totalCount' => $count,
                'defaultPageSize' => 20
            ]);
            $news = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            // echo '<pre>' . $tag->title . '</pre>';
            // echo '<pre>' . print_r($results) . '</pre>';
            return $this->render('/proacademy/search', compact('news', 'pagination'));
        }
    }
    // Search Page
    public function actionStatisticsMaster()
    {
        $applicants = [
            'unknown' => Applicants::find()->where(['status' => 'unknown', 'academic_year_id' => $this->active_year])->count(),
            'accepted' => Applicants::find()->where(['status' => 'accepted', 'academic_year_id' => $this->active_year])->count(),
            'updated' => Applicants::find()->where(['status' => 'updated', 'academic_year_id' => $this->active_year])->count(),
            'aborted' => Applicants::find()->where(['status' => 'aborted', 'academic_year_id' => $this->active_year])->count(),
            'cancelled' => Applicants::find()->where(['status' => 'cancelled', 'academic_year_id' => $this->active_year])->count()
        ];

        $faculties = ArrayHelper::map(AdmissionFaculties::findAll(['status' => 'active']), function ($item) {
            return MyHelpers::t($item->title);
        }, function ($item) {
            return Applicants::find()->where(['faculty' => $item->id, 'academic_year_id' => $this->active_year])->count();
        });
        $facultiesAndType = ArrayHelper::map(AdmissionFaculties::findAll(['status' => 'active']), function ($item) {
            return MyHelpers::t($item->title);
        }, function ($item) {
            return [
                'inner' => Applicants::find()->where(['faculty' => $item->id, 'academic_year_id' => $this->active_year, 'applicant_type' => 'inner'])->count(),
                'outer' => Applicants::find()->where(['faculty' => $item->id, 'academic_year_id' => $this->active_year, 'applicant_type' => 'outer'])->count()
            ];
        });
        $app_type = [
            'inner' => Applicants::find()->where(['applicant_type' => 'inner', 'academic_year_id' => $this->active_year])->count(),
            'outer' => Applicants::find()->where(['applicant_type' => 'outer', 'academic_year_id' => $this->active_year])->count()
        ];
        return $this->render('stat_master', compact('applicants', 'faculties', 'app_type', 'facultiesAndType'));
        # code...
    }
}
