<?php

namespace app\controllers;

use app\models\Menu;
use app\models\Post;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Yii;

class LawActsController extends \yii\web\Controller
{
    public $menu;
    public function init()
    {
        parent::init();
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => Yii::t('front', 'site'),
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:type',
            'content' => 'website',
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => 'Et docere et discere servitute legis',
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => Url::to('/images/logo.png', true),
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image:type',
            'content' => 'image/png',
        ]);
        $this->menu = ArrayHelper::index(Menu::findBySql('SELECT id, title, category_id FROM menu')->where(['status' => 'active'])->all(), 'id');
    }
    public function actionIndex()
    {
        $query = Post::find()->where(['group' => 'law-acts'])->orderBy('sort_order', SORT_DESC);
        $category = Menu::findOne(['id' => 62]);
        // get the total number of articles (but do not fetch the article data yet)
        $count = $query->count();

        // create a pagination object with the total count
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 9]);

        // limit the query using the pagination and retrieve the articles
        $laws = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        return $this->render('index',   compact('laws', 'pagination', 'category'));
    }
}
