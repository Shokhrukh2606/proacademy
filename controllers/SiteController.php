<?php

namespace app\controllers;

use app\models\AdmissionYear;
use app\models\Applicants;
use app\models\Category;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Post;
use app\models\User;
use app\models\SignupForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use ozerich\filestorage\models\File;
use yii\web\BadRequestHttpException;
use yii\base\InvalidParamException;
use yii\bootstrap\ActiveForm;
use app\models\Menu;
use app\models\News;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class SiteController extends Controller
{
    protected $academic_year;
    public function init()
    {
        $this->academic_year = AdmissionYear::findOne(['status' => 1]);
        parent::init();
        if (\Yii::$app->language == 'en-US') {
            \Yii::$app->language = 'ru';
        }
        Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => Yii::t('front', 'site'),
        ]);

        Yii::$app->view->registerMetaTag([
            'property' => 'og:type',
            'content' => 'website',
        ]);

        Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => 'Et docere et discere servitute legis',
        ]);
        Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => Url::to('/images/logo.png', true),
        ]);
        Yii::$app->view->registerMetaTag([
            'property' => 'og:image:type',
            'content' => 'image/png',
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /** 
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $interactives = Post::find()->where(['group' => 'interactives_category', 'status' => 'active'])->all();
        $heads = Post::find()->where(['group' => 'heads', 'status' => 'active'])->orderBy(['created_at' => SORT_DESC])->all();
        $news_cats = Category::find()->where(['prop' => 'active'])->all();
        $most_read = News::find()->orderBy(['views' => SORT_DESC])->limit(10)->all();
        $links = Post::findAll(['group' => 'recommended_links']);
        $statistics = Post::findAll(['group' => 'statistics']);
        $videos = Post::find()->where(['group' => 'videos'])->andWhere(['!=', new Expression("JSON_EXTRACT(title, '$.\"" . Yii::$app->language . "\"')"), ''])->orderBy(['created_at' => SORT_DESC])->limit(5)->all();
        return $this->render(
            'index',
            compact('interactives', 'heads', 'news_cats', 'links', 'most_read', 'videos', 'statistics')
        );
    }
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    /**
     * Registred user activation action.
     *
     * @return Response|string
     */
    public function actionValidateKey($key)
    {
        $user = User::findOne(['activation_key' => $key]);
        $admission_year = AdmissionYear::findOne(['status' => 1]);
        $today = date("Y-m-d H:i:s");
        $start_date = date("Y-m-d H:i:s", strtotime($admission_year->start_from));
        $end_date = date("Y-m-d H:i:s", strtotime($admission_year->finishes_on));
        if (($start_date < $today) && ($today < $end_date)) {
            if ($user) {
                if ($user->status == User::STATUS_ACTIVE) {
                    $asOldApplicant = Applicants::findOne(['user_id' => $user->id, 'academic_year_id' => $this->academic_year->id]);
                    if ($asOldApplicant) {
                        Yii::$app->session->setFlash('success', 'Bu email faollashtirilgan holatda!');
                        return $this->goHome();
                    } else {
                        $applicant = new Applicants();
                        $active_year = AdmissionYear::findOne(['status' => AdmissionYear::CURRENT_ACTIVE]);
                        $applicant->academic_year_id = $active_year->id;
                        $applicant->user_id = $user->id;
                        $applicant->generateCheckCode();
                        if ($applicant->save()) {
                            Yii::$app->user->login($user, 3600 * 24 * 30);
                            return $this->redirect(['applicant/default']);
                        } else {
                            print_r($applicant->getErrors());
                        }
                    }
                }
                if ($user->activateUser()) {
                    $applicant = new Applicants();
                    $active_year = AdmissionYear::findOne(['status' => AdmissionYear::CURRENT_ACTIVE]);
                    $applicant->academic_year_id = $active_year->id;
                    $applicant->user_id = $user->id;
                    $applicant->generateCheckCode();
                    if ($applicant->save()) {
                        Yii::$app->user->login($user, 3600 * 24 * 30);
                        return $this->redirect(['applicant/default']);
                    } else {
                        print_r($applicant->getErrors());
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'Bu email ro\'yhatdan o\'tkazilmagagan!');
                    return $this->goHome();
                }
            }
        } else {
            Yii::$app->session->setFlash('error', 'Kechirasiz ushbu yil uchun magistraturaga qabul jarayoni tugagan!');
            return $this->goHome();
        }
    }

    /**
     * Registration action.
     *
     * @return Response|string
     */
    // public function actionRegistration()
    // {
    //     $model = new SignupForm();
    //     if ($model->load($post)) {
    //         if ($errors = $model->signup()) {
    //             Yii::$app->session->setFlash('success', 'Elektron pochta manzilingizga kirib akkauntingizni faollashtiring!');
    //         } else {
    //             Yii::$app->session->setFlash('error', 'Bu email ro\'yhatdan o\'tkazilgan!');
    //         }
    //     }

    //     return $this->render('signup', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $admission_year = AdmissionYear::findOne(['status' => 1]);
        $post = Yii::$app->request->post();
        $today = date("Y-m-d H:i:s");
        $start_date = date("Y-m-d H:i:s", strtotime($admission_year->start_from));
        $end_date = date("Y-m-d H:i:s", strtotime($admission_year->finishes_on));
        $model = new LoginForm();
        $s_model = new SignupForm();
        $r_model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $role = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
            if (Yii::$app->user->can('uploadInformation')) {
                if (($start_date < $today) && ($today < $end_date)) {

                    $asOldApplicant = Applicants::findOne(['user_id' => Yii::$app->user->id, 'academic_year_id' => $this->academic_year->id]);
                    if ($asOldApplicant) {
                        return $this->redirect(['applicant/default/index']);
                    } else {
                        $applicant = new Applicants();
                        $active_year = AdmissionYear::findOne(['status' => AdmissionYear::CURRENT_ACTIVE]);
                        $applicant->academic_year_id = $active_year->id;
                        $applicant->user_id = Yii::$app->user->id;
                        $applicant->generateCheckCode();
                        if ($applicant->save()) {
                            return $this->redirect(['applicant/default']);
                        } else {
                            print_r($applicant->getErrors());
                        }
                    }
                    return $this->redirect(['applicant/default/index']);
                } else {
                    Yii::$app->session->setFlash('error', 'Kechirasiz ushbu yil uchun magistraturaga hujjatlarni qabul qilish jarayoni tugagan!');
                    return $this->goHome();
                }
            } else {
                return $this->redirect(['admin/default/index']);
            }
        }

        if ($s_model->load(Yii::$app->request->post())) {
            if ($s_model->signup()) {
                Yii::$app->session->setFlash('success', 'Elektron pochta manzilingizga kirib akkauntingizni faollashtiring!');
            } else {
                Yii::$app->session->setFlash('error', 'Bu email ro\'yhatdan o\'tkazilgan!');
            }
        }

        if ($r_model->load(Yii::$app->request->post()) && $r_model->validate()) {
            if ($r_model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Emailingizga kirib keyingi bosqichlarni amalga oshiring!');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Kechirasiz bu email parolini tiklab berolmaymiz.');
            }
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
            's_model' => $s_model,
            'r_model' => $r_model,

        ]);
    }
    // public function actionRasm()
    // {
    //     $file=File::findOne(['user_id'=>86, 'scenario'=>'avatar']);
    //     Yii::$app->media->deleteFile($file);
    //     return $file->getPath();
    // }
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    /**
     * Development action.
     *
     * @return Response
     */
    public function actionDevelopment()
    {
        return $this->render('development');
    }


    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Emailingizga kirib keyingi bosqichlarni amalga oshiring!');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Kechirasiz bu email parolini tiklab berolmaymiz.');
            }
        }

        return $this->render('passwordResetRequestForm', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Parol muvaffaqiyatli o\'zgartirildi. Qaytadan kiring!');
            return $this->goHome();
        }

        return $this->render('resetPasswordForm', [
            'model' => $model
        ]);
    }
    public function actionAddme()
    {
        $model = User::find()->where(['username' => 'Admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'Admin';
            $user->middle_name = 'Adminovich';
            $user->last_name = 'Adminov';
            $user->email = 'admin@webper.uz';
            $user->status = 10;
            $user->setPassword('admin');
            $user->generateAuthKey();
            if ($user->save()) {
                $auth = Yii::$app->authManager;
                $adminRole = $auth->getRole('admin');
                $auth->assign($adminRole, $user->id);
                echo 'good';
            } else {
                return print_r($user->getErrors());
            }
        }
    }
}
