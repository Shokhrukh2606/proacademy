<?php

namespace app\controllers;

use Yii;
use app\models\Menu;
use app\models\Post;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class BranchesController extends Controller
{
    public $branchMenus;
    public $menu;

    public function init()
    {
        parent::init();
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => Yii::t('front', 'site'),
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:type',
            'content' => 'website',
        ]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:description',
            'content' => 'Et docere et discere servitute legis',
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => Url::to('/images/logo.png', true),
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image:type',
            'content' => 'image/png',
        ]);
        $this->branchMenus = ArrayHelper::index(Post::findBySql('SELECT id, title, category_id FROM posts')->where(['status' => 'active', 'group'=>'branches_category'])->all(), 'id');
        $this->menu = ArrayHelper::index(Menu::findBySql('SELECT id, title, category_id FROM menu')->where(['status' => 'active'])->all(), 'id');
    }
    public function actionIndex()
    {
        $branches = Post::find()->where(['group' => 'branches_category'])->all();
        $category=Menu::findOne(['id' => 66]);
        $categories = Menu::menuWithSameCategory(54);
        
        return $this->render('index', compact('branches', 'categories', 'category'));
    }
    public function actionView($alias)
    {
        $post=Post::findOne(['alias'=>$alias]);
        if($post){
            $categories = Menu::menuWithSameCategory(54);
            $category=Menu::findOne(['id' => $post->category_id]);
            return $this->render('/static/view', compact('categories','post'));
        }
    }

}
