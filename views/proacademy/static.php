<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\MenuCategoryWidget;

$this->title = MyHelpers::t($post->title);


?>
<!--==================================================================== 
            Start Banner Section
        =====================================================================-->
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
      
    </h2>
</div>
<section class="blog-single-page bg-light pb-50 pt-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="blog-details mb-50">
                    <!-- <div class="blog-details-content page">
                        <h2><?= $this->title ?></h2>
                    </div> -->
                </div>
                <?= MyHelpers::t($post->content) ?>

            </div>
            <div class="col-12 col-md-4">
                <div class="sidebar rmt-55">
                    <?= MenuCategoryWidget::widget(['cat_id' => $menuCat, 'activeId' => $menuActive, 'parentTitle' =>  MyHelpers::t(Yii::$app->controller->menu[$menuCat]['title'])]) ?>
                </div>
            </div>
        </div>
    </div>
</section>