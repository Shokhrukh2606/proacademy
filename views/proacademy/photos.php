<?php

use app\components\helpers\MyHelpers;
use himiklab\thumbnail\EasyThumbnailImage;
use ozerich\filestorage\models\File;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = MyHelpers::t(Yii::$app->controller->menu[70]['title']);

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<div class="video-tape gallery-page pb-45 pt-45">
    <div class="back-area"></div>
    <div class="container">
        <!-- <div class="section-title text-center">
            <h2><?= $this->title ?></h2>
        </div> -->
        <div class="row">
            <div class="col-12 text-center media-btns">
                <a href="<?= Url::to(['gallery/index', 'category' => 'photos']) ?>" class="navigator active"><?= Yii::t('front', 'photos') ?></a>
                <a href="<?= Url::to(['gallery/index', 'category' => 'videos']) ?>" class="navigator"><?= Yii::t('front', 'videos') ?></a>

            </div>
            <?php foreach ($images as $item) {
                $file = File::findOne($item['id']);
            ?>
                <div class="col-xl-4 col-md-6 col-12 mb-4">
                    <a class="big-video fancybox" data-fancybox href="<?= $file->getUrl() ?>">
                        <?php
                        echo EasyThumbnailImage::thumbnailImg(
                            $file->getAbsolutePath(),
                            595,
                            300,
                            EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                            ['alt' => $item['name'], 'class' => 'poster'],
                            100
                        );
                        ?>
                        <div class="video-title">
                            <img src="<?= Url::to('/') ?>images/icons/photo-mini.svg" alt="play" class="player">
                        </div>
                    </a>
                </div>
            <?php } ?>
            <div class="col-12">
                <?php
                echo LinkPager::widget([
                    'pagination' => $pagination,
                    'pageCssClass'=>'page-item',
                    'disabledPageCssClass'=>'page-item disabled',
                    'disabledListItemSubTagOptions'=>['tag'=>'a', 'class'=>'page-link', 'href'=>'#' ,'tabindex'=>'-1'],
                    'linkOptions'=>['class'=>'page-link']
                ]);
                ?>
            </div>
        </div>
    </div>
</div>