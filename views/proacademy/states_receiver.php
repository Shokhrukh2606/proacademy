<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\MenuCategoryWidget;
use app\models\Post;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = MyHelpers::t(Yii::$app->controller->menu[92]['title']);
$post = Post::findOne(['id' => 78]);
$this->registerJS("
$(\".cool-b4-form .form-control\").on(\"input\", function() {
    if ($(this).val()) {
      $(this).addClass(\"hasValue\");
    } else {
      $(this).removeClass(\"hasValue\");
    }
  });
  $('#exampleModalLong').modal({backdrop: 'static', keyboard: false, show: true});
  $('#statesreceivermodel-captcha-image').trigger('click');
");
$options_input = [
    'template' =>
    "   {input}
        <span class=\"input-highlight\"></span>\n
        {label}
    <div class=\"col-12\">{error}</div>"
];
?>
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title"><?= MyHelpers::t($post->title) ?></h5>
                <!-- </button> -->
            </div>
            <div class="modal-body">
                <?= MyHelpers::t($post->short_content) ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary ml-auto mr-auto" data-backdrop="static" data-keyboard="false" data-dismiss="modal"><?= Yii::t('front', 'accepted') ?></button>
            </div>
        </div>
    </div>
</div>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title" style="background: #f6f6f6;">
    <h2>
        <?= $this->title?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<div class="states-receiver-page pb-45 pt-45">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                <div class="cool-b4-form">
                    <div class="form-row">
                        <div class="col-md-12">
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                            <?= $form->field($model, 'last_name', $options_input)->textInput() ?>
                            <?= $form->field($model, 'first_name', $options_input)->textInput() ?>
                            <?= $form->field($model, 'middle_name', $options_input)->textInput() ?>
                            <?= $form->field($model, 'phone', $options_input)->textInput() ?>
                            <?= $form->field($model, 'email', $options_input)->textInput() ?>
                            <?= $form->field($model, 'position_place', $options_input)->textInput() ?>
                            <?= $form->field($model, 'priority', $options_input)->textInput() ?>
                            <?= $form->field($model, 'filer', ['labelOptions' => ['class' => 'custom-file-label ' . Yii::$app->language], 'template' =>
                            "   <div class=\"custom-file\">{input}
                                    {label}
                                    <div class=\"col-12\" style=\"position: relative; margin-top: 25px;\">{error}</div>
                                </div>
                                "])->fileInput(['class' => 'custom-file-input']) ?>
                            <?= $form->field($model, 'captcha')->widget(
                                Captcha::className(),
                                [
                                    'template' => '<div class="captcha_img">{image}</div>'
                                        . '<a class="refreshcaptcha" href="#">'
                                        . Html::img('/images/imageName.png', []) . '</a>'
                                        . Yii::t('front', 'captcha').'{input}',
                                ]
                            )->label(FALSE); ?>
                            <div class="form-group text-center">
                                <?= Html::submitButton(Yii::t('front', 'send'), ['class' => 'btn btn-lg theme-btn mt-3 mb-3 ml-auto mr-auto']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <textarea name="message" id="message" class="form-control"></textarea>
                                <label for="message">Message</label>
                                <span class="input-highlight"></span>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="sidebar mt-30 rmt-55">
                    <?= MenuCategoryWidget::widget(['cat_id' => 55, 'activeId' => 92, 'parentTitle' => MyHelpers::t(Yii::$app->controller->menu[55]['title'])]) ?>
                </div>
            </div>
        </div>
    </div>
</div>