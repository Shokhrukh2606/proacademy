<?php

use app\components\helpers\MyHelpers as My;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\Html;

$this->title = 'Qidiruv natijalari';

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">

    </h2>
</div>
<section class="blog-single-page bg-light pb-50 pt-70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="list-group">
                    <?php
                    if (isset($news)) {
                        foreach ($news as $item) { ?>
                            <li class="list-group-item">

                                <a href="<?= Url::to(['news/view', 'alias' => $item['id']]) ?>"> <?= My::t($item['title']) ?></a>
                            </li>
                    <?php  }
                    } else {
                        echo Html::tag('h3','Hech narsa topilmadi!', ['class'=>'text-center']);
                    } ?>
                </ul>
                <?=isset($pagination)?LinkPager::widget([
                    'pagination' => $pagination,
                    'pageCssClass' => 'page-item',
                    'disabledPageCssClass' => 'page-item disabled',
                    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link', 'href' => '#', 'tabindex' => '-1'],
                    'linkOptions' => ['class' => 'page-link']
                ]):'';
                ?>
            </div>
        </div>
    </div>
</section>