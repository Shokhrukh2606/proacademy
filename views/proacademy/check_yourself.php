<?php

use app\components\helpers\MyHelpers;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\View;
use app\assets\PluginsAsset;

PluginsAsset::register($this);

$cats = ArrayHelper::index($categories, 'id');
$cat_id = Yii::$app->getRequest()->getQueryParam('category');
$this->title = $cat_id ? MyHelpers::t($cats[$cat_id]->title) : MyHelpers::t($categories[0]->title);

$changes_questions = ArrayHelper::getColumn($questions, function ($element) {
    if ($element->id % 2 == 0) {
        return [
            'id' => $element->id, 'q' => MyHelpers::t($element->title),
            'options' => [
                MyHelpers::t($element->meta_title),
                MyHelpers::t($element->short_content),
                MyHelpers::t($element->meta_description),
                MyHelpers::t($element->meta_keywords)
            ],
            'correctIndex' => 1,
            'correctResponse' => '<i class="fa fa-check" />',
            'incorrectResponse' => '<i class="fa fa-close" />'
        ];
    } else if ($element->id % 3 == 0) {
        return [
            'id' => $element->id, 'q' => MyHelpers::t($element->title),
            'options' => [
                MyHelpers::t($element->meta_title),
                MyHelpers::t($element->meta_description),
                MyHelpers::t($element->meta_keywords),
                MyHelpers::t($element->short_content)
            ],
            'correctIndex' => 3,
            'correctResponse' => '<i class="fa fa-check" />',
            'incorrectResponse' => '<i class="fa fa-close" />'
        ];
    } else {
        return [
            'id' => $element->id, 'q' => MyHelpers::t($element->title),
            'options' => [
                MyHelpers::t($element->short_content),
                MyHelpers::t($element->meta_title),
                MyHelpers::t($element->meta_description),
                MyHelpers::t($element->meta_keywords)
            ],
            'correctIndex' => 0,
            'correctResponse' => '<i class="fa fa-check" />',
            'incorrectResponse' => '<i class="fa fa-close" />'
        ];
    }
});
$json_questions = JSON::encode($changes_questions);
$time = $cat_id ? $cats[$cat_id]->options : $categories[0]->options;
$this->registerJs("
var answerCallback = function(currentQuestion, selected, question) {

    // Determine if the question was correct.
    if (selected === question.correctIndex) {
      console.log('Correct');
    } else {
      console.log('Incorrect')

      // Highlight correct answer.
      $('.active-question ul.answers a[data-index=' + question.correctIndex + ']').addClass('hint');
    }
};
var timer=${time}
    var url = window.location.pathname
    var curLang = url.substring(1, 7)
    var n;
    switch (curLang) {
        case \"uz-lat\":
            n='oz'
            break;
        case \"uz-cyr\":
            n='uz'
            break;
        default:
            n='ru'
    }
    var words={
        ru: {
            counterFormat: '%current-вопрос из %total',
            nextButtonText: 'Следующий',
            finishButtonText: 'Завершить тест',
            restartButtonText: 'Реши тест снова',
            resultsFormat: 'Вы набрали %score балла из %total!'        
        },
        uz: {
            counterFormat: '%total саволдан %current-савол',
            nextButtonText: 'Кейингиси',
            finishButtonText: 'Тугатиш',
            restartButtonText: 'Қайта ўтиш',
            resultsFormat: 'Сиз %total баллдан %score балл тўпладингиз!'        

        },
        oz: {
            counterFormat: '%total savoldan %current-savol',
            nextButtonText: 'Keyingisi',
            finishButtonText: 'Tugatish',
            restartButtonText: 'Qayta o\'tish',
            resultsFormat: 'Siz %total balldan %score ball topladingiz!'        
        }
    }
    $('#quiz').quiz({
    //resultsScreen: '#results-screen',
    //counter: false,
    //homeButton: '#custom-home',
    counterFormat: words[n].counterFormat,
    answerCallback: answerCallback,
    nextButtonText: words[n].nextButtonText,
    finishButtonText: words[n].finishButtonText,
    restartButtonText: words[n].restartButtonText,
    resultsFormat: words[n].resultsFormat,
    questions: ${json_questions}
  });
", View::POS_READY);
?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<div class="check-yourself-page pb-70 pt-90 bg-light shape-bg-after law-acts">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8">
                <div id="quiz">
                    <!-- <div id="quiz-header">
                        <h3>Basic Quiz Demo</h3>
                        <p><a id="quiz-home-btn">Home</a></p>
                    </div> -->
                    <div id="quiz-start-screen">
                        <p><a href="#" id="quiz-start-btn" class="quiz-button"><?= Yii::t('front', 'start') ?></a></p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="sidebar rmt-55">
                    <div class="widget">
                        <h2><?= MyHelpers::t(Yii::$app->controller->interactives[10]['title']) ?></h2>
                        <div class="category-widget">
                            <ul>
                                <?php foreach ($categories as $item) { ?>
                                    <li class="<?= ($cat_id == $item->id) ? 'active' : null ?>"><a href="<?= Url::to(['proacademy/check-yourself', 'category' => $item->id]) ?>"><?= MyHelpers::t($item->title) ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>