<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\BreadCrumbs;
use app\components\widgets\MenuCategoryWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = MyHelpers::t(Yii::$app->controller->interactives[6]['title']);


?>

<style>
.service-icon span.glyph-icon::before {
    font-size: 35px!important;
    margin-left: 0px!important;
    position: absolute!important;
    transform: translateX(-50%)!important;
    left: 50%!important;
}
</style>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">

    </h2>
</div>
<div class="services-section pb-70 pt-90 bg-light shape-bg-after branches">
    <div class="container">
        <div class="row">
            <?php
            foreach ($polls as $item) {
            ?>
                <div class="col-sm-12 col-md-4  mb-3">
                    <div class="service-box">
                        <div class="service-icon">
                            <span class="glyph-icon flaticon-sports-and-competition"></span>
                        </div>
                        <h3><?= Html::a($item->question, ['polls-view', 'id' => $item->id]) ?></h3>
                    </div>
                </div>
            <?php } ?>
            <div class="col-12">
                <?php
                echo LinkPager::widget([
                    'pagination' => $pagination,
                    'pageCssClass' => 'page-item',
                    'disabledPageCssClass' => 'page-item disabled',
                    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link', 'href' => '#', 'tabindex' => '-1'],
                    'linkOptions' => ['class' => 'page-link']
                ]);
                ?>

            </div>
        </div>
    </div>
</div>