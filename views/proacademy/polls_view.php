<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('API_KEY', "1173658716:AAE-pvrtluS-r4cYM1aj3DMkpTis--4bdsA");

use app\components\helpers\MyHelpers;
use app\models\Poll;
$this->title = MyHelpers::t(Yii::$app->controller->interactives[6]['title']);
?>

<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<style>
  .chart {
    width: 100%;
    height: 500px;
    padding: 40px;
  }
</style>


<section class="banner-section">
  <div class="container">
    <div class="banner-inner pt-55 text-center">
      <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
      <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
    </div>
  </div>
</section>
<div class="text-center pt-55 hyper-page-title">
  <h2>
    <?= $this->title ?>
    <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">

  </h2>
</div>
<section class="blog-single-page bg-light pb-50 pt-70">
  <div class="container">
      <div class="row">
        <div class="col-12 mr-auto ml-auto text-center">
          <h5><?= $lastPoll->question ?></h5>
          <!-- <h6>Available Options</h6> -->
          <ul>
            <?php foreach (json_decode($lastPoll->options)  as $option) : ?>
              <li><?= $option->text ?></li>
            <?php endforeach; ?>
          </ul>

        </div>
        <div class="col-12 col-md-8 ml-auto mr-auto">
          <div id='q1' class="chart"></div>
        </div>
      </div>
  </div>
</section>


  <script>
    am4core.ready(function() {

      // Themes begin
      am4core.useTheme(am4themes_animated);
      // Themes end

      var chart = am4core.create('q1', am4charts.PieChart3D);
      chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

      chart.data = [
        <?php foreach (json_decode($lastPoll->options) as $option) : ?> {
            country: "<?= $option->text ?>",
            litres: <?= $option->voter_count ?>
          },
        <?php endforeach; ?>
      ];

      chart.innerRadius = am4core.percent(40);
      chart.depth = 50;

      chart.legend = new am4charts.Legend();

      var series = chart.series.push(new am4charts.PieSeries3D());
      series.dataFields.value = "litres";
      series.dataFields.depthValue = "litres";
      series.dataFields.category = "country";
      series.slices.template.cornerRadius = 5;
      series.colors.step = 3;

    }); // end am4core.ready()
  </script>
