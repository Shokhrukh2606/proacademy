<?php

use app\components\helpers\MyHelpers;
use himiklab\thumbnail\EasyThumbnailImage;
use app\components\widgets\MenuCategoryWidget;

$cat_title = MyHelpers::t(Yii::$app->controller->interactives[11]['title']);
$this->title = MyHelpers::t($book->title);
?>
<section class="banner-section">
  <div class="container">
    <div class="banner-inner pt-55 text-center">
      <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
      <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
    </div>
  </div>
</section>

<section class="library-page inner-library hyper-page-title">
  <div class="container">
    <h2 class="text-center pt-55">
      <?= MyHelpers::t($book->title) ?>
      <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
    <div class="row">
      <div class="col-lg-9">
        <div class="book-info">
          <div class="book-img">
            <?= EasyThumbnailImage::thumbnailImg(
              Yii::$app->basePath . '/web/' . MyHelpers::getPoster($book['id'], 'books'),
              400,
              500,
              EasyThumbnailImage::THUMBNAIL_OUTBOUND,
              ['alt' => 'book'],
              100
            ) ?>
          </div>
          <div class="infos">
            <div>
              <span><?= Yii::t('front', 'num_pages') ?>:</span>
              <?= $book['f_phone'] ?>
            </div>
            <div>
              <span><?= Yii::t('front', 'published_year') ?>:</span>
              <?= $book['f_position'] ?>

            </div>
            <div>
              <span><?= Yii::t('front', 'author') ?>:</span>
              <?= $book['f_name'] ?>

            </div>
            <div>
              <span><?= Yii::t('front', 'short_info') ?>:</span>
              <?= MyHelpers::t($book['content']) ?>
            </div>
            <a target="_blank" href="<?= MyHelpers::getFirstMediaFileFull($book->id, 'books', 'zip')['parentDir'] . '/index.html' ?>"><i class="fas fa-book"></i>&nbsp; <?= Yii::t('front', 'online_read') ?></a>
          </div>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="sidebar rmt-55">
          <?= MenuCategoryWidget::widget(['cat_id' => 61, 'activeId' => 87, 'parentTitle' => $cat_title]) ?>
        </div>
      </div>
    </div>
  </div>
</section>