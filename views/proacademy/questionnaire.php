<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\BreadCrumbs;
use app\components\widgets\MenuCategoryWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = MyHelpers::t(Yii::$app->controller->menu[88]['title']);

if (isset($question)) { ?>
     <section class="banner-section">
        <div class="container">
            <div class="banner-inner pt-55 text-center">
                <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
                <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
            </div>
        </div>
    </section>
    <div class="text-center pt-55 hyper-page-title">
        <h2>
            <?= $this->title ?>
            <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">

        </h2>
    </div>
    <div class="services-section pb-70 pt-90 bg-light shape-bg-after branches">
        <div class="container">
            <div class="row">
                <div class="col-12 branches-list">
                    <img src="<?=Url::to(MyHelpers::getFirstMedia($question->id))?>" alt="" class="mr-auto ml-auto d-block" >
                    <p><?=MyHelpers::t($question->short_content)?></p>
                </div>
            </div>
        </div>
    </div>
<?php } else {
?>


    <section class="banner-section">
        <div class="container">
            <div class="banner-inner pt-55 text-center">
                <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
                <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
            </div>
        </div>
    </section>
    <div class="text-center pt-55 hyper-page-title">
        <h2>
            <?= $this->title ?>
            <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book"  alt="">
        </h2>
    </div>
    <div class="services-section pb-70 pt-90 bg-light shape-bg-after branches">
        <div class="container">
            <div class="row">
                <div class="col-12 branches-list">
                    <?php
                    echo Html::ul($questionnaire, ['item' => function ($child) {
                        return Html::tag('li', Html::a(MyHelpers::t($child->title),  Url::to(['proacademy/questionnaire-view', 'alias' => $child->alias])));
                    }]);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>