<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\MenuCategoryWidget;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Url;

$this->title = MyHelpers::t(Yii::$app->controller->menu[88]['title']);

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<div class="masters-page pb-45 pt-45 bg-light shape-bg-after law-acts">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                <div class="row">
                    <?php
                    foreach ($literacy as $item) {
                    ?>
                        <div class="col-xl-4 col-sm-6 col-12 mb-30">
                            <div class="service-box master-item">
                                <h3><a href="<?=$item->link?Url::to([$item->link]):Url::to(['literacy/index', 'alias'=>$item->alias]) ?>"><?= MyHelpers::t($item->title) ?></a></h3>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="sidebar rmt-55">
                    <?= MenuCategoryWidget::widget(['cat_id' => 61, 'activeId' => 88, 'parentTitle' => MyHelpers::t(Yii::$app->controller->menu[61]['title'])]) ?>
                </div>
            </div>
        </div>
    </div>
</div>  