<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;
use yii\helpers\Url;
use app\assets\PluginsAsset;

$this->title = MyHelpers::t(Yii::$app->controller->interactives[4]['title']);
PluginsAsset::register($this);

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<div class="states-receiver-page pb-50 pt-70">
    <div class="container">
        <div class="row">
            <div class="col-8 ml-auto mr-auto">
                <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/cert.svg" alt="attestat" class="ml-auto mr-auto d-block mb-3">
                <?php if (isset($model)) {
                    $form = ActiveForm::begin([
                        'fieldConfig' => [
                            'labelOptions' => ['class' => 'col-lg-12 control-label'],
                        ],
                    ]); ?>
                    <?= $form->field($model, 'certificate_number', ['labelOptions'=>['label'=>Yii::t('front', 'cer_number')]])->textInput([
                        'autofocus' => true, 'class' => 'form-control',
                    ]) ?>
                    <div class="form-group">
                        <div class="col-12 text-center">
                            <?= Html::submitButton(Yii::t('front', 'check_but').'&nbsp;<i class="flaticon-loupe"></i>', ['class' => 'btn btn-primary btn-lg mr-auto ml-auto d-flex justify-content-center align-items-center']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end() ?>
                <?php                } else if(isset($found)) {
                    $q=$found->certificate_number;
                    $this->registerJS("
                        let _url = window.location.protocol+'\//'+window.location.hostname+'/check-attestation?certificate='+`${q}`
                        console.log(_url)
                         $('#qr-container').kjua({text: _url});
                    ");
                    echo  DetailView::widget([
                        'model' => $found,
                        'attributes' => [
                            'full_name',
                            'course_name',
                            'begin_date',
                            'end_date',
                            'score',
                            'certificate_number',
                        ],
                    ]);
                }else{
                    echo Html::tag('h2','Bu sertifikat raqami topilmadi!', ['class'=>'text-center']);
                } ?>
            </div>

            <div class="col-12 col-md-8  mr-auto ml-auto text-center">
                <div id="qr-container">
                    
                </div>
            </div>
        </div>
    </div>
</div>