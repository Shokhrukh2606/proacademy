<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\BreadCrumbs;
use app\components\widgets\MenuCategoryWidget;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;


$this->title = isset($cat)?MyHelpers::t($cat->title):MyHelpers::t(Yii::$app->controller->interactives[12]['title']);
$this->registerJs(
    "$('#accordion').on('hide.bs.collapse', function(e) {
        const self=$(e.target).attr(\"aria-labelledby\")
        $('#'+self).find(\"button i\").removeClass('fa-chevron-down').addClass(\"fa-chevron-left\")
        console.log(self)
        // .addClass('fa-chevron-down')
      }).on('show.bs.collapse', function(e) {
        const self=$(e.target).attr(\"aria-labelledby\")
        $('#'+self).find(\"button i\").removeClass('fa-chevron-left').addClass(\"fa-chevron-down\")
        console.log(self)

        // .addClass('fa-chevron-left')
      });",
    View::POS_READY
);
?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<div class="faqs-page pb-70 pt-90 bg-light shape-bg-after law-acts">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-8">
                <div id="accordion">
                    <?php foreach ($faqs as $key => $item) { ?>
                        <div class="card">
                            <div class="card-header" id="heading<?= $item->id ?>">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?= $item->id ?>" aria-expanded="true" aria-controls="collapse<?= $item->id ?>">
                                        <?= MyHelpers::t($item->title) ?>
                                        <i class="fa <?= $key == 0 ? 'fa-chevron-down' : 'fa-chevron-left' ?> float-right"></i>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse<?= $item->id ?>" class="collapse" aria-labelledby="heading<?= $item->id ?>" data-parent="#accordion">
                                <div class="card-body">
                                    <?= MyHelpers::t($item->content) ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="sidebar rmt-55">
                    <div class="widget">
                        <h2><?=$this->title?></h2>
                        <div class="category-widget">
                            <ul>
                                <?php foreach ($categories as $item) { ?>
                                    <li><a href="<?= Url::to(['proacademy/faq', 'category' => $item->id]) ?>"><?= MyHelpers::t($item->title) ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <?php
                echo LinkPager::widget([
                    'pagination' => $pagination,
                    'pageCssClass'=>'page-item',
                    'disabledPageCssClass'=>'page-item disabled',
                    'disabledListItemSubTagOptions'=>['tag'=>'a', 'class'=>'page-link', 'href'=>'#' ,'tabindex'=>'-1'],
                    'linkOptions'=>['class'=>'page-link']
                ]);
                ?>
            </div>
        </div>
    </div>
</div>