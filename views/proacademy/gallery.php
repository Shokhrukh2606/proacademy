<?php

use app\components\helpers\MyHelpers;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = MyHelpers::t(Yii::$app->controller->menu[70]['title']);

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>

        <?= $this->title?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">

    </h2>
</div>
<div class="video-tape gallery-page pb-45 pt-45">
    <div class="back-area"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center media-btns">
                <a href="<?= Url::to(['gallery/index', 'category' => 'photos']) ?>" class="navigator"><?= Yii::t('front', 'photos') ?></a>
                <a href="<?= Url::to(['gallery/index', 'category' => 'videos']) ?>" class="navigator active"><?= Yii::t('front', 'videos') ?></a>

            </div>
            <?php foreach ($images as $item) { ?>
                <div class="col-xl-4 col-md-6 col-12 mb-4">
                    <a class="big-video fancybox" data-fancybox href="<?= $item->link ? 'http://www.youtube.com/watch?v=' . $item->link : MyHelpers::getFirstMedia($item->id) ?>">
                        <?= MyHelpers::hasMedia($item->id) ? EasyThumbnailImage::thumbnailImg(
                            MyHelpers::getFirstMediaFile($item->id)->getAbsolutePath(),
                            595,
                            300,
                            EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                            ['alt' => $item->alias, 'class' => 'poster'],
                            100
                        ) : ''
                        ?>
                        <div class="video-title">
                            <p><?= MyHelpers::limiter(MyHelpers::t($item->title), 45) ?></p>
                            <img src="<?= Url::to('/') ?>images/icons/play.svg" alt="play" class="player">
                        </div>
                    </a>
                </div>
            <?php } ?>
            <div class="col-12">
                <?php
                echo LinkPager::widget([
                    'pagination' => $pagination,
                    'pageCssClass' => 'page-item',
                    'disabledPageCssClass' => 'page-item disabled',
                    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link', 'href' => '#', 'tabindex' => '-1'],
                    'linkOptions' => ['class' => 'page-link']
                ]);
                ?>
            </div>
        </div>
    </div>
</div>