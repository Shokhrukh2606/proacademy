<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\BreadCrumbs;
use app\components\widgets\MenuCategoryWidget;
use yii\helpers\Url;
use yii\widgets\LinkPager;


$this->title = MyHelpers::t(Yii::$app->controller->menu[65]['title']);
$this->params['breadcrumbs'][] = ['label' => MyHelpers::t(Yii::$app->controller->menu[54]['title']), 'url' => '#'];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?=Yii::t('front', 'site')?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
       
    </h2>
</div>
<div class="heads-page pb-70 pt-90 bg-light shape-bg-after law-acts">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12 ">
                <?php 
                foreach ($heads as $item) { ?>
                    <div class="head-item">
                        <div class="col-sm-4 col-12 text-center">
                            <img src="<?=MyHelpers::hasMedia($item->id)?MyHelpers::getFirstMedia($item->id):Yii::$app->request->baseUrl.'/images/heads/avatar.svg' ?>" alt="">
                        </div>
                        <div class="col-sm-8 col-12">
                            <div class="head-info">
                                <h3><?=MyHelpers::t($item->title)?></h3>
                                <h6><?=MyHelpers::t($item->content)?></h6>
                                <a>
                                    <span class="fa fa-phone"></span>
                                    <?=$item->icon?>
                                </a>
                                <a href="mailto: <?=$item->link?>">
                                    <span class="fa fa-at"></span>
                                    <?=$item->link?>
                                </a>
                                <p>
                                    <span class="fa fa-clock"></span>
                                    <?=MyHelpers::t($item->short_content)?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-4 col-12">
                <div class="sidebar rmt-55">
                    <?= MenuCategoryWidget::widget(['cat_id' => 54, 'activeId' => 65, 'parentTitle' => $this->params['breadcrumbs'][0]['label']]) ?>
                </div>
            </div>
        </div>
    </div>
</div>