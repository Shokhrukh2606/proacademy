<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\MenuCategoryWidget;
use yii\helpers\Url;
$this->title = MyHelpers::t(Yii::$app->controller->menu[72]['title']);

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55  hyper-page-title">
    <h2>
        <?= $this->title?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<div class="archive-tests-page pb-50 pt-70 bg-light shape-bg-after law-acts">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                <div class="row">
                    <!-- <div class="col-12">
                        <h2 class="mb-5 text-center"><?=$this->title?></h2>
                    </div> -->
                    <?php foreach ($sets as $item) { ?>
                        <div class="col-xl-4 col-sm-6 col-12 mb-4">
                            <a class="test-item" href="<?= Url::to(['/tests-archive/index', 'alias' => $item->alias]) ?>">
                                <img class="logo" src="<?= Yii::getAlias('@web') . '/images/logo.png' ?>" alt="">
                                <span><?= MyHelpers::t($item->title, 'ru') ?></span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="sidebar rmt-55">
                    <?= MenuCategoryWidget::widget(['cat_id' => 55, 'activeId' => 72, 'parentTitle' => MyHelpers::t(Yii::$app->controller->menu[55]['title'])]) ?>
                </div>
            </div>
        </div>
    </div>
</div>