<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\CategoryWidget;
use app\components\widgets\NewsCategoryWidget;
use app\components\widgets\NewsWidget;
use app\models\Category;
use app\models\NewsCategory;
use yii\helpers\Url as U;
use yii\widgets\LinkPager;


$this->title = Yii::t('front', 'news');

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= Yii::t('front', 'news') ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<section class="news-page pb-50 pt-30 bg-light shape-bg-after">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <?php
                    foreach ($masters as $item) {

                        echo NewsWidget::widget(['id' => $item['id'], 'title' => $item['title'], 'short_content' => $item['short_content'], 'link' => $item['id'], 'comment_count' => $item['views'], 'category' => '', 'date' => $item['published_date'], 'imglink' => MyHelpers::getFirstNewsMedia($item['id'])]);
                    }
                    ?>
                </div>
            </div>
            <div class="col-12">
                <?php
                echo LinkPager::widget([
                    'pagination' => $pagination,
                    'pageCssClass' => 'page-item',
                    'disabledPageCssClass' => 'page-item disabled',
                    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link', 'href' => '#', 'tabindex' => '-1'],
                    'linkOptions' => ['class' => 'page-link']
                ]);
                ?>
            </div>
        </div>
    </div>
</section>