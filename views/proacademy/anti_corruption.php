<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\MenuCategoryWidget;
use yii\web\View;
use app\assets\PluginsAsset;

PluginsAsset::register($this);

$this->title = MyHelpers::t(Yii::$app->controller->menu[110]['title']);
$this->registerJs("
    const current_origin=window.location.origin
    const params=new URL(window.location.href).pathname
    let arrayed_params=params.split('/')
    if(arrayed_params[1]=='uz-cyr'){
        arrayed_params[1]='uz-Cyr'
    }else if(arrayed_params[1]=='uz-lat'){
        arrayed_params[1]='uz-Lat'
    }
    unflatten = function( array, parent, tree ){
        tree = typeof tree !== 'undefined' ? tree : [];
        parent = typeof parent !== 'undefined' ? parent : { id: 0 };
            
        var children = _.filter( array, function(child){ return child.category_id == parent.id; });
        
        if( !_.isEmpty( children )  ){
            if( parent.id == 0 ){
               tree = children;   
            }else{
               parent['children'] = children
            }
            _.each( children, function( child ){ unflatten( array, child ) } );                    
        }
        
        return tree;
    }
    console.log(${dataProvider})
    const  menus=${dataProvider}.map(item=>({
        \"id\": item.id,
        \"category_id\": (item.id!=110?item.category_id:0),
        \"children\": null,
        \"name\": JSON.parse(item.title)[arrayed_params[1]],
        \"alias\": item.alias,
    }));
    const structured=unflatten(menus)
    console.log(structured)
    $('#listWithHandle').tree({
        data: structured,
        autoOpen: true,
        onCreateLi: function(node, lit) {
            // Append a link to the jqtree-element div.
            // The link has an url '#node-[id]' and a data property 'node-id'.
            lit.find('.jqtree-element').append(
                '<div class=\"action-navs\">'+
                (node.children.length==0?'<a href=\"'+current_origin+'/'+arrayed_params[1]+'/menu/'+ node.alias+ '\" class=\"edit circle\" data-node-id=\"'+ node.id +'\">".Yii::t('front', 'read_more')."<span class=\"fas fa-arrow-right ml-3\"></span></a>':'')+
                '</div>'
            );
        }
    });
    $('input[type=\"checkbox\"]').on('change',function(e){
        var self=$(this)
        var stat=self.is(':checked')
        var m_id=self.data('id')
        $.ajax({
            url: current_origin+'/'+arrayed_params[1]+'/admin/menu/change-status?m_id='+m_id+'&stat='+(stat?'active':'inactive'),
            success: function(res){
                if(!res.success){
                    alert('Произошла ошибка! Проверь свой интернет!')
                    return false
                }else{
                    return false
                }
            }
        })
        return false
    })
", View::POS_END);

?>
<!--==================================================================== 
            Start Banner Section
        =====================================================================-->
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">

    </h2>
</div>
<section class="blog-single-page bg-light pb-50 pt-70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog-details mb-50">
                    <div class="blog-details-content page">
                        <div id="listWithHandle">

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>