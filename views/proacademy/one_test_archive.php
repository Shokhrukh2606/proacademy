<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\MenuCategoryWidget;
use yii\helpers\Url;

$this->title = MyHelpers::t(Yii::$app->controller->menu[72]['title']);
?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
      
    </h2>
</div>
<div class="archive-tests-page pb-45 pt-45  bg-light shape-bg-after law-acts">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                <div class="row">
                    <!-- <div class="col-12">
                        <h2 class="mb-5 text-center"><?=$this->title?> <?= MyHelpers::t($set->title, 'ru') ?></h2>
                    </div> -->
                    <?php foreach ($files as $item) { ?>
                        <div class="col-xl-4 col-sm-6 col-12 mb-4">
                            <div class="test-item">
                                <a href="<?= MyHelpers::getMediaInnerDirctory($item['hash'], 'sets') . '/index.html' ?>" target="_blank">
                                    <img class="logo" src="<?= Yii::getAlias('@web') . '/images/logo.png' ?>" alt="">
                                </a>
                                <span><?= strstr($item['name'], '.zip', true)  ?></span>
                                <a class="read" href="<?= MyHelpers::getMediaInnerDirctory($item['hash'], 'sets') . '/index.html' ?>" target="_blank">
                                    <?=Yii::t('front', 'online_read')?><span class="flaticon-next-1"></span>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="sidebar rmt-55">
                    <?= MenuCategoryWidget::widget(['cat_id' => 55, 'activeId' => 72, 'parentTitle' => MyHelpers::t(Yii::$app->controller->menu[55]['title'])]) ?>
                </div>
            </div>
        </div>
    </div>
</div>