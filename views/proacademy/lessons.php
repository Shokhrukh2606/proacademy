<?php

use app\components\helpers\MyHelpers;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\models\VideoLessons;
$this->title = Yii::t('front', 'video_lessons');

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>

        <?= $this->title ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">

    </h2>
</div>
<div class="video-tape online-study gallery-page pb-45 pt-45">
    <div class="back-area"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 mb-5">
                <ul class="nav nav-pills">
                    <?php  foreach($categories as $item): ?>
                    <?php if($item->category==0){
                            $subs= VideoLessons::find()->where(['lesson_group' => 'cats', 'category'=>$item->id])->all();
                        
                        ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"><?=MyHelpers::t($item->title)?></a>
                        <div class="dropdown-menu">
                            <?php foreach($subs as $sub):?>
                                <a class="dropdown-item" href="<?=Url::to(['proacademy/lessons', 'category'=>$sub->id])?>"><?=MyHelpers::t($sub->title)?></a>        
                            <?php endforeach;?>
                        </div>
                    </li>

                    <?php }
                endforeach;?>
                </ul>

            </div>
            <?php foreach ($videos as $item) { ?>
                <div class="col-xl-4 col-md-6 col-12 mb-4">
                    <a class="big-video fancybox" data-fancybox href="<?= $item->youtube_link ? 'http://www.youtube.com/watch?v=' . $item->youtube_link : 'https://mover.uz/watch/' . $item->mover_link ?>">
                        <?= EasyThumbnailImage::thumbnailImg(
                            Yii::$app->basePath . '/web/uploads/lessons/' . $item->cover,
                            595,
                            300,
                            EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                            ['alt' => 'lesson', 'class' => 'poster'],
                            100
                        )
                        ?>
                        <div class="video-title">
                            <p><?= MyHelpers::limiter(MyHelpers::t($item->title), 45) ?></p>
                            <img src="<?= Url::to('/') ?>images/icons/play.svg" alt="play" class="player">
                        </div>
                    </a>
                </div>
            <?php } ?>
            <div class="col-12">
                <?php
                echo LinkPager::widget([
                    'pagination' => $pagination,
                    'pageCssClass' => 'page-item',
                    'disabledPageCssClass' => 'page-item disabled',
                    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link', 'href' => '#', 'tabindex' => '-1'],
                    'linkOptions' => ['class' => 'page-link']
                ]);
                ?>
            </div>
        </div>
    </div>
</div>