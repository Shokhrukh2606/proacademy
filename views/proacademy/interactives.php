<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\MenuCategoryWidget;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Url;

$this->title = MyHelpers::t(Yii::$app->controller->menu[87]['title']);

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<div class="interactives page pb-45 pt-45 section" id="interactives">
    <div class="back-area"></div>
    <div class="container">
        <!-- <div class="section-title">
            <h2><?= Yii::t('front', 'interactives') ?></h2>
        </div> -->
        <div class="row">
            <div class="col-lg-8 col-12">
                <div class="row">
                    <?php foreach ($interactives as $item) { ?>
                        <div class="col-xl-4 col-lg-6 col-12 col-sm-6 mb-30">
                            <a class="service-box interactives-item" href="<?= ($item->link) ? Url::to([$item->link]) : Url::to(['menu/' . $item->alias]) ?>">
                                <div class="service-icon"><i class="<?= $item->icon ?>"></i></div>
                                <h3>
                                    <?= MyHelpers::t($item->title) ?>
                                </h3>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="sidebar rmt-55">
                    <?= MenuCategoryWidget::widget(['cat_id' => 61, 'activeId' => 87, 'parentTitle' => MyHelpers::t(Yii::$app->controller->menu[61]['title'])]) ?>
                </div>
            </div>
        </div>
    </div>
</div>