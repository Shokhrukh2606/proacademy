<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\BreadCrumbs;
use app\components\widgets\MenuCategoryWidget;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;
use himiklab\thumbnail\EasyThumbnailImage;
use app\models\OrderMedia;

$this->title = MyHelpers::t(Yii::$app->controller->interactives[11]['title']);
?>
<section class="banner-section">
  <div class="container">
    <div class="banner-inner pt-55 text-center">
      <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
      <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
    </div>
  </div>
</section>
<div class="text-center pt-55 hyper-page-title">
  <h2>
    <?= $this->title ?>
    <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
  </h2>
</div>
<div class="library-page pb-30 pt-50 bg-light shape-bg-after">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-9 library-main">
        <div class="row experts-load">
          <?php foreach ($books as $item) { ?>
            <div class="col-sm-12 col-lg-4 col-lg-3">
              <div class="every-expert">
                <div class="expert-img">
                  <?= EasyThumbnailImage::thumbnailImg(
                    Yii::$app->basePath . '/web/' . MyHelpers::getPoster($item->id, 'books'),
                    250,
                    300,
                    EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                    ['alt' => 'book', 'style'=>'height: unset;'],
                    100
                  ) ?>
                  <div class="expert-overlay">
                    <?php 
                      $book=MyHelpers::getFirstMediaFileFull($item->id, 'books', 'zip')['parentDir'];
                    ?>
                    <a target="_blank" href="<?= $book? $book. '/index.html':($pdf=OrderMedia::find()->where(['model_id' => $item->id])->andWhere(['like', 'url', "%pdf", false])->one()?Url::to('@web/postfiles/books').$pdf->url:'')  ?>"> <?= Yii::t('front', 'online_read') ?></a>
                  </div>
                </div>
                <div class="expert-description text-left">
                  <a href="<?=Url::to(['e-library', 'alias'=>$item->id])?>"><?=MyHelpers::limiter(MyHelpers::t($item->title),50) ?>&nbsp; <i class="fa fa-arrow-right"></i></a>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
      <div class="col-12 col-md-3">
        <div class="sidebar rmt-55">
          <div class="widget">
            <h4><?= $this->title ?></h4>
            <div class="category-widget">
              <ul>
                <?php foreach ($categories as $item) { ?>
                  <li><a href="<?= Url::to(['e-library', 'category' => $item->id]) ?>"><?= MyHelpers::t($item->title) ?></a></li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12">
        <?php
        echo LinkPager::widget([
          'pagination' => $pagination,
          'pageCssClass' => 'page-item',
          'disabledPageCssClass' => 'page-item disabled',
          'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link', 'href' => '#', 'tabindex' => '-1'],
          'linkOptions' => ['class' => 'page-link']
        ]);
        ?>
      </div>
    </div>
  </div>
</div>

