<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\BreadCrumbs;
use app\components\widgets\CategoryWidget;
use app\components\widgets\MenuCategoryWidget;
use yii\helpers\Url;
use yii\widgets\LinkPager;



$this->title = MyHelpers::t($category->title);
$this->params['breadcrumbs'][] = ['label' => MyHelpers::t(Yii::$app->controller->menu[54]['title']), 'url' => '#'];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= $this->title ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<div class="services-section pb-70 pt-90 bg-light shape-bg-after law-acts">
    <div class="container">
        <div class="row laws-row">
            <div class="col-lg-8 col-12">
                <div class="row">
                    <?php
                    foreach ($laws as $item) {
                        ?>
                        <div class="col-md-6 col-sm-12">
                            <div class="service-box">
                                <div class="service-icon"> <img src="<?= Url::to('/') ?>images/Emblem_of_Uzbekistan.svg" alt=""> </div>
                                <h3><a href="<?= (MyHelpers::t($item->short_content)) ? MyHelpers::t($item->short_content) : '' ?>"><?= MyHelpers::t($item->title) ?></a></h3>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-12">
                        <?php
                        echo LinkPager::widget([
                            'pagination' => $pagination,
                            'pageCssClass' => 'page-item',
                            'disabledPageCssClass' => 'page-item disabled',
                            'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link', 'href' => '#', 'tabindex' => '-1'],
                            'linkOptions' => ['class' => 'page-link']
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="sidebar rmt-55">
                    <?= MenuCategoryWidget::widget(['cat_id' => 54, 'activeId' => 62, 'parentTitle' => $this->params['breadcrumbs'][0]['label']]) ?>
                </div>
            </div>

        </div>
    </div>
</div>