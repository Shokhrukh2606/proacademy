<?php

use app\components\helpers\MyHelpers;
use yii\helpers\Html as H;
use yii\helpers\Json as J;
use app\components\widgets\BreadCrumbs;
use app\components\widgets\SimplePostInnerWidget;
use app\components\widgets\CategoryWidget;


$this->title = J::decode($menu->title)[Yii::$app->language];
// $this->params['breadcrumbs'][] = ['label' => MyHelpers::t(Yii::$app->controller->menu[$menu->category_id]['title']), 'url' => '#'];
// $this->params['breadcrumbs'][] = $this->title;
?>
<!--==================================================================== 
            Start Banner Section
        =====================================================================-->
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<!--==================================================================== 
            End Banner Section
        =====================================================================-->


<div class="text-center pt-55 hyper-page-title">
    <h2>
        <?= J::decode($menu->title)[Yii::$app->language] ?>
        <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
    </h2>
</div>
<!--==================================================================== 
            Start Blog Single page
        =====================================================================-->
<section class="blog-single-page bg-light pb-50 pt-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="blog-details mb-50">
                    <div class="blog-details-content page">
                        <?= json_decode($menu->short_content, true)[Yii::$app->language] ?>
                        <?= json_decode($menu->content, true)[Yii::$app->language] ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar rmt-55">
                    <?=CategoryWidget::widget(['links' => (isset($categories)) ? $categories : '', 'activeId' => $menu->id, 'parentTitle' =>  MyHelpers::t(Yii::$app->controller->menu[$menu->category_id]['title'])]) ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--==================================================================== 
            End Blog Single page
        =====================================================================-->