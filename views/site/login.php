<?php

use app\components\helpers\MyHelpers as My;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('front', 'login');
$this->params['breadcrumbs'][] = $this->title;

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner text-center pt-55">
            <h2 class="page-title"><?= Yii::t('front', 'login') ?></h2>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html"></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?= Yii::t('front', 'login') ?></li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<!-- Login form -->
<div class="gorom-login-form pb-70 pt-1">
    <div class="container text-center">
        <div class="contact-form mt-100">
            <!-- <div class="section-title">
                <h2>Kirish</h2>
            </div> -->
            <div class="row align-items-center auth-block">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="fas fa-user"></i>
                        <br>
                        <span class="m-hide"> <?= Yii::t('front', 'login') ?></span>
                    </a>
                    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><i class="fas fa-user-edit"></i>
                        <br>
                        <span class="m-hide"><?= Yii::t('front', 'reg') ?></span>
                    </a>
                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><i class="fas fa-lock"></i><br>
                        <span class="m-hide"><?= Yii::t('front', 'reset_p') ?></span>
                    </a>
                </div>
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'fieldConfig' => [
                                'labelOptions' => ['class' => 'col-lg-12 control-label'],
                            ],
                        ]); ?>
                        <?= $form->field($model, 'email', [
                            'template' => "\n<div class=\"col-md-12 m-auto\"><div class=\"input-group\"> 
                        <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-user\"></i></span>
                        </div>{input}</div></div>\n<div class=\"col-lg-8\">{error}</div>",
                        ])->textInput([
                            'autofocus' => true, 'class' => 'form-control',
                            'placeholder' => Yii::t('front', 'email')
                        ]) ?>
                        <?= $form->field($model, 'password', [
                            'template' => "\n<div class=\"col-md-12 m-auto\"><div class=\"input-group\"> 
                         <div class=\"input-group-prepend\">
                             <span class=\"input-group-text\"><i class=\"fas fa-lock\"></i></span>
                         </div>{input}</div></div>\n<div class=\"col-lg-8\">{error}</div>"
                        ])->passwordInput([
                            'placeholder' => Yii::t('front', 'password')
                        ]) ?>
                        <?= $form->field($model, 'rememberMe')->checkbox([
                            'template' => "<div class=\"col-lg-offset-1 col-lg-8 mr-auto ml-auto\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        ]) ?>
                        <div class="col-12 mt-4 mb-4">
                            <?= Html::a(Html::img(Url::to('@web/images/icons/telegram.svg', true)) . Yii::t('front', 'telegram_channel'), My::t(My::getSetting(26), 'ru'), ['class' => 'telegram-link']) ?>
                            <?= Html::a('FAQ', Url::to(['/faq', 'category'=>266])) ?>                            

                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-1 col-lg-11 m-auto">
                                <?= Html::submitButton(Yii::t('front', 'login'), ['class' => 'theme-btn', 'name' => 'login-button']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'fieldConfig' => [
                                'labelOptions' => ['class' => 'col-lg-12 control-label'],
                            ],
                        ]); ?>
                        <?= $form->field($r_model, 'email', [
                            'template' => "\n<div class=\"col-md-12 m-auto\"><div class=\"input-group\"> 
                        <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-user\"></i></span>
                        </div>{input}</div></div>\n<div class=\"col-lg-8\">{error}</div>",
                        ])->textInput([
                            'autofocus' => true, 'class' => 'form-control',
                            'placeholder' => Yii::t('front', 'email')
                        ]) ?>
                        <div class="form-group">
                            <div class="col-lg-offset-1 col-lg-11 m-auto">
                                <?= Html::submitButton(Yii::t('front', 'login'), ['class' => 'theme-btn', 'name' => 'login-button']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                        <?php $form = ActiveForm::begin([
                            'layout' => 'horizontal',
                            'fieldConfig' => [
                                'labelOptions' => ['class' => 'col-12 control-label']
                            ]
                        ]); ?>
                        <?= $form->field($s_model, 'last_name', [
                            'template' => "\n<div class=\"col-md-12 m-auto\"><div class=\"input-group\"> 
                        <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fas fa-user\"></i></span>
                        </div>{input}</div></div>\n<div class=\"col-lg-8\">{error}</div>",
                        ])->textInput([
                            'placeholder' => $s_model->getAttributeLabel('last_name')
                        ]) ?>
                        <?= $form->field(
                            $s_model,
                            'username',
                            [
                                'template' => "\n<div class=\"col-md-12 m-auto\"><div class=\"input-group\"> 
                <div class=\"input-group-prepend\">
                    <span class=\"input-group-text\"><i class=\"fas fa-user-tie\"></i></span>
                </div>{input}</div></div>\n<div class=\"col-lg-8\">{error}</div>",
                            ]
                        )->textInput([
                            'autofocus' => true,
                            'placeholder' => $s_model->getAttributeLabel('username')
                        ]) ?>
                        <?= $form->field($s_model, 'middle_name', [
                            'template' => "\n<div class=\"col-md-12 m-auto\"><div class=\"input-group\"> 
                        <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fas fa-user-tag\"></i></span>
                        </div>{input}</div></div>\n<div class=\"col-lg-8\">{error}</div>",
                        ])->textInput([
                            'placeholder' => $s_model->getAttributeLabel('middle_name')
                        ]) ?>
                        <?= $form->field($s_model, 'email', [
                            'template' => "\n<div class=\"col-md-12 m-auto\"><div class=\"input-group\"> 
                        <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fas fa-envelope\"></i></span>
                        </div>{input}</div></div>\n<div class=\"col-lg-8\">{error}</div>",
                        ])->input('email', [
                            'placeholder' => $s_model->getAttributeLabel('email')
                        ]) ?>
                        <?= $form->field($s_model, 'password', [
                            'template' => "\n<div class=\"col-md-12 m-auto\"><div class=\"input-group\"> 
                        <div class=\"input-group-prepend\">
                            <span class=\"input-group-text\"><i class=\"fa fa-lock\"></i></span>
                        </div>{input}</div></div>\n<div class=\"col-lg-8\">{error}</div>",
                        ])->passwordInput([
                            'placeholder' => $s_model->getAttributeLabel('password')
                        ]) ?>
                        <div class="col-12 mt-4 mb-4">
                            <?= Html::a(Html::img(Url::to('@web/images/icons/telegram.svg', true)) . Yii::t('front', 'telegram_channel'), My::t(My::getSetting(26), 'ru'), ['class' => 'telegram-link']) ?>
                            <?= Html::a('FAQ', Url::to(['/faq', 'category'=>266])) ?>
                        </div>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('front', 'reg'), ['class' => 'theme-btn mt-5', 'name' => 'signup-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--End Login form-->