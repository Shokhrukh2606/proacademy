<?php

/* @var $this yii\web\View */

use app\components\helpers\MyHelpers as M;
use yii\helpers\Url as U;
use app\assets\FullPageAsset;
use app\components\helpers\MyHelpers;
use app\components\widgets\HomeCardWidget;
use yii\helpers\Html;
use yii\web\View;
use himiklab\thumbnail\EasyThumbnailImage;

FullPageAsset::register($this);

$this->title = Yii::t('front', 'site');
$img = \yii\helpers\Url::to('@web/images/background/');

$script = <<< JS


$('#fullpage').fullpage({
    anchors: ['header_section', 'main_section','news_section', 'interactives_section', 'staff_section', 'video_tape_section', 'statistics_section','recommended_links_section', 'footer_section'],
        afterLoad: function(anchorLink, index) {
                    console.log(index);
                    if (index == 1) {
                        $('.scrolltop').fadeOut();
                    }
                    if (index == 2) {
                        $('.scrolltop').fadeIn();
                    }
                    
                }
       
});


$(window).on('scroll',function(e) {
if ($(window).scrollTop() > (window.innerHeight)) {
    console.log('chiq')
    $('.scrolltop').fadeIn();

} else {
    console.log('korinma')
    $('.scrolltop').fadeOut();
}
})
$(".scrolltop").on("click", function(e) {
    $.fn.fullpage.moveTo('main_section');
})
JS;
$this->registerJS($script, View::POS_READY);
?>
<section class="main-slider section">
    <video autoplay muted loop id="myVideo">
        <source src="<?= U::to('/') ?>images/pro.webm" type="video/mp4">
    </video>
    <img src="<?= U::to('/') ?>images/pro-small.webp" id="smallVideo" alt="">
    <div class="container-fluid">
        <div class="main-banner">
            <img src="<?= U::to('/') ?>images/logo.png" alt="" class="main-logo">
            <h2><?= Yii::t('front', 'site') ?></h2>
            <div class="text-center promoto">
                <h3>Et docere et discere servitute legis</h3>
            </div>
        </div>
    </div>
</section>





<div class="news pb-100 pt-90  section" id="news">
    <div class="back-area"></div>
    <div class="container">
        <div class="section-title text-center">
            <h2 class="text-white"><?= Yii::t('front', 'news') ?></h2>
            <!-- <p>Ахборот-коммуникация технологиялари ахборот алмашиш жараёнини янада тезлаштиришга хизмат қилади</p> -->
        </div>
        <div class="row  mt-130">
            <div class="col-xl-8 col-lg-12">
                <div class="top">
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <?php foreach ($news_cats as $key => $item) { ?>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link <?= $key == 0 ? 'active' : '' ?>" id="pills-home-tab" data-toggle="pill" href="#pills-home<?= $key ?>" role="tab" aria-controls="pills-home" aria-selected="true"><?= MyHelpers::t($item->title) ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <?php foreach ($news_cats as $key => $item) {
                        $cat_news = MyHelpers::getCategoryNews($item->id);
                    ?>
                        <div class="tab-pane fade show <?= $key == 0 ? 'active' : '' ?>" id="pills-home<?= $key ?>" role="tabpanel" aria-labelledby="pills-home-tab<?= $key ?>">
                            <div class="row">
                                <?php
                                if (count($cat_news) > 0) {
                                    foreach ($cat_news as $news_item) {

                                        echo HomeCardWidget::widget([
                                            'title' => $news_item['title'],
                                            'short_content' => $news_item['short_content'],
                                            'link' => $news_item['alias'], 'comment_count' => $news_item['views'],
                                            'date' => $news_item['published_date'],
                                            'imglink' =>
                                            EasyThumbnailImage::thumbnailImg(
                                                // Yii::$app->request->baseUrl . 'images/building.jpg',
                                                M::getFirstImagePath($news_item['id']),
                                                400,
                                                260,
                                                EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                                                ['alt' => $news_item['alias']],
                                                100
                                            )
                                        ]);
                                ?>
                                <?php }
                                } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xl-4 col-lg-12 popular bg-light">
                <h3>
                    <?= Yii::t('front', 'most_read') ?>
                </h3>
                <div class="small-news-wrapper">
                    <?php foreach ($most_read as $item) { ?>
                        <div class="small-news news-item-carousel">
                            <div class="content">
                                <a href="">
                                    <?= M::limiter(M::t($item->title), 60) ?>
                                </a>
                                <div class="infos">
                                    <div class="date">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="18" viewBox="0 0 18 20">
                                            <path id="ic_date_range_24px" d="M9,11H7v2H9Zm4,0H11v2h2Zm4,0H15v2h2Zm2-7H18V2H16V4H8V2H6V4H5A1.991,1.991,0,0,0,3.01,6L3,20a2,2,0,0,0,2,2H19a2.006,2.006,0,0,0,2-2V6A2.006,2.006,0,0,0,19,4Zm0,16H5V9H19Z" transform="translate(-3 -2)" fill="#9e9da6" />
                                        </svg>
                                        <span><?= M::parse_date($item->published_date) ?></span>
                                    </div>
                                    <div class="views">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="13" viewBox="0 0 22 15">
                                            <path id="ic_remove_red_eye_24px" d="M12,4.5A11.827,11.827,0,0,0,1,12a11.817,11.817,0,0,0,22,0A11.827,11.827,0,0,0,12,4.5ZM12,17a5,5,0,1,1,5-5A5,5,0,0,1,12,17Zm0-8a3,3,0,1,0,3,3A3,3,0,0,0,12,9Z" transform="translate(-1 -4.5)" fill="#9e9da6" />
                                        </svg>
                                        <span><?= $item->views ?></span></div>

                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="all-navigator text-center">
            <?= Html::a(Yii::t('front', 'all_news'), U::to('news/index')); ?>
        </div>
    </div>
</div>





<div class="interactives pb-70 pt-90 section" id="interactives">
    <div class="back-area"></div>
    <div class="container text-center">
        <div class="section-title">
            <h2 class="text-white"><?= Yii::t('front', 'interactives') ?></h2>
            <!-- <p>Ахборот-коммуникация технологиялари ахборот алмашиш жараёнини янада тезлаштиришга хизмат қилади</p> -->
        </div>
        <div class="row">
            <?php foreach ($interactives as $item) { ?>
                <div class="col-lg-3 col-sm-6 mb-5">
                    <a class="service-box" href="<?= ($item->link) ? U::to($item->link) : U::to(['menu/' . $item->alias]) ?>">
                        <div class="service-icon"><i class="<?= $item->icon ?>"></i></div>
                        <h3>
                            <?= M::t($item->title) ?>
                        </h3>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>






<div class="staff pb-10 pt-90 section" id="staff">
    <div class="back-area"></div>
    <div class="container text-center">
        <div class="section-title ">
            <h2 class="color-white"><?= Yii::t('front', 'staff') ?></h2>
        </div>
        <div class="experts-inner three-item-carousel  mt-150">
            <?php foreach ($heads as $item) { ?>
                <div class="every-expert">
                    <div class="expert-img">
                        <?= M::hasMedia($item->id) ? EasyThumbnailImage::thumbnailImg(
                            M::getFirstMediaFile($item->id)->getAbsolutePath(),
                            350,
                            350,
                            EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                            ['alt' => $item->alias],
                            100
                        ) : '' ?>
                        <div class="expert-overlay">
                            <div class="social-style-one">
                                <?= M::t($item->content) ?>
                            </div>
                        </div>
                    </div>
                    <div class="expert-description">
                        <h5>
                            <?= M::t($item->title) ?>
                        </h5>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>


<div class="video-tape pb-100 pt-90  section" id="news">
    <div class="back-area"></div>
    <div class="container">
        <div class="section-title text-center">
            <h2 class="color-white"><?= Yii::t('front', 'video_tape') ?></h2>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a class="big-video fancybox" data-fancybox href="<?= $videos[0]->link ? 'http://www.youtube.com/watch?v=' . $videos[0]->link : M::getFirstMedia($videos[0]->id) ?>">
                    <?= M::hasMedia($videos[0]->id, 'videos') ? EasyThumbnailImage::thumbnailImg(
                        M::getFirstMediaFile($videos[0]->id)->getAbsolutePath(),
                        595,
                        300,
                        EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                        ['alt' => $videos[0]->alias, 'class' => 'poster'],
                        100
                    ) : ''
                    ?>
                    <div class="video-title">
                        <p><?= M::limiter(M::t($videos[0]->title), 45) ?></p>
                        <img src="<?= U::to('/') ?>images/icons/play.svg" alt="play" class="player">
                    </div>
                </a>
            </div>
            <div class="col-md-6">
                <a class="big-video fancybox" data-fancybox href="<?= $videos[1]->link ? 'http://www.youtube.com/watch?v=' . $videos[1]->link : M::getFirstMedia($videos[1]->id) ?>">
                    <?= M::hasMedia($videos[1]->id, 'videos') ? EasyThumbnailImage::thumbnailImg(
                        M::getFirstMediaFile($videos[1]->id)->getAbsolutePath(),
                        595,
                        300,
                        EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                        ['alt' => $videos[0]->alias, 'class' => 'poster'],
                        100
                    ) : ''
                    ?>c
                    <div class="video-title">
                        <p><?= M::limiter(M::t($videos[1]->title), 45) ?></p>
                        <img src="<?= U::to('/') ?>images/icons/play.svg" alt="play" class="player">
                    </div>
                </a>
            </div>
        </div>
        <div class="small-video-wrapper mt-4">
            <div class="row">
                <?php
                $otherVideos = array_splice($videos, 2, 3);
                foreach ($otherVideos as $key => $item) {
                ?>
                    <div class="col-lg-4">
                        <div class="small-video">
                            <a data-fancybox class="fancybox" href="<?= $item->link ? 'http://www.youtube.com/watch?v=' . $item->link : M::getFirstMedia($item->id) ?>">
                                <?php
                                echo EasyThumbnailImage::thumbnailImg(
                                    M::getFirstMediaFile($item->id)->getAbsolutePath(),
                                    400,
                                    230,
                                    EasyThumbnailImage::THUMBNAIL_OUTBOUND,
                                    ['alt' => $item->alias, 'class' => 'poster'],
                                    100
                                );
                                ?>
                                <div class="video-title">
                                    <img src="<?= U::to('/') ?>images/icons/play-mini.svg" alt="play" class="player">
                                    <p> <?= M::limiter(M::t($item->title), 45) ?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php
                } ?>
                <!-- <div class="col-lg-4">
                    <div class="small-video">
                        <a class="img" href="#">
                            <img src="<?= U::to('/') ?>images/inst2.jpg" alt="video" class="poster">
                            <div class="video-title">
                                <img src="<?= U::to('/') ?>images/icons/play-mini.svg" alt="play" class="player">
                                <p>В Яшнабаде возникают пробки из-за строительства
                                    трехуровневого
                                    моста.</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="small-video">
                        <a class="img" href="#">
                            <img src="<?= U::to('/') ?>images/inst5.jpg" alt="video" class="poster">
                            <div class="video-title">
                                <img src="<?= U::to('/') ?>images/icons/play-mini.svg" alt="play" class="player">
                                <p>Депутаты устроили массовую драку в парламенте
                                    Турции</p>
                            </div>
                        </a>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div class="all-navigator text-center">
        <?= Html::a(Yii::t('front', 'all_video'), U::to(['gallery/index'])); ?>
    </div>
</div>
<div class="pb-70 pt-90 section" id="statistics">
    <div class="back-area"></div>
    <div class="container text-center">
        <div class="section-title">
            <h2 class="text-white"><?= Yii::t('front', 'stat') ?></h2>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="single-fact">
                    <span class="count-text" data-speed="5000" data-stop="78">0</span>
                    <i class="text-statistic"><?= Yii::t('front', 'stat_1') ?></i>
                    <p></p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-fact">
                    <span class="count-text symbol" data-speed="5000" data-stop="31">0</span>
                    <i class="text-statistic"><?= Yii::t('front', 'stat_2') ?></i>
                    <p></p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-fact">
                    <span class="count-text symbol" data-speed="5000" data-stop="4713">0</span>
                    <i class="text-statistic"><?= Yii::t('front', 'stat_3') ?></i>
                    <p></p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-fact">
                    <span class="count-text" data-speed="5000" data-stop="1485">0</span>
                    <i class="text-statistic"><?= Yii::t('front', 'stat_4') ?></i>
                    <p></p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-fact">
                    <span class="count-text symbol" data-speed="5000" data-stop="2237">0</span>
                    <i class="text-statistic"><?= Yii::t('front', 'stat_5') ?></i>
                    <p></p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-fact">
                    <span class="count-text" data-speed="5000" data-stop="24">0</span>
                    <i class="text-statistic"><?= Yii::t('front', 'stat_6') ?></i>
                    <p></p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-fact">
                    <span class="count-text symbol" data-speed="5000" data-stop="34">0</span>
                    <i class="text-statistic"><?= Yii::t('front', 'stat_7') ?></i>
                    <p></p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="single-fact">
                    <span class="count-text symbol" data-speed="5000" data-stop="17">0</span>
                    <i class="text-statistic"><?= Yii::t('front', 'stat_8') ?></i>
                    <p></p>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="useful-links pb-100 pt-90 section" id="news">
    <div class="back-area"></div>
    <div class="container">
        <div class="section-title text-center">
            <h2 class="text-white"><?= Yii::t('front', 'recommended_links') ?></h2>
        </div>
        <div class="client-testimonial-inner single-news-carousel text-white mt-150">
            <?php foreach ($links as $item) { ?>
                <div class="item">
                    <div class="service-box">
                        <div class="useful-links-img"> <img src="<?= MyHelpers::getFirstMedia($item->id) ?>" alt=""> </div>
                        <h3><a href="<?= $item->link ?>"><?= MyHelpers::t($item->title) ?></a></h3>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- <div class="our-address pb-100 pt-90 section" id="news">
    <div class="back-area"></div>
    <div class="container">
        <div class="section-title text-center">
            <h2 class="text-white"><?= Yii::t('front', 'address') ?></h2>
        </div>
        <div class="row mt-130">
            <div class="col-4">
<script src="https://snapwidget.com/js/snapwidget.js"></script>
<iframe src="https://snapwidget.com/embed/755292" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; "></iframe>
            </div>
            <div class="col-12 col-md-8 m-auto">
                <div style="position:relative;overflow:hidden;"><a href="https://yandex.uz/maps/org/208528954476/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Академия генеральной прокуратуры Республики Узбекистан</a><a href="https://yandex.uz/maps/10335/tashkent/category/prosecutor_office/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:14px;">Прокуратура в Ташкенте</a><iframe src="https://yandex.uz/map-widget/v1/-/CCQpNWes-C" height="400" frameborder="1" allowfullscreen="true" style="position:relative; width:100%;"></iframe></div>
            </div>
        </div>
    </div>
</div> -->