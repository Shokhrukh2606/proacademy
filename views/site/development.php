        <!--==================================================================== 
            Start 404 Page Area
        =====================================================================-->
        <section class="error-page align-items-center d-flex">
            <div class="container">
                <div class="error-inner text-center">
                    <h2 class="mb-55">
                        <?=Yii::t('front', 'on_development')?>
                    </h2>
                </div>
            </div>
            <div class="black">

            </div>
        </section>
        <!--==================================================================== 
            End 404 Page Area
        =====================================================================-->