<?php

use yii\helpers\Html as H;
use app\components\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('front', 'reg');
$this->params['breadcrumbs'][] = $this->title;
?>
<!--==================================================================== 
            Start Banner Section
        =====================================================================-->
<section class="banner-section">
    <div class="container">
        <div class="banner-inner text-center pt-55">
            <h2 class="page-title"><?= $this->title ?></h2>
            <nav aria-label="breadcrumb">
                <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                ?>
            </nav>
        </div>
    </div>
</section>
<div class="gorom-login-form pb-70 pt-1">
    <div class="container text-center">
        <div class="contact-form mt-100">
            <div class="section-title">
                <h2><?= $this->title ?></h2>
            </div>
            <?php $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableAjaxValidation' => true,
                'fieldConfig' => [
                    'labelOptions' => ['class' => 'col-12 control-label'],
                    'template' => "\n<div class=\"col-12 col-sm-12 col-md-6 col-lg-6 ml-auto mr-auto\">{label}<div class=\"form-group\" >{input}</div></div>\n<div class=\"col-lg-8\">{error}</div>"
                ]
            ]); ?>
            <?= $form->field($model, 'last_name')->textInput() ?>
            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'middle_name')->textInput() ?>
            <?= $form->field($model, 'email', ['enableAjaxValidation' => true]) ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            Agar ro'yhatdan o'tgan bo'lsangiz  <?=H::a('kirish','login')?> bosing
            <div class="form-group">
                <?= H::submitButton('Ro\'yhatdan o\'tish', ['class' => 'theme-btn mt-5', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>