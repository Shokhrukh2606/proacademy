<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<section class="blog-single-page password-reset bg-gray pb-50 pt-70">
    <div class="container">
    <div class="row">
        <div class="col-lg-6 m-auto">
        <h2><?=Yii::t('front', 'Parolni tiklash' )?></h2>

            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('front', 'send' ), ['class' => 'theme-btn']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
    </div>
</section>