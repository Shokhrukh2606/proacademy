        <!--==================================================================== 
            Start 404 Page Area
        =====================================================================-->
        <section class="error-page align-items-center d-flex">
            <div class="container">
                <div class="error-inner text-center">
                    <img src="<?=\yii\helpers\Url::to('/') ?>images/websitedev.png" width="500" alt="User Image"/>
                    <h2 class="mb-55"><?=Yii::t('front', '404')?></h2>
<!--                    <div class="error-search d-flex flex-row">-->
<!--                        <input type="text" placeholder="Type Here">-->
<!--                        <button class="theme-btn">Search</button>-->
<!--                    </div>-->
                </div>
                <div class="black">

                </div>
            </div>
        </section>
        <!--==================================================================== 
            End 404 Page Area
        =====================================================================-->