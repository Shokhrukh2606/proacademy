<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use app\assets\FrontAsset;
use app\assets\ZoomerAsset;
use app\components\widgets\MenuWidget;
use yii\helpers\Url;
use app\components\helpers\MyHelpers as My;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\web\View;

if (Yii::$app->urlManager->createUrl(['site/index']) != $_SERVER['REQUEST_URI']) {
  $this->registerJsFile('@web/gorom/js/jquery.min.js');
}

FrontAsset::register($this);
ZoomerAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript">
    (function(m, e, t, r, i, k, a) {
      m[i] = m[i] || function() {
        (m[i].a = m[i].a || []).push(arguments)
      };
      m[i].l = 1 * new Date();
      k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(66841585, "init", {
      clickmap: true,
      trackLinks: true,
      accurateTrackBounce: true
    });
  </script>
  <noscript>
    <div><img src="https://mc.yandex.ru/watch/66841585" style="position:absolute; left:-9999px;" alt="" /></div>
  </noscript>
  <!-- /Yandex.Metrika counter -->
  <link rel="shortcut icon" href="<?= Yii::$app->request->baseUrl ?>/images/logo.png" type="image/png">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<?php
$script = <<< JS
$("#search_button").click(function(e){
    var search_param=$("#search_place input").val()
    if(search_param==''){
        return false
    }else if(search_param.includes('#')){
        $("#search_place").trigger("submit")
    }    else{
        $("#search_place").trigger("submit")
    }
})
$("#search_place").on("submit", function(){
    var search_param=$("#search_place input").val()
    if(search_param.includes('#')){
        $('#search_place input[name="hashed"]').val('1')
        return true
    } else {
        $('#search_place input[name="hashed"]').val('0')
        return true
    }
})
JS;
$this->registerJS($script, View::POS_READY);

?>

<body>
  <?php $this->beginBody() ?>
  <?php if (My::matchUrl('site', 'index')) { ?>
    <div class="scrolltop">
      <i class="fa fa-arrow-up"></i>
    </div>
  <?php } ?>

  <div class="page-wrapper" id="fullpage">
    <!-- Preloader -->
    <div class="preloader"></div>

    <?php // if (!Yii::$app->user->getIsGuest()) { 
    ?>
    <!-- <div class="user-signed">
                <?= Html::a(Html::tag('i', '', ['class' => 'flaticon-user']), ['/applicant']) ?></br>
                <?= Html::a(Html::tag('i', '', ['class' => 'flaticon-next-1']), ['/site/logout']) ?>
            </div> -->
    <?php // } 
    ?>

    <div class="beta-icon">
      <i><?= Yii::t('front', 'test_mode') ?></i>
    </div>
    <!--==================================================================== 
                                Start Header area
        =====================================================================-->
    <header class="main-header <?= My::matchUrl('site', 'index') ? 'home' : ''  ?> section fp-auto-height">

      <!--Header-Upper-->
      <div class="header-upper">
        <div class="container clearfix">

          <div class="header-inner d-lg-flex align-items-center">

            <!-- <div class="logo-outer d-flex align-items-center">
                            <div class="logo"><a href="index.html"><img src="<?= Yii::$app->request->baseUrl ?>/gorom/images/logo.png" alt="" title=""></a></div>

                        </div> -->

            <!-- <div class="nav-outer clearfix ml-5"> -->
            <div class="nav-outer clearfix">
              <!-- Main Menu -->
              <nav class="main-menu navbar-expand-lg">
                <div class="navbar-header clearfix">
                  <!-- Toggle Button -->
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>

                <div class="navbar-collapse collapse clearfix">
                  <?= MenuWidget::widget() ?>
                </div>

              </nav>
              <!-- Main Menu End-->
            </div>

            <!--     <div class="menu-btn">
                            <button class="button-search btn bg-transparent text-white ml-20 mr-50"><i class="flaticon-loupe"></i></button>
                            <a href="contact.html" class="theme-btn br-5">Contact</a>

                            Menu Serch Box
                            <div class="nav-search hide">
                                <form><input type="text" placeholder="Search" class="searchbox" required>
                                    <button type="submit" class="searchbutton fa fa-search"></button>
                                </form>
                            </div>
                        </div> -->
          </div>

        </div>
      </div>
      <!--End Header Upper-->

    </header>
    <!--==================================================================== 
                                End Header area
        =====================================================================-->
    <?php
    echo Alert::widget([
      'options' => ['class' => 'my-alert']
    ]);
    ?>

    <div class="right first">
      <div class="item">
        <div class="icon">
          <i class="fa fa-search" id="search_button"></i>
        </div>
        <div class="show">
          <form action="<?= Url::to(Yii::$app->language . '/proacademy/global-search', true) ?>" id="search_place" method="GET">
            <?php //\yii\helpers\Html::hiddenInput(Yii::$app->request->csrfParam, Yii::$app->request->getCsrfToken());
            ?>
            <input type="text" name="param" placeholder="<?=Yii::t('front', 'search')?>">
            <input type="hidden" name="hashed">
          </form>
        </div>
      </div>
    </div>
    <div class="right second">
      <div class="item">
        <div class="icon">
          <i class="fab fa-telegram-plane"></i>
        </div>
        <div class="show">
          <a href="<?= My::t(My::getSetting(27), 'ru') ?>">ProAcademy</a>
        </div>
      </div>
    </div>
    <div class="right third">
      <div class="item">
        <div class="icon">
          <i class="fa fa-phone-alt"></i>
        </div>
        <div class="show">
          <a href="tel:712020496">(71) 202-04-96</a>
        </div>
      </div>
    </div>
    <div class="right fourth">
      <div class="item">
        <div class="icon">
          <i class="fa fa-envelope"></i>
        </div>
        <div class="show">
          <a href="mailto:info@proacademy.uz">info@proacademy.uz</a>
        </div>
      </div>
    </div>
    <div class="right fifth">
      <div class="item">
        <div class="icon">
          <i class="fa fa-eye"></i>
        </div>
        <div class="show">
          <a href="#" class="vi-open" href="javascript:(0)"><?= Yii::t('front', 'for_eyed') ?></a>
        </div>
      </div>
    </div>

    <?= $content ?>
    <!--==================================================================== 
                            Start Footer Section
    =====================================================================-->

    <footer class="main-footer section fp-auto-height">

      <div class="container">
        <div class="row clearfix">

          <!--Big Column-->
          <div class="big-column col-lg-4 col-12">
            <div class="row clearfix">

              <!--Footer Column-->
              <div class="footer-column col-12 col-md-12">
                <div class="footer-widget logo-widget">
                  <div class="footer-logo"><a href="index.html"><img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" alt=""></a>
                    <span><?= Yii::t('front', 'site') ?></span>
                  </div>
                  <div class="widget-content">
                    <h2><?= Yii::t('front', 'call') ?></h2>

                    <!-- <div class="text">At the end of the day, going forward, a new normal that has evolved generation X is on the runway heading towards a streamlined cloud solution. </div> -->
                    <div class="contact-item">
                      <i class="fa fa-phone-alt"></i>
                      <div>
                        <span><?= Yii::t('front', 'call_center') ?></span>
                        <a href="tel:712020496">(71) 202-04-96</a>
                      </div>
                    </div>
                    <div class="contact-item">
                      <i class="fa fa-envelope"></i>
                      <div>
                        <a href="mailto:info@proacademy.uz">info@proacademy.uz</a>
                      </div>
                    </div>
                    <div class="contact-item">
                      <i class="fa fa-map-marker-alt"></i>
                      <div>
                        <span><?= Yii::t('front', 'add') ?></span>
                      </div>
                    </div>
                    <style>
                      #subscribe{
                        margin-top: 20px;
                      }
                      #subscribe input{
                        background-color: #fff;
                        margin: 0;
                        display: inline-block;
                        height: 36px;
                        width: 210px;
                        padding: 5px 10px;
                        color: black;
                      }
                      #subscribe input::placeholder{
                        color: black;
                      }
                      #subscribe button{
                        padding: 5.5px 10px;
                        background-color: #2962ff;
                        color: white;
                        margin: 0;
                        cursor: pointer;
                      }
                    </style>
                    <form action="/proacademy/subscribe" id="subscribe">
                      <input type="email" required name="email" placeholder="<?=Yii::t('front','email')?>"><button><?=Yii::t('front', 'subscribe')?></button>
                    </form>
                    <div class="social-style-one mt-15">
                      <a href="<?= My::t(My::getSetting(30), 'ru') ?>"><i class="fab fa-facebook-f"></i></a>
                      <a href="<?= My::t(My::getSetting(29), 'ru') ?>"><i class="fab fa-twitter"></i></a>
                      <a href="<?= My::t(My::getSetting(27), 'ru') ?>"><i class="fab fa-telegram-plane"></i></a>
                      <a href="<?= My::t(My::getSetting(28), 'ru') ?>"><i class="fab fa-instagram"></i></a>
                    </div>
                    <div class="developed-by">
                      <?= Yii::t('front', 'developed_by') ?> &nbsp;<a href="http://napaautomative.com/" target="_blank"> Napa Automative</a> </div>
                  </div>
                </div>
              </div>

              <!--Footer Column-->
              <!-- <div class="footer-column col-md-4 col-sm-6 col-xs-12">
                                <div class="footer-widget links-widget">
                                    <h2>QUICK LINKS</h2>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li><a href="#">Home</a></li>
                                            <li><a href="#">About Us</a></li>
                                            <li><a href="#">Services</a></li>
                                            <li><a href="#">News</a></li>
                                            <li><a href="#">Shop</a></li>
                                            <li><a href="#">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->

            </div>
          </div>

          <!--Big Column-->
          <div class="big-column col-lg-8 col-12">
            <div class="row clearfix">
              <div class="col-12">
                <?php if (\Yii::$app->language == 'ru') { ?>
                  <div style="position:relative;overflow:hidden;"><a href="https://yandex.uz/maps/org/208528954476/?utm_medium=mapframe&utm_source=maps?lang=ru_RU" style="color:#eee;font-size:12px;position:absolute;top:0px;">Академия генеральной прокуратуры Республики Узбекистан</a><a href="https://yandex.uz/maps/10335/tashkent/category/prosecutor_office/?utm_medium=mapframe&utm_source=maps?lang=ru_RU" style="color:#eee;font-size:12px;position:absolute;top:14px;">Прокуратура в Ташкенте</a><iframe src="https://yandex.uz/map-widget/v1/-/CCQpNWes-C?lang=ru_RU" height="400" frameborder="1" allowfullscreen="true" style="position:relative; width:100%;"></iframe></div>
                <?php } else { ?>
                  <div style="position:relative;overflow:hidden;"><a href="https://yandex.uz/maps/org/208528954476/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Академия генеральной прокуратуры Республики Узбекистан</a><a href="https://yandex.uz/maps/10335/tashkent/category/prosecutor_office/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:14px;">Прокуратура в Ташкенте</a><iframe src="https://yandex.uz/map-widget/v1/-/CCQpNWes-C" height="400" frameborder="1" allowfullscreen="true" style="position:relative; width:100%;"></iframe></div>
                <?php } ?>
              </div>
              <!--Footer Column-->
              <!-- <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <h2><?= Yii::t('front', 'latest_news') ?></h2>
                                Latest News
                                <div class="posts-widget">
                                    <div class="posts">
                                        <div class="post">
                                            <figure class="post-thumb"><img src="<?= Yii::$app->request->baseUrl ?>/gorom/images/gallery/lp1.jpg" alt=""></figure>
                                            <div class="desc-text"><a href="blog-details.html">Finance and legal work throughout the project</a></div>
                                            <div class="time">Sep 27, 2020</div>
                                        </div>
                                        <div class="post">
                                            <figure class="post-thumb"><img src="<?= Yii::$app->request->baseUrl ?>/gorom/images/gallery/lp2.jpg" alt=""></figure>
                                            <div class="desc-text"><a href="blog-details.html">Finance and legal work throughout the project</a></div>
                                            <div class="time">Oct 04, 2020</div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

              <!--Footer Column-->
              <!-- <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <h2>Instagram</h2>

                                <div class="footer-widget gallery-widget">
                                    <div class="row clearfix">

                                    </div>
                                </div>

                            </div> -->

            </div>
          </div>

        </div>

      </div>

      <!--Footer Bottom-->
      <div class="footer-bottom">
        <div class="copyright">&copy; <?= date('Y') ?> <?= Yii::t('front', 'site') ?>. <?= Yii::t('front', 'privacy') ?>.</div>
      </div>


    </footer>

  </div>


  <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>