<?php

use app\components\helpers\MyHelpers as M;
use app\components\helpers\MyHelpers;
use app\components\widgets\NewsCategoryWidget;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = MyHelpers::t($post->title);

// $tags=A

?>
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<section class="blog-single-page bg-light pt-100 pb-80 news-view">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12 news-content">
                <div class="posts-images-carousel">
                    <div class="owl-carousel images-carousel">
                        <?php foreach ($medias as $item) { ?>
                            <img src="<?= Url::to('/postfiles/documents/' . $item->url) ?>" alt="" class="item">
                        <?php } ?>
                    </div>
                </div>
                <h3>
                    <?= M::t($post->title) ?>
                </h3>
                <span class="anons">
                    <!-- <?= M::t($post->short_content) ?> -->
                </span>
                <?= M::t($post->content) ?>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f0f7b550e81dbdc"></script>

            </div>
            <div class="col-lg-4 col-12">
                <div class="sidebar rmt-55">
                    <?= NewsCategoryWidget::widget(['links' => Yii::$app->controller->news_category, 'parentTitle' => Yii::t('front', 'news')]) ?>
                    <div class="widget">
                        <h3><?= Yii::t('front', 'tags') ?></h3>
                        <div class="tag-widget">
                            <?php
                            foreach ($tags as $item) {
                                echo Html::a(MyHelpers::t($item['title']), '#');
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>