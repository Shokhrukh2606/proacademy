<?php

use app\components\helpers\MyHelpers as M;
use app\components\widgets\BreadCrumbs;
use app\components\widgets\SimplePostInnerWidget;
use app\components\widgets\CategoryWidget;


$this->params['parentMenu'] =  M::t(Yii::$app->controller->menu[54]['title']);
$this->params['childMenu'] = M::t(Yii::$app->controller->menu[66]['title']);
$this->params['subChildMenu'] = M::t(Yii::$app->controller->branchMenus[$post->category_id]['title']);

$this->title = M::t($post->title);
$this->params['breadcrumbs'][] = ['label' => $this->params['parentMenu'], 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->params['childMenu'], 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->params['subChildMenu'], 'url' => '#'];

$this->params['breadcrumbs'][] = $this->title;
?>
<!--==================================================================== 
            Start Banner Section
        =====================================================================-->
<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?= Yii::t('front', 'site') ?></p>
        </div>
    </div>
</section>
<section class="blog-single-page bg-light pb-50 pt-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="blog-details mb-30">
                    <div class="blog-details-content page">
                        <h3 class="text-center"><?= $this->title ?></h3>
                    </div>
                </div>
                <?= M::t($post->content) ?>

            </div>
            <div class="col-lg-4">
                <div class="sidebar rmt-55">
                    <?= CategoryWidget::widget(['activeId' => 66, 'links' => (isset($categories)) ? $categories : '', 'parentTitle' => $this->params['breadcrumbs'][0]['label']]) ?>
                </div>
            </div>
        </div>
    </div>
</section>