<?php

use app\components\helpers\MyHelpers;
use app\components\widgets\BreadCrumbs;
use app\components\widgets\MenuCategoryWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = MyHelpers::t($category->title);
$this->params['breadcrumbs'][] = ['label' => MyHelpers::t(Yii::$app->controller->menu[54]['title']), 'url' => '#'];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="banner-section">
    <div class="container">
        <div class="banner-inner pt-55 text-center">
            <img src="<?= Yii::$app->request->baseUrl ?>/images/logo.png" class="logo" alt="">
            <p class="pro-title"><?=Yii::t('front', 'site')?></p>
        </div>
    </div>
</section>
<div class="text-center pt-55 hyper-page-title">
    <h2>
       <?= $this->title?>
       <img src="<?= Yii::$app->request->baseUrl ?>/images/icons/book.svg" class="title-book" alt="">
       
    </h2>
</div>
<div class="services-section pb-70 pt-90 bg-light shape-bg-after branches">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12 branches-list">
                <?php
                foreach ($branches as $item) {
                    $childrens = MyHelpers::getCategoryPosts($item->id, 'branches');
                ?>
                    <h2 class="mb-5 text-center"><a href="#"><?= MyHelpers::t($item->title) ?></a></h2>
                <?php
                    echo Html::ul($childrens, ['item' => function ($child) {
                        return Html::tag('li', Html::a(MyHelpers::t($child->title), ['view', 'alias' => $child->alias]));
                    }]);
                } ?>
            </div>
            <div class="col-lg-4 col-12">
                <div class="sidebar rmt-55">
                    <?= MenuCategoryWidget::widget(['cat_id' => 54, 'activeId' => 66, 'parentTitle' => $this->params['breadcrumbs'][0]['label']]) ?>
                </div>
            </div>
        </div>
    </div>
</div>