<?php

return [
    // 'class' => 'yii\db\Connection',
    // 'dsn' => 'mysql:host=localhost;dbname=proacademy_db',
    // 'username' => 'root',
    // 'password' => '',
    // 'charset' => 'utf8',
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=prodb',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
