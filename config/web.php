<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name'=>'Proacademy UZ',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'admin', 'media', 'thumbnail'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module',
            // other module settings
        ],
        'applicant' => [
            'class' => 'app\modules\applicant\Module',
        ],
        'imagemanager' => [
            'class' => 'noam148\imagemanager\Module',
            //set accces rules ()

            // 'layout'=>'themes\admin\layouts\main',
            'canUploadImage' => true,
            'canRemoveImage' => function(){
                return true;
            },
            //add css files (to use in media manage selector iframe)
            'cssFiles' => [
                'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css',
            ],
        ],
    ],
    'components' => [
        'imagemanager' => [
            'class' => 'noam148\imagemanager\components\ImageManagerGetPath',
            //set media path (outside the web folder is possible)
            'mediaPath' => 'postfiles/imagemanager/tinymce',
            //path relative web folder to store the cache images
            'cachePath' => 'assets/images',
            //use filename (seo friendly) for resized images else use a hash
            'useFilename' => true,
            //show full url (for example in case of a API)
            'absoluteUrl' => false,
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,   // do not publish the bundle
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'js' => [
                        'gorom/js/jquery.min.js',
                    ]
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'thumbnail' => [
            'class' => 'himiklab\thumbnail\EasyThumbnail',
            'cacheAlias' => 'assets/gallery_thumbnails',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i',
            'timeFormat' => 'php:H:i',
        ],
        'i18n' => [
            'translations' => [
                'admin' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'admin' => 'admin.php',
                        'admin/error' => 'adminError.php',
                    ],
                ],
                'front' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'front' => 'front.php',
                        'front/error' => 'frontError.php',
                    ],
                ],
            ],
        ],
        'request' => [
            'baseUrl' => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'CUCwJjPVsC4l9IoqciZ72vbL8oJqbwQ6'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],

        'media' => [
            'class' => 'ozerich\filestorage\FileStorage',
            'scenarios' => [
                'avatar' => [
                    'storage' => [
                        'type' => 'file',
                        'saveOriginalFilename' => false,
                        'uploadDirPath' => 'uploads/avatars',
                        'uploadDirUrl' => '/uploads/avatars',
                    ],
                    'validator' => [
                        'maxSize' => 2 * 1024 * 1024,     // 2 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['jpg', 'jpeg', 'bmp', 'gif', 'png']
                    ],
                    'thumbnails' => [
                        [
                            'width' => 413
                        ],
                        [
                            'height' => 531
                        ],
                        [
                            'alias' => 'preview',
                            'width' => 413,
                            'height' => 531,
                            'exact' => true
                        ],
                    ],
                    'quality' => 100
                ],
                'document' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'postfiles/documents',
                        'uploadDirUrl' => '/postfiles/documents',
                    ],
                    'validator' => [
                        'maxSize' => 20 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['pdf', 'docx', 'doc', 'img', 'jpeg', 'png', 'mp4'],
                    ],
                ],
                'sets' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'postfiles/sets',
                        'uploadDirUrl' => '/postfiles/sets',
                    ],
                    'validator' => [
                        'maxSize' => 20 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['zip','rar', 'img', 'jpeg', 'png'],
                    ],
                ],
                'books' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'postfiles/books',
                        'uploadDirUrl' => '/postfiles/books',
                    ],
                    'validator' => [
                        'maxSize' => 20 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['zip','rar', 'img', 'jpeg', 'png', 'pdf'],
                    ],
                ],
                'diplomas' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'uploads/diplomas',
                        'uploadDirUrl' => '/uploads/diplomas',
                    ],
                    'validator' => [
                        'maxSize' => 10 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['pdf', 'img', 'jpeg', 'png'],
                    ],
                ],
                'passports' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'uploads/passports',
                        'uploadDirUrl' => '/uploads/passports',
                    ],
                    'validator' => [
                        'maxSize' => 10 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['img', 'pdf', 'jpeg', 'png'],
                    ],
                ],
                'certificates' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'uploads/certificates',
                        'uploadDirUrl' => '/uploads/certificates',
                    ],
                    'validator' => [
                        'maxSize' => 10 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['pdf', 'img', 'png', 'jpeg'],
                    ],
                ],
                'decisions' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'uploads/decisions',
                        'uploadDirUrl' => '/uploads/decisions',
                    ],
                    'validator' => [
                        'maxSize' => 10 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['pdf', 'img', 'jpeg', 'png'],
                    ],
                ],
                'lessons' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'uploads/lessons',
                        'uploadDirUrl' => '/uploads/lessons',
                    ],
                    'validator' => [
                        'maxSize' => 10 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['jpeg', 'png'],
                    ],
                ],
                'recomendations' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'uploads/recomendations',
                        'uploadDirUrl' => '/uploads/recomendations',
                    ],
                    'validator' => [
                        'maxSize' => 10 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['pdf'],
                    ],
                ],
                'biographies' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'uploads/biographies',
                        'uploadDirUrl' => '/uploads/biographies',
                    ],
                    'validator' => [
                        'maxSize' => 10 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['pdf'],
                    ],
                ],
                'relatives' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'uploads/relatives',
                        'uploadDirUrl' => '/uploads/relatives',
                    ],
                    'validator' => [
                        'maxSize' => 10 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['pdf'],
                    ],
                ],
                'careers' => [
                    'storage' => [
                        'type' => 'file',
                        'uploadDirPath' => 'uploads/careers',
                        'uploadDirUrl' => '/uploads/careers',
                    ],
                    'validator' => [
                        'maxSize' => 10 * 1024 * 1024,      // 20 MB
                        'checkExtensionByMimeType' => true,
                        'extensions' => ['pdf'],
                    ],
                ]
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'toshniyozovshohruh@gmail.com',
                'password' => 'Webper1q2w',
                'port' => '587',
                'encryption' => 'tls'
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        // 'language' => 'ru',
        // 'sourceLanguage'=>'ru',
        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //   'hostInfo' => 'http://qr.proacademy.uz',
            //   'hostInfo' => 'http://192.168.5.32',
            // 'secureHostInfo' => 'https://www.ekomarked.uz',
            // 'secureRoutes' => array(
            // 	'site/login',   // site/login action
            // 	'site/signup',  // site/signup action
            //     'applicants/default'
            // ),
            'rules' => [
                [
                    'pattern' => 'menu/<alias:[0-9a-zA-Z\-]+>',
                    'route' => 'menu/index'
                ],
                [
                    'pattern' => 'check-attestation',
                    'route' => 'proacademy/check-attestation'
                ],
                [
                    'pattern' => 'proacademy/cooperation-int/index',
                    'route' => 'proacademy/cooperation-int'
                ],
                [
                    'pattern' => 'proacademy/cooperation-inter/index',
                    'route' => 'proacademy/cooperation-inter'
                ],
                [
                    'pattern' => 'nocorruption/index',
                    'route' => 'proacademy/anti-corruption'
                ],
                [
                    'pattern' => 'e-library/index',
                    'route' => 'proacademy/e-library'
                ],
                [
                    'pattern' => 'masters-degree/index',
                    'route' => 'proacademy/masters'
                ],
                [
                    'pattern' => 'interactive/index',
                    'route' => 'proacademy/interactives'
                ],
                [
                    'pattern' => 'literacy/index',
                    'route' => 'proacademy/literacy'
                ],
                [
                    'pattern' => 'check-yourself',
                    'route' => 'proacademy/check-yourself'
                ],
                [
                    'pattern' => 'gallery/index',
                    'route' => 'proacademy/gallery'
                ],
                [
                    'pattern' => 'faq',
                    'route' => 'proacademy/faq'
                ],
                [
                    'pattern' => 'tests-archive/index',
                    'route' => 'proacademy/tests'
                ],
                [
                    'pattern' => 'states-receiver/index',
                    'route' => 'proacademy/states-receiver'
                ],
                [
                    'pattern' => 'heads/index',
                    'route' => 'proacademy/heads'
                ],
                [
                    'pattern' => 'login',
                    'route' => 'site/login'
                ],
                [
                    'pattern' => 'reset-password',
                    'route' => 'site/request-password-reset'
                ],
                [
                    'pattern' => 'signup',
                    'route' => 'site/registration'
                ]
            ],
            'languages' => ['ru', 'uz-Lat', 'uz-Cyr']
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/classic/views/',
                    // '@app/widgets' => '@app/themes/classic/widgets'
                ],
            ],
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
