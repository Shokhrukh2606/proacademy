<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%messages_order}}`.
 */
class m200607_224338_create_messages_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%messages_order}}', [
            'id' => $this->primaryKey(),
            'from' => $this->integer()->notNull(),
            'to'=>$this->integer()->notNull(),
            'message_id'=>$this->integer()->notNull(),
            'status' => "ENUM('active', 'inactive')"
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%messages_order}}');
    }
}
