<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%menu}}`.
 */
class m200512_163436_create_menu_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%menu}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(11)->Null(),
            'category_id2' => $this->integer(11)->Null(),
            'group' => $this->string(30)->Null(),
            'alias' => $this->text()->notNull(),
            'title' => $this->json()->defaultValue('{"ru":"","en":"","oz":"","uz":""}'),
            'content' => $this->json()->defaultValue('{"ru":"","en":"","oz":"","uz":""}'),
            'content_html' => $this->json()->defaultValue('{"ru":"","en":"","oz":"","uz":""}'),
            'short_content' => $this->json()->defaultValue('{"ru":"","en":"","oz":"","uz":""}'),
            'status' => "ENUM('active', 'inactive')",
            'option' => "ENUM('yes', 'no')",
            'options' => $this->string(255)->Null(),
            'views' => $this->integer(11)->Null(),
            'tags' => $this->text()->Null(),
            'meta_title' => $this->json()->defaultValue('{"ru":"","en":"","oz":"","uz":""}'),
            'meta_description' => $this->json()->defaultValue('{"ru":"","en":"","oz":"","uz":""}'),
            'meta_keywords' => $this->json()->defaultValue('{"ru":"","en":"","oz":"","uz":""}'),
            'sort_order' => $this->integer(11)->Null(),
            'created_at' => $this->datetime()->notNull(),
            'updated_at' => $this->datetime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%menu}}');
    }
}
