<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%admission_faculties}}`.
 */
class m200526_001246_create_admission_faculties_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%admission_faculties}}', [
            'id' => $this->primaryKey(),
            'title' => $this->json()->defaultValue('{"ru":"","en":"","uz-Lat":"","uz-Cyr":""}'),
            'status' => "ENUM('active', 'inactive')",
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%admission_faculties}}');
    }
}
