<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%applicants}}`.
 */
class m200526_004231_create_applicants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%applicants}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'unique_code' => $this->integer(7)->notNull(),
            'academic_year_id' => $this->integer(4)->notNull(),
            'avatar'=>$this->string(255)->Null(),
            'applicant_type'=>"ENUM('inner', 'outer')",
            'birth_date'=>$this->date()->Null(),
            'district_birth'=>$this->integer(3)->Null(),
            'district_temporary'=>$this->integer(3)->Null(),
            'address_temporary'=>$this->string(255)->Null(),
            'district_permanent'=>$this->integer(3)->Null(),
            'address_permanent'=>$this->string(255)->Null(),
            'phone'=>$this->string(15)->Null(),
            'telegram_phone'=>$this->string(15)->Null(),
            'nationality'=>$this->string(30)->Null(),
            'other_nationality'=>$this->string(30)->Null(),
            'diplom_number'=>$this->string(255)->Null(),
            'high_school_name'=>$this->string(255)->Null(),
            'high_school_specialty'=>$this->string(255)->Null(),
            'high_school_finished'=>$this->date()->Null(),
            'inner_recomendation'=>$this->string(255)->Null(),
            'can_speak'=>$this->integer(1)->Null(),
            'other_can_speak'=>$this->string(255)->Null(),
            'faculty' => $this->smallInteger()->Null(),
            'view_status'=>"ENUM('active', 'inactive')",
            'diploma'=>$this->string(255)->Null(),
            'passport'=>$this->string(255)->Null(),
            'certificate'=>$this->string(255)->Null(),
            'biography'=>$this->string(255)->null(),
            'relatives'=>$this->string(255)->null(),
            'career'=>$this->string(255)->null(),
            'step'=>$this->smallInteger()->defaultValue(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%applicants}}');
    }
}
