<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%districts}}`.
 */
class m200526_005731_create_districts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%districts}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'title' => $this->json()->defaultValue('{"ru":"","en":"","uz-Lat":"","uz-Cyr":""}'),
        ]);
        $this->createIndex(
            'idx-district-city_id',
            'districts',
            'city_id'
        );
        $this->addForeignKey(
            'fk-district-city_id',
            'districts',
            'city_id',
            'cities',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // $this->dropTable('{{%districts}}');
    }
}
