<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%admission_year}}`.
 */
class m200525_232222_create_admission_year_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%admission_year}}', [
            'id' => $this->primaryKey(),
            'title' => $this->text()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'description'=>$this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%admission_year}}');
    }
}
