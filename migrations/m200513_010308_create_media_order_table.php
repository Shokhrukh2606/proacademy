<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%media_order}}`.
 */
class m200513_010308_create_media_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%media_order}}', [
            'id' => $this->primaryKey(),
            'media_id'=>$this->integer(11),
            'model_id'=>$this->integer(11),
            'category'=>$this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%media_order}}');
    }
}
